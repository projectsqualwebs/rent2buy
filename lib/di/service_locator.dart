import 'package:flutter_session/flutter_session.dart';
import 'package:rent2buy/models/common_repository.dart';
import 'package:rent2buy/networking/repository/account_repository.dart';
import 'package:rent2buy/networking/repository/message_repository.dart';
import 'package:rent2buy/networking/repository/order_repository.dart';
import 'package:rent2buy/networking/repository/payment_repository.dart';
import 'package:rent2buy/networking/repository/seller_repository.dart';
import 'package:rent2buy/screens/address/set_location_viewmodel.dart';
import 'package:rent2buy/screens/address/shipping_address_viewmodel.dart';
import 'package:rent2buy/screens/buyer/allsellers/sellers_viewmodel.dart';
import 'package:rent2buy/screens/buyer/cart/cart_viewmodel.dart';
import 'package:rent2buy/screens/buyer/categories/product_by_category_viewmodel.dart';
import 'package:rent2buy/screens/buyer/dispute/dispute_viewmodel.dart';
import 'package:rent2buy/screens/buyer/home/buyer_home_viewmodel.dart';
import 'package:rent2buy/screens/buyer/orders/feedback_viewmodel.dart';
import 'package:rent2buy/screens/buyer/orders/my_orderviewmodel.dart';
import 'package:rent2buy/screens/buyer/profile/profile_viewmodel.dart';
import 'package:rent2buy/screens/buyer/search/search_viewmodel.dart';
import 'package:rent2buy/screens/buyer/signup/register_viewmodel.dart';
import 'package:get_it/get_it.dart';
import 'package:rent2buy/screens/common/chat/chat_viewmodel.dart';
import 'package:rent2buy/screens/common/forget/forget_password_viewmodel.dart';
import 'package:rent2buy/screens/common/login/login_viewmodel.dart';
import 'package:rent2buy/screens/common/notifications/notification_viewmodel.dart';
import 'package:rent2buy/screens/common/reset/reset_password_viewmodel.dart';
import 'package:rent2buy/screens/common/verification/OTPVerifyViewModel.dart';
import 'package:rent2buy/screens/dialogs/become_seller_viewmodel.dart';
import 'package:rent2buy/screens/payment/add_card_viewmodel.dart';
import 'package:rent2buy/screens/payment/card_viewmodel.dart';
import 'package:rent2buy/screens/payment/payment_viewmodel.dart';
import 'package:rent2buy/screens/seller/bank/bank_viewmodel.dart';
import 'package:rent2buy/screens/seller/home/seller_home_viewmodel.dart';
import 'package:rent2buy/screens/seller/inbox/inbox_viewmodel.dart';
import 'package:rent2buy/screens/seller/listing/add_listing_viewmodel.dart';
import 'package:rent2buy/screens/seller/listing/listing_viewmodel.dart';
import 'package:rent2buy/screens/seller/listing/product_detail_viewmodel.dart';
import 'package:rent2buy/screens/seller/order/seller_order_viewmodel.dart';
import 'package:rent2buy/screens/seller/settings/seller_setting_viewmodel.dart';
import 'package:rent2buy/utils/cart_manager.dart';
import 'package:rent2buy/utils/chat_manager.dart';
import 'package:rent2buy/utils/data_manager.dart';

GetIt serviceLocator = GetIt.instance;

void setupServiceLocator() {
  // serviceLocator.registerLazySingleton<LoginRepository>(() => LoginRepoImpl());
  // serviceLocator.registerLazySingleton<LoginRemote>(() => LoginRemoteImpl());

  serviceLocator.registerLazySingleton<DataManager>(() => DataManager());
  serviceLocator.registerLazySingleton<ChatManager>(() => ChatManager());
  serviceLocator.registerLazySingleton<CartManager>(() => CartManager());
  serviceLocator
      .registerLazySingleton<AccountRepository>(() => AccountRepository());
  serviceLocator
      .registerLazySingleton<MessageRepository>(() => MessageRepository());
  serviceLocator
      .registerLazySingleton<OrderRepository>(() => OrderRepository());
  serviceLocator
      .registerLazySingleton<PaymentRepository>(() => PaymentRepository());
  serviceLocator
      .registerLazySingleton<SellerRepository>(() => SellerRepository());
  serviceLocator.registerLazySingleton<FlutterSession>(() => FlutterSession());
  serviceLocator
      .registerLazySingleton<CommonRepository>(() => CommonRepository());

  serviceLocator.registerFactory<RegisterViewModel>(() => RegisterViewModel());
  serviceLocator.registerFactory<LoginViewModel>(() => LoginViewModel());
  serviceLocator
      .registerFactory<OTPVerifyViewModel>(() => OTPVerifyViewModel());
  serviceLocator
      .registerFactory<SellerHomeViewModel>(() => SellerHomeViewModel());
  serviceLocator
      .registerFactory<AddListingViewModel>(() => AddListingViewModel());
  serviceLocator
      .registerFactory<ProductDetailViewModel>(() => ProductDetailViewModel());
  serviceLocator
      .registerFactory<BuyerHomeViewModel>(() => BuyerHomeViewModel());
  serviceLocator.registerFactory<CartViewModel>(() => CartViewModel());

  serviceLocator
      .registerFactory<BecomeSellerViewModel>(() => BecomeSellerViewModel());
  serviceLocator.registerFactory<AddCardViewModel>(() => AddCardViewModel());
  serviceLocator.registerFactory<CardViewModel>(() => CardViewModel());

  serviceLocator.registerFactory<PaymentViewModel>(() => PaymentViewModel());

  serviceLocator.registerFactory<ShippingAddressViewModel>(
      () => ShippingAddressViewModel());
  serviceLocator.registerFactory<MyOrderViewModel>(() => MyOrderViewModel());
  serviceLocator
      .registerFactory<SetLocationViewModel>(() => SetLocationViewModel());
  serviceLocator
      .registerFactory<SellerOrderViewModel>(() => SellerOrderViewModel());
  serviceLocator.registerFactory<ProfileViewModel>(() => ProfileViewModel());
  serviceLocator.registerFactory<ChatViewModel>(() => ChatViewModel());
  serviceLocator.registerFactory<InBoxViewModel>(() => InBoxViewModel());
  serviceLocator.registerFactory<SearchViewModel>(() => SearchViewModel());
  serviceLocator.registerFactory<BankViewModel>(() => BankViewModel());
  serviceLocator.registerFactory<FeedBackViewModel>(() => FeedBackViewModel());
  serviceLocator.registerFactory<ListingViewModel>(() => ListingViewModel());
  serviceLocator
      .registerFactory<NotificationViewModel>(() => NotificationViewModel());
  serviceLocator.registerFactory<DisputeViewModel>(() => DisputeViewModel());
  serviceLocator.registerFactory<ProductByCategoryViewModel>(
      () => ProductByCategoryViewModel());

  serviceLocator.registerFactory<SellersViewModel>(() => SellersViewModel());
  serviceLocator
      .registerFactory<ResetPasswordViewModel>(() => ResetPasswordViewModel());
  serviceLocator.registerFactory<ForgetPasswordViewModel>(
      () => ForgetPasswordViewModel());
  serviceLocator
      .registerFactory<SellerSettingViewModel>(() => SellerSettingViewModel());
}
