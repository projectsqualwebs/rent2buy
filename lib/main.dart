import 'package:firebase_core/firebase_core.dart';
import 'package:flutter/material.dart';
import 'package:provider/provider.dart';
import 'package:rent2buy/di/service_locator.dart';
import 'package:rent2buy/networking/repository/account_repository.dart';
import 'package:rent2buy/screens/address/set_location.dart';
import 'package:rent2buy/screens/buyer/allsellers/sellers.dart';
import 'package:rent2buy/screens/buyer/bank_accounts.dart';
import 'package:rent2buy/screens/buyer/cart/my_cart.dart';
import 'package:rent2buy/screens/buyer/cart_items.dart';
import 'package:rent2buy/screens/buyer/home/home.dart';
import 'package:rent2buy/screens/buyer/orders/my_orders.dart';
import 'package:rent2buy/screens/buyer/profile/profile.dart';
import 'package:rent2buy/screens/buyer/search/search_screen.dart';
import 'package:rent2buy/screens/common/common_setting_screen.dart';
import 'package:rent2buy/screens/common/feedback/feedback_screen.dart';
import 'package:rent2buy/screens/common/forget/forget_password_step_1.dart';
import 'package:rent2buy/screens/common/forget/forget_password_step_2.dart';
import 'package:rent2buy/screens/common/login/login.dart';
import 'package:rent2buy/screens/buyer/signup/signup.dart';
import 'package:rent2buy/screens/buyer/dispute/dispute.dart';
import 'package:rent2buy/screens/buyer/categories.dart';
import 'package:rent2buy/screens/buyer/chat_screen.dart';
import 'package:rent2buy/screens/buyer/chats.dart';
import 'package:rent2buy/screens/buyer/confirmed_screen.dart';
import 'package:rent2buy/screens/buyer/add_card_detail.dart';
import 'package:rent2buy/screens/buyer/first_screen.dart';
import 'package:rent2buy/screens/buyer/forget_password.dart';
import 'package:rent2buy/screens/buyer/image_slider.dart';
import 'package:rent2buy/screens/common/reset/reset_password.dart';
import 'package:rent2buy/screens/common/success_screen.dart';
import 'package:rent2buy/screens/common/verification/otp_verify.dart';
import 'package:rent2buy/screens/buyer/payable.dart';
import 'package:rent2buy/screens/buyer/product_detail.dart';
import 'package:rent2buy/screens/buyer/qr_code.dart';
import 'package:rent2buy/screens/buyer/rent_confirm_details.dart';
import 'package:rent2buy/screens/buyer/reset_password.dart';
import 'package:rent2buy/screens/buyer/search.dart';
import 'package:rent2buy/screens/buyer/search_for_rent.dart';
import 'package:rent2buy/screens/buyer/search_results.dart';
import 'package:rent2buy/screens/buyer/signup/register_viewmodel.dart';
import 'package:rent2buy/screens/payment/add_card.dart';
import 'package:rent2buy/screens/payment/cards_screen.dart';
import 'package:rent2buy/screens/payment/payment_screen.dart';
import 'package:rent2buy/screens/seller/bank/add_bank_account.dart';
import 'package:rent2buy/screens/seller/dispute/dispute.dart';
import 'package:rent2buy/screens/seller/inbox/seller_inbox_screen.dart';
import 'package:rent2buy/screens/seller/listing/add_listing.dart';
import 'package:rent2buy/screens/seller/all_listing.dart';
import 'package:rent2buy/screens/seller/app_settings.dart';
import 'package:rent2buy/screens/seller/bank_account_details.dart';
import 'package:rent2buy/screens/seller/chat_screen.dart';
import 'package:rent2buy/screens/seller/chats.dart';
import 'package:rent2buy/screens/seller/forget_password.dart';
import 'package:rent2buy/screens/seller/home/home.dart';
import 'package:rent2buy/screens/seller/item_add.dart';
import 'package:rent2buy/screens/seller/login.dart';
import 'package:rent2buy/screens/seller/notifications.dart';
import 'package:rent2buy/screens/seller/order/order.dart';
import 'package:rent2buy/screens/seller/otp_verify.dart';
import 'package:rent2buy/screens/seller/profile.dart';
import 'package:rent2buy/screens/seller/reset_password.dart';
import 'package:rent2buy/screens/seller/selling_report.dart';
import 'package:rent2buy/screens/seller/settings/seller_settings.dart';
import 'package:rent2buy/screens/seller/signup.dart';
import 'package:rent2buy/screens/splashscreen.dart';
import 'package:rent2buy/screens/buyer/user.dart';
import 'package:rent2buy/screens/buyer/app_settings.dart';
import 'package:rent2buy/screens/buyer/notifications.dart';

import 'screens/seller/listing/product_detail.dart';

Future<void> main() async {
  WidgetsFlutterBinding.ensureInitialized();
  await Firebase.initializeApp();
  setupServiceLocator();
  runApp(
    MyApp(),
  );
}

class MyApp extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      debugShowCheckedModeBanner: false,
      title: 'Rent2Buy',
      theme: ThemeData(
          fontFamily: 'Dosis',
          scaffoldBackgroundColor: Colors.yellow[100],
          primaryColor: Color(0xFFF7A74D),
          buttonTheme: ButtonThemeData(buttonColor: Color(0xFFF7A74D)),
          appBarTheme:
              AppBarTheme(iconTheme: IconThemeData(color: Color(0xFF97080E)))),
      routes: {
        '/login': (_) => Login(),
        '/signup': (_) => SignUp(),
        '/forget_pwd': (_) => ForgetPassword(),
        '/otp_verify': (_) => OtpVerify(),
        '/reset_pwd': (_) => ResetPassword(),
        '/set_location': (_) => SetLocation(),
        Home.routeName: (_) => Home(),
        '/profile': (_) => Profile(),
        '/user': (_) => User(),
        '/addCardDetail': (_) => AddCardDetail(),
        '/first_screen': (_) => FirstScreen(),
        '/search_for_rent': (_) => SearchForRent(),
        '/search': (_) => Search(),
        '/app_settings': (_) => AppSettings(),
        '/notifications': (_) => Notifications(),
        '/categories': (_) => Categories(),
        '/confirmed_screen': (_) => ConfirmedScreen(),
        '/dispute': (_) => Dispute(),
        '/bank_accounts': (_) => BankAccounts(),
        '/my_orders': (_) => MyOrders(),
        Chats.routeName: (_) => Chats(),
        '/sellers': (_) => Sellers(),
        '/cart_items': (_) => CartItems(),
        '/search_results': (_) => SearchResults(),
        ProductDetails.routeName: (_) => ProductDetails(),
        MyCart.routeName: (_) => MyCart(),
        '/image_slider': (_) => ImageSlider(),
        '/qr_code': (_) => Qrcode(),
        '/payable': (_) => Payable(),
        '/rent_confirmation': (_) => RentConfirmation(),
        '/chat_screen': (_) => ChatScreen(),
        '/seller_login': (_) => SellerLogin(),
        '/seller_signup': (_) => SellerSignup(),
        '/seller_home': (_) => SellerHome(),
        '/seller_forget_pwd': (_) => SellerForgetPassword(),
        '/seller_OTP_verify': (_) => SellerOTPVerify(),
        '/seller_reset_pwd': (_) => SellerResetPassword(),
        '/seller_app_settings': (_) => SellerAppSettings(),
        '/seller_notifications': (_) => SellerNotifications(),
        '/seller_chats': (_) => SellerChats(),
        '/seller_profile': (_) => SellerProfile(),
        '/seller_settings': (_) => SellerSettings(),
        '/selling_report': (_) => SellingReport(),
        AddListing.routeName: (_) => AddListing(),
        '/bank_account': (_) => BankAccount(),
        '/seller_dispute': (_) => SellerDispute(),
        '/seller_order': (_) => SellerOrder(),
        '/item_added': (_) => ItemAdded(),
        SellerProductDetail.routeName: (_) => SellerProductDetail(),
        '/all_listing': (_) => AllListing(),
        '/seller_chat_screen': (_) => SellerChatScreen(),
        AddBankAccount.routeName: (_) => AddBankAccount(),
        SplashScreen.routeName: (_) => SplashScreen(),
        AddCard.routeName: (_) => AddCard(),
        PaymentScreen.routeName: (_) => PaymentScreen(),
        SuccessScreen.routeName: (_) => SuccessScreen(),
        SellerInBoxScreen.routeName: (_) => SellerInBoxScreen(),
        ResetPasswordScreen.routeName: (_) => ResetPasswordScreen(),
        ForgetPasswordStep1Screeen.routeName: (_) =>
            ForgetPasswordStep1Screeen(),
        ForgetPasswordStep2Screen.routeName: (_) => ForgetPasswordStep2Screen(),
        CardsScreen.routeName: (_) => CardsScreen(),
        FeedbackScreen.routeName: (_) => FeedbackScreen(),
        CommonScreen.routeName: (_) => CommonScreen()
      },
      home: SplashScreen(),
    );
  }
}
