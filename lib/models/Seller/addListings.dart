import 'package:json_annotation/json_annotation.dart';
import 'package:rent2buy/models/Seller/availability.dart';
import 'package:rent2buy/models/Seller/rentDiscountPercent.dart';
import 'package:rent2buy/models/Seller/rentPrice.dart';
part 'addListings.g.dart';

@JsonSerializable(explicitToJson: true)
class AddListings {
  String name;
  String category_id;
  String sub_category_id;
  String description;
  List delivery_type;
  List purchase_action;
  int sell_price;
  int sell_discount_percent;
  RentPrice rent_price;
  RentDiscountPercent rent_discount_percent;
  Availability availability;
  List images;

  AddListings(
      {this.name,
      this.category_id,
      this.sub_category_id,
      this.description,
      this.delivery_type,
      this.purchase_action,
      this.sell_price,
      this.sell_discount_percent,
      this.rent_price,
      this.rent_discount_percent,
      this.availability,
      this.images});
  factory AddListings.fromJson(Map<String, dynamic> json) =>
      _$AddListingsFromJson(json);

  Map<String, dynamic> toJson() => _$AddListingsToJson(this);
}
