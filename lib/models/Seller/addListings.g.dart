// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'addListings.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

AddListings _$AddListingsFromJson(Map<String, dynamic> json) {
  return AddListings(
    name: json['name'] as String,
    category_id: json['category_id'] as String,
    sub_category_id: json['sub_category_id'] as String,
    description: json['description'] as String,
    delivery_type: json['delivery_type'] as List,
    purchase_action: json['purchase_action'] as List,
    sell_price: json['sell_price'] as int,
    sell_discount_percent: json['sell_discount_percent'] as int,
    rent_price: json['rent_price'] == null
        ? null
        : RentPrice.fromJson(json['rent_price'] as Map<String, dynamic>),
    rent_discount_percent: json['rent_discount_percent'] == null
        ? null
        : RentDiscountPercent.fromJson(
            json['rent_discount_percent'] as Map<String, dynamic>),
    availability: json['availability'] == null
        ? null
        : Availability.fromJson(json['availability'] as Map<String, dynamic>),
    images: json['images'] as List,
  );
}

Map<String, dynamic> _$AddListingsToJson(AddListings instance) =>
    <String, dynamic>{
      'name': instance.name,
      'category_id': instance.category_id,
      'sub_category_id': instance.sub_category_id,
      'description': instance.description,
      'delivery_type': instance.delivery_type,
      'purchase_action': instance.purchase_action,
      'sell_price': instance.sell_price,
      'sell_discount_percent': instance.sell_discount_percent,
      'rent_price': instance.rent_price?.toJson(),
      'rent_discount_percent': instance.rent_discount_percent?.toJson(),
      'availability': instance.availability?.toJson(),
      'images': instance.images,
    };
