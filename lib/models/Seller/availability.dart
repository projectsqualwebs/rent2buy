import 'package:json_annotation/json_annotation.dart';
part 'availability.g.dart';
@JsonSerializable()
class Availability{
  String available_from;
  String available_to;
  Availability({this.available_from,this.available_to});
  factory Availability.fromJson(Map<String, dynamic> json) => _$AvailabilityFromJson(json);
  Map<String, dynamic> toJson() => _$AvailabilityToJson(this);
}