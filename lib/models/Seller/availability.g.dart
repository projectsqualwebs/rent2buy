// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'availability.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

Availability _$AvailabilityFromJson(Map<String, dynamic> json) {
  return Availability(
    available_from: json['available_from'] as String,
    available_to: json['available_to'] as String,
  );
}

Map<String, dynamic> _$AvailabilityToJson(Availability instance) =>
    <String, dynamic>{
      'available_from': instance.available_from,
      'available_to': instance.available_to,
    };
