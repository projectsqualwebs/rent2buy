import 'package:json_annotation/json_annotation.dart';
part 'companyModel.g.dart';
@JsonSerializable()
class Company{
  String company_name;
  String vat_number;

  Company({this.company_name,this.vat_number});
  factory Company.fromJson(Map<String, dynamic> json) => _$CompanyFromJson(json);
  Map<String, dynamic> toJson() => _$CompanyToJson(this);
}