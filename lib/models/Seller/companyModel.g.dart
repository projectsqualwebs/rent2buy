// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'companyModel.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

Company _$CompanyFromJson(Map<String, dynamic> json) {
  return Company(
    company_name: json['company_name'] as String,
    vat_number: json['vat_number'] as String,
  );
}

Map<String, dynamic> _$CompanyToJson(Company instance) => <String, dynamic>{
      'company_name': instance.company_name,
      'vat_number': instance.vat_number,
    };
