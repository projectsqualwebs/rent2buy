import 'package:json_annotation/json_annotation.dart';
part 'rentDiscountPercent.g.dart';
@JsonSerializable()
class RentDiscountPercent{
  int more_than_3_days;
  int more_than_1_week;
  int more_than_1_month;

  RentDiscountPercent({this.more_than_3_days,this.more_than_1_week,this.more_than_1_month});
  factory RentDiscountPercent.fromJson(Map<String, dynamic> json) => _$RentDiscountPercentFromJson(json);
  Map<String, dynamic> toJson() => _$RentDiscountPercentToJson(this);
}