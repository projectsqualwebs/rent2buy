// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'rentDiscountPercent.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

RentDiscountPercent _$RentDiscountPercentFromJson(Map<String, dynamic> json) {
  return RentDiscountPercent(
    more_than_3_days: json['more_than_3_days'] as int,
    more_than_1_week: json['more_than_1_week'] as int,
    more_than_1_month: json['more_than_1_month'] as int,
  );
}

Map<String, dynamic> _$RentDiscountPercentToJson(
        RentDiscountPercent instance) =>
    <String, dynamic>{
      'more_than_3_days': instance.more_than_3_days,
      'more_than_1_week': instance.more_than_1_week,
      'more_than_1_month': instance.more_than_1_month,
    };
