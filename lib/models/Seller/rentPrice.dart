import 'package:json_annotation/json_annotation.dart';
part 'rentPrice.g.dart';

@JsonSerializable()
class RentPrice{
  int per_hour;
  int per_day;

  RentPrice({this.per_hour,this.per_day});
  factory RentPrice.fromJson(Map<String, dynamic> json) => _$RentPriceFromJson(json);
  Map<String, dynamic> toJson() => _$RentPriceToJson(this);
}