// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'rentPrice.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

RentPrice _$RentPriceFromJson(Map<String, dynamic> json) {
  return RentPrice(
    per_hour: json['per_hour'] as int,
    per_day: json['per_day'] as int,
  );
}

Map<String, dynamic> _$RentPriceToJson(RentPrice instance) => <String, dynamic>{
      'per_hour': instance.per_hour,
      'per_day': instance.per_day,
    };
