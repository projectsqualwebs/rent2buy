import 'package:json_annotation/json_annotation.dart';
import 'package:rent2buy/models/Seller/companyModel.dart';
import 'package:rent2buy/models/User/userAddress.dart';

part 'sellerRegister.g.dart';

@JsonSerializable()
class SellerRegister{
  String first_name;
  String last_name;
  String email;
  String phone;
  UserAddress address;
  String is_company;
  Company company;
  String dob;
  String password;
  String confirm_password;

  SellerRegister({this.first_name,this.last_name,this.email,this.phone,this.address,this.is_company,this.company,this.dob,this.password,this.confirm_password});
  factory SellerRegister.fromJson(Map<String, dynamic> json) => _$SellerRegisterFromJson(json);
  Map<String, dynamic> toJson() => _$SellerRegisterToJson(this);
}