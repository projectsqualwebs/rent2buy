// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'sellerRegister.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

SellerRegister _$SellerRegisterFromJson(Map<String, dynamic> json) {
  return SellerRegister(
    first_name: json['first_name'] as String,
    last_name: json['last_name'] as String,
    email: json['email'] as String,
    phone: json['phone'] as String,
    address: json['address'] == null
        ? null
        : UserAddress.fromJson(json['address'] as Map<String, dynamic>),
    is_company: json['is_company'] as String,
    company: json['company'] == null
        ? null
        : Company.fromJson(json['company'] as Map<String, dynamic>),
    dob: json['dob'] as String,
    password: json['password'] as String,
    confirm_password: json['confirm_password'] as String,
  );
}

Map<String, dynamic> _$SellerRegisterToJson(SellerRegister instance) =>
    <String, dynamic>{
      'first_name': instance.first_name,
      'last_name': instance.last_name,
      'email': instance.email,
      'phone': instance.phone,
      'address': instance.address,
      'is_company': instance.is_company,
      'company': instance.company,
      'dob': instance.dob,
      'password': instance.password,
      'confirm_password': instance.confirm_password,
    };
