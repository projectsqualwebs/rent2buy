import 'package:json_annotation/json_annotation.dart';
import 'package:rent2buy/models/User/address.dart';
part 'addAddress.g.dart';

@JsonSerializable()
class AddAddress{
  Address address;
  AddAddress({this.address});
  factory AddAddress.fromJson(Map<String, dynamic> json) => _$AddAddressFromJson(json);
  Map<String, dynamic> toJson() => _$AddAddressToJson(this);
}