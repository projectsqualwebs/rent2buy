// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'addAddress.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

AddAddress _$AddAddressFromJson(Map<String, dynamic> json) {
  return AddAddress(
    address: json['address'] == null
        ? null
        : Address.fromJson(json['address'] as Map<String, dynamic>),
  );
}

Map<String, dynamic> _$AddAddressToJson(AddAddress instance) =>
    <String, dynamic>{
      'address': instance.address,
    };
