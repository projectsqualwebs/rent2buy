import 'package:json_annotation/json_annotation.dart';
part 'address.g.dart';
@JsonSerializable()
class Address{
  String location;
  String zip_code;
  double latitude;
  double longitude;

  Address({this.location,this.zip_code,this.latitude,this.longitude});
  factory Address.fromJson(Map<String, dynamic> json) => _$AddressFromJson(json);
  Map<String, dynamic> toJson() => _$AddressToJson(this);

}