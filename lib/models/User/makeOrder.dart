import 'package:json_annotation/json_annotation.dart';
import 'package:rent2buy/models/User/address.dart';
part 'makeOrder.g.dart';
@JsonSerializable()
class MakeOrder {
  String delivery_type;
  Address address;
  String stripe_card_id;
  MakeOrder({this.delivery_type,this.address,this.stripe_card_id});
  factory MakeOrder.fromJson(Map<String, dynamic> json) => _$MakeOrderFromJson(json);
  Map<String, dynamic> toJson() => _$MakeOrderToJson(this);
}