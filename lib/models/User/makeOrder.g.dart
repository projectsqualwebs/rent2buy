// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'makeOrder.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

MakeOrder _$MakeOrderFromJson(Map<String, dynamic> json) {
  return MakeOrder(
    delivery_type: json['delivery_type'] as String,
    address: json['address'] == null
        ? null
        : Address.fromJson(json['address'] as Map<String, dynamic>),
    stripe_card_id: json['stripe_card_id'] as String,
  );
}

Map<String, dynamic> _$MakeOrderToJson(MakeOrder instance) => <String, dynamic>{
      'delivery_type': instance.delivery_type,
      'address': instance.address,
      'stripe_card_id': instance.stripe_card_id,
    };
