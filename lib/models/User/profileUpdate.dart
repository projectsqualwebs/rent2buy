import 'package:json_annotation/json_annotation.dart';
import 'package:rent2buy/models/User/userAddress.dart';
part 'profileUpdate.g.dart';
@JsonSerializable()
class ProfileUpdate{
  String first_name;
  String last_name;
  String dob;
  UserAddress address;

  ProfileUpdate({this.first_name,this.last_name,this.dob,this.address});
  factory ProfileUpdate.fromJson(Map<String, dynamic> json) => _$ProfileUpdateFromJson(json);
  Map<String, dynamic> toJson() => _$ProfileUpdateToJson(this);
}