// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'profileUpdate.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

ProfileUpdate _$ProfileUpdateFromJson(Map<String, dynamic> json) {
  return ProfileUpdate(
    first_name: json['first_name'] as String,
    last_name: json['last_name'] as String,
    dob: json['dob'] as String,
    address: json['address'] == null
        ? null
        : UserAddress.fromJson(json['address'] as Map<String, dynamic>),
  );
}

Map<String, dynamic> _$ProfileUpdateToJson(ProfileUpdate instance) =>
    <String, dynamic>{
      'first_name': instance.first_name,
      'last_name': instance.last_name,
      'dob': instance.dob,
      'address': instance.address,
    };
