import 'package:json_annotation/json_annotation.dart';
part 'updateProfile.g.dart';
@JsonSerializable()
class UpdateProfile{
  String full_name;

  UpdateProfile({this.full_name});
  factory UpdateProfile.fromJson(Map<String, dynamic> json) => _$UpdateProfileFromJson(json);
  Map<String, dynamic> toJson() => _$UpdateProfileToJson(this);
}