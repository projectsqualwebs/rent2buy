// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'updateProfile.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

UpdateProfile _$UpdateProfileFromJson(Map<String, dynamic> json) {
  return UpdateProfile(
    full_name: json['full_name'] as String,
  );
}

Map<String, dynamic> _$UpdateProfileToJson(UpdateProfile instance) =>
    <String, dynamic>{
      'full_name': instance.full_name,
    };
