import 'package:json_annotation/json_annotation.dart';
part 'userAddress.g.dart';
@JsonSerializable()
class UserAddress{
  String location;
  double latitude;
  double longitude;

  UserAddress({this.location,this.latitude,this.longitude});
  factory UserAddress.fromJson(Map<String, dynamic> json) => _$UserAddressFromJson(json);
  Map<String, dynamic> toJson() => _$UserAddressToJson(this);
}