import 'package:json_annotation/json_annotation.dart';
part 'userLogin.g.dart';

@JsonSerializable()
class UserLogin{
  String email;
  String password;

  UserLogin({this.email,this.password});
  factory UserLogin.fromJson(Map<String, dynamic> json) => _$UserLoginFromJson(json);
  Map<String, dynamic> toJson() => _$UserLoginToJson(this);
}