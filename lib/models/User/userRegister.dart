import 'package:json_annotation/json_annotation.dart';
import 'package:rent2buy/models/Seller/companyModel.dart';
import 'package:rent2buy/models/User/address.dart';
import 'package:rent2buy/models/User/userAddress.dart';

part 'userRegister.g.dart';

@JsonSerializable()
class UserRegister {
  String first_name;
  String last_name;
  String email;
  String phone;
  UserAddress address;
  String is_company;
  Company company;
  String dob;
  String password;
  String confirm_password;

  UserRegister(
      {this.first_name,
      this.last_name,
      this.email,
      this.phone,
      this.dob,
      this.address,
      this.is_company,
      this.company,
      this.password,
      this.confirm_password});

  factory UserRegister.fromJson(Map<String, dynamic> json) =>
      _$UserRegisterFromJson(json);

  Map<String, dynamic> toJson() => _$UserRegisterToJson(this);
}
