import 'package:json_annotation/json_annotation.dart';
part 'resendOtp.g.dart';

@JsonSerializable()
class ResendOtp{
  String email;

  ResendOtp({this.email});
  factory ResendOtp.fromJson(Map<String, dynamic> json) => _$ResendOtpFromJson(json);
  Map<String, dynamic> toJson() => _$ResendOtpToJson(this);
}