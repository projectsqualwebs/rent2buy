// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'resendOtp.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

ResendOtp _$ResendOtpFromJson(Map<String, dynamic> json) {
  return ResendOtp(
    email: json['email'] as String,
  );
}

Map<String, dynamic> _$ResendOtpToJson(ResendOtp instance) => <String, dynamic>{
      'email': instance.email,
    };
