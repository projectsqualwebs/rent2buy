import 'package:json_annotation/json_annotation.dart';
part 'resetPassword.g.dart';

@JsonSerializable()
class ResetPassword{
  String current_password;
  String new_password;
  String confirm_password;

  ResetPassword({this.current_password,this.new_password,this.confirm_password});
  factory ResetPassword.fromJson(Map<String, dynamic> json) => _$ResetPasswordFromJson(json);
  Map<String, dynamic> toJson() => _$ResetPasswordToJson(this);
}