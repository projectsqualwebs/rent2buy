import 'package:json_annotation/json_annotation.dart';
part 'verifyOtp.g.dart';

@JsonSerializable()
class VerifyOtp {
  String email;
  int otp;
  VerifyOtp({this.email,this.otp});
  factory VerifyOtp.fromJson(Map<String, dynamic> json) => _$VerifyOtpFromJson(json);
  Map<String, dynamic> toJson() => _$VerifyOtpToJson(this);
}