// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'verifyOtp.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

VerifyOtp _$VerifyOtpFromJson(Map<String, dynamic> json) {
  return VerifyOtp(
    email: json['email'] as String,
    otp: json['otp'] as int,
  );
}

Map<String, dynamic> _$VerifyOtpToJson(VerifyOtp instance) => <String, dynamic>{
      'email': instance.email,
      'otp': instance.otp,
    };
