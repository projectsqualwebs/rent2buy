import 'package:json_annotation/json_annotation.dart';

part 'address.g.dart';

@JsonSerializable(explicitToJson: true)
class Address {
  int id;
  String location;
  dynamic latitude;
  dynamic longitude;
  @JsonKey(name: 'default')
  int defaultAddress = 0;

  Address(
      {this.id,
      this.location,
      this.latitude,
      this.longitude,
      this.defaultAddress});

  static Address fromJson(Map<String, dynamic> json) {
    return _$AddressFromJson(json);
  }

  Map<String, dynamic> toJson() {
    return _$AddressToJson(this);
  }
}
