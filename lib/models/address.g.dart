// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'address.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

Address _$AddressFromJson(Map<String, dynamic> json) {
  return Address(
    id: json['id'] as int,
    location: json['location'] as String,
    latitude: json['latitude'],
    longitude: json['longitude'],
    defaultAddress: json['default'] as int,
  );
}

Map<String, dynamic> _$AddressToJson(Address instance) => <String, dynamic>{
      'id': instance.id,
      'location': instance.location,
      'latitude': instance.latitude,
      'longitude': instance.longitude,
      'default': instance.defaultAddress,
    };
