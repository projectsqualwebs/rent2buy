class Availability {
  String availableFrom;
  String availableTo;

  Availability({this.availableFrom, this.availableTo});

  Availability.fromJson(Map<String, dynamic> json) {
    availableFrom = json['available_from'];
    availableTo = json['available_to'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['available_from'] = this.availableFrom;
    data['available_to'] = this.availableTo;
    return data;
  }
}