import 'package:json_annotation/json_annotation.dart';
import 'package:rent2buy/models/cart_item.dart';

part 'cart.g.dart';

@JsonSerializable(explicitToJson: true)
class Cart {
  List<CartItem> items;
  @JsonKey(name: 'sub_total')
  double subTotal;
  double tax;
  @JsonKey(name: 'delivery_charge')
  int deliveryCharge;
  @JsonKey(name: 'order_total_amount')
  double orderTotalAmount;

  Cart(
      {this.items,
      this.subTotal,
      this.tax,
      this.deliveryCharge,
      this.orderTotalAmount});

  static Cart fromJson(Map<String, dynamic> json) {
    return _$CartFromJson(json);
  }

  Map<String, dynamic> toJson() {
    return _$CartToJson(this);
  }
}
