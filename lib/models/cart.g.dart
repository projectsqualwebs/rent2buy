// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'cart.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

Cart _$CartFromJson(Map<String, dynamic> json) {
  return Cart(
    items: (json['items'] as List)
        ?.map((e) =>
            e == null ? null : CartItem.fromJson(e as Map<String, dynamic>))
        ?.toList(),
    subTotal: (json['sub_total'] as num)?.toDouble(),
    tax: (json['tax'] as num)?.toDouble(),
    deliveryCharge: json['delivery_charge'] as int,
    orderTotalAmount: (json['order_total_amount'] as num)?.toDouble(),
  );
}

Map<String, dynamic> _$CartToJson(Cart instance) => <String, dynamic>{
      'items': instance.items?.map((e) => e?.toJson())?.toList(),
      'sub_total': instance.subTotal,
      'tax': instance.tax,
      'delivery_charge': instance.deliveryCharge,
      'order_total_amount': instance.orderTotalAmount,
    };
