import 'package:json_annotation/json_annotation.dart';
import 'package:rent2buy/models/cart_response.dart';
import 'package:rent2buy/models/product.dart';

part 'cart_item.g.dart';

@JsonSerializable(explicitToJson: true)
class CartItem {
  @JsonKey(name: '_id')
  String sId;
  CartResponse cart;
  Product listing;
  String actionType;
  String quantity;
  int price;
  @JsonKey(name: 'discount_price')
  double discountPrice;
  @JsonKey(name: 'total_amount')
  double totalAmount;
  @JsonKey(name: 'updated_at')
  String updatedAt;
  @JsonKey(name: 'created_at')
  String createdAt;

  CartItem(
      {this.sId,
      this.cart,
      this.listing,
      this.actionType,
      this.quantity,
      this.price,
      this.discountPrice,
      this.totalAmount,
      this.updatedAt,
      this.createdAt});

  factory CartItem.fromJson(Map<String, dynamic> json) {
    return _$CartItemFromJson(json);
  }

  Map<String, dynamic> toJson() {
    return _$CartItemToJson(this);
  }
}
