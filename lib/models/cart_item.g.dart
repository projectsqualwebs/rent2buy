// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'cart_item.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

CartItem _$CartItemFromJson(Map<String, dynamic> json) {
  return CartItem(
    sId: json['_id'] as String,
    cart: json['cart'] == null
        ? null
        : CartResponse.fromJson(json['cart'] as Map<String, dynamic>),
    listing: json['listing'] == null
        ? null
        : Product.fromJson(json['listing'] as Map<String, dynamic>),
    actionType: json['actionType'] as String,
    quantity: json['quantity'] as String,
    price: json['price'] as int,
    discountPrice: (json['discount_price'] as num)?.toDouble(),
    totalAmount: (json['total_amount'] as num)?.toDouble(),
    updatedAt: json['updated_at'] as String,
    createdAt: json['created_at'] as String,
  );
}

Map<String, dynamic> _$CartItemToJson(CartItem instance) => <String, dynamic>{
      '_id': instance.sId,
      'cart': instance.cart?.toJson(),
      'listing': instance.listing?.toJson(),
      'actionType': instance.actionType,
      'quantity': instance.quantity,
      'price': instance.price,
      'discount_price': instance.discountPrice,
      'total_amount': instance.totalAmount,
      'updated_at': instance.updatedAt,
      'created_at': instance.createdAt,
    };
