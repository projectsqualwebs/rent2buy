import 'package:json_annotation/json_annotation.dart';

part 'cart_response.g.dart';

@JsonSerializable()
class CartResponse {
  @JsonKey(name: 'cart_unique_id')
  String cartUniqueId;
  String id;

  CartResponse({this.cartUniqueId, this.id});

  static CartResponse fromJson(Map<String, dynamic> json) {
    return _$CartResponseFromJson(json);
  }

  Map<String, dynamic> toJson() {
    return _$CartResponseToJson(this);
  }
}
