// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'cart_response.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

CartResponse _$CartResponseFromJson(Map<String, dynamic> json) {
  return CartResponse(
    cartUniqueId: json['cart_unique_id'] as String,
    id: json['id'] as String,
  );
}

Map<String, dynamic> _$CartResponseToJson(CartResponse instance) =>
    <String, dynamic>{
      'cart_unique_id': instance.cartUniqueId,
      'id': instance.id,
    };
