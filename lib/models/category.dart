import 'package:json_annotation/json_annotation.dart';
import 'package:rent2buy/models/image.dart';

part 'category.g.dart';

@JsonSerializable(explicitToJson: true)
class ListingCategory {
  @JsonKey(name: "_id")
  String id;
  String name;
  Images images;

  ListingCategory({
    this.id,
    this.name,
    this.images,
  });

  static ListingCategory fromJson(Map<String, dynamic> json) {
    return _$ListingCategoryFromJson(json);
  }

  Map<String, dynamic> toJson() {
    return _$ListingCategoryToJson(this);
  }
}
