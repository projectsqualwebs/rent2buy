// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'category.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

ListingCategory _$ListingCategoryFromJson(Map<String, dynamic> json) {
  return ListingCategory(
    id: json['_id'] as String,
    name: json['name'] as String,
    images: json['images'] == null
        ? null
        : Images.fromJson(json['images'] as Map<String, dynamic>),
  );
}

Map<String, dynamic> _$ListingCategoryToJson(ListingCategory instance) =>
    <String, dynamic>{
      '_id': instance.id,
      'name': instance.name,
      'images': instance.images?.toJson(),
    };
