import 'package:json_annotation/json_annotation.dart';
import 'package:rent2buy/models/userinfo.dart';

// part 'chat_message.g.dart';

class ChatMessage {
  String key;
  String message;
  int status;
  Userinfo sender;
  int send_timestamp;
  bool isChecked = false;

  ChatMessage({this.message, this.sender, this.status, this.send_timestamp});

  static ChatMessage fromJson(Map str) {
    return ChatMessage(
        message: str['message'],
        sender: getSender(str['sender']),
        status: str['status'],
        send_timestamp: str['send_timestamp']);
  }

  static getSender(dynamic sender) {
    return sender == null ? null : Userinfo(sId: sender['_id']);
  }

  // Map<String, dynamic> toJson() => _$ChatMessageToJson(this);
}
