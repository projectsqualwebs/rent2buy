import 'package:flutter/material.dart';
import 'package:rent2buy/di/service_locator.dart';
import 'package:rent2buy/models/address.dart';
import 'package:rent2buy/models/cart.dart';
import 'package:rent2buy/models/cart_item.dart';
import 'package:rent2buy/models/cart_response.dart';
import 'package:rent2buy/models/delivery_type.dart';
import 'package:rent2buy/models/dispute_order.dart';
import 'package:rent2buy/models/notification_response.dart';
import 'package:rent2buy/models/product.dart';
import 'package:rent2buy/models/userinfo.dart';
import 'package:rent2buy/networking/dioclient.dart';
import 'package:rent2buy/networking/standard_response.dart';
import 'package:rent2buy/networking/standard_response_list.dart';
import 'package:rent2buy/services/apiProvider.dart';
import 'package:rent2buy/utils/data_manager.dart';

import 'category.dart';

class CommonRepository {
  DioClient dioClient;
  DataManager _dataManager = serviceLocator<DataManager>();

  CommonRepository() {
    dioClient = DioClient();
    initToken();
  }

  void initToken() async {
    String token = await _dataManager.loadToken();
    var options = dioClient.http.options;
    options.headers['Authorization'] = "Bearer $token";
  }

  Future<StandardResponseList<ListingCategory>> getCategories() async {
    try {
      final response = await dioClient.get(ApiProvider.getCategories);
      return StandardResponseList.fromJsonObject(
          response, ListingCategory.fromJson);
    } catch (e) {
      print(e.toString);
      throw e;
    }
  }

  Future<StandardResponseList<Product>> getListings(
      Map<String, dynamic> params, Map<String, dynamic> queryParams) async {
    try {
      final response = await dioClient.post(ApiProvider.listings,
          data: params, queryParameters: queryParams);
      return StandardResponseList.fromJsonObject(response, Product.fromJson);
    } catch (e) {
      print(e.toString);
      throw e;
    }
  }

  Future<StandardResponseList<Product>> getFeedback(String feedbackId) async {
    try {
      final response =
          await dioClient.get(ApiProvider.getFeedback + feedbackId);

      return StandardResponseList.fromJsonObject(response, Product.fromJson);
    } catch (e) {
      print(e.toString());
      throw e;
    }
  }

  Future<StandardResponse<Cart>> getCartItems(String cartId) async {
    try {
      if (cartId.isEmpty) {
        final response = await dioClient.get(ApiProvider.getLoggedInCartItem);
        return StandardResponse.fromJsonObject(response, Cart.fromJson);
      } else {
        final response = await dioClient.get(ApiProvider.getCartItem + cartId);
        return StandardResponse.fromJsonObject(response, Cart.fromJson);
      }
    } catch (e) {
      print(e.toString);
      throw e;
    }
  }

  Future<StandardResponseList<String>> removeCartItem(String cartItemId) async {
    try {
      final response =
          await dioClient.delete(ApiProvider.getCartItem + cartItemId);
      return StandardResponseList.fromJson(response);
    } catch (e) {
      print(e.toString);
      throw e;
    }
  }

  Future<StandardResponseList<String>> clearCart(String cartItemId) async {
    try {
      final response = await dioClient.get(ApiProvider.clearCart + cartItemId);
      return StandardResponseList.fromJson(response);
    } catch (e) {
      print(e.toString);
      throw e;
    }
  }

  Future<StandardResponse<CartResponse>> addToCart(
      Map<String, dynamic> params, String cartId) async {
    try {
      if (cartId.isEmpty) {
        final response =
            await dioClient.post(ApiProvider.addToCartLoggedIn, data: params);
        if (response['status'] == 201) {
          return StandardResponse(
              response['status'], response['message'], null);
        } else {
          return StandardResponse.fromJsonObject(
              response, CartResponse.fromJson);
        }
      } else {
        final response =
            await dioClient.post(ApiProvider.addToCart + cartId, data: params);
        if (response['status'] == 201) {
          return StandardResponse(
              response['status'], response['message'], null);
        } else {
          return StandardResponse.fromJsonObject(
              response, CartResponse.fromJson);
        }
      }
    } catch (e) {
      throw e;
    }
  }

  Future<StandardResponseList<String>> placeOrder(
      Map<String, dynamic> params) async {
    try {
      final response = await dioClient.post(ApiProvider.order, data: params);
      return StandardResponseList.fromJson(response);
    } catch (e) {
      throw e;
    }
  }

  Future<StandardResponseList<String>> resetPassword(
      Map<String, dynamic> params) async {
    try {
      final response =
          await dioClient.post(ApiProvider.resetPassword, data: params);
      return StandardResponseList.fromJson(response);
    } catch (e) {
      throw e;
    }
  }

  Future<StandardResponseList<String>> submitReview(
      Map<String, dynamic> params) async {
    try {
      final response =
          await dioClient.post(ApiProvider.listingFeedback, data: params);
      return StandardResponseList.fromJson(response);
    } catch (e) {
      throw e;
    }
  }

  Future<StandardResponseList<String>> addAddress(
      Map<String, dynamic> params) async {
    try {
      final response = await dioClient.post(ApiProvider.address, data: params);
      return StandardResponseList.fromJson(response);
    } catch (e) {
      throw e;
    }
  }

  Future<StandardResponseList<String>> submitFeedback(
      Map<String, dynamic> params) async {
    try {
      final response =
          await dioClient.post(ApiProvider.appFeedBack, data: params);
      return StandardResponseList.fromJson(response);
    } catch (e) {
      throw e;
    }
  }

  Future<StandardResponseList<DeliveryType>> getDeliveryTypes() async {
    try {
      final response = await dioClient.get(ApiProvider.getDeliveryTypes);
      return StandardResponseList.fromJsonObject(
          response, DeliveryType.fromJson);
    } catch (e) {
      print(e.toString);
      throw e;
    }
  }

  Future<StandardResponseList<NotificationResponse>> getNotifications() async {
    try {
      final response = await dioClient.get(ApiProvider.notifications);
      return StandardResponseList.fromJsonObject(
          response, NotificationResponse.fromJson);
    } catch (e) {
      print(e.toString);
      throw e;
    }
  }

  Future<StandardResponse<dynamic>> getCartItemsCount(String cartId) async {
    try {
      final response = await dioClient.get(ApiProvider.cartItemCount + cartId);
      return StandardResponse.fromJson(response);
    } catch (e) {
      print(e.toString);
      throw e;
    }
  }

  Future<StandardResponseList<DeliveryType>> getDeliveryMethods(
      String cartId) async {
    try {
      print(ApiProvider.getDeliveryMethods + cartId);
      final response =
          await dioClient.get(ApiProvider.getDeliveryMethods + cartId);

      return StandardResponseList(
          response['status'],
          response['message'],
          List<DeliveryType>.from(response['response']['delivery_type']
              .map((x) => DeliveryType.fromJson(x))));
    } catch (e) {
      print(e.toString);
      throw e;
    }
  }

  Future<StandardResponseList<Address>> getDeliveryAddress() async {
    try {
      final response = await dioClient.get(ApiProvider.address);
      return StandardResponseList.fromJsonObject(response, Address.fromJson);
    } catch (e) {
      print(e.toString);
      throw e;
    }
  }

  Future<StandardResponseList<DisputeOrder>> getDisputes() async {
    try {
      final response = await dioClient.get(ApiProvider.userDisputes);
      return StandardResponseList.fromJsonObject(
          response, DisputeOrder.fromJson);
    } catch (e) {
      print(e.toString);
      throw e;
    }
  }

  Future<StandardResponseList<DisputeOrder>> getSellerDisputes() async {
    try {
      final response = await dioClient.get(ApiProvider.sellerDisputes);
      return StandardResponseList.fromJsonObject(
          response, DisputeOrder.fromJson);
    } catch (e) {
      print(e.toString);
      throw e;
    }
  }

  Future<StandardResponseList<Userinfo>> getSellers() async {
    try {
      final response = await dioClient.get(ApiProvider.getSellersWithRating);
      return StandardResponseList.fromJsonObject(response, Userinfo.fromJson);
    } catch (e) {
      print(e.toString);
      throw e;
    }
  }

  Future<StandardResponseList<String>> markDispute(String listingId) async {
    try {
      final response = await dioClient.get(ApiProvider.dispute + listingId);
      return StandardResponseList.fromJson(response);
    } catch (e) {
      print(e.toString);
      throw e;
    }
  }
}
