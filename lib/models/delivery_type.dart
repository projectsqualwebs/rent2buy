import 'dart:convert';

import 'package:json_annotation/json_annotation.dart';

part 'delivery_type.g.dart';

@JsonSerializable()
class DeliveryType {
  DeliveryType({
    this.id,
    this.name,
    this.updatedAt,
    this.createdAt,
  });

  @JsonKey(name: "_id")
  String id;
  String name;
  DateTime updatedAt;
  DateTime createdAt;

  static DeliveryType fromJson(Map<String, dynamic> json) =>   _$DeliveryTypeFromJson(json);

  Map<String, dynamic> toJson() => _$DeliveryTypeToJson(this);
}
