class DiscountPercent {
  int moreThan3Days;
  int moreThan1Week;
  int moreThan1Month;

  DiscountPercent(
      {this.moreThan3Days, this.moreThan1Week, this.moreThan1Month});

  DiscountPercent.fromJson(Map<String, dynamic> json) {
    moreThan3Days = json['more_than_3_days'];
    moreThan1Week = json['more_than_1_week'];
    moreThan1Month = json['more_than_1_month'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['more_than_3_days'] = this.moreThan3Days;
    data['more_than_1_week'] = this.moreThan1Week;
    data['more_than_1_month'] = this.moreThan1Month;
    return data;
  }
}
