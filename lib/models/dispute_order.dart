import 'package:json_annotation/json_annotation.dart';
import 'package:rent2buy/models/product.dart';
import 'package:rent2buy/models/userinfo.dart';

part 'dispute_order.g.dart';

@JsonSerializable(explicitToJson: true)
class DisputeOrder {
  String sId;
  Userinfo user;
  Product listing;
  String updatedAt;
  String createdAt;

  DisputeOrder(
      {this.sId, this.user, this.listing, this.updatedAt, this.createdAt});

  static DisputeOrder fromJson(Map<String, dynamic> params) =>
      _$DisputeOrderFromJson(params);

  Map<String, dynamic> toJson() => _$DisputeOrderToJson(this);
}
