// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'dispute_order.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

DisputeOrder _$DisputeOrderFromJson(Map<String, dynamic> json) {
  return DisputeOrder(
    sId: json['sId'] as String,
    user: json['user'] == null
        ? null
        : Userinfo.fromJson(json['user'] as Map<String, dynamic>),
    listing: json['listing'] == null
        ? null
        : Product.fromJson(json['listing'] as Map<String, dynamic>),
    updatedAt: json['updatedAt'] as String,
    createdAt: json['createdAt'] as String,
  );
}

Map<String, dynamic> _$DisputeOrderToJson(DisputeOrder instance) =>
    <String, dynamic>{
      'sId': instance.sId,
      'user': instance.user?.toJson(),
      'listing': instance.listing?.toJson(),
      'updatedAt': instance.updatedAt,
      'createdAt': instance.createdAt,
    };
