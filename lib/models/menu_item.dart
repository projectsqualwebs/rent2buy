import 'package:flutter/material.dart';

class MenuItem {
  int id;
  String name;
  IconData image;
  String path;

  MenuItem(this.id, this.name, this.image,this.path);
}
