class MutableLiveData<T> {
  T currentValue;


  void postValue(T value) {
    currentValue = value;
  }

}
