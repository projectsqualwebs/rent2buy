import 'package:json_annotation/json_annotation.dart';
import 'package:rent2buy/models/order.dart';

part 'notification_response.g.dart';

@JsonSerializable(explicitToJson: true)
class NotificationResponse {
  String title;
  String body;
  Order data;

  NotificationResponse({this.title, this.body, this.data});

  static NotificationResponse fromJson(Map<String, dynamic> params) =>
      _$NotificationResponseFromJson(params);

  Map<String, dynamic> toJson() => _$NotificationResponseToJson(this);
}
