import 'package:json_annotation/json_annotation.dart';
import 'package:rent2buy/models/address.dart';
import 'package:rent2buy/models/delivery_type.dart';
import 'package:rent2buy/models/order_item.dart';
import 'package:rent2buy/models/userinfo.dart';

part 'order.g.dart';

@JsonSerializable(explicitToJson: true)
class Order {
  @JsonKey(name: '_id')
  String sId;
  Userinfo user;
  List<Address> shipping;
  List<Address> billing;
  DeliveryType deliveryType;
  @JsonKey(name: 'order_items')
  List<OrderItem> orderItems;
  String status;
  String updatedAt;
  @JsonKey(name: 'created_at')
  String createdAt;
  int deliveryCharge;
  double orderDiscount;
  double orderSubTotal;
  double orderTax;
  double totalOrderPrice;
  String stripeChargeId;
  String transactionRef;

  Order(
      {this.sId,
      this.user,
      this.shipping,
      this.billing,
      this.deliveryType,
      this.orderItems,
      this.status,
      this.updatedAt,
      this.createdAt,
      this.deliveryCharge,
      this.orderDiscount,
      this.orderSubTotal,
      this.orderTax,
      this.totalOrderPrice,
      this.stripeChargeId,
      this.transactionRef});

  static Order fromJson(Map<String, dynamic> json) => _$OrderFromJson(json);

  Map<String, dynamic> toJson() => _$OrderToJson(this);
}
