// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'order.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

Order _$OrderFromJson(Map<String, dynamic> json) {
  return Order(
    sId: json['_id'] as String,
    user: json['user'] == null
        ? null
        : Userinfo.fromJson(json['user'] as Map<String, dynamic>),
    shipping: (json['shipping'] as List)
        ?.map((e) =>
            e == null ? null : Address.fromJson(e as Map<String, dynamic>))
        ?.toList(),
    billing: (json['billing'] as List)
        ?.map((e) =>
            e == null ? null : Address.fromJson(e as Map<String, dynamic>))
        ?.toList(),
    deliveryType: json['deliveryType'] == null
        ? null
        : DeliveryType.fromJson(json['deliveryType'] as Map<String, dynamic>),
    orderItems: (json['order_items'] as List)
        ?.map((e) =>
            e == null ? null : OrderItem.fromJson(e as Map<String, dynamic>))
        ?.toList(),
    status: json['status'] as String,
    updatedAt: json['updatedAt'] as String,
    createdAt: json['created_at'] as String,
    deliveryCharge: json['deliveryCharge'] as int,
    orderDiscount: (json['orderDiscount'] as num)?.toDouble(),
    orderSubTotal: (json['orderSubTotal'] as num)?.toDouble(),
    orderTax: (json['orderTax'] as num)?.toDouble(),
    totalOrderPrice: (json['totalOrderPrice'] as num)?.toDouble(),
    stripeChargeId: json['stripeChargeId'] as String,
    transactionRef: json['transactionRef'] as String,
  );
}

Map<String, dynamic> _$OrderToJson(Order instance) => <String, dynamic>{
      '_id': instance.sId,
      'user': instance.user?.toJson(),
      'shipping': instance.shipping?.map((e) => e?.toJson())?.toList(),
      'billing': instance.billing?.map((e) => e?.toJson())?.toList(),
      'deliveryType': instance.deliveryType?.toJson(),
      'order_items': instance.orderItems?.map((e) => e?.toJson())?.toList(),
      'status': instance.status,
      'updatedAt': instance.updatedAt,
      'created_at': instance.createdAt,
      'deliveryCharge': instance.deliveryCharge,
      'orderDiscount': instance.orderDiscount,
      'orderSubTotal': instance.orderSubTotal,
      'orderTax': instance.orderTax,
      'totalOrderPrice': instance.totalOrderPrice,
      'stripeChargeId': instance.stripeChargeId,
      'transactionRef': instance.transactionRef,
    };
