import 'package:json_annotation/json_annotation.dart';
import 'package:rent2buy/models/product.dart';
import 'package:rent2buy/models/userinfo.dart';

part 'order_item.g.dart';

@JsonSerializable(explicitToJson: true)
class OrderItem {
  @JsonKey(name: 'order_product')
  Product orderProduct;
  String actionType;
  int price;
  String quantity;
  @JsonKey(name: 'total_price')
  double totalPrice;
  String date = "";
  String id;
  Userinfo user;

  OrderItem(
      {this.orderProduct,
      this.actionType,
      this.price,
      this.quantity,
      this.totalPrice});

  static OrderItem fromJson(Map<String, dynamic> json) {
    return _$OrderItemFromJson(json);
  }

  Map<String, dynamic> toJson() {
    return _$OrderItemToJson(this);
  }
}
