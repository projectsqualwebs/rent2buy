// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'order_item.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

OrderItem _$OrderItemFromJson(Map<String, dynamic> json) {
  return OrderItem(
    orderProduct: json['order_product'] == null
        ? null
        : Product.fromJson(json['order_product'] as Map<String, dynamic>),
    actionType: json['actionType'] as String,
    price: json['price'] as int,
    quantity: json['quantity'] as String,
    totalPrice: (json['total_price'] as num)?.toDouble(),
  )
    ..date = json['date'] as String
    ..id = json['id'] as String
    ..user = json['user'] == null
        ? null
        : Userinfo.fromJson(json['user'] as Map<String, dynamic>);
}

Map<String, dynamic> _$OrderItemToJson(OrderItem instance) => <String, dynamic>{
      'order_product': instance.orderProduct?.toJson(),
      'actionType': instance.actionType,
      'price': instance.price,
      'quantity': instance.quantity,
      'total_price': instance.totalPrice,
      'date': instance.date,
      'id': instance.id,
      'user': instance.user?.toJson(),
    };
