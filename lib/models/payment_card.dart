import 'package:json_annotation/json_annotation.dart';

part 'payment_card.g.dart';

@JsonSerializable(explicitToJson: true)
class PaymentCard {
  String id;

  @JsonKey(name: 'card_holder')
  String cardHolder;
  @JsonKey(name: 'card_number')
  String cardNumber;
  bool checked = false;

  PaymentCard({this.id, this.cardHolder, this.cardNumber});

  static PaymentCard fromJson(Map<String, dynamic> json) {
    return _$PaymentCardFromJson(json);
  }

  Map<String, dynamic> toJson() {
    return _$PaymentCardToJson(this);
  }
}
