// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'payment_card.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

PaymentCard _$PaymentCardFromJson(Map<String, dynamic> json) {
  return PaymentCard(
    id: json['id'] as String,
    cardHolder: json['card_holder'] as String,
    cardNumber: json['card_number'] as String,
  )..checked = json['checked'] as bool;
}

Map<String, dynamic> _$PaymentCardToJson(PaymentCard instance) =>
    <String, dynamic>{
      'id': instance.id,
      'card_holder': instance.cardHolder,
      'card_number': instance.cardNumber,
      'checked': instance.checked,
    };
