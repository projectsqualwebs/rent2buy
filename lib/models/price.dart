class Price {
  int perDay;
  int perHour;

  Price({this.perDay, this.perHour});

  Price.fromJson(Map<String, dynamic> json) {
    perDay = json['per_day'];
    perHour = json['per_hour'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['per_day'] = this.perDay;
    data['per_hour'] = this.perHour;
    return data;
  }
}