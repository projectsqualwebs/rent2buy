import 'dart:convert';

import 'package:json_annotation/json_annotation.dart';
import 'package:rent2buy/models/userinfo.dart';

import 'category.dart';
import 'delivery_type.dart';
import 'image.dart';
import 'purchase_actions.dart';

/// This allows the `User` class to access private members in
/// the generated file. The value for this is *.g.dart, where
/// the star denotes the source file name.
part 'product.g.dart';

/// An annotation for the code generator to know that this class needs the
/// JSON serialization logic to be generated.
///
@JsonSerializable(explicitToJson: true)
class Product {
  Product({
    this.id,
    this.seller,
    this.name,
    this.description,
    this.purchaseActions,
    this.listingCategory,
    this.listingSubCategory,
    this.deliveryType,
    this.images,
    this.rating,
    this.review,
    this.status,
    this.active,
    this.updatedAt,
    this.createdAt,
  });

  @JsonKey(name: '_id')
  String id;
  Userinfo seller;
  String name;
  String description;
  @JsonKey(name: 'purchase_actions')
  List<PurchaseActions> purchaseActions;
  @JsonKey(name: 'listing_category')
  ListingCategory listingCategory;
  @JsonKey(name: 'listing_sub_category')
  ListingCategory listingSubCategory;
  @JsonKey(name: 'delivery_type')
  List<DeliveryType> deliveryType;
  Images images;
  int status;
  int active;
  double rating;
  String review;
  DateTime updatedAt;
  @JsonKey(name: 'created_at')
  DateTime createdAt;

  /// A necessary factory constructor for creating a new User instance
  /// from a map. Pass the map to the generated `_$UserFromJson()` constructor.
  /// The constructor is named after the source class, in this case, User.
  static Product fromJson(Map<String, dynamic> str) => _$ProductFromJson(str);

  /// `toJson` is the convention for a class to declare support for serialization
  /// to JSON. The implementation simply calls the private, generated
  /// helper method `_$ProductToJson`.
  Map<String, dynamic> toJson() => _$ProductToJson(this);
}
