// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'product.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

Product _$ProductFromJson(Map<String, dynamic> json) {
  return Product(
    id: json['_id'] as String,
    seller: json['seller'] == null
        ? null
        : Userinfo.fromJson(json['seller'] as Map<String, dynamic>),
    name: json['name'] as String,
    description: json['description'] as String,
    purchaseActions: (json['purchase_actions'] as List)
        ?.map((e) => e == null
            ? null
            : PurchaseActions.fromJson(e as Map<String, dynamic>))
        ?.toList(),
    listingCategory: json['listing_category'] == null
        ? null
        : ListingCategory.fromJson(
            json['listing_category'] as Map<String, dynamic>),
    listingSubCategory: json['listing_sub_category'] == null
        ? null
        : ListingCategory.fromJson(
            json['listing_sub_category'] as Map<String, dynamic>),
    deliveryType: (json['delivery_type'] as List)
        ?.map((e) =>
            e == null ? null : DeliveryType.fromJson(e as Map<String, dynamic>))
        ?.toList(),
    images: json['images'] == null
        ? null
        : Images.fromJson(json['images'] as Map<String, dynamic>),
    rating: (json['rating'] as num)?.toDouble(),
    review: json['review'] as String,
    status: json['status'] as int,
    active: json['active'] as int,
    updatedAt: json['updatedAt'] == null
        ? null
        : DateTime.parse(json['updatedAt'] as String),
    createdAt: json['created_at'] == null
        ? null
        : DateTime.parse(json['created_at'] as String),
  );
}

Map<String, dynamic> _$ProductToJson(Product instance) => <String, dynamic>{
      '_id': instance.id,
      'seller': instance.seller?.toJson(),
      'name': instance.name,
      'description': instance.description,
      'purchase_actions':
          instance.purchaseActions?.map((e) => e?.toJson())?.toList(),
      'listing_category': instance.listingCategory?.toJson(),
      'listing_sub_category': instance.listingSubCategory?.toJson(),
      'delivery_type': instance.deliveryType?.map((e) => e?.toJson())?.toList(),
      'images': instance.images?.toJson(),
      'status': instance.status,
      'active': instance.active,
      'rating': instance.rating,
      'review': instance.review,
      'updatedAt': instance.updatedAt?.toIso8601String(),
      'created_at': instance.createdAt?.toIso8601String(),
    };
