import 'dart:convert';

import 'package:json_annotation/json_annotation.dart';

part 'purchase_actions.g.dart';

@JsonSerializable()
class PurchaseActions {
  PurchaseActions({
    this.id,
    this.name,
    this.priceVariable,
    this.price,
    this.discountPercent,
    this.discountPrice,
  });

  @JsonKey(name: "_id")
  String id;
  String name;
  @JsonKey(name: "price_variable")
  String priceVariable;
  dynamic price;
  @JsonKey(name: "discount_percent")
  dynamic discountPercent;
  @JsonKey(name: "discount_price")
  dynamic discountPrice;

  static PurchaseActions fromJson(Map<String, dynamic> str) =>
      _$PurchaseActionsFromJson(str);

  Map<String, dynamic> toJson() => _$PurchaseActionsToJson(this);
}
