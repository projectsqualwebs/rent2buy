// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'purchase_actions.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

PurchaseActions _$PurchaseActionsFromJson(Map<String, dynamic> json) {
  return PurchaseActions(
    id: json['_id'] as String,
    name: json['name'] as String,
    priceVariable: json['price_variable'] as String,
    price: json['price'],
    discountPercent: json['discount_percent'],
    discountPrice: json['discount_price'],
  );
}

Map<String, dynamic> _$PurchaseActionsToJson(PurchaseActions instance) =>
    <String, dynamic>{
      '_id': instance.id,
      'name': instance.name,
      'price_variable': instance.priceVariable,
      'price': instance.price,
      'discount_percent': instance.discountPercent,
      'discount_price': instance.discountPrice,
    };
