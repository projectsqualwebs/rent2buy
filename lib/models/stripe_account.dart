import 'package:json_annotation/json_annotation.dart';

part 'stripe_account.g.dart';

@JsonSerializable()
class StripeAccount {
  String id;
  @JsonKey(name: "account_holder")
  String accountHolder;
  @JsonKey(name: "account_number")
  String accountNumber;
  @JsonKey(name: "account_bank")
  String accountBank;

  StripeAccount(
      {this.id, this.accountHolder, this.accountNumber, this.accountBank});

  static StripeAccount fromJson(Map<String, dynamic> json) {
    return _$StripeAccountFromJson(json);
  }

  Map<String, dynamic> toJson() {
    return _$StripeAccountToJson(this);
  }
}
