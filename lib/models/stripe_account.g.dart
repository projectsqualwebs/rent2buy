// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'stripe_account.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

StripeAccount _$StripeAccountFromJson(Map<String, dynamic> json) {
  return StripeAccount(
    id: json['id'] as String,
    accountHolder: json['account_holder'] as String,
    accountNumber: json['account_number'] as String,
    accountBank: json['account_bank'] as String,
  );
}

Map<String, dynamic> _$StripeAccountToJson(StripeAccount instance) =>
    <String, dynamic>{
      'id': instance.id,
      'account_holder': instance.accountHolder,
      'account_number': instance.accountNumber,
      'account_bank': instance.accountBank,
    };
