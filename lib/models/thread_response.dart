import 'package:json_annotation/json_annotation.dart';
import 'package:rent2buy/models/userinfo.dart';

part 'thread_response.g.dart';

@JsonSerializable(explicitToJson: true)
class ThreadResponse {
  @JsonKey(name: '_id')
  String id;
  Userinfo user;
  Userinfo seller;

  ThreadResponse({this.id, this.user, this.seller});

  static ThreadResponse fromJson(Map<String, dynamic> str) =>
      _$ThreadResponseFromJson(str);

  Map<String, dynamic> toJson() => _$ThreadResponseToJson(this);
}
