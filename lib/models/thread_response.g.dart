// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'thread_response.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

ThreadResponse _$ThreadResponseFromJson(Map<String, dynamic> json) {
  return ThreadResponse(
    id: json['_id'] as String,
    user: json['user'] == null
        ? null
        : Userinfo.fromJson(json['user'] as Map<String, dynamic>),
    seller: json['seller'] == null
        ? null
        : Userinfo.fromJson(json['seller'] as Map<String, dynamic>),
  );
}

Map<String, dynamic> _$ThreadResponseToJson(ThreadResponse instance) =>
    <String, dynamic>{
      '_id': instance.id,
      'user': instance.user?.toJson(),
      'seller': instance.seller?.toJson(),
    };
