class User{
  final int id;
  final String name;
  final String imageUrl;
  final String msg;
  User({this.id,this.name,this.imageUrl,this.msg});
}

final User user1 = User(
  id: 0,
  name: 'User 1',
  imageUrl: 'https://www.epicentrofestival.com/wp-content/uploads/2019/12/Person-Clipart-Person-Clip-Art-Image-Clip-Art-Library-Hypertext-Transfer-Protocol-720x962.jpg'
);
final User user2 = User(
    id: 1,
    name: 'User 2',
    imageUrl: ''
);
final User user3 = User(
    id: 2,
    name: 'User 3',
    imageUrl: ''
);