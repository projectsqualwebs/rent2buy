import 'dart:convert';

import 'package:json_annotation/json_annotation.dart';
import 'package:rent2buy/models/User/userAddress.dart';

import 'address.dart';

part 'userinfo.g.dart';

@JsonSerializable(explicitToJson: true)
class Userinfo {
  @JsonKey(name: '_id')
  String sId;
  @JsonKey(name: 'first_name')
  String firstName;
  @JsonKey(name: 'last_name')
  String lastName;
  String email;
  dynamic address;
  String phone;
  String dob;
  @JsonKey(name: 'is_seller')
  int isSeller;
  String token;
  List<dynamic> compay;

  Userinfo(
      {this.sId,
      this.firstName,
      this.lastName,
      this.email,
      this.address,
      this.phone,
      this.dob,
      this.isSeller,
      this.token});

  static Userinfo fromJson(Map<String, dynamic> str) => _$UserinfoFromJson(str);

  Map<String, dynamic> toJson() => _$UserinfoToJson(this);
}
