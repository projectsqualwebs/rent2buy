// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'userinfo.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

Userinfo _$UserinfoFromJson(Map<String, dynamic> json) {
  return Userinfo(
    sId: json['_id'] as String,
    firstName: json['first_name'] as String,
    lastName: json['last_name'] as String,
    email: json['email'] as String,
    address: (json['address'] as List)
        ?.map((e) =>
            e == null ? null : Address.fromJson(e as Map<String, dynamic>))
        ?.toList(),
    phone: json['phone'] as String,
    dob: json['dob'] as String,
    isSeller: json['is_seller'] as int,
    token: json['token'] as String,
  )..compay = json['compay'] as List;
}

Map<String, dynamic> _$UserinfoToJson(Userinfo instance) => <String, dynamic>{
      '_id': instance.sId,
      'first_name': instance.firstName,
      'last_name': instance.lastName,
      'email': instance.email,
      'address': instance.address?.map((e) => e?.toJson())?.toList(),
      'phone': instance.phone,
      'dob': instance.dob,
      'is_seller': instance.isSeller,
      'token': instance.token,
      'compay': instance.compay,
    };
