import 'package:rent2buy/networking/network_status.dart';

class NetworkResponse<T> {
  NetworkStatus status;
  String message;
  T response;
  int statuscode;

  NetworkResponse({this.status, this.response, this.message}) {
    this.status = status;
    this.message = message;
    this.response = response;
    this.statuscode = statuscode;
  }

    NetworkResponse.fromStatus({this.status, this.response, this.message, this.statuscode}) {
    this.status = status;
    this.message = message;
    this.response = response;
    this.statuscode = statuscode;
  }

  static NetworkResponse<T> idle<T>() {
    return NetworkResponse(status: NetworkStatus.IDLE);
  }

  static NetworkResponse<T> loading<T>() {
    return NetworkResponse(status: NetworkStatus.LOADING);
  }

  static NetworkResponse<T> success<T>(T data) {
    return NetworkResponse(status: NetworkStatus.SUCCESS, response: data);
  }

  static NetworkResponse<T> successWithMsg<T>(T data, String message) {
    return NetworkResponse(
        status: NetworkStatus.SUCCESS, response: data, message: message);
  }

  static NetworkResponse<T> successWithMsgAndStatus<T>(
      T data, String message, int statusCode) {
    return NetworkResponse.fromStatus(
        status: NetworkStatus.SUCCESS, response: data, message: message,statuscode: statusCode);
  }

  static NetworkResponse<T> error<T>(String error) {
    return NetworkResponse(status: NetworkStatus.ERROR, message: error);
  }

//  static <T> NetworkResponse<T> loading() {
//   return new NetworkResponse<>(Status.LOADING, null, null);
// }
//
//  static <T> NetworkResponse<T> success(T data, String message) {
//   return new NetworkResponse<>(Status.SUCCESS, message, data);
// }
//
//  static <T> NetworkResponse<T> success(T data) {
//   return new NetworkResponse<>(Status.SUCCESS, null, data);
// }
//
//  static <T> NetworkResponse<T> success(String message) {
//   return new NetworkResponse<>(Status.SUCCESS, message, null);
// }
//
//  static <T> NetworkResponse<T> error(String error) {
//   return new NetworkResponse<>(Status.ERROR, error, null);
// }
}
