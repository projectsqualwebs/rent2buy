import 'package:rent2buy/networking/network_status.dart';

class NetworkResponseList<T> {
  NetworkStatus status;
  String message;
  List<T> response;

  NetworkResponseList({this.status, this.response, this.message}) {
    this.status = status;
    this.message = message;
    this.response = response;
  }

  static NetworkResponseList<T> idle<T>() {
    return NetworkResponseList(status: NetworkStatus.IDLE);
  }

  static NetworkResponseList<T> loading<T>() {
    return NetworkResponseList(status: NetworkStatus.LOADING);
  }

  static NetworkResponseList<T> success<T>(List<T> data) {
    return NetworkResponseList(status: NetworkStatus.SUCCESS, response: data);
  }

  static NetworkResponseList<T> error<T>(String error) {
    return NetworkResponseList(status: NetworkStatus.ERROR, message: error);
  }

//  static <T> NetworkResponse<T> loading() {
//   return new NetworkResponse<>(Status.LOADING, null, null);
// }
//
//  static <T> NetworkResponse<T> success(T data, String message) {
//   return new NetworkResponse<>(Status.SUCCESS, message, data);
// }
//
//  static <T> NetworkResponse<T> success(T data) {
//   return new NetworkResponse<>(Status.SUCCESS, null, data);
// }
//
//  static <T> NetworkResponse<T> success(String message) {
//   return new NetworkResponse<>(Status.SUCCESS, message, null);
// }
//
//  static <T> NetworkResponse<T> error(String error) {
//   return new NetworkResponse<>(Status.ERROR, error, null);
// }
}
