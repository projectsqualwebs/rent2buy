import 'dart:convert';

import 'package:dio/dio.dart';
import 'package:flutter/material.dart';
import 'package:rent2buy/di/service_locator.dart';
import 'package:rent2buy/models/User/userRegister.dart';
import 'package:rent2buy/models/userinfo.dart';
import 'package:rent2buy/networking/api_result.dart';
import 'package:rent2buy/networking/standard_response_list.dart';
import 'package:rent2buy/services/apiProvider.dart';
import 'package:rent2buy/utils/data_manager.dart';

import '../dioclient.dart';
import '../network_exceptions.dart';
import '../standard_response.dart';

class AccountRepository {
  DioClient dioClient;
  DataManager _dataManager = serviceLocator<DataManager>();

  AccountRepository() {
    dioClient = DioClient();
    initToken();
  }

  void initToken() async {
    String token = await _dataManager.loadToken();
    var options = dioClient.http.options;
    options.headers['Authorization'] = "Bearer $token";
  }

  // Future<String> registerUser(UserRegister userRegister) async {
  //   try {
  //     final response = await dioClient.post(ApiProvider.signUpUser,
  //         data: json.encode(userRegister.toJson()));
  //
  //     return StandardResponse.fromJson(response).message;
  //   } catch (e) {
  //     return NetworkExceptions.getErrorMessage(
  //         NetworkExceptions.getDioException(e));
  //   }
  // }

  Future<StandardResponseList<String>> registerUser(
      Map<String, dynamic> params) async {
    try {
      final response =
          await dioClient.post(ApiProvider.signUpUser, data: params);
      return StandardResponseList.fromJson(response);
    } catch (e) {
      throw e;
    }
  }

  Future<StandardResponseList<String>> disableSeller() async {
    try {
      final response =
          await dioClient.post(ApiProvider.deactivateAccount, data: Map());
      return StandardResponseList.fromJson(response);
    } catch (e) {
      throw e;
    }
  }

  Future<StandardResponse<Userinfo>> loginUser(
      Map<String, dynamic> params) async {
    try {
      final response =
          await dioClient.post(ApiProvider.loginUser, data: params);

      return StandardResponse<Userinfo>(response['status'], response['message'],
          Userinfo.fromJson(response['response']));
    } catch (e) {
      throw e;
    }
  }

  Future<StandardResponse<Userinfo>> verifyOTP(
      Map<String, dynamic> params) async {
    try {
      final response =
          await dioClient.post(ApiProvider.verifyOtp, data: params);
      String token = response['response']['token'];
      Userinfo userinfo = Userinfo.fromJson(response['response']['user']);
      userinfo.token = token;
      return StandardResponse<Userinfo>(
          response['status'], response['message'], userinfo);
    } catch (e) {
      throw e;
    }
  }

  Future<StandardResponseList<String>> resendOTP(
      Map<String, dynamic> params) async {
    try {
      final response =
          await dioClient.post(ApiProvider.resendOtp, data: params);
      return StandardResponseList.fromJson(response);
    } catch (e) {
      throw e;
    }
  }

  Future<StandardResponseList<String>> forgotPassword(
      Map<String, dynamic> params) async {
    try {
      final response =
          await dioClient.post(ApiProvider.forgotPassword, data: params);
      return StandardResponseList.fromJson(response);
    } catch (e) {
      throw e;
    }
  }

  Future<StandardResponse<Userinfo>> updateProfile(
      Map<String, dynamic> params) async {
    try {
      final response =
          await dioClient.post(ApiProvider.getProfile, data: params);
      return StandardResponse.fromJsonObject(response, Userinfo.fromJson);
    } catch (e) {
      throw e;
    }
  }

  Future<StandardResponse<Userinfo>> getUserProfile() async {
    try {
      final response = await dioClient.get(ApiProvider.getProfile);
      return StandardResponse.fromJsonObject(response, Userinfo.fromJson);
    } catch (e) {
      print(e.toString);
      throw e;
    }
  }
}
