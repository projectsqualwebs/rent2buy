import 'package:rent2buy/di/service_locator.dart';
import 'package:rent2buy/models/chat_message.dart';
import 'package:rent2buy/models/thread_response.dart';
import 'package:rent2buy/networking/dioclient.dart';
import 'package:rent2buy/networking/standard_response.dart';
import 'package:rent2buy/networking/standard_response_list.dart';
import 'package:rent2buy/services/apiProvider.dart';
import 'package:rent2buy/utils/data_manager.dart';

class MessageRepository {
  DioClient dioClient;
  DataManager _dataManager = serviceLocator<DataManager>();

  MessageRepository() {
    dioClient = DioClient();
    initToken();
  }

  void initToken() async {
    String token = await _dataManager.loadToken();
    var options = dioClient.http.options;
    options.headers['Authorization'] = "Bearer $token";
  }

//  Future<StandardResponseList<ChatMessage>> getOrders() async {
//     try {
//       final response = await dioClient.get(ApiProvider.order);
//       print(response['response']);
//       return StandardResponseList.fromJsonObject(response, Order.fromJson);
//     } catch (e) {
//       print(e);
//       throw e;
//     }
//   }

  Future<StandardResponse<ThreadResponse>> createThread(
      Map<String, dynamic> params) async {
    try {
      final response = await dioClient.post(ApiProvider.thread, data: params);
      return StandardResponse.fromJsonObject(response, ThreadResponse.fromJson);
    } catch (e) {
      print(e.toString);
      throw e;
    }
  }

  Future<StandardResponseList<String>> deleteThread(String threadId) async {
    try {
      final response =
          await dioClient.delete(ApiProvider.thread + '/' + threadId);
      return StandardResponseList.fromJson(response);
    } catch (e) {
      print(e.toString);
      throw e;
    }
  }

  Future<StandardResponseList<ThreadResponse>> getThreads(
      Map<String, dynamic> params) async {
    try {
      final response = await dioClient.get(ApiProvider.thread);
      return StandardResponseList.fromJsonObject(
          response, ThreadResponse.fromJson);
    } catch (e) {
      print(e.toString);
      throw e;
    }
  }

  Future<StandardResponseList<String>> sendMessage(
      Map<String, dynamic> params) async {
    try {
      final response = await dioClient.post(ApiProvider.chat, data: params);
      return StandardResponseList.fromJson(response);
    } catch (e) {
      print(e.toString);
      throw e;
    }
  }
}
