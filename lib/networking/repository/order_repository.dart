import 'package:rent2buy/di/service_locator.dart';
import 'package:rent2buy/models/order.dart';
import 'package:rent2buy/networking/dioclient.dart';
import 'package:rent2buy/networking/standard_response_list.dart';
import 'package:rent2buy/services/apiProvider.dart';
import 'package:rent2buy/utils/data_manager.dart';

class OrderRepository {
  DioClient dioClient;
  DataManager _dataManager = serviceLocator<DataManager>();

  OrderRepository() {
    dioClient = DioClient();
    initToken();
  }

  void initToken() async {
    String token = await _dataManager.loadToken();
    var options = dioClient.http.options;
    options.headers['Authorization'] = "Bearer $token";
  }

  Future<StandardResponseList<Order>> getOrders() async {
    try {
      final response = await dioClient.get(ApiProvider.order);
      print(response['response']);
      return StandardResponseList.fromJsonObject(response, Order.fromJson);
    } catch (e) {
      print(e);
      throw e;
    }
  }
}
