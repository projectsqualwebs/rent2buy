import 'package:rent2buy/di/service_locator.dart';
import 'package:rent2buy/models/payment_card.dart';
import 'package:rent2buy/models/stripe_account.dart';
import 'package:rent2buy/networking/dioclient.dart';
import 'package:rent2buy/networking/standard_response.dart';
import 'package:rent2buy/networking/standard_response_list.dart';
import 'package:rent2buy/screens/seller/bank_account_details.dart';
import 'package:rent2buy/services/apiProvider.dart';
import 'package:rent2buy/utils/data_manager.dart';

class PaymentRepository {
  DioClient dioClient;
  DataManager _dataManager = serviceLocator<DataManager>();

  PaymentRepository() {
    dioClient = DioClient();
    initToken();
  }

  void initToken() async {
    String token = await _dataManager.loadToken();
    var options = dioClient.http.options;
    options.headers['Authorization'] = "Bearer $token";
  }

  Future<StandardResponseList<String>> addCard(
      Map<String, dynamic> params) async {
    try {
      final response = await dioClient.post(ApiProvider.addCard, data: params);
      return StandardResponseList.fromJson(response);
    } catch (e) {
      print(e);
      throw e;
    }
  }

  Future<StandardResponseList<String>> addBankAccount(
      Map<String, dynamic> params) async {
    try {
      final response =
          await dioClient.post(ApiProvider.sellerBankAcc, data: params);
      return StandardResponseList.fromJson(response);
    } catch (e) {
      print(e);
      throw e;
    }
  }

  Future<StandardResponseList<String>> removeBankAccount(
      String accountId) async {
    try {
      final response =
          await dioClient.delete(ApiProvider.sellerBankAcc + accountId);
      return StandardResponseList.fromJson(response);
    } catch (e) {
      print(e);
      throw e;
    }
  }

  Future<StandardResponseList<String>> markAsDefault(
      String cardId) async {
    try {
      final response =
          await dioClient.get(ApiProvider.defaultCard + cardId);
      return StandardResponseList.fromJson(response);
    } catch (e) {
      print(e);
      throw e;
    }
  }

  Future<StandardResponseList<PaymentCard>> getCards() async {
    try {
      final response = await dioClient.get(ApiProvider.addCard);

      return StandardResponseList(
          response['status'],
          response['message'],
          List<PaymentCard>.from(response['response']['cards']
              .map((x) => PaymentCard.fromJson(x))));
    } catch (e) {
      print(e);
      throw e;
    }
  }

  Future<StandardResponseList<StripeAccount>> getBankAccount() async {
    try {
      final response = await dioClient.get(ApiProvider.sellerBankAcc);

      return StandardResponseList<StripeAccount>(
          response['status'],
          response['message'],
          List<StripeAccount>.from(response['response']['accounts']
              .map((x) => StripeAccount.fromJson(x))));
    } catch (e) {
      print(e);
      throw e;
    }
  }
}
