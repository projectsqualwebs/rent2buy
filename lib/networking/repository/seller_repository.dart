import 'dart:convert';

import 'package:dio/dio.dart';
import 'package:rent2buy/di/service_locator.dart';
import 'package:rent2buy/models/Seller/addListings.dart';
import 'package:rent2buy/models/cart_response.dart';
import 'package:rent2buy/models/delivery_type.dart';
import 'package:rent2buy/models/order.dart';
import 'package:rent2buy/models/product.dart';
import 'package:rent2buy/models/purchase_actions.dart';
import 'package:rent2buy/models/userinfo.dart';
import 'package:rent2buy/networking/dioclient.dart';
import 'package:rent2buy/networking/standard_response_list.dart';
import 'package:rent2buy/services/apiProvider.dart';
import 'package:rent2buy/utils/data_manager.dart';

import '../standard_response.dart';

class SellerRepository {
  DioClient dioClient;
  DataManager _dataManager = serviceLocator<DataManager>();

  SellerRepository() {
    dioClient = DioClient();
    initToken();
  }

  void initToken() async {
    String token = await _dataManager.loadToken();
    var options = dioClient.http.options;
    options.headers['Authorization'] = "Bearer $token";
  }

// Future<StandardResponseList<String>> registerUser(
//     Map<String, dynamic> params) async {
//   try {
//     final response =
//         await dioClient.post(ApiProvider.signUpUser, data: params);
//     return StandardResponseList.fromJson(response);
//   } catch (e) {
//     throw e;
//   }
// }

  Future<StandardResponse<Product>> getSingleProduct(String id) async {
    try {
      String token = await _dataManager.loadToken();
      var options = dioClient.http.options;
      options.headers['Authorization'] = "Bearer $token";
      final response = await dioClient.get(ApiProvider.getSingleItem + id);
      return StandardResponse.fromJsonObject(response, Product.fromJson);
    } catch (e) {
      print(e.toString());
      throw e;
    }
  }

  Future<StandardResponseList<Product>> getProducts(
      Map<String, dynamic> params) async {
    try {
      String token = await _dataManager.loadToken();
      var options = dioClient.http.options;
      options.headers['Authorization'] = "Bearer $token";
      final response =
          await dioClient.post(ApiProvider.sellerListing, data: params);
      return StandardResponseList.fromJsonObject(response, Product.fromJson);
    } catch (e) {
      print(e.toString());
      throw e;
    }
  }

  Future<StandardResponseList<DeliveryType>> getDeliveryTypes() async {
    try {
      final response = await dioClient.get(ApiProvider.getDeliveryTypes);
      return StandardResponseList.fromJsonObject(
          response, DeliveryType.fromJson);
    } catch (e) {
      print(e.toString);
      throw e;
    }
  }

  Future<StandardResponseList<String>> updateListingStatus(
      Map<String, dynamic> params, String listingId) async {
    try {
      final response = await dioClient
          .post(ApiProvider.updateListingStatus + listingId, data: params);
      return StandardResponseList.fromJson(response);
    } catch (e) {
      print(e.toString);
      throw e;
    }
  }

  Future<StandardResponseList<PurchaseActions>> getActions() async {
    try {
      final response = await dioClient.get(ApiProvider.getActions);
      return StandardResponseList.fromJsonObject(
          response, PurchaseActions.fromJson);
    } catch (e) {
      print(e.toString);
      throw e;
    }
  }

  Future<StandardResponseList<Product>> addListing(AddListings params) async {
    try {
      String token = await _dataManager.loadToken();
      var options = dioClient.http.options;
      options.headers['Authorization'] = "Bearer $token";
      final response = await dioClient.post(ApiProvider.addListing,
          data: json.encode(params));
      return StandardResponseList.fromJson(response);
    } catch (e) {
      throw e;
    }
  }

  Future<StandardResponseList<Product>> editListing(
      AddListings params, String id) async {
    try {
      String token = await _dataManager.loadToken();
      var options = dioClient.http.options;
      options.headers['Authorization'] = "Bearer $token";
      final response = await dioClient.post(ApiProvider.editListing + id,
          data: json.encode(params));
      return StandardResponseList.fromJson(response);
    } catch (e) {
      throw e;
    }
  }

  Future<StandardResponseList<String>> becomeSeller() async {
    try {
      final response = await dioClient.get(ApiProvider.userToSeller);
      return StandardResponseList.fromJson(response);
    } catch (e) {
      print(e);
      throw e;
    }
  }

  Future<StandardResponse<CartResponse>> addToCart(
      Map<String, dynamic> params, String cartId) async {
    try {
      if (cartId.isEmpty) {
        final response =
            await dioClient.post(ApiProvider.addToCartLoggedIn, data: params);
        if (response['status'] == 201) {
          return StandardResponse(
              response['status'], response['message'], null);
        } else {
          return StandardResponse.fromJsonObject(
              response, CartResponse.fromJson);
        }
      } else {
        final response =
            await dioClient.post(ApiProvider.addToCart + cartId, data: params);
        if (response['status'] == 201) {
          return StandardResponse(
              response['status'], response['message'], null);
        } else {
          return StandardResponse.fromJsonObject(
              response, CartResponse.fromJson);
        }
      }
    } catch (e) {
      throw e;
    }
  }

  Future<StandardResponse<Userinfo>> updateSeller(
      Map<String, dynamic> params) async {
    try {
      final response =
          await dioClient.post(ApiProvider.sellerProfile, data: params);
      return StandardResponse.fromJsonObject(response, Userinfo.fromJson);
    } catch (e) {
      throw e;
    }
  }

  Future<StandardResponseList<String>> updateOrderStatus(
      Map<String, dynamic> params, String orderId) async {
    try {
      final response = await dioClient
          .post(ApiProvider.updateOrderStatus + orderId, data: params);
      return StandardResponseList.fromJson(response);
    } catch (e) {
      throw e;
    }
  }

  Future<StandardResponseList<Order>> getOrders(
      Map<String, dynamic> params) async {
    try {
      final response =
          await dioClient.post(ApiProvider.sellerOrders, data: params);
      return StandardResponseList.fromJsonObject(response, Order.fromJson);
    } catch (e) {
      print(e);
      throw e;
    }
  }

  Future<StandardResponse<Order>> getOrderInfo(String id) async {
    try {
      final response = await dioClient.get(ApiProvider.singleOrder + id);
      return StandardResponse.fromJsonObject(response, Order.fromJson);
    } catch (e) {
      print(e);
      throw e;
    }
  }
}
