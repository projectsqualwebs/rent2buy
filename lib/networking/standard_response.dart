class StandardResponse<T> {
  int status;
  String message;
  T response;

  StandardResponse(this.status, this.message, this.response);

  StandardResponse.fromJson(dynamic params) {
    status = params['status'];
    message = params['message'];
    response = params['response'];
  }

  StandardResponse.fromJsonObject(
      Map<String, dynamic> params, Function fromJson) {
    status = params['status'];
    message = params['message'];
    response = fromJson(params['response']);
  }
}
