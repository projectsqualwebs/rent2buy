class StandardResponseList<T> {
  int status;
  String message;
  List<T> response;

  StandardResponseList(this.status, this.message, this.response);

  StandardResponseList.fromJson(Map<String, dynamic> params) {
    status = params['status'];
    message = params['message'];
    response = List<T>.from(params['response'].map((x) => x));
  }

  StandardResponseList.fromJsonObject(
      Map<String, dynamic> params, Function fromJson) {
    status = params['status'];
    message = params['message'];
    response = List<T>.from(params['response'].map((x) => fromJson(x)));
  }
}
