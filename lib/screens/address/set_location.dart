import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_google_places/flutter_google_places.dart';
import 'package:geocoder/geocoder.dart';
import 'package:geocoding/geocoding.dart';
import 'package:geolocator/geolocator.dart';
import 'package:google_maps_webservice/places.dart';
import 'package:rent2buy/di/service_locator.dart';
import 'package:rent2buy/models/User/addAddress.dart';
import 'package:rent2buy/models/address.dart' as UserAddress;
import 'package:rent2buy/networking/network_status.dart';
import 'package:rent2buy/screens/address/set_location_viewmodel.dart';
import 'package:rent2buy/screens/common/loading_dialog.dart';
import 'package:rent2buy/utils/constants.dart';
import 'package:rent2buy/utils/constants.dart' as Constants;
import 'package:rent2buy/utils/design_utils.dart';
import 'package:rent2buy/utils/dimens.dart';
import 'package:rent2buy/utils/strings.dart';
import 'package:rent2buy/widgets/shared_widgets.dart';

class SetLocation extends StatefulWidget {
  final SetLocationViewModel viewModel = serviceLocator<SetLocationViewModel>();

  @override
  _SetLocationState createState() => _SetLocationState();
}

class _SetLocationState extends State<SetLocation> {
  TextEditingController _addressController = TextEditingController();
  TextEditingController _zipController = TextEditingController();

  bool isVisible = false;
  double lat;
  double long;
  Map<String, dynamic> params = Map();
  AddAddress addAddress = AddAddress();
  Placemark currentPosition;

  GoogleMapsPlaces _places = GoogleMapsPlaces(apiKey: kGoogleApiKey);

  final _formKey = GlobalKey<FormState>();
  final _keyLoader = GlobalKey<State>();
  final _keyScafold = GlobalKey<ScaffoldState>();

  @override
  void initState() {
    // TODO: implement initState
    super.initState();
    widget.viewModel.addressValue.addListener(() {
      WidgetsBinding.instance.addPostFrameCallback((_) {
        var response = widget.viewModel.addressValue.value;
        switch (response.status) {
          case NetworkStatus.LOADING:
            Dialogs.showLoadingDialog(_keyScafold.currentContext, _keyLoader,
                Strings.txtSavingLocation);
            break;
          case NetworkStatus.ERROR:
            Navigator.of(_keyLoader.currentContext).pop();
            break;
          case NetworkStatus.SUCCESS:
            Navigator.of(_keyLoader.currentContext).pop();
            Navigator.of(_keyScafold.currentContext).pop();
            break;
        }
      });
    });
  }

  @override
  Widget build(BuildContext context) {
    return SafeArea(
      child: Scaffold(
        key: _keyScafold,
        appBar: AppBar(
          title: Text(
            'Set Location',
            style: TextStyle(
                fontWeight: FontWeight.bold,
                color: Color(0xFF97080E),
                fontSize: 24),
          ),
          centerTitle: true,
          actions: [
            FlatButton(
              child: Text(
                'Cancel',
                style: TextStyle(color: Color(0xFF97080E)),
              ),
              onPressed: () {
                
              },
            )
          ],
          automaticallyImplyLeading: false,
        ),
        body: Container(
          margin: EdgeInsets.all(margin_large),
          child: SingleChildScrollView(
            child: Column(
              crossAxisAlignment: CrossAxisAlignment.center,
              children: [
                SizedBox(height: 80),
                Text('Where you are Searching?',
                    style:
                        TextStyle(fontWeight: FontWeight.w700, fontSize: 20)),
                SizedBox(height: 20),
                CustomIconButton(
                  textColor: Color(0xFF97080E),
                  hinttext: 'Get my Location',
                  icon: Icon(Icons.location_pin, color: Color(0xFF97080E)),
                  onPressed: () {
                    getLocation();
                  },
                ),
                SizedBox(height: 20),
                // Visibility(
                //   visible: isVisible,
                //   child: Column(
                //     mainAxisAlignment: MainAxisAlignment.center,
                //     children: [
                //       Text(
                //         currentPosition == null ? '' : currentPosition.street,
                //         style: TextStyle(fontSize: 16),
                //       ),
                //       Text(
                //           currentPosition == null
                //               ? ''
                //               : currentPosition.locality,
                //           style: TextStyle(fontSize: 16)),
                //       Text(
                //           currentPosition == null
                //               ? ''
                //               : currentPosition.country,
                //           style: TextStyle(fontSize: 16)),
                //     ],
                //   ),
                // ),
                // SizedBox(height: 20),
                Text('OR',
                    style: TextStyle(
                        fontSize: 16,
                        fontWeight: FontWeight.w700,
                        color: Colors.grey[700])),
                SizedBox(height: 30),
                Form(
                  key: _formKey,
                  child: Column(
                    children: [
                      TextFormField(
                        validator: (value) {
                          if (value.isEmpty) {
                            return Strings.fieldRequired;
                          }
                          return null;
                        },
                        controller: _addressController,
                        autocorrect: true,
                        decoration: InputDecoration(
                            hintText: "Enter Location",
                            suffixIcon: Icon(Icons.edit),
                            focusedBorder: DesignUtils.getDefaultBorder(),
                            enabledBorder: DesignUtils.getDefaultBorder()),
                        keyboardType: TextInputType.text,
                        onTap: () async {
                          Prediction p = await PlacesAutocomplete.show(
                              context: context,
                              apiKey: kGoogleApiKey,
                              mode: Mode.overlay);
                          displayPrediction(p, context);
                        },
                      ),
                      SizedBox(height: 20),
                      TextFormField(
                        validator: (value) {
                          if (value.isEmpty) {
                            return Strings.fieldRequired;
                          }
                          return null;
                        },
                        controller: _zipController,
                        autocorrect: true,
                        decoration: InputDecoration(
                            hintText: "Enter Zip Code",
                            suffixIcon: Icon(Icons.edit),
                            focusedBorder: DesignUtils.getDefaultBorder(),
                            enabledBorder: DesignUtils.getDefaultBorder()),
                        keyboardType: TextInputType.number,
                        onTap: () async {},
                      ),
                    ],
                  ),
                ),

                Padding(
                  padding: const EdgeInsets.all(16.0),
                  child: MyButton(
                      text: 'Save Location',
                      color: Color(0xFF97080E),
                      onPressed: () {
                        if (_formKey.currentState.validate()) {
                          List<UserAddress.Address> addressList = List();
                          addressList.add(UserAddress.Address.fromJson(params));
                          Map<String, dynamic> addressParams = Map();
                          addressParams[address] = addressList;
                          
                          widget.viewModel.addLocation(addressParams);
                        }
                      }),
                ),
              ],
            ),
          ),
        ),
      ),
    );
  }

  Future<Null> displayPrediction(Prediction p, BuildContext context) async {
    Dialogs.showLoadingDialog(context, _keyLoader, Strings.txtGettingLocation);
    if (p != null) {
      PlacesDetailsResponse detail =
          await _places.getDetailsByPlaceId(p.placeId);

      var placeId = p.placeId;
      lat = detail.result.geometry.location.lat;
      long = detail.result.geometry.location.lng;

      print(lat);
      print(long);
      var a = await Geocoder.local.findAddressesFromQuery(p.description);

      setState(() {
        _addressController.text = p.description;
        _zipController.text = a[0]?.postalCode;
        params[Constants.location] = p.description;
        params[Constants.latitude] = lat;
        params[Constants.longitude] = long;
        Navigator.of(_keyLoader.currentContext).pop();
      });
      print(p.description);
      print(lat);
      print(long);
    }
  }

  getLocation() async {
    Dialogs.showLoadingDialog(context, _keyLoader, Strings.txtGettingLocation);
    LocationPermission permission = await Geolocator.requestPermission();
    Position position = await Geolocator.getCurrentPosition(
        desiredAccuracy: LocationAccuracy.high);
    lat = position.latitude;
    long = position.longitude;
    print(lat);
    print(long);
    List<Placemark> placemarks = await placemarkFromCoordinates(lat, long);
    currentPosition = placemarks[0];
    print(currentPosition);
    isVisible = true;
    setState(() {
      _addressController.text = currentPosition.street +
          "," +
          currentPosition.subLocality +
          "," +
          currentPosition.locality +
          "," +
          currentPosition.administrativeArea +
          "," +
          currentPosition.country;
      _zipController.text = currentPosition.postalCode;
      params[Constants.location] = _addressController.text;
      params[Constants.latitude] = lat;
      params[Constants.longitude] = long;
      Navigator.of(_keyLoader.currentContext).pop();
    });
  }
}
