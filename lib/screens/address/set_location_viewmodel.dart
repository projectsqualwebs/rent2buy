import 'package:flutter/material.dart';
import 'package:rent2buy/di/service_locator.dart';
import 'package:rent2buy/models/common_repository.dart';
import 'package:rent2buy/networking/network_response.dart';
import 'package:rent2buy/utils/base_change_notifier.dart';

class SetLocationViewModel extends BaseChangeNotifier {
  final CommonRepository _commonRepository = serviceLocator<CommonRepository>();

  ValueNotifier<NetworkResponse<String>> addressValue =
      ValueNotifier(NetworkResponse.idle());

  void addLocation(Map<String, dynamic> params) {
    addressValue.value = NetworkResponse.loading();
    _commonRepository.addAddress(params).then((response) {
      addressValue.value =
          NetworkResponse.successWithMsg(null, response.message);
    }).catchError((e) {
      addressValue.value = NetworkResponse.error(parseError(e));
    });
  }
}
