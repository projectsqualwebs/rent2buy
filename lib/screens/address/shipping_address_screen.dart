import 'package:flutter/material.dart';
import 'package:fluttertoast/fluttertoast.dart';
import 'package:rent2buy/di/service_locator.dart';
import 'package:rent2buy/models/address.dart';
import 'package:rent2buy/networking/network_responselist.dart';
import 'package:rent2buy/networking/network_status.dart';
import 'package:rent2buy/screens/address/set_location.dart';
import 'package:rent2buy/screens/address/shipping_address_viewmodel.dart';
import 'package:rent2buy/screens/common/base_app_bar.dart';
import 'package:rent2buy/screens/common/error_screen.dart';
import 'package:rent2buy/screens/common/progress_view.dart';
import 'package:rent2buy/utils/data_manager.dart';
import 'package:rent2buy/utils/design_utils.dart';
import 'package:rent2buy/utils/dimens.dart';
import 'package:rent2buy/utils/strings.dart';

class ShippinAddressScreen extends StatefulWidget {
  final ShippingAddressViewModel viewModel =
      serviceLocator<ShippingAddressViewModel>();

  @override
  State<StatefulWidget> createState() {
    // TODO: implement createState
    return _ShippingAddressState();
  }
}

class _ShippingAddressState extends State<ShippinAddressScreen>
    with WidgetsBindingObserver {
  List<Address> shippingAddresses = List();
  var selectedAddress;
  var _keyList = GlobalKey<AnimatedListState>();

  @override
  void initState() {
    WidgetsBinding.instance.addObserver(this);
    // TODO: implement initState
    super.initState();
    widget.viewModel.getDeliveryAddress();
  }

  @override
  void dispose() {
    WidgetsBinding.instance.removeObserver(this);
    super.dispose();
  }

  @override
  void didChangeAppLifecycleState(AppLifecycleState state) {
    // TODO: implement didChangeAppLifecycleState
    super.didChangeAppLifecycleState(state);
    if (state == AppLifecycleState.resumed) {
      widget.viewModel.getDeliveryAddress();
    }
  }

  @override
  Widget build(BuildContext context) {
    // TODO: implement build
    return Scaffold(
      appBar: CommonAppBar(
        appBar: AppBar(),
        title: Strings.titleShippingAddress,
        automaticallyImplyLeading: true,
        centerTitle: false,
      ),
      body: ValueListenableBuilder(
        valueListenable: widget.viewModel.addressValue,
        builder: (context, value, child) {
          var response = value as NetworkResponseList<Address>;
          switch (response.status) {
            case NetworkStatus.LOADING:
              return CircularProgress();
            case NetworkStatus.ERROR:
              return ErrorScreen(response.message);
            case NetworkStatus.SUCCESS:
              addItems(response.response);
              return CustomScrollView(
                slivers: [
                  SliverList(
                    delegate: SliverChildListDelegate([
                      AnimatedList(
                        key: _keyList,
                        shrinkWrap: true,
                        initialItemCount: shippingAddresses.length,
                        itemBuilder: (context, index, animation) {
                          if (index < shippingAddresses.length) {
                            return _buildItem(index, shippingAddresses[index]);
                          } else {
                            return Container();
                          }
                        },
                      )
                    ]),
                  ),
                  SliverFillRemaining(
                    hasScrollBody: false,
                    child: Container(
                      alignment: Alignment.bottomCenter,
                      child: Column(
                        mainAxisSize: MainAxisSize.min,
                        children: [
                          InkWell(
                            child: Text(Strings.addNewAddress),
                            onTap: () async {
                              await Navigator.push(
                                  context,
                                  MaterialPageRoute(
                                      builder: (context) => SetLocation()));
                            },
                          ),
                          SizedBox(
                            height: margin_large,
                          ),
                          ElevatedButton(
                            style: DesignUtils.roundedButtonStyle(),
                            child: Text(
                              Strings.proceed,
                              style: DesignUtils.textApperanceButton(),
                            ),
                            onPressed: () {
                              if (selectedAddress == null) {
                                Fluttertoast.showToast(
                                    msg: Strings.msgSelectAddress);
                              }
                              Navigator.pop(context, selectedAddress.toJson());
                            },
                          ),
                        ],
                      ),
                    ),
                  )
                ],
              );
          }
          return Container();
        },
      ),
    );
  }

  void addItems(List<Address> response) {
    WidgetsBinding.instance.addPostFrameCallback((_) {
      shippingAddresses.clear();
      shippingAddresses.addAll(response);
      for (int offset = 0; offset < response.length; offset++) {
        _keyList.currentState.insertItem(offset);
      }
    });
  }

  Widget _buildItem(int index, Address shippingAddress) {
    try {
      // selectedAddress =
      //     shippingAddress.defaultAddress == 1 ? shippingAddress : null;
      return Card(
        margin: EdgeInsets.all(margin_medium),
        child: Container(
          padding: EdgeInsets.all(margin_small),
          height: 96,
          child: Row(
            mainAxisAlignment: MainAxisAlignment.spaceBetween,
            children: [
              Flexible(
                flex: 1,
                child: Radio(
                  groupValue: selectedAddress,
                  value: shippingAddress,
                  onChanged: (value) {
                    _keyList.currentState.setState(() {
                      selectedAddress = value;
                    });
                  },
                ),
              ),
              Flexible(
                  flex: 2,
                  child: Text(shippingAddress.location == null
                      ? ""
                      : shippingAddress.location)),
              Flexible(
                flex: 1,
                child: Icon(Icons.edit),
              )
            ],
          ),
        ),
      );
    } catch (exception) {}
  }
}
