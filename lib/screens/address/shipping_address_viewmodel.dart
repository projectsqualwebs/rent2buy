import 'package:flutter/material.dart';
import 'package:rent2buy/di/service_locator.dart';
import 'package:rent2buy/models/address.dart';
import 'package:rent2buy/models/common_repository.dart';
import 'package:rent2buy/models/payment_card.dart';
import 'package:rent2buy/networking/network_responselist.dart';
import 'package:rent2buy/utils/base_change_notifier.dart';

class ShippingAddressViewModel extends BaseChangeNotifier {
  final CommonRepository paymentRepository = serviceLocator<CommonRepository>();

  ValueNotifier<NetworkResponseList<PaymentCard>> cardValue =
      ValueNotifier(NetworkResponseList.idle());

  ValueNotifier<NetworkResponseList<Address>> addressValue =
      ValueNotifier(NetworkResponseList.idle());

  void getDeliveryAddress() {
    addressValue.value = NetworkResponseList.loading();
    paymentRepository.getDeliveryAddress().then((response) {

      addressValue.value = NetworkResponseList.success(response.response);
    }).catchError((e) {
      addressValue.value = NetworkResponseList.error(parseError(e));
    });
  }
}
