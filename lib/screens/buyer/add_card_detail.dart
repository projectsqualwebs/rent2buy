import 'dart:io';
import 'package:flutter/material.dart';
import 'package:flutter/rendering.dart';
import 'package:rent2buy/services/apiProvider.dart';
import 'package:rent2buy/widgets/shared_widgets.dart';
import 'package:http/http.dart' as http;
import 'package:shared_preferences/shared_preferences.dart';

class AddCardDetail extends StatefulWidget {
  @override
  _AddCardDetailState createState() => _AddCardDetailState();
}

class _AddCardDetailState extends State<AddCardDetail> {
  TextEditingController cardnumber = TextEditingController();
  TextEditingController expMonth = TextEditingController();
  TextEditingController expYear = TextEditingController();
  TextEditingController cvv = TextEditingController();
  TextEditingController cardHolder = TextEditingController();

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: BaseAppBar(
        isCentered: true,
        title: 'Add Card Details',
        titleColor: Color(0xFF97080E),
        iconTheme: Color(0xFF97080E),
        automaticallyImplyLeading: true,
        widgets: [],
        appBar: AppBar(),
      ),
      body: SingleChildScrollView(
        child: Container(
          padding: EdgeInsets.fromLTRB(20, 30, 20, 20),
          child: Column(
            children: [
              CustomTextField(
                  textEditingController: cardHolder,
                  isNumber: false,
                  hinttext: 'Card Holder',
                  borderColor: Color(0xFF97080E),
                  isPassword: false),
              SizedBox(
                height: 20,
              ),
              CustomTextField(
                  textEditingController: cardnumber,
                  isNumber: true,
                  hinttext: 'Debit Card Number',
                  borderColor: Color(0xFF97080E),
                  isPassword: false),
              SizedBox(
                height: 20,
              ),
              Row(
                mainAxisAlignment: MainAxisAlignment.spaceAround,
                children: [
                  SizedBox(
                      width: 150,
                      child: CustomTextField(
                          textEditingController: expMonth,
                          isNumber: true,
                          hinttext: 'Expire Month',
                          borderColor: Color(0xFF97080E),
                          isPassword: false)),
                  SizedBox(
                    width: 150,
                    child: CustomTextField(
                        textEditingController: expYear,
                        isNumber: true,
                        hinttext: 'Expire Year',
                        borderColor: Color(0xFF97080E),
                        isPassword: false),
                  ),
                ],
              ),
              SizedBox(
                height: 20,
              ),
              Padding(
                padding: const EdgeInsets.only(left: 16.0),
                child: Row(
                  children: [
                    SizedBox(
                      width: 150,
                      child: CustomTextField(
                        textEditingController: cvv,
                        hinttext: 'CVV',
                        isNumber: true,
                        isPassword: false,
                        borderColor: Color(0xFF97080E),
                      ),
                    ),
                  ],
                ),
              ),
              SizedBox(height: 40),
              SizedBox(
                  width: 150,
                  child: MyButton(
                      text: 'Save',
                      color: Color(0xFF97080E),
                      onPressed: () {
                        print(cardnumber.text);
                        print(expMonth.text);
                        print(expYear.text);
                        print(cvv.text);
                        print(cardHolder.text);
                        addCard();
                      }))
            ],
          ),
        ),
      ),
    );
  }

  addCard() async {
    SharedPreferences prefs = await SharedPreferences.getInstance();
    String token = prefs.getString('token');
    http.Response response = await http.post('${ApiProvider.addCard}',
        headers: {HttpHeaders.authorizationHeader:'Bearer $token'},
        body: {
      "card_number": cardnumber.text,
      "exp_month": expMonth.text,
      "exp_year": expYear.text,
      "cvc": cvv.text,
      "card_holder": cardHolder.text
    });
    if(response.statusCode == 200){
      print(response.body);
      Navigator.pop(context);
    }
    else{
      print(response.body);
    }
  }
}
