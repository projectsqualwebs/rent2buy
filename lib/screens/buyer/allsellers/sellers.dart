import 'dart:convert';
import 'package:flutter/material.dart';
import 'package:rent2buy/di/service_locator.dart';
import 'package:rent2buy/models/userinfo.dart';
import 'package:rent2buy/networking/network_responselist.dart';
import 'package:rent2buy/networking/network_status.dart';
import 'package:rent2buy/screens/buyer/allsellers/sellers_viewmodel.dart';
import 'package:rent2buy/screens/common/progress_view.dart';
import 'package:rent2buy/services/apiProvider.dart';
import 'package:rent2buy/utils/strings.dart';
import 'package:rent2buy/utils/utils.dart';
import 'package:rent2buy/widgets/shared_widgets.dart';
import 'package:smooth_star_rating/smooth_star_rating.dart';
import 'package:http/http.dart' as http;

class Sellers extends StatefulWidget {
  final SellersViewModel viewModel = serviceLocator<SellersViewModel>();

  @override
  _SellersState createState() => _SellersState();
}

class _SellersState extends State<Sellers> {
  List<Userinfo> sellers = List();

  @override
  void initState() {
    widget.viewModel.getSellers();
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
        appBar: BaseAppBar(
          title: 'Sellers',
          automaticallyImplyLeading: true,
          titleColor: Color(0xFF97080E),
          iconTheme: Color(0xFF97080E),
          widgets: [],
          appBar: AppBar(),
        ),
        body: ValueListenableBuilder(
            valueListenable: widget.viewModel.sellersValue,
            builder: (context, value, _) {
              var response = value as NetworkResponseList<Userinfo>;
              switch (response.status) {
                case NetworkStatus.LOADING:
                  return CircularProgress();
                  break;
                case NetworkStatus.ERROR:
                  return Utils.getEmptyView(Strings.somethingWentWrong);
                case NetworkStatus.SUCCESS:
                  sellers.clear();
                  sellers.addAll(response.response);

                  return Container(
                    padding: EdgeInsets.symmetric(vertical: 10, horizontal: 10),
                    child: ListView.builder(
                        shrinkWrap: true,
                        scrollDirection: Axis.vertical,
                        itemCount: sellers.length,
                        itemBuilder: (BuildContext context, int index) {
                          return _buildItem(sellers[index]);
                        }),
                  );
              }
              return Utils.getEmptyView(Strings.noSellersFound);
            }));
  }

  Widget _buildItem(Userinfo seller) {
    return Card(
      elevation: 8,
      color: Colors.yellow[50],
      child: Row(
        children: [
          Flexible(
            flex: 1,
            child: Padding(
              padding: const EdgeInsets.all(8.0),
              child: Image.network(
                'https://www.clipartmax.com/png/middle/34-340027_user-login-man-human-body-mobile-person-comments-person-icon-png.png',
                width: 64,
                height: 64,
              ),
            ),
          ),
          Flexible(
            flex: 2,
            child: Padding(
              padding: const EdgeInsets.all(16.0),
              child: Column(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                  children: [
                    Text(seller.firstName,
                        style: TextStyle(
                            color: Color(0xFF97080E),
                            fontWeight: FontWeight.w700,
                            fontSize: 24)),
                    Row(
                      mainAxisAlignment: MainAxisAlignment.start,
                      children: [
                        Icon(
                          Icons.location_on,
                          color: Color(0xFFF7A74D),
                        ),
                        Flexible(
                          flex: 1,
                          child: Text(
                            seller.address.first.location,
                            overflow: TextOverflow.ellipsis,
                            style: TextStyle(color: Colors.grey, fontSize: 16),
                          ),
                        )
                      ],
                    ),
                    SmoothStarRating(
                      allowHalfRating: false,
                      starCount: 5,
                      rating: 0,
                      color: Color(0xFF97080E),
                      borderColor: Color(0xFF97080E),
                    )
                  ]),
            ),
          )
        ],
      ),
    );
  }
}
