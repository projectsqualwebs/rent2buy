import 'package:flutter/material.dart';
import 'package:rent2buy/di/service_locator.dart';
import 'package:rent2buy/models/common_repository.dart';
import 'package:rent2buy/models/payment_card.dart';
import 'package:rent2buy/models/userinfo.dart';
import 'package:rent2buy/networking/network_responselist.dart';
import 'package:rent2buy/utils/base_change_notifier.dart';

class SellersViewModel extends BaseChangeNotifier {
  final CommonRepository _commonRepository = serviceLocator<CommonRepository>();

  ValueNotifier<NetworkResponseList<Userinfo>> sellersValue =
      ValueNotifier(NetworkResponseList.idle());

  void getSellers() {
    sellersValue.value = NetworkResponseList.loading();
    _commonRepository.getSellers().then((response) {
      sellersValue.value = NetworkResponseList.success(response.response);
    }).catchError((e) {
      sellersValue.value = NetworkResponseList.error(parseError(e));
    });
  }
}
