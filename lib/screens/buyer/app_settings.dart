import 'package:flutter/material.dart';
import 'package:rent2buy/screens/common/common_setting_screen.dart';
import 'package:rent2buy/screens/common/feedback/feedback_screen.dart';
import 'dart:math' as math;
import 'package:rent2buy/widgets/shared_widgets.dart';

class AppSettings extends StatefulWidget {
  @override
  _AppSettingsState createState() => _AppSettingsState();
}

class _AppSettingsState extends State<AppSettings> {
  final _bigFont = const TextStyle(fontSize: 20, fontWeight: FontWeight.w600);
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: BaseAppBar(
        title: 'App Settings',
        titleColor: Color(0xFF97080E),
        iconTheme: Color(0xFF97080E),
        automaticallyImplyLeading: true,
        widgets: [],
        appBar: AppBar(),
      ),
      body: Container(
        padding: EdgeInsets.only(top: 20),
        child: ListView(
          children: [
            ListTile(
              contentPadding: EdgeInsets.symmetric(horizontal: 50),
              title: Text(
                'About',
                style: _bigFont,
              ),
              subtitle: Text('Rent and sell items at freat prices'),
              onTap: () {
                Navigator.pushNamed(context, CommonScreen.routeName,
                    arguments: "About");
              },
            ),
            Divider(
              color: Color(0xFFF7A74D),
              height: 5.0,
              thickness: 3,
            ),
            ListTile(
              contentPadding: EdgeInsets.symmetric(horizontal: 50),
              title: Text(
                'Share this',
                style: _bigFont,
              ),
              onTap: () {},
              leading: Transform(
                  alignment: Alignment.center,
                  transform: Matrix4.rotationY(math.pi),
                  child: Icon(
                    Icons.reply,
                    color: Colors.black,
                  )),
            ),
            Divider(color: Color(0xFFF7A74D), height: 5.0, thickness: 3),
            ListTile(
              contentPadding: EdgeInsets.symmetric(horizontal: 50),
              title: Text(
                'Please Rate us',
                style: _bigFont,
              ),
              onTap: () {},
              leading: Icon(
                Icons.star,
                color: Colors.black,
              ),
            ),
            Divider(
              color: Color(0xFFF7A74D),
              height: 5.0,
              thickness: 3,
            ),
            ListTile(
              contentPadding: EdgeInsets.symmetric(horizontal: 50),
              title: Text(
                'Feedback/Contact Us',
                style: _bigFont,
              ),
              subtitle: Text('Got any queries? We are here to help you!'),
              onTap: () {
                Navigator.pushNamed(context, FeedbackScreen.routeName);
              },
            ),
            Divider(
              color: Color(0xFFF7A74D),
              height: 5.0,
              thickness: 3,
            ),
            ListTile(
              contentPadding: EdgeInsets.symmetric(horizontal: 50),
              title: Text(
                'Privacy Policy',
                style: _bigFont,
              ),
              onTap: () {
                Navigator.pushNamed(context, CommonScreen.routeName,
                    arguments: "Privacy Policy");
              },
            ),
            Divider(
              color: Color(0xFFF7A74D),
              height: 5.0,
              thickness: 3,
            ),
            ListTile(
              contentPadding: EdgeInsets.symmetric(horizontal: 50),
              title: Text(
                'Terms and Conditions',
                style: _bigFont,
              ),
              onTap: () {
                Navigator.pushNamed(context, CommonScreen.routeName,
                    arguments: "Terms and Conditions");
              },
            ),
            Divider(
              color: Color(0xFFF7A74D),
              height: 5.0,
              thickness: 3,
            ),
          ],
        ),
      ),
    );
  }
}
