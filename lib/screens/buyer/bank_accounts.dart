import 'dart:io';

import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:rent2buy/services/apiProvider.dart';
import 'package:rent2buy/widgets/shared_widgets.dart';
import 'package:http/http.dart'as http;
import 'dart:convert';
import 'package:shared_preferences/shared_preferences.dart';

class BankAccounts extends StatefulWidget {

  @override
  _BankAccountsState createState() => _BankAccountsState();
}

class _BankAccountsState extends State<BankAccounts> {
  List<dynamic> cards = List();

  getCards() async {
    SharedPreferences prefs = await SharedPreferences.getInstance();
    String token = prefs.getString('token');
    http.Response response = await http.get('${ApiProvider.addCard}',
      headers: {HttpHeaders.authorizationHeader:'Bearer $token'},);
    print(response.body);
    setState(() {
      cards.add(jsonDecode(response.body)['response']['cards']);
    });

    print(cards[0][0]['card_number']);
  }
  @override
  void initState() {
    getCards();
    super.initState();
  }
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar:  BaseAppBar(
        isCentered: true,
        title: 'Bank Account Details',
        titleColor: Color(0xFF97080E),
        iconTheme: Color(0xFF97080E),
        automaticallyImplyLeading: true,
        widgets: [],
        appBar: AppBar(),
      ),
      body:Container(
        padding:EdgeInsets.fromLTRB(20, 30, 20, 20),
        child: Column(
          children:[
            Row(
              children: [
                Expanded(
                  child: MyButton(
                    text: 'Add Debit Card',
                    onPressed: (){
                      Navigator.pushNamed(context, '/addCardDetail');
                    },
                    color: Color(0xFF97080E),
                  ),
                ),
              ],
            ),
            SizedBox(height: 20),
            ListView.builder(itemCount: cards.length,
                shrinkWrap: true,
                itemBuilder: (BuildContext context,int index){
                  return Card(
                    shape: RoundedRectangleBorder(borderRadius: BorderRadius.circular(16)),
                    color: Colors.yellow.shade800,
                    child: Container(
                      height: 100,
                      child: ListTile(
                        contentPadding: EdgeInsets.only(top:12,left: 12,right: 12),
                        leading: Padding(
                          padding: const EdgeInsets.only(left:6.0,top: 8),
                          child: Image.asset('assets/mastercard.png',fit: BoxFit.fill,),
                        ),
                        title: Text(cards[index][index]["card_holder"],style: TextStyle(color: Colors.black,fontSize: 24,fontWeight: FontWeight.w600),),
                        subtitle: Padding(
                          padding: const EdgeInsets.only(top:16.0),
                          child: Text(cards[index][index]["card_number"] ?? 'null'),
                        ),
                        trailing: Icon(Icons.check_circle_outline,size: 36,color: Colors.green.shade700,),
                      ),
                    ),
                  );
            })
          ]
        ),
      )
    );
  }
}
