import 'package:flutter/material.dart';
import 'package:rent2buy/di/service_locator.dart';
import 'package:rent2buy/models/cart.dart';
import 'package:rent2buy/models/cart_response.dart';
import 'package:rent2buy/models/common_repository.dart';
import 'package:rent2buy/models/delivery_type.dart';
import 'package:rent2buy/models/payment_card.dart';
import 'package:rent2buy/networking/network_response.dart';
import 'package:rent2buy/networking/network_responselist.dart';
import 'package:rent2buy/networking/repository/payment_repository.dart';
import 'package:rent2buy/utils/base_change_notifier.dart';

class CartViewModel extends BaseChangeNotifier {
  ValueNotifier<NetworkResponse<Cart>> cartItemsValue =
      ValueNotifier(NetworkResponse.idle());
  ValueNotifier<NetworkResponse<String>> removeItemValue =
      ValueNotifier(NetworkResponse.idle());
  ValueNotifier<NetworkResponse<String>> clearValue =
      ValueNotifier(NetworkResponse.idle());
  ValueNotifier<NetworkResponse<CartResponse>> qtyValue =
      ValueNotifier(NetworkResponse.idle());


  ValueNotifier<NetworkResponseList<PaymentCard>> cardsValue =
      ValueNotifier(NetworkResponseList.idle());

  ValueNotifier<NetworkResponseList<DeliveryType>> deliveryTypes =
      ValueNotifier(NetworkResponseList.idle());

  CommonRepository _commonRepository = serviceLocator<CommonRepository>();
  PaymentRepository _paymentRepository = serviceLocator<PaymentRepository>();

  void getCartItems(String cartId) {
    cartItemsValue.value = NetworkResponse.loading();
    _commonRepository.getCartItems(cartId).then((response) {
      cartItemsValue.value = NetworkResponse.success(response.response);
    }).catchError((e) {
      cartItemsValue.value = NetworkResponse.error(parseError(e));
    });
  }

  void removeCartItem(String cartItemId) {
    removeItemValue.value = NetworkResponse.loading();
    _commonRepository.removeCartItem(cartItemId).then((response) {
      removeItemValue.value = NetworkResponse.success(response.message);
    }).catchError((e) {
      removeItemValue.value = NetworkResponse.error(parseError(e));
    });
  }

  void getDeliveryTypes() {
    deliveryTypes.value = NetworkResponseList.loading();
    try {
      _commonRepository
          .getDeliveryTypes()
          .then((value) =>
              deliveryTypes.value = NetworkResponseList.success(value.response))
          .catchError((e) {
        print(e);
        deliveryTypes.value = NetworkResponseList.error(parseError(e));
      });
    } catch (exception) {
      print(exception);
      deliveryTypes.value = NetworkResponseList.error(parseError(exception));
    }
  }

  void clearCart(String cartItemId) {
    clearValue.value = NetworkResponse.loading();
    _commonRepository.clearCart(cartItemId).then((response) {
      clearValue.value = NetworkResponse.success(response.message);
    }).catchError((e) {
      clearValue.value = NetworkResponse.error(parseError(e));
    });
  }

  void addToCart(Map<String, dynamic> params, String cartId) {
    qtyValue.value = NetworkResponse.loading();
    _commonRepository.addToCart(params, cartId).then((response) {
      qtyValue.value =
          NetworkResponse.successWithMsg(response.response, response.message);
    }).catchError((e) {
      qtyValue.value = NetworkResponse.error(parseError(e));
    });
  }

 
}
