import 'dart:convert';
import 'dart:io';
import 'package:flutter/material.dart';
import 'package:fluttertoast/fluttertoast.dart';
import 'package:rent2buy/di/service_locator.dart';
import 'package:rent2buy/models/User/address.dart';
import 'package:rent2buy/models/User/makeOrder.dart';
import 'package:rent2buy/models/cart.dart';
import 'package:rent2buy/models/cart_item.dart';
import 'package:rent2buy/models/payment_card.dart';
import 'package:rent2buy/models/product.dart';
import 'package:rent2buy/networking/network_response.dart';
import 'package:rent2buy/networking/network_responselist.dart';
import 'package:rent2buy/networking/network_status.dart';
import 'package:rent2buy/screens/buyer/cart/cart_viewmodel.dart';
import 'package:rent2buy/screens/buyer/chat_screen.dart';
import 'package:rent2buy/screens/common/error_screen.dart';
import 'package:rent2buy/screens/common/progress_view.dart';
import 'package:rent2buy/screens/custom/payment_card.dart';
import 'package:rent2buy/screens/custom/rounded_border_spinner.dart';
import 'package:rent2buy/screens/payment/add_card.dart';
import 'package:rent2buy/screens/payment/payment_screen.dart';
import 'package:rent2buy/screens/payment/pop_over.dart';
import 'package:rent2buy/utils/constants.dart' as Constants;
import 'package:rent2buy/services/apiProvider.dart';
import 'package:rent2buy/utils/cart_manager.dart';
import 'package:rent2buy/utils/colors.dart';
import 'package:rent2buy/utils/constants.dart';
import 'package:rent2buy/utils/data_manager.dart';
import 'package:rent2buy/utils/dimens.dart';
import 'package:rent2buy/utils/strings.dart';
import 'package:rent2buy/utils/utils.dart';
import 'package:rent2buy/widgets/shared_widgets.dart';
import 'package:http/http.dart' as http;
import 'package:shared_preferences/shared_preferences.dart';

class MyCart extends StatefulWidget {
  final CartViewModel viewModel = serviceLocator<CartViewModel>();
  final CartManager cartManager = serviceLocator<CartManager>();
  final DataManager dataManager = serviceLocator<DataManager>();

  String cart_id;
  String product_id;

  static var routeName = '/my_cart';

  MyCart({Key key, this.cart_id, this.product_id}) : super(key: key);

  @override
  _MyCartState createState() => _MyCartState();
}

class _MyCartState extends State<MyCart> {
  List<dynamic> cards = List<dynamic>();
  MakeOrder makeOrder = MakeOrder();

  List<CartItem> cartItems = List();
  List<PaymentCard> paymentCards = List();
  Map<String, dynamic> item = Map();
  Map<String, dynamic> map = Map();
  Map<String, dynamic> paramsAddress = Map();
  GlobalKey<ScaffoldState> _scaffoldState = GlobalKey<ScaffoldState>();
  GlobalKey<AnimatedListState> _listKey = GlobalKey<AnimatedListState>();
  String delivery_type_id;
  String card_id;
  String name;
  String seller_name;
  String price;
  String delivery_fee;
  String taxes;
  String total;
  int removedIndex = 0;
  double cartTotal = 0;
  String _currentQty;
  String currentCartUniqueId, currentCartId;
  List<String> _quantities = ['1', '2', '3', '4', '5'];
  final _textStyle = const TextStyle(
      fontWeight: FontWeight.w600, fontSize: 16, color: Color(0xff717171));

  String deliveryTypeID;
  String selfPickUpID;

  // getItem() async {
  //   http.Response response =
  //       await http.get('${ApiProvider.getSingleItem}' + widget.product_id);
  //   setState(() {
  //     item = jsonDecode(response.body)['response'];
  //     name = item['name'];
  //     seller_name = item['seller']['first_name'];
  //     price = item['purchase_actions'][0]['price'].toString();
  //     delivery_type_id = item['delivery_type'][0]['_id'];
  //     print(response.body);
  //   });
  // }

  // getCartItem() async {
  //   SharedPreferences prefs = await SharedPreferences.getInstance();
  //   String token = prefs.getString('token');
  //   http.Response response = await http.get(
  //       '${ApiProvider.getCartItem}' + widget.cart_id,
  //       headers: {HttpHeaders.authorizationHeader: 'Bearer $token'});
  //   setState(() {
  //     map = jsonDecode(response.body)['response'];
  //     delivery_fee = map['delivery_charge'].toString();
  //     taxes = map['tax'].toString();
  //     total = map['order_total_amount'].toString();
  //   });
  // }

  // getCard() async {
  //   SharedPreferences prefs = await SharedPreferences.getInstance();
  //   String token = prefs.getString('token');
  //   http.Response response = await http.get('${ApiProvider.addCard}',
  //       headers: {HttpHeaders.authorizationHeader: 'Bearer $token'});
  //   cards = jsonDecode(response.body)['response']['cards'];
  //   card_id = cards[0]['id'];
  //   print(card_id);
  // }

  removeCartItem() async {
    SharedPreferences prefs = await SharedPreferences.getInstance();
    String token = prefs.getString('token');
    http.Response response = await http.delete(
        '${ApiProvider.getCartItem}' + widget.product_id,
        headers: {HttpHeaders.authorizationHeader: 'Bearer $token'});
    if (response.statusCode == 200) {
      Navigator.pop(context);
    }
    print(response.body.toString());
  }

  // placeOrder() async {
  //   SharedPreferences prefs = await SharedPreferences.getInstance();
  //   String token = prefs.getString('token');
  //   address.zip_code = 111111.toString();
  //   makeOrder = MakeOrder(
  //       delivery_type: delivery_type_id,
  //       address: address,
  //       stripe_card_id: card_id);
  //   http.Response response = await http.post('${ApiProvider.order}',
  //       body: jsonEncode(makeOrder.toJson()),
  //       headers: {
  //         HttpHeaders.authorizationHeader: 'Bearer $token',
  //         "Content-Type": "application/json"
  //       });
  //   if (response.statusCode == 200) {
  //     print(response.body);
  //     Navigator.pushNamed(context, '/confirmed_screen');
  //   }
  // }

  createThread() async {
    SharedPreferences prefs = await SharedPreferences.getInstance();
    String token = prefs.getString('token');
    http.Response response = await http.post('${ApiProvider.thread}',
        headers: {HttpHeaders.authorizationHeader: 'Bearer $token'},
        body: {'listing_id': widget.product_id});
    String chat_id = jsonDecode(response.body)["response"]["_id"];
    print(chat_id);
    Navigator.push(context,
        MaterialPageRoute(builder: (context) => ChatScreen(id: chat_id)));
  }

  @override
  void initState() {
    // getItem();
    // getCard();
    _currentQty = _quantities.first;
    super.initState();
    getCart();
    widget.viewModel.removeItemValue.addListener(() {
      var response = widget.viewModel.removeItemValue.value;
      if (response.status == NetworkStatus.ERROR) {
        _scaffoldState.currentState.showSnackBar(showMessage(response.message));
      } else if (response.status == NetworkStatus.SUCCESS) {
        _scaffoldState.currentState
            .showSnackBar(showMessage(response.response));
        CartItem removedItem = cartItems.removeAt(removedIndex);
        _listKey.currentState.removeItem(removedIndex, (context, animation) {
          return _buildItem(removedItem, removedIndex, animation);
        });
        getCart();
      }
    });

    widget.viewModel.clearValue.addListener(() {
      var response = widget.viewModel.clearValue.value;
      if (response.status == NetworkStatus.ERROR) {
        _scaffoldState.currentState.showSnackBar(showMessage(response.message));
      } else if (response.status == NetworkStatus.SUCCESS) {
        _scaffoldState.currentState
            .showSnackBar(showMessage(response.response));

        for (int i = 0; i < cartItems.length; i++) {
          CartItem removedItem = cartItems.removeAt(i);
          _listKey.currentState.removeItem(i, (context, animation) {
            return _buildItem(removedItem, i, animation);
          });
        }

        getCart();
      }
    });
    getDeliveryTypes();
  }

  getDeliveryTypes() {
    widget.viewModel.getDeliveryTypes();
    widget.viewModel.deliveryTypes.addListener(() {
      var value = widget.viewModel.deliveryTypes.value;
      if (value.status == NetworkStatus.SUCCESS && value.response.isNotEmpty) {
        deliveryTypeID = value.response.first.id;
        selfPickUpID = value.response.last.id;
        print(deliveryTypeID);
      }
    });
  }

  SnackBar showMessage(String message) {
    return SnackBar(content: Text(message));
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      key: _scaffoldState,
      resizeToAvoidBottomInset: false,
      // appBar: BaseAppBar(
      //   title: 'My Cart',
      //   automaticallyImplyLeading: true,
      //   titleColor: Color(0xFF97080E),
      //   iconTheme: Color(0xFF97080E),
      //   widgets: [
      //     IconButton(
      //         icon: Icon(Icons.delete),
      //         onPressed: () {
      //           showAlertDialog(
      //               context,
      //               Strings.msgClearCart,
      //               Strings.msgAreYouSureClear,
      //               Strings.cancel,
      //               Strings.msgClearCart, () {
      //             Navigator.of(context, rootNavigator: true).pop('dialog');
      //             widget.viewModel.clearCart(currentCartId);
      //           });
      //         })
      //   ],
      //   appBar: AppBar(),
      // ),
      body: NestedScrollView(
        headerSliverBuilder: (context, innerBoxIsScrolled) {
          return <Widget>[
            new SliverAppBar(
              pinned: true,
              title: Text(
                "My Cart",
                style: TextStyle(color: colorAccent),
              ),
              actions: [
                IconButton(
                    icon: Icon(Icons.delete),
                    onPressed: () {
                      showAlertDialog(
                          context,
                          Strings.msgClearCart,
                          Strings.msgAreYouSureClear,
                          Strings.cancel,
                          Strings.msgClearCart, () {
                        Navigator.of(context, rootNavigator: true)
                            .pop('dialog');
                        widget.viewModel.clearCart(currentCartId);
                      });
                    })
              ],
            ),
          ];
        },
        body: ValueListenableBuilder(
          valueListenable: widget.viewModel.cartItemsValue,
          builder: (context, value, child) {
            var response = value as NetworkResponse<Cart>;
            switch (response.status) {
              case NetworkStatus.LOADING:
                return CircularProgress();
              case NetworkStatus.ERROR:
                return ErrorScreen(response.message);
              case NetworkStatus.SUCCESS:
                var cart = response.response;
                WidgetsBinding.instance.addPostFrameCallback((_) {
                  setState(() {
                    cartTotal = cart.orderTotalAmount;
                    currentCartId =
                        cart.items.isNotEmpty ? cart.items.first.cart.id : "";

                    widget.cartManager.saveCartListingId(currentCartId);
                  });
                  cartItems.clear();
                  cartItems.addAll(cart.items);
                  for (int offset = 0; offset < cart.items.length; offset++) {
                    _listKey.currentState.insertItem(offset);
                  }
                });
                return AnimatedCrossFade(
                  duration: Duration(milliseconds: 500),
                  crossFadeState: cartItems.isEmpty
                      ? CrossFadeState.showFirst
                      : CrossFadeState.showSecond,
                  firstChild: Utils.getEmptyView(Strings.cartIsEmpty),
                  secondChild: new Column(
                    mainAxisSize: MainAxisSize.min,
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: [
                      Expanded(
                        child: AnimatedList(
                            key: _listKey,
                            primary: false,
                            shrinkWrap: true,
                            physics: ClampingScrollPhysics(),
                            scrollDirection: Axis.vertical,
                            initialItemCount: cartItems.length,
                            itemBuilder: (context, position, animation) {
                              if (position < cartItems.length) {
                                CartItem cartItem = cartItems[position];
                                return _buildItem(
                                    cartItem, position, animation);
                              }
                              return Container();
                            }),
                      ),
                      SizedBox(height: 30),
                      Container(
                        padding: EdgeInsets.all(padding_large),
                        child: Column(
                          mainAxisSize: MainAxisSize.min,
                          children: [
                            Text(
                              'PRICE DETAILS',
                              style:
                                  TextStyle(fontSize: 18, color: Colors.grey),
                            ),
                            SizedBox(
                              height: 10,
                            ),
                            Row(
                              mainAxisAlignment: MainAxisAlignment.spaceBetween,
                              children: [
                                Text(Utils.getCartNoOfItem(cart.items.length),
                                    style: TextStyle(fontSize: 20)),
                                Text(
                                  Utils.getPriceInDollar(cart.subTotal),
                                  style: TextStyle(
                                      fontSize: 20, color: Colors.grey),
                                ),
                              ],
                            ),
                            Row(
                              mainAxisAlignment: MainAxisAlignment.spaceBetween,
                              children: [
                                Text('Taxes', style: TextStyle(fontSize: 20)),
                                Text(
                                  Utils.getPriceInDollar(cart.tax),
                                  style: TextStyle(
                                      fontSize: 20, color: Colors.grey),
                                ),
                              ],
                            ),
                            SizedBox(
                              height: 10,
                            ),
                            Row(
                              mainAxisAlignment: MainAxisAlignment.spaceBetween,
                              children: [
                                Text('Total Amount',
                                    style: TextStyle(fontSize: 20)),
                                Text(
                                  Utils.getPriceInDollar(cart.orderTotalAmount),
                                  style: TextStyle(fontSize: 20),
                                ),
                              ],
                            ),
//                             Spacer(),
//                             FlatButton(
//                                 onPressed: () {
//                                   createThread();
// //                Navigator.pushNamed(context, '/chats');
//                                 },
//                                 child: Text(
//                                   'Chat with Dealer',
//                                   style: TextStyle(
//                                       color: Color(0xFF97080E), fontSize: 18),
//                                 )),
//                             Spacer()
                          ],
                        ),
                      )
                    ],
                  ),
                );
            }
            return Utils.getEmptyView(Strings.cartIsEmpty);
          },
        ),
      ),
      bottomNavigationBar: Visibility(
        visible: cartItems.isNotEmpty,
        child: Container(
          height: 80,
          child: Column(
            children: [
              Divider(
                color: Color(0xFFF7A74D),
                height: 5.0,
                thickness: 3,
              ),
              Padding(
                padding:
                    const EdgeInsets.symmetric(vertical: 8.0, horizontal: 20),
                child: Row(
                  mainAxisAlignment: MainAxisAlignment.spaceAround,
                  children: [
                    Text(
                      Utils.getPriceInDollar(cartTotal),
                      style: TextStyle(fontSize: 32, color: Color(0xFF97080E)),
                    ),
                    MyButton(
                      text: 'Place Order',
                      color: Color(0xFF97080E),
                      onPressed: () async {
                        String token = await widget.dataManager.loadToken();
                        if (token == null) {
                          Fluttertoast.showToast(
                              msg: 'You have to login first.');
                          Navigator.pushNamed(context, '/login');
                        } else {
                          Navigator.pushNamed(context, PaymentScreen.routeName);
                          // Map<String, dynamic> params = Map();
                          // params[deliveryType] = deliveryTypeID;
                          // params[address] = paramsAddress;
                          // params[stripeCardId] = card_id;
                          // widget.viewModel.placeOrder(params);
                        }
                      },
                    )
                  ],
                ),
              ),
              Divider(
                color: Color(0xFFF7A74D),
                height: 5.0,
                thickness: 3,
              ),
            ],
          ),
        ),
      ),
    );
  }

  void getCart() async {
    currentCartUniqueId = await widget.cartManager.getCartId();
    widget.viewModel.getCartItems(currentCartUniqueId);
  }

  showAlertDialog(BuildContext context, String title, String message,
      String btnCancel, String btnOk, VoidCallback pCallBack) {
    // set up the buttons
    Widget cancelButton = FlatButton(
      child: Text("Cancel"),
      onPressed: () {
        Navigator.of(context, rootNavigator: true).pop('dialog');
      },
    );
    Widget continueButton = FlatButton(
      child: Text("Continue"),
      onPressed: pCallBack,
    );

    // set up the AlertDialog
    AlertDialog alert = AlertDialog(
      title: Text(title),
      content: Text(message),
      actions: [
        cancelButton,
        continueButton,
      ],
    );

    // show the dialog
    newMethod(context, alert);
  }

  Future newMethod(BuildContext context, AlertDialog alert) {
    return showDialog(
      context: context,
      builder: (BuildContext context) {
        return alert;
      },
    );
  }

  Widget _buildItem(CartItem cartItem, int position, Animation animation) {
    try {
      return Container(
        height: 196,
        padding: EdgeInsets.symmetric(horizontal: 12),
        child: Row(
          mainAxisAlignment: MainAxisAlignment.spaceEvenly,
          children: [
            Expanded(
              flex: 1,
              child: Column(
                mainAxisSize: MainAxisSize.min,
                crossAxisAlignment: CrossAxisAlignment.start,
                mainAxisAlignment: MainAxisAlignment.start,
                children: [
                  Text(
                    cartItem.listing.name,
                    style: TextStyle(fontWeight: FontWeight.w600, fontSize: 24),
                    overflow: TextOverflow.ellipsis,
                  ),
                  Text(
                    cartItem.listing.description,
                    style: TextStyle(fontWeight: FontWeight.w600, fontSize: 24),
                    overflow: TextOverflow.ellipsis,
                  ),
                  Text(
                    'Seller: ' +
                        Utils.getFullName(cartItem.listing.seller.firstName,
                            cartItem.listing.seller.lastName),
                    style: _textStyle,
                  ),
                  SizedBox(
                    height: 20,
                  ),
                  Text(
                    Utils.getPriceInDollar(cartItem.price.toDouble()),
                    style: TextStyle(fontSize: 28),
                  ),
                  Container(
                    alignment: Alignment.bottomLeft,
                    child: FlatButton.icon(
                      icon: Icon(Icons.delete),
                      label: Text(
                        'Remove Item',
                        style: TextStyle(fontSize: 18),
                      ),
                      onPressed: () {
                        showAlertDialog(
                            context,
                            Strings.txtRemoveCartItem,
                            Strings.msgDeleteCartItem,
                            Strings.cancel,
                            Strings.remove, () {
                          removedIndex = position;
                          widget.viewModel.removeCartItem(cartItem.sId);
                          Navigator.of(context, rootNavigator: true)
                              .pop('dialog');
                        });
                      },
                    ),
                  ),
                ],
              ),
            ),
            Expanded(
              flex: 1,
              child: Column(
                mainAxisSize: MainAxisSize.min,
                children: [
                  Image.network(
                    cartItem.listing.images.path.isNotEmpty
                        ? cartItem.listing.images.path[0]
                        : "",
                    width: 120,
                    height: 96,
                  ),
                  Visibility(
                    visible: cartItem.actionType == ACTION_SELL,
                    child: Container(
                      height: 28,
                      padding: EdgeInsets.symmetric(horizontal: padding_large),
                      decoration: BoxDecoration(
                          borderRadius: BorderRadius.circular(default_radius),
                          border: Border.all(
                              color: colorBorder,
                              style: BorderStyle.solid,
                              width: defaultStrokeWidth)),
                      child: DropdownButtonHideUnderline(
                        child: new DropdownButton<String>(
                          value: cartItem.quantity,
                          items: _quantities.map((x) {
                            return new DropdownMenuItem<String>(
                              value: x,
                              child: Text(Utils.getQty(x)),
                            );
                          }).toList(),
                          onChanged: (value) {
                            setState(() {
                              cartItem.quantity = value;
                              addToCart(cartItem.listing, value);
                            });
                          },
                        ),
                      ),
                    ),
                  ),
                ],
              ),
            )
          ],
        ),
      );
    } catch (exceptoin) {}
  }

  addToCart(Product product, String value) async {
    Map<String, Object> params = Map();
    params[Constants.listingId] = product.id;
    params[Constants.actionType] = Constants.ACTION_SELL;
    params[Constants.quanity] = value;

    widget.viewModel.addToCart(params, currentCartUniqueId);
  }

  void showPaymentCards() {
    showModalBottomSheet<int>(
      backgroundColor: Colors.transparent,
      context: context,
      builder: (context) {
        return Popover(
            child: paymentCards.isEmpty
                ? Center(
                    child: Text("No Payment Methods"),
                  )
                : ListView.builder(
                    scrollDirection: Axis.vertical,
                    shrinkWrap: true,
                    itemCount: paymentCards.length,
                    itemBuilder: (context, index) {
                      return _buildCardItem(paymentCards[index]);
                    }));
      },
    );
  }

  Widget _buildCardItem(PaymentCard item) {
    return Container(
      child: Row(
        children: [
          Radio(
            value: item.checked,
          ),
          Image(
            height: 24.0,
            width: 24.0,
            image: AssetImage('icons/visa.png'),
          ),
          Flexible(
            flex: 1,
            child: Column(
              children: [
                Text(item.cardHolder),
                Text(item.cardNumber),
              ],
            ),
          ),
        ],
      ),
    );
  }
}
