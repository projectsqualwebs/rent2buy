import 'dart:convert';
import 'dart:io';

import 'package:flutter/material.dart';
import 'package:rent2buy/services/apiProvider.dart';
import 'package:rent2buy/widgets/shared_widgets.dart';
import 'package:http/http.dart' as http;
import 'package:shared_preferences/shared_preferences.dart';

class CartItems extends StatefulWidget {
  @override
  _CartItemsState createState() => _CartItemsState();
}

class _CartItemsState extends State<CartItems> {
  List<dynamic> cartItems = List();
  int count;
  getCartCount()async{
    SharedPreferences prefs = await SharedPreferences.getInstance();
    String token = prefs.getString('token');
    String cart_id = prefs.getString('cart_unique_id');
    http.Response response = await http.get('${ApiProvider.getCartCount}'+'$cart_id',
        headers: {HttpHeaders.authorizationHeader: 'Bearer $token'});
    setState(() {
      count = jsonDecode(response.body)['response']['cart_items'];
    });

  }

  getCartItems() async{
    SharedPreferences prefs = await SharedPreferences.getInstance();
    String token = prefs.getString('token');
    String cart_id = prefs.getString('cart_unique_id');
    http.Response response = await http.get('${ApiProvider.getCartItem}'+'$cart_id',
        headers: {HttpHeaders.authorizationHeader: 'Bearer $token'});
    if(response.statusCode == 200){
      setState(() {
        cartItems.add(jsonDecode(response.body)['response']['items']);
        //print(cartItems[0][2]['listing']['name']);
      });
    }
  }
  @override
  void initState() {
    super.initState();
    getCartCount();
    getCartItems();

  }
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: BaseAppBar(
        title: 'My Cart',
        automaticallyImplyLeading: true,
        titleColor: Color(0xFF97080E),
        iconTheme: Color(0xFF97080E),
        widgets: [],
        appBar: AppBar(),
      ),
      body: cartItems.isEmpty ? Center(child: CircularProgressIndicator()) :ListView.builder(
          itemCount: count,
          itemBuilder: (BuildContext context,int index){
            return Column(
              children: [
                ListTile(
                  contentPadding: EdgeInsets.all(8),
                  leading: Image.network('https://apinizer.com/wp-content/uploads/revslider/the7-web-master-hero-image/laptop.png',height: 60,),
                  title: Text(cartItems[0][index]["listing"]['name'],style: TextStyle(fontSize: 22),),
                  subtitle: Text(cartItems[0][index]["listing"]['description'],maxLines: 2,overflow: TextOverflow.ellipsis,),
//                  trailing: Padding(
//                    padding: const EdgeInsets.only(right:8.0),
//                    child: Text(cartItems[index]['purchase_actions'][0]['name'],style: TextStyle(color: Color(0xFF97080E),fontWeight: FontWeight.w600,fontSize: 22),),
//                  ),
                  onTap: (){
//                    if(products[index]['purchase_actions'][0]['name'] == 'sell'){
//                      Navigator.push(context, MaterialPageRoute(builder: (context) => ProductDetails(product: products[index],)));
//                    }else{
//                      Navigator.push(context, MaterialPageRoute(builder: (context) => RentProductDetail(product: products[index])));
//                    }
                  },
                ),
                Divider(
                  color: Color(0xFFF7A74D),
                  height: 5.0,
                  thickness: 3,
                ),
              ],
            );
          })
    );
  }
}
