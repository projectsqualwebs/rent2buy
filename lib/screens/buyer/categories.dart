import 'package:flutter/material.dart';
import 'package:rent2buy/screens/buyer/categories/products_by_category.dart';
import 'package:rent2buy/screens/buyer/sub_categories.dart';
import 'package:rent2buy/services/apiProvider.dart';
import 'package:rent2buy/widgets/shared_widgets.dart';
import 'package:http/http.dart' as http;
import 'dart:convert';
class Categories extends StatefulWidget {

  @override
  _CategoriesState createState() => _CategoriesState();
}

class _CategoriesState extends State<Categories> {
  List<dynamic> categories = List();

  getCategories()async{
    http.Response response = await http.get('${ApiProvider.getCategories}');
    setState(() {
      categories = jsonDecode(response.body)['response'];
    });
    print(categories[0]['name']);
    return jsonDecode(response.body)['response'];
  }
  @override
  void initState() {
    super.initState();
    getCategories();
  }
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: BaseAppBar(
        title: 'All categories',
        automaticallyImplyLeading: true,
        titleColor: Color(0xFF97080E),
        iconTheme: Color(0xFF97080E),
        widgets: [],
        appBar: AppBar(),
      ),
      body: ListView.builder(
          itemCount: categories.length,
          itemBuilder: (BuildContext context, int index) {
            return Column(
              children: [
                ListTile(
                  contentPadding: EdgeInsets.symmetric(horizontal: 50),
                  title: Text(
                    categories[index]['name'],
                    style: TextStyle(fontWeight: FontWeight.w600, fontSize: 20),
                  ),
                  onTap: () {
                    print(categories[index]['name']);

                    Navigator.push(context, MaterialPageRoute(
                        builder: (context) =>
                            ProductsByCategory(categoryName: categories[index]['name'],category: categories[index]['_id'],subCategory: '',)));
                  },
                ),
                Divider(
                  color: Color(0xFFF7A74D),
                  height: 5.0,
                  thickness: 3,
                ),
              ],
            );
          }),
    );
  }
}
