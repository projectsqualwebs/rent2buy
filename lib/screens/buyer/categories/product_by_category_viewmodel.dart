import 'package:flutter/material.dart';
import 'package:rent2buy/di/service_locator.dart';
import 'package:rent2buy/models/common_repository.dart';
import 'package:rent2buy/models/product.dart';
import 'package:rent2buy/networking/network_responselist.dart';
import 'package:rent2buy/utils/base_change_notifier.dart';

class ProductByCategoryViewModel extends BaseChangeNotifier {
  final CommonRepository commonRepository = serviceLocator<CommonRepository>();

  ValueNotifier<NetworkResponseList<Product>> productsValue =
      ValueNotifier(NetworkResponseList.idle());

  void getProducts(Map<String, dynamic> params,Map<String,dynamic> query) {
    productsValue.value = NetworkResponseList.loading();
    commonRepository.getListings(params, query).then((response) {
      productsValue.value = NetworkResponseList.success(response.response);
    }).catchError((e) {
      productsValue.value = NetworkResponseList.error(parseError(e));
    });
  }
}
