import 'dart:convert';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:geolocator/geolocator.dart';
import 'package:http/http.dart' as http;
import 'package:rent2buy/di/service_locator.dart';
import 'package:rent2buy/models/address.dart';
import 'package:rent2buy/models/bundle.dart';
import 'package:rent2buy/models/product.dart';
import 'package:rent2buy/models/purchase_actions.dart';
import 'package:rent2buy/models/userinfo.dart';
import 'package:rent2buy/networking/network_status.dart';
import 'package:rent2buy/screens/buyer/categories/product_by_category_viewmodel.dart';
import 'package:rent2buy/screens/buyer/product_detail.dart';
import 'package:rent2buy/screens/buyer/rent_product_detail.dart';
import 'package:rent2buy/services/apiProvider.dart';
import 'package:rent2buy/utils/constants.dart' as Constants;
import 'package:rent2buy/utils/constants.dart';
import 'package:rent2buy/utils/data_manager.dart';
import 'package:rent2buy/utils/design_utils.dart';
import 'package:rent2buy/utils/strings.dart';
import 'package:rent2buy/utils/utils.dart';
import 'package:rent2buy/widgets/shared_widgets.dart';
import 'package:shimmer/shimmer.dart';

class ProductsByCategory extends StatefulWidget {
  final String category;
  final String subCategory;
  final String categoryName;
  final ProductByCategoryViewModel viewModel =
      serviceLocator<ProductByCategoryViewModel>();
  final DataManager dataManager = serviceLocator<DataManager>();

  ProductsByCategory(
      {Key key,
      @required this.categoryName,
      @required this.category,
      @required this.subCategory})
      : super(key: key);

  @override
  _ProductsByCategoryState createState() => _ProductsByCategoryState();
}

class _ProductsByCategoryState extends State<ProductsByCategory> {
  double currentLatitude = 0, currentLongitude = 0;
  bool showImageWidget = false;
  String _selectedSort = 'Relevant';
  static const _sorting = <String>[
    'Relevant',
    'Newest',
    'Oldest',
  ];

  final List<DropdownMenuItem<String>> _dropDownMenuItems = _sorting
      .map(
        (String value) => DropdownMenuItem<String>(
          value: value,
          child: Text(value,
              style: TextStyle(fontWeight: FontWeight.w600, fontSize: 20)),
        ),
      )
      .toList();

  List<Product> products = List();

  getLocation() async {
    LocationPermission permission = await Geolocator.requestPermission();
    Position position = await Geolocator.getCurrentPosition(
        desiredAccuracy: LocationAccuracy.high);
    setState(() {
      currentLatitude = position.latitude;
      currentLongitude = position.longitude;
    });
  }

  @override
  void initState() {
    super.initState();

    WidgetsBinding.instance.addPostFrameCallback((_) async {
      bool isGuest = await widget.dataManager.isGuestUser();
      if (isGuest) {
        getLocation();
      } else {
        Userinfo userinfo =
            Userinfo.fromJson(await widget.dataManager.getUser());
        Address defaultAddress = userinfo.address.firstWhere(
          (x) {
            return x.defaultAddress == 1;
          },
          orElse: () {
            return null;
          },
        );
        currentLatitude = defaultAddress?.latitude;
        currentLongitude = defaultAddress?.longitude;
      }
      fetchListings(currentLatitude, currentLongitude);
      setState(() {
        showImageWidget = true;
      });

      widget.viewModel.productsValue.addListener(() {
        var response = widget.viewModel.productsValue.value;
        if (response.status == NetworkStatus.SUCCESS) {
          setState(() {
            products.clear();
            products.addAll(response.response);
          });
        }
      });
    });
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: BaseAppBar(
        title: widget.categoryName,
        automaticallyImplyLeading: true,
        titleColor: Color(0xFF97080E),
        iconTheme: Color(0xFF97080E),
        widgets: [],
        appBar: AppBar(),
      ),
      body: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [
          SizedBox(height: 10),
          Row(
            mainAxisAlignment: MainAxisAlignment.spaceEvenly,
            children: [
              SizedBox(
                width: 150,
                child: DropdownButtonFormField(
                  decoration: InputDecoration(
                    enabled: true,
                    enabledBorder: OutlineInputBorder(
                        borderSide:
                            BorderSide(color: Color(0xFF97080E), width: 4),
                        borderRadius: BorderRadius.all(Radius.circular(12))),
                    focusedBorder: OutlineInputBorder(
                        borderSide:
                            BorderSide(color: Color(0xFF97080E), width: 4),
                        borderRadius: BorderRadius.all(Radius.circular(12))),
                  ),
                  icon: Container(
                    color: Color(0xFFF7A74D),
                    child: Icon(
                      Icons.keyboard_arrow_down,
                      color: Color(0xFF97080E),
                    ),
                  ),
                  value: _selectedSort,
                  items: _dropDownMenuItems,
                  hint: Text(
                    'Filter',
                    style: TextStyle(
                        fontWeight: FontWeight.w600, fontSize: 20, height: 1.1),
                  ),
                  onChanged: ((String val) {
                    setState(() {
                      _selectedSort = val;
                    });
                  }),
                ),
              ),
              SizedBox(
                width: 150,
                child: DropdownButtonFormField(
                  decoration: InputDecoration(
                    enabled: true,
                    enabledBorder: OutlineInputBorder(
                        borderSide:
                            BorderSide(color: Color(0xFF97080E), width: 4),
                        borderRadius: BorderRadius.all(Radius.circular(12))),
                    focusedBorder: OutlineInputBorder(
                        borderSide:
                            BorderSide(color: Color(0xFF97080E), width: 4),
                        borderRadius: BorderRadius.all(Radius.circular(12))),
                  ),
                  icon: Container(
                    color: Color(0xFFF7A74D),
                    child: Icon(
                      Icons.keyboard_arrow_down,
                      color: Color(0xFF97080E),
                    ),
                  ),
                  value: _selectedSort,
                  items: _dropDownMenuItems,
                  hint: Text(
                    'Sort by',
                    style: TextStyle(
                        fontWeight: FontWeight.w600, fontSize: 20, height: 1.1),
                  ),
                  onChanged: ((String val) {
                    setState(() {
                      _selectedSort = val;
                      showImageWidget = true;
                      fetchListings(currentLatitude, currentLongitude);
                    });
                  }),
                ),
              ),
            ],
          ),
          Padding(
            padding: const EdgeInsets.fromLTRB(40, 16, 0, 16),
            child: Text('Number of ads found', style: TextStyle(fontSize: 16)),
          ),
          showImageWidget
              ? Expanded(
                  child: SizedBox(
                    height: 200,
                    child:
                        products.isNotEmpty ? _buildListView() : _emptyView(),
                  ),
                )
              : Expanded(
                  child: Shimmer.fromColors(
                      child: SizedBox(
                        height: 200,
                        child: ListView.builder(
                            itemCount: 8,
                            itemBuilder: (context, int index) {
                              return Column(
                                children: [
                                  ListTile(
                                    leading: Container(
                                        width: 70,
                                        height: 60,
                                        color: Colors.white),
                                    subtitle: Column(
                                      children: [
                                        Container(
                                          height: 16,
                                          color: Colors.white,
                                        ),
                                        SizedBox(
                                          height: 06,
                                        ),
                                        Container(
                                          height: 16,
                                          color: Colors.white,
                                        ),
                                      ],
                                    ),
                                  ),
                                  Divider(
                                    color: Color(0xFFF7A74D),
                                    height: 5.0,
                                    thickness: 3,
                                  ),
                                ],
                              );
                            }),
                      ),
                      baseColor: Colors.grey[300],
                      highlightColor: Colors.grey[100]),
                )
        ],
      ),
    );
  }

  _emptyView() {
    return Container(
      alignment: Alignment.center,
      child: Text(
        Strings.msgNoListingFound,
        style: DesignUtils.textApperanceRegular(),
      ),
    );
  } 

  String getPrice(Product product) {
    var purchaseType = Constants.SELL;
    var sellPrice, rentPrice;

    if (product.purchaseActions?.length > 1) {
      purchaseType = Constants.SELL_AND_RENT;
      sellPrice = product.purchaseActions.first.price;
      rentPrice = product.purchaseActions.last.price['per_day'];

      return "Price: " +
          '\u0024' +
          '${sellPrice.toString()}' +
          " " +
          "Rent Price: " +
          '\u0024' +
          '${rentPrice.toString()}';
    } else {
      purchaseType = product.purchaseActions.first.name == "sell"
          ? Constants.SELL
          : Constants.RENT;
      if (purchaseType == Constants.SELL) {
        sellPrice = sellPrice = product.purchaseActions.first.price;
        return '\u0024' + sellPrice.toString();
      } else {
        rentPrice = product.purchaseActions.last.price['per_day'];
        return '\u0024' + rentPrice.toString();
      }
    }
  }

  void fetchListings(double lat, double lng) async {
    Map<String, dynamic> params = Map();
    params[Constants.category] = widget.category;

    Map<String, dynamic> query = Map();
    query[Constants.lat] = lat;
    query[Constants.long] = lng;
    if (_selectedSort == 'Oldest') {
      params[Constants.sort] = 2;
    } else if (_selectedSort == 'Newest') {
      params[Constants.sort] = 1;
    }

    widget.viewModel.getProducts(params, query);
  }

  _buildListView() {
    return ListView.builder(
        itemCount: products?.length,
        itemBuilder: (BuildContext context, int index) {
          Product product = products[index] as Product;

          return Column(
            children: [
              ListTile(
                contentPadding: EdgeInsets.all(8),
                leading: Image.network(
                  product.images.path[0],
                  height: 60,
                  width: 70,
                ),
                title: Padding(
                  padding: const EdgeInsets.only(left: 6.0),
                  child: Text(
                    product.name,
                    style: TextStyle(fontSize: 16, color: Color(0xFF97080E)),
                  ),
                ),
                subtitle: Column(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: [
                    Row(
                      mainAxisAlignment: MainAxisAlignment.start,
                      children: [
                        Icon(
                          Icons.location_on,
                          color: Colors.black54,
                        ),
                        Flexible(
                          child: Text(
                            product.seller.address.first.location,
                            style: TextStyle(
                                color: Color(0xFF656565), fontSize: 16),
                            overflow: TextOverflow.ellipsis,
                          ),
                        )
                      ],
                    ),
                    Padding(
                      padding: const EdgeInsets.only(left: 8.0),
                      child: Text(getPrice(product),
                          style: TextStyle(
                              color: Color(0xFFA6B316), fontSize: 18)),
                    )
                  ],
                ),
                onTap: () {
                  String action =
                      Utils.getPurchaseActions(products[index].purchaseActions);
                  Navigator.pushNamed(context, ProductDetails.routeName,
                      arguments:
                          Bundle(products[index].id, purchaseAction: action));
                },
              ),
              Divider(
                color: Color(0xFFF7A74D),
                height: 5.0,
                thickness: 3,
              ),
            ],
          );
        });
  }
}
//     : SizedBox(
// width: MediaQuery.of(context).size.width,
// height: 400.0,
// child: Shimmer.fromColors(
// child: Card(
// color: Colors.grey,
// ),
// baseColor: Colors.white70,
// highlightColor: Colors.grey[700],
// direction: ShimmerDirection.ltr,
// )),
