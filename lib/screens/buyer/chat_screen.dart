import 'dart:io';
import 'package:firebase_database/firebase_database.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:rent2buy/models/user_model.dart';
import 'package:rent2buy/widgets/received_msg_widget.dart';
import 'package:rent2buy/widgets/sended_msg_widget.dart';
import 'package:rent2buy/widgets/shared_widgets.dart';
import 'package:http/http.dart' as http;
import 'package:rent2buy/services/apiProvider.dart';
import 'package:shared_preferences/shared_preferences.dart';

class ChatScreen extends StatefulWidget {
  String id;
  String name;

  ChatScreen({Key key, this.id,this.name}) : super(key: key);
  @override
  _ChatScreenState createState() => _ChatScreenState();
}

class _ChatScreenState extends State<ChatScreen> {
  User user;
  List<String> msg = List();
  TextEditingController _text = TextEditingController();
  ScrollController _scrollController = ScrollController();
  var childList = <Widget>[];
  Map sortMap;
  @override
  void initState() {
    super.initState();
    getMsgFireBase();
    childList.add(Align(
      alignment: Alignment(0, 0),
      child: Container(
        margin: EdgeInsets.only(top: 5.0),
        height: 25,
        width: 50,
        decoration: BoxDecoration(
            color: Colors.black12,
            borderRadius: BorderRadius.all(Radius.circular(8.0))),
        child: Center(
          child: Text('Today', style: TextStyle(fontSize: 11)),
        ),
      ),
    ));
  }

  getMsgFireBase()async{
    final databaseReference = FirebaseDatabase.instance.reference().child('chat_line').child(widget.id);
    databaseReference.onValue.listen((event) {
      Map<dynamic,dynamic> message = event.snapshot.value;
      childList.clear();
      sortMap = Map.fromEntries(message.entries.toList()..sort((e1, e2) =>
          e1.value["send_timestamp"].compareTo(e2.value["send_timestamp"])));
      sortMap.forEach((key, value) {
        if(value['sender']['is_seller'] == 1){
          print(value['message'] +' '+ value['send_timestamp'].toString());
          setState(() {
            childList.add(Align(
              alignment: Alignment(-1, 0),
              child: ReceivedMsgWidget(content: value['message']),
            ));
          });
        }else{
          print(value['message'] + ' ' + value['send_timestamp'].toString());
          setState(() {
            childList.add(Align(
              alignment: Alignment(1, 0),
              child: SendedMsgWidget(content:value['message']),
            ));
          });
        }
      });
    });
  }




  sendMsg() async {
    SharedPreferences prefs = await SharedPreferences.getInstance();
    String token = prefs.getString('token');
    http.Response response = await http.post('${ApiProvider.chat}', headers: {
      HttpHeaders.authorizationHeader: 'Bearer $token'
    }, body: {
      "chat_id": widget.id,
      "message": _text.text,
      "message_files": ''
    });
    if (response.statusCode == 200) {
      print(response.body);
      _text.clear();
      sortMap.clear();
    } else {
      print(response.body);
    }
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      resizeToAvoidBottomInset: true,
      appBar: BaseAppBar(
        title: 'chats',
        titleColor: Color(0xFF97080E),
        iconTheme: Color(0xFF97080E),
        automaticallyImplyLeading: true,
        widgets: [
          PopupMenuButton(itemBuilder: (BuildContext context) {
            return [
              PopupMenuItem(child: Text('Pay Now')),
              PopupMenuItem(child: Text('Details')),
            ];
          })
        ],
        appBar: AppBar(),
      ),
      body: SafeArea(
        child: Container(
          height: MediaQuery.of(context).size.height,
          padding: EdgeInsets.symmetric(horizontal: 6),
          child: Stack(
            fit: StackFit.loose,
            children: [
              Column(
                mainAxisAlignment: MainAxisAlignment.end,
                crossAxisAlignment: CrossAxisAlignment.start,
                children: [
                  Flexible(
                    fit: FlexFit.tight,
                    child: Container(
                      width: MediaQuery.of(context).size.width,
                      child: SingleChildScrollView(
                          controller: _scrollController,
                          reverse: true,
                          child: GestureDetector(
                            onLongPress: (){
                            },
                            child: Column(
                              crossAxisAlignment: CrossAxisAlignment.start,
                              mainAxisAlignment: MainAxisAlignment.end,
                              children: childList,
                            ),
                          )),
                    ),
                  ),
                  Padding(
                    padding: const EdgeInsets.all(8.0),
                    child: TextField(
                      controller: _text,
                      decoration: InputDecoration(
                        prefixIcon: Padding(
                          padding: const EdgeInsets.all(10.0),
                          child: Image.asset(
                            'assets/speech-bubbles.png',
                            height: 10,
                            color: Colors.grey,
                          ),
                        ),
                        suffixIcon: IconButton(
                          icon: Icon(Icons.send),
                          onPressed: () {
                            sendMsg();
                          },
                        ),
                        enabled: true,
                        enabledBorder: OutlineInputBorder(
                            borderSide:
                                BorderSide(color: Color(0xFF97080E), width: 3),
                            borderRadius:
                                BorderRadius.all(Radius.circular(12))),
                        focusedBorder: OutlineInputBorder(
                            borderSide:
                                BorderSide(color: Color(0xFF97080E), width: 4),
                            borderRadius:
                                BorderRadius.all(Radius.circular(12))),
                        hintText: 'Message',
                      ),
                    ),
                  )
                ],
              )
            ],
          ),
        ),
      ),
    );
  }
}
