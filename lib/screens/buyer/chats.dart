import 'dart:convert';
import 'dart:io';
import 'package:fluttertoast/fluttertoast.dart';
import 'package:http/http.dart' as http;
import 'package:flutter/material.dart';
import 'package:rent2buy/di/service_locator.dart';
import 'package:rent2buy/models/thread_response.dart';
import 'package:rent2buy/networking/network_status.dart';
import 'package:rent2buy/screens/buyer/chat_screen.dart';
import 'package:rent2buy/screens/common/chat/buyer_chat_screen.dart';
import 'package:rent2buy/screens/common/progress_view.dart';
import 'package:rent2buy/screens/seller/inbox/inbox_viewmodel.dart';
import 'package:rent2buy/services/apiProvider.dart';
import 'package:rent2buy/utils/strings.dart';
import 'package:rent2buy/utils/utils.dart';
import 'package:rent2buy/widgets/shared_widgets.dart';
import 'package:rent2buy/models/user_model.dart';
import 'package:shared_preferences/shared_preferences.dart';

class Chats extends StatefulWidget {
  static final String routeName = "/chats";
  final InBoxViewModel viewModel = serviceLocator<InBoxViewModel>();
  @override
  _ChatsState createState() => _ChatsState();
}

class _ChatsState extends State<Chats> {
  bool isSelected = false;
  List<ThreadResponse> threads = List();

  deleteThread(String id) async {
    SharedPreferences prefs = await SharedPreferences.getInstance();
    String token = prefs.getString('token');
    http.Response response = await http.delete(
        '${ApiProvider.thread}' + '/' + id,
        headers: {HttpHeaders.authorizationHeader: 'Bearer $token'});
    print(response.body.toString());
    getThreads();
  }

  @override
  void initState() {
    getThreads();
    super.initState();
  }

  void getThreads() {
    widget.viewModel.getThreads(Map());
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: BaseAppBar(
        title: 'Chats',
        automaticallyImplyLeading: true,
        titleColor: Color(0xFF97080E),
        iconTheme: Color(0xFF97080E),
        widgets: [],
        appBar: AppBar(),
      ),
      body: ValueListenableBuilder(
        valueListenable: widget.viewModel.threadsValue,
        builder: (context, value, _) {
          var response = widget.viewModel.threadsValue.value;
          switch (response.status) {
            case NetworkStatus.LOADING:
              return CircularProgress();
            case NetworkStatus.ERROR:
              Fluttertoast.showToast(msg: response.message);
              break;
            case NetworkStatus.SUCCESS:
              WidgetsBinding.instance.addPostFrameCallback((_) {
                setState(() {
                  threads.clear();
                  threads.addAll(response.response);
                });
              });
              return AnimatedCrossFade(
                duration: Duration(milliseconds: 500),
                crossFadeState: threads.isNotEmpty
                    ? CrossFadeState.showFirst
                    : CrossFadeState.showSecond,
                firstChild: ListView.builder(
                    shrinkWrap: true,
                    scrollDirection: Axis.vertical,
                    itemCount: threads.length,
                    itemBuilder: (BuildContext context, int index) {
                      return _buildItem(threads[index]);
                    }),
                secondChild: Utils.getEmptyView(Strings.noChats),
              );
          }
          return Utils.getEmptyView(Strings.noChats);
        },
      ),
    );
  }

  Widget _buildItem(ThreadResponse item) {
    try {
      return Column(
        children: [
          Padding(
            padding: const EdgeInsets.all(12.0),
            child: ListTile(
              selected: isSelected,
              leading: CircleAvatar(
                child: Image.network(
                    'https://encrypted-tbn0.gstatic.com/images?q=tbn%3AANd9GcQlR_mhU1bn_sa7j_M5UeMmIbKmR3UI1evxNw&usqp=CAU'),
                radius: 30,
                backgroundColor: Colors.transparent,
              ),
              title: Padding(
                padding: const EdgeInsets.only(left: 6.0, bottom: 8),
                child: Text(
                  Utils.getFullName(
                      item.seller.firstName, item.seller.lastName),
                  style: TextStyle(fontSize: 24, fontWeight: FontWeight.w700),
                ),
              ),
              subtitle: Row(
                mainAxisAlignment: MainAxisAlignment.start,
                children: [
                  Icon(
                    Icons.location_on,
                    color: Color(0xFF656565),
                  ),
                  Flexible(
                    child: Text(
                      item.seller.address.first.location,
                      overflow: TextOverflow.ellipsis,
                      style: TextStyle(color: Colors.grey, fontSize: 16),
                    ),
                  )
                ],
              ),
              trailing: Visibility(
                visible: isSelected,
                child: IconButton(icon: Icon(Icons.delete), onPressed: () {}),
              ),
              onLongPress: () {
                setState(() {
                  isSelected = true;
                });
              },
              onTap: () {
                Navigator.push(
                  context,
                  MaterialPageRoute(
                    builder: (context) => BuyerChatScreen(
                      chatId: item.id,
                      receiverName: item.user.firstName,
                    ),
                  ),
                );
              },
            ),
          ),
          Divider(
            color: Color(0xFF71C7E3),
            height: 5.0,
            thickness: 3,
          ),
        ],
      );
    } catch (e) {}
  }
}
