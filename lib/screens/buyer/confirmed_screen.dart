import 'package:flutter/material.dart';
import 'package:lottie/lottie.dart';
import 'package:rent2buy/widgets/shared_widgets.dart';
class ConfirmedScreen extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: appBar(Color(0xFF97080E)),
      body: Column(
        mainAxisAlignment: MainAxisAlignment.center,
        children: [
          Center(
            child: Lottie.network('https://assets6.lottiefiles.com/datafiles/Wv6eeBslW1APprw/data.json',repeat: false,reverse: true),
          ),
          Text('Successful',style: TextStyle(fontSize: 36,color: Color(0xFF97080E)))
        ],
      ),
    );
  }
}
