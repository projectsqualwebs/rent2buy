import 'package:flutter/material.dart';
import 'package:fluttertoast/fluttertoast.dart';
import 'package:rent2buy/di/service_locator.dart';
import 'package:rent2buy/models/dispute_order.dart';
import 'package:rent2buy/models/product.dart';
import 'package:rent2buy/networking/network_status.dart';
import 'package:rent2buy/screens/buyer/dispute/dispute_viewmodel.dart';
import 'package:rent2buy/screens/buyer/orders/feedback.dart';
import 'package:rent2buy/screens/common/progress_view.dart';
import 'package:rent2buy/utils/strings.dart';
import 'package:rent2buy/utils/time_utils.dart';
import 'package:rent2buy/utils/utils.dart';
import 'package:rent2buy/widgets/shared_widgets.dart';
import 'package:rent2buy/data.dart';

class Dispute extends StatefulWidget {
  final DisputeViewModel viewModel = serviceLocator<DisputeViewModel>();

  @override
  State<StatefulWidget> createState() {
    return _DisputeScreenState();
  }
}

class _DisputeScreenState extends State<Dispute> {
  final List<DisputeOrder> disputes = List();

  @override
  void initState() {
    // TODO: implement initState
    super.initState();
    widget.viewModel.getDisputes();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: BaseAppBar(
        title: 'Dispute',
        titleColor: Color(0xFF97080E),
        iconTheme: Color(0xFF97080E),
        automaticallyImplyLeading: true,
        widgets: [],
        appBar: AppBar(),
      ),
      body: ValueListenableBuilder(
        valueListenable: widget.viewModel.disputeValue,
        builder: (context, value, _) {
          var response = widget.viewModel.disputeValue.value;
          switch (response.status) {
            case NetworkStatus.LOADING:
              return CircularProgress();
            case NetworkStatus.ERROR:
              Fluttertoast.showToast(msg: response.message);
              break;
            case NetworkStatus.SUCCESS:
              WidgetsBinding.instance.addPostFrameCallback((_) {
                setState(() {
                  disputes.clear();
                  disputes.addAll(response.response);
                });
              });
              return AnimatedCrossFade(
                duration: Duration(milliseconds: 500),
                crossFadeState: disputes.isEmpty
                    ? CrossFadeState.showSecond
                    : CrossFadeState.showFirst,
                secondChild: Utils.getEmptyView(Strings.msgNoDisputesFound),
                firstChild: ListView.builder(
                    scrollDirection: Axis.vertical,
                    shrinkWrap: true,
                    itemCount: disputes.length,
                    itemBuilder: (BuildContext context, int index) {
                      return _buildItem(index, disputes[index].listing);
                    }),
              );
              break;
          }
          return Utils.getEmptyView(Strings.msgNoDisputesFound);
        },
      ),
    );
  }

  Widget _buildItem(int index, Product product) {
    try {
      return Column(
        children: [
          Padding(
            padding: const EdgeInsets.all(12.0),
            child: ListTile(
              leading: Image.network(
                product.images.path[0],
                fit: BoxFit.fitHeight,
                width: 100,
                height: 100,
              ),
              title: Text(
                product.name,
                style: TextStyle(fontSize: 24),
              ),
              subtitle: Padding(
                padding: const EdgeInsets.only(top: 8.0),
                child: Text(
                  TimeUtils.getMMDDYYY(product.createdAt),
                  style: TextStyle(fontSize: 16),
                ),
              ),
              trailing: Column(
                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                children: [
                  Text(
                    Utils.getPriceInDollar(product.purchaseActions.first.price),
                    style: TextStyle(color: Color(0xFF97080E), fontSize: 24),
                  ),
                  Text(
                    "In-Progress",
                    style: TextStyle(color: Colors.green[600], fontSize: 16),
                  )
                ],
              ),
              onTap: () {},
            ),
          ),
          Divider(
            color: Color(0xFFF7A74D),
            height: 5.0,
            thickness: 3,
          ),
        ],
      );
    } catch (e) {
      debugPrint(e.toString());
    }
  }
}
