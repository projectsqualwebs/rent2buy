import 'package:flutter/material.dart';
import 'package:rent2buy/di/service_locator.dart';
import 'package:rent2buy/models/common_repository.dart';
import 'package:rent2buy/models/dispute_order.dart';
import 'package:rent2buy/models/product.dart';
import 'package:rent2buy/networking/network_responselist.dart';
import 'package:rent2buy/utils/base_change_notifier.dart';

class DisputeViewModel extends BaseChangeNotifier {
  final CommonRepository _commonRepository = serviceLocator<CommonRepository>();

  ValueNotifier<NetworkResponseList<DisputeOrder>> disputeValue =
      ValueNotifier(NetworkResponseList.idle());

  void getDisputes() {
    disputeValue.value = NetworkResponseList.loading();
    _commonRepository.getDisputes().then((response) {
      disputeValue.value = NetworkResponseList.success(response.response);
    }).catchError((e) {
      disputeValue.value = NetworkResponseList.error(parseError(e));
    });
  }

  void getSellerDisputes() {
    disputeValue.value = NetworkResponseList.loading();
    _commonRepository.getSellerDisputes().then((response) {
      disputeValue.value = NetworkResponseList.success(response.response);
    }).catchError((e) {
      disputeValue.value = NetworkResponseList.error(parseError(e));
    });
  }
}
