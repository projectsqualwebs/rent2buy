import 'package:flutter/material.dart';
import 'package:rent2buy/widgets/shared_widgets.dart';

class FirstScreen extends StatefulWidget {
  @override
  _FirstScreenState createState() => _FirstScreenState();
}

class _FirstScreenState extends State<FirstScreen> {
  String _selectedcountry;
  String _selectedlang;

  static const _countries = <String>[
    'India',
    'USA',
    'Australia',
  ];

  static const _languages = <String>[
    'English',
    'Hindi',
    'Marathi',
  ];

  final List<DropdownMenuItem<String>> _dropDownMenuItems = _countries
      .map(
        (String value) => DropdownMenuItem<String>(
          value: value,
          child: Text(value,
              style: TextStyle(
                  fontWeight: FontWeight.w600, fontSize: 20)),
        ),
      )
      .toList();

  final List<DropdownMenuItem<String>> _dropDownMenuItems2 = _languages
      .map(
        (String value) => DropdownMenuItem<String>(
          value: value,
          child: Text(value,
              style: TextStyle(
                  fontWeight: FontWeight.w600, fontSize: 20)),
        ),
      )
      .toList();

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        backgroundColor: Color(0xFFF7A74D),
        title: Text('R2B',
            style: TextStyle(
                fontFamily: 'Rammetto One',
                color: Color(0xFF97080E),
                fontSize: 24)),
        centerTitle: true,
        automaticallyImplyLeading: false,
      ),
      body: Container(
        padding: EdgeInsets.symmetric(horizontal: 30),
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: [
            SizedBox(height: 60),
            Padding(
              padding: EdgeInsets.only(left: 12.0),
              child: Text(
                'Choose Country',
                style: TextStyle(fontWeight: FontWeight.w700, fontSize: 24),
              ),
            ),
            SizedBox(height: 20),
            DropdownButtonFormField(
              decoration: InputDecoration(
                enabled: true,
                enabledBorder: OutlineInputBorder(
                    borderSide: BorderSide(color: Color(0xFF97080E), width: 4),
                    borderRadius: BorderRadius.all(Radius.circular(12))),
                focusedBorder: OutlineInputBorder(
                    borderSide: BorderSide(color: Color(0xFF97080E), width: 4),
                    borderRadius: BorderRadius.all(Radius.circular(12))),
                contentPadding: EdgeInsets.fromLTRB(36, 24, 24, 24),
              ),
              icon: Icon(
                Icons.keyboard_arrow_down,
                color: Color(0xFF97080E),
              ),
              value: _selectedcountry,
              items: _dropDownMenuItems,
              hint: Text(
                'Select Country',
                style: TextStyle(
                    fontWeight: FontWeight.w600, fontSize: 20, height: 1.1),
              ),
              onChanged: ((String val) {
                setState(() {
                  _selectedcountry = val;
                });
              }),
            ),
            SizedBox(height: 60),
            Padding(
              padding: EdgeInsets.only(left: 12.0),
              child: Text(
                'Choose Language',
                style: TextStyle(fontWeight: FontWeight.w700, fontSize: 24),
              ),
            ),
            SizedBox(height: 20),
            DropdownButtonFormField(
              decoration: InputDecoration(
                enabled: true,
                enabledBorder: OutlineInputBorder(
                    borderSide: BorderSide(color: Color(0xFF97080E), width: 4),
                    borderRadius: BorderRadius.all(Radius.circular(12))),
                focusedBorder: OutlineInputBorder(
                    borderSide: BorderSide(color: Color(0xFF97080E), width: 4),
                    borderRadius: BorderRadius.all(Radius.circular(12))),
                contentPadding: EdgeInsets.fromLTRB(36, 24, 24, 24),
              ),
              icon: Icon(
                Icons.keyboard_arrow_down,
                color: Color(0xFF97080E),
              ),
              value: _selectedlang,
              items: _dropDownMenuItems2,
              hint: Text(
                'Select Language',
                style: TextStyle(
                    fontWeight: FontWeight.w600, fontSize: 20, height: 1.1),
              ),
              onChanged: ((String val) {
                setState(() {
                  _selectedlang = val;
                });
              }),
            ),
            SizedBox(height: 100),
            Center(child: MyButton(text: 'Select', color: Color(0xFF97080E),onPressed: () {Navigator.pushNamed(context, '/login');}))
          ],
        ),
      ),
    );
  }
}
