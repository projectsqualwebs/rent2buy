import 'package:flutter/material.dart';
import 'package:rent2buy/di/service_locator.dart';
import 'package:rent2buy/models/category.dart';
import 'package:rent2buy/models/common_repository.dart';
import 'package:rent2buy/models/delivery_type.dart';
import 'package:rent2buy/networking/network_responselist.dart';
import 'package:rent2buy/utils/base_change_notifier.dart';

class BuyerHomeViewModel extends BaseChangeNotifier {
  final CommonRepository _commonRepository = serviceLocator<CommonRepository>();
  ValueNotifier<NetworkResponseList<ListingCategory>> categoriesValue =
      ValueNotifier(NetworkResponseList.idle());

  ValueNotifier<NetworkResponseList<DeliveryType>> deliveryTypes =
      ValueNotifier(NetworkResponseList.idle());
      
  void getCategories() {
    categoriesValue.value = NetworkResponseList.loading();
    try {
      _commonRepository
          .getCategories()
          .then((value) => categoriesValue.value =
              NetworkResponseList.success(value.response))
          .catchError((e) {
        print(e);
        categoriesValue.value = NetworkResponseList.error(parseError(e));
      });
    } catch (exception) {
      print(exception);
      categoriesValue.value = NetworkResponseList.error(parseError(exception));
    }
  }

   void getDeliveryTypes() {
    deliveryTypes.value = NetworkResponseList.loading();
    try {
      _commonRepository
          .getDeliveryTypes()
          .then((value) =>
              deliveryTypes.value = NetworkResponseList.success(value.response))
          .catchError((e) {
        print(e);
        deliveryTypes.value = NetworkResponseList.error(parseError(e));
      });
    } catch (exception) {
      print(exception);
      deliveryTypes.value = NetworkResponseList.error(parseError(exception));
    }
  }
}

