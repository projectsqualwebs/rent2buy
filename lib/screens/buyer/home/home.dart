import 'dart:convert';

import 'package:flutter/material.dart';
import 'package:rent2buy/di/service_locator.dart';
import 'package:rent2buy/models/category.dart';
import 'package:rent2buy/networking/network_responselist.dart';
import 'package:rent2buy/networking/network_status.dart';
import 'package:rent2buy/screens/buyer/categories/products_by_category.dart';
import 'package:rent2buy/screens/buyer/home/buyer_home_viewmodel.dart';
import 'package:rent2buy/screens/common/error_screen.dart';
import 'package:rent2buy/screens/common/loading.dart';
import 'package:rent2buy/screens/common/progress_view.dart';
import 'package:rent2buy/services/apiProvider.dart';
import 'package:rent2buy/utils/colors.dart';
import 'package:rent2buy/utils/data_manager.dart';
import 'package:rent2buy/utils/design_utils.dart';
import 'package:rent2buy/utils/strings.dart';
import 'package:rent2buy/utils/utils.dart';
import 'package:rent2buy/widgets/shared_widgets.dart';
import 'package:rent2buy/data.dart';
import 'package:http/http.dart' as http;

class Home extends StatefulWidget {
  static final routeName = '/home';
  final DataManager dataManager = serviceLocator<DataManager>();
  final BuyerHomeViewModel viewModel = serviceLocator<BuyerHomeViewModel>();
  @override
  _HomeState createState() => _HomeState();
}

class _HomeState extends State<Home> {
  List<ListingCategory> categories = List();
  List<ListingCategory> searchResults = List();

  var _searchController = TextEditingController();
  search() async {
    http.Response response =
        await http.post('${ApiProvider.productsByCategory}', body: {
      "category": '',
      "sub_category": '',
      "delivery_type": '',
      "keywords": "Lenevo 420"
    });
    print(response.body);
  }

  @override
  void initState() {
    getCategories();
    widget.viewModel.getDeliveryTypes();
    WidgetsBinding.instance.addPostFrameCallback((_) {
      var response = widget.viewModel.deliveryTypes.value;
      switch (response.status) {
        case NetworkStatus.ERROR:
          break;
        case NetworkStatus.SUCCESS:
          String deliveryTypeID = response.response.first.id;
          String selfPickUpID = response.response.last.id;

          widget.dataManager.saveDeliveryId(deliveryTypeID);
          widget.dataManager.saveSelfPickUpId(selfPickUpID);
          break;
      }
    });
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return SafeArea(
      child: Scaffold(
        appBar: AppBar(
          iconTheme: IconThemeData(color: Color(0xFF97080E)),
          title: Text(
            'Rent2S',
            style: TextStyle(
                fontSize: 24,
                fontWeight: FontWeight.bold,
                color: Color(0xFF97080E)),
          ),
          actions: [
            IconButton(
                icon: Icon(
                  Icons.search,
                ),
                onPressed: () {
                  Navigator.pushNamed(context, '/search');
                }),
            IconButton(
                icon: Icon(
                  Icons.location_pin,
                ),
                onPressed: () {
                  Navigator.pushNamed(context, '/set_location');
                })
          ],
        ),
        drawer: CustomDrawer(),
        body: SingleChildScrollView(
          child: Container(
            width: MediaQuery.of(context).size.width,
            child: Column(
              mainAxisAlignment: MainAxisAlignment.start,
              children: [
                Stack(
                  children: [
                    Image.network(
                      'https://www.breathintravel.com/wp-content/uploads/2020/09/best-backpacking-binoculars-1150x766.jpg',
                      width: MediaQuery.of(context).size.width,
                    ),
                    Positioned(
                      top: 30,
                      left: 20,
                      child: Text(
                        'Search from \nThousands of \nAds',
                        style: TextStyle(
                            color: Color(0xFF97080E),
                            fontWeight: FontWeight.w700,
                            fontSize: 30),
                      ),
                    ),
                    Positioned(
                      top: 150,
                      left: 20,
                      child: Text(
                        'Here you can Search',
                        style: TextStyle(
                            color: Color(0xFF97080E),
                            fontWeight: FontWeight.w700,
                            fontSize: 16),
                      ),
                    ),
                    Positioned(
                      top: 180,
                      width: MediaQuery.of(context).size.width,
                      child: Padding(
                        padding: const EdgeInsets.symmetric(horizontal: 20.0),
                        child: SuffixedIconTextField(
                            textEditingController: _searchController,
                            filled: true,
                            fillColor: Colors.white54,
                            borderColor: Color(0xFF97080E),
                            hinttext: 'Search by Keyword',
                            textAlign: TextAlign.justify,
                            isNumber: false,
                            onChanged: (String text) {
                              onSearchTextChanged(text);
                            },
                            suffixIcon: Padding(
                              padding: const EdgeInsets.all(4),
                              child: Container(
                                color: Color(0xFFF7A74D),
                                child: IconButton(
                                    icon: Icon(
                                      Icons.search,
                                      color: Color(0xFF97080E),
                                    ),
                                    onPressed: () {
                                      search();
                                    }),
                              ),
                            )),
                      ),
                    )
                  ],
                ),
                Container(
                  padding: EdgeInsets.all(16),
                  child: ValueListenableBuilder(
                      valueListenable: widget.viewModel.categoriesValue,
                      builder: (context, value, child) {
                        var responseList =
                            value as NetworkResponseList<ListingCategory>;
                        switch (responseList.status) {
                          case NetworkStatus.LOADING:
                            return new CircularProgress();
                          case NetworkStatus.ERROR:
                            return ErrorScreen(responseList.message);
                          case NetworkStatus.SUCCESS:
                            udpateList(responseList.response);
                            break;
                          default:
                            break;
                        }
                        return searchResults.isNotEmpty ||
                                _searchController.text.isNotEmpty
                            ? _buildCategorySearchListView()
                            : _buildCategoryListView();
                      }),
                ),
                Visibility(
                  visible: searchResults.isNotEmpty || categories.isNotEmpty,
                  child: Padding(
                    padding: const EdgeInsets.only(bottom: 12.0),
                    child: SizedBox(
                        width: 170,
                        child: MyButton(
                            text: 'View All Categories',
                            color: Color(0xFF97080E),
                            onPressed: () {
                              Navigator.pushNamed(context, '/categories');
                            })),
                  ),
                )
              ],
            ),
          ),
        ),
      ),
    );
  }

  void udpateList(List<ListingCategory> response) {
    WidgetsBinding.instance.addPostFrameCallback((_) {
      setState(() {
        categories.clear();
        categories.addAll(response);
      });
    });
  }

  void getCategories() {
    widget.viewModel.getCategories();
  }

  void onSearchTextChanged(String text) {
    searchResults.clear();
    if (text.isEmpty) {
      setState(() {});
      return;
    }
    categories.forEach((x) {
      if (x.name.contains(text)) {
        searchResults.add(x);
      }
    });
    setState(() {});
  }

  Widget _buildCategoryListView() {
    if (categories.isEmpty) {
      return Center(child: Utils.getEmptyView(Strings.msgNoCategoriesFound));
    } else {
      return GridView.builder(
          physics: ScrollPhysics(),
          scrollDirection: Axis.vertical,
          shrinkWrap: true,
          itemCount: categories.length,
          gridDelegate: SliverGridDelegateWithFixedCrossAxisCount(
              crossAxisCount: 3, crossAxisSpacing: 6, mainAxisSpacing: 10),
          itemBuilder: (BuildContext context, int index) {
            return _buildItem(categories[index]);
          });
    }
  }

  Widget _buildCategorySearchListView() {
    return GridView.builder(
        physics: ScrollPhysics(),
        scrollDirection: Axis.vertical,
        shrinkWrap: true,
        itemCount: searchResults.length,
        gridDelegate: SliverGridDelegateWithFixedCrossAxisCount(
            crossAxisCount: 3, crossAxisSpacing: 6, mainAxisSpacing: 10),
        itemBuilder: (BuildContext context, int index) {
          return _buildItem(searchResults[index]);
        });
  }

  Widget _buildItem(ListingCategory category) {
    return GestureDetector(
      onTap: () {
        Navigator.push(
            context,
            MaterialPageRoute(
                builder: (context) => ProductsByCategory(
                      categoryName: category.name,
                      category: category.id,
                      subCategory: '',
                    )));
      },
      child: Container(
          decoration: BoxDecoration(
              border: Border.all(width: 3, color: Color(0xFFF7A74D))),
          child: Column(
            crossAxisAlignment: CrossAxisAlignment.center,
            mainAxisAlignment: MainAxisAlignment.center,
            mainAxisSize: MainAxisSize.min,
            children: [
              category.images != null
                  ? Image.network(
                      category.images.path?.isEmpty
                          ? ""
                          : category.images?.path[0],
                      fit: BoxFit.contain,
                    )
                  : Container(),
              Text(category.name,
                  overflow: TextOverflow.ellipsis,
                  maxLines: 1,
                  style: DesignUtils.textApperanceRegular()),
            ],
          )),
    );
  }
}
