import 'package:flutter/material.dart';
import 'package:fluttertoast/fluttertoast.dart';
import 'package:rent2buy/di/service_locator.dart';
import 'package:rent2buy/models/notification_response.dart';
import 'package:rent2buy/networking/network_response.dart';
import 'package:rent2buy/networking/network_responselist.dart';
import 'package:rent2buy/networking/network_status.dart';
import 'package:rent2buy/screens/common/notifications/notification_viewmodel.dart';
import 'package:rent2buy/screens/common/progress_view.dart';
import 'package:rent2buy/utils/strings.dart';
import 'package:rent2buy/widgets/shared_widgets.dart';

class Notifications extends StatefulWidget {
  final NotificationViewModel viewModel =
      serviceLocator<NotificationViewModel>();
  @override
  _NotificationsState createState() => _NotificationsState();
}

class _NotificationsState extends State<Notifications> {
  final List<NotificationResponse> _notifications = List();

  @override
  void initState() {
    widget.viewModel.getNotifications();
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: BaseAppBar(
        title: 'Notifications',
        automaticallyImplyLeading: true,
        titleColor: Color(0xFF97080E),
        iconTheme: Color(0xFF97080E),
        widgets: [],
        appBar: AppBar(),
      ),
      body: ValueListenableBuilder(
          valueListenable: widget.viewModel.notificationValue,
          builder: (context, value, _) {
            var response = value as NetworkResponseList<NotificationResponse>;
            switch (response.status) {
              case NetworkStatus.LOADING:
                return CircularProgress();
              case NetworkStatus.ERROR:
                Fluttertoast.showToast(msg: response.message);
                break;
              case NetworkStatus.SUCCESS:
                WidgetsBinding.instance.addPostFrameCallback((_) {
                  setState(() {
                    _notifications.clear();
                    _notifications.addAll(response.response);
                  });
                });
                return Container(
                  child: ListView.builder(
                      itemCount: _notifications.length,
                      itemBuilder: (BuildContext context, int index) {
                        return Column(
                          children: [
                            ListTile(
                              contentPadding:
                                  EdgeInsets.symmetric(horizontal: 50),
                              title: Text(
                                _notifications[index].body,
                                style: TextStyle(
                                    fontWeight: FontWeight.w600, fontSize: 20),
                              ),
                              onTap: () {

                              },
                            ),
                            Divider(
                              color: Color(0xFFF7A74D),
                              height: 5.0,
                              thickness: 3,
                            ),
                          ],
                        );
                      }),
                );
            }
            return Container(
                height: double.infinity,
                width: double.infinity,
                child: Text(Strings.msgNoNotifications));
          }),
    );
  }
}
