import 'dart:io';

import 'package:flutter/material.dart';
import 'package:fluttertoast/fluttertoast.dart';
import 'package:rent2buy/di/service_locator.dart';
import 'package:rent2buy/models/order_item.dart';
import 'package:rent2buy/models/product.dart';
import 'package:rent2buy/networking/network_status.dart';
import 'package:rent2buy/screens/buyer/orders/feedback_viewmodel.dart';
import 'package:rent2buy/screens/common/chat/buyer_chat_screen.dart';
import 'package:rent2buy/screens/common/loading_dialog.dart';
import 'package:rent2buy/services/apiProvider.dart';
import 'package:rent2buy/utils/constants.dart';
import 'package:rent2buy/utils/dimens.dart';
import 'package:rent2buy/utils/strings.dart';
import 'package:rent2buy/widgets/shared_widgets.dart';
import 'package:shared_preferences/shared_preferences.dart';
import 'package:smooth_star_rating/smooth_star_rating.dart';
import 'package:http/http.dart' as http;

class FeedBack extends StatefulWidget {
  final FeedBackViewModel viewModel = serviceLocator<FeedBackViewModel>();
  final OrderItem orderItem;

  FeedBack({Key key, this.orderItem}) : super(key: key);

  @override
  _FeedBackState createState() => _FeedBackState();
}

class _FeedBackState extends State<FeedBack> {
  Product product;
  TextEditingController _reviewController = TextEditingController();

  double currentRating = 0.0;

  GlobalKey<State> _keyLoader = GlobalKey<State>();

  var _keyScafold = GlobalKey<ScaffoldState>();

  @override
  void initState() {
    // TODO: implement initState
    super.initState();
    product = widget.orderItem.orderProduct;
    widget.viewModel.getFeedback(product.id);
    widget.viewModel.productValue.addListener(() {
      var response = widget.viewModel.productValue.value;
      if (response.status == NetworkStatus.SUCCESS) {
        var newProduct = response.response;
        setState(() {
          currentRating = newProduct.rating;
          _reviewController.text = newProduct.review;
        });
      }
    });
    widget.viewModel.submitValue.addListener(() {
      var response = widget.viewModel.submitValue.value;
      switch (response.status) {
        case NetworkStatus.LOADING:
          Dialogs.showLoadingDialog(_keyScafold.currentContext, _keyLoader,
              Strings.msgSubmittingReview);
          break;
        case NetworkStatus.ERROR:
          Navigator.pop(_keyLoader.currentContext);
          Fluttertoast.showToast(msg: response.message);
          break;
        case NetworkStatus.SUCCESS:
          Navigator.pop(_keyLoader.currentContext);
          Navigator.pop(_keyScafold.currentContext);
          break;
      }
    });

    widget.viewModel.disputeValue.addListener(() {
      var response = widget.viewModel.disputeValue.value;
      switch (response.status) {
        case NetworkStatus.LOADING:
          Dialogs.showLoadingDialog(
              _keyScafold.currentContext, _keyLoader, Strings.loading);
          break;
        case NetworkStatus.ERROR:
          Navigator.pop(_keyLoader.currentContext);
          Fluttertoast.showToast(msg: response.message);
          break;
        case NetworkStatus.SUCCESS:
          Navigator.pop(_keyLoader.currentContext);
          Navigator.pop(_keyScafold.currentContext);
          break;
      }
    });
  }

  @override
  Widget build(BuildContext context) {

    return Scaffold(
      key: _keyScafold,
      appBar: BaseAppBar(
        appBar: AppBar(),
        automaticallyImplyLeading: true,
        title: '',
        iconTheme: Color(0xFF97080E),
        widgets: [
          InkWell(
              onTap: () {
                showAlertDialog(
                    context,
                    Strings.txtDisputeItem,
                    Strings.msgDisputeItem,
                    Strings.cancel,
                    Strings.dispute, () {
                  widget.viewModel
                      .disputeProduct(widget.orderItem.orderProduct.id);
                });
              },
              child: Container(
                  padding: EdgeInsets.all(padding_large),
                  margin: EdgeInsets.only(right: margin_large),
                  child: Image.asset('assets/dispute.png', height: 32)))
        ],
      ),
      body: SingleChildScrollView(
        child: Container(
          child: Column(
            children: [
              SizedBox(
                height: 250,
                child: Image.network(
                  product.images.path.isNotEmpty
                      ? product.images.path.first
                      : "",
                  width: MediaQuery.of(context).size.width,
                  fit: BoxFit.cover,
                ),
              ),
              Container(
                padding: EdgeInsets.all(16),
                child: Column(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: [
                    Text(product.name, style: TextStyle(fontSize: 20)),
                    SizedBox(height: 10),
                    Text(product.description, style: TextStyle(fontSize: 20))
                  ],
                ),
              ),
              Text('Rate Now',
                  style: TextStyle(color: Color(0xFF97080E), fontSize: 24)),
              SizedBox(height: 10),
              SmoothStarRating(
                size: 48,
                color: Color(0xFF97080E),
                rating: currentRating,
                isReadOnly: product.rating != null,
                allowHalfRating: false,
                borderColor: Color(0xFF97080E),
                starCount: 5,
                onRated: (value) {
                  setState(() {
                    currentRating = value;
                  });
                },
              ),
              SizedBox(height: 10),
              Container(
                margin:
                    EdgeInsets.only(left: margin_large, right: margin_large),
                width: double.infinity,
                child: TextFormField(
                  keyboardType: TextInputType.multiline,
                  controller: _reviewController,
                  decoration: InputDecoration(
                      enabled: true,
                      enabledBorder: OutlineInputBorder(
                          borderSide:
                              BorderSide(color: Color(0xFF97080E), width: 4),
                          borderRadius: BorderRadius.all(Radius.circular(12))),
                      focusedBorder: OutlineInputBorder(
                          borderSide:
                              BorderSide(color: Color(0xFF97080E), width: 4),
                          borderRadius: BorderRadius.all(Radius.circular(12))),
                      contentPadding: EdgeInsets.all(50)),
                ),
              ),
              SizedBox(height: 15),
              MyButton(
                color: Color(0xFF97080E),
                text: 'Submit review',
                onPressed: () {
                  Map<String, dynamic> params = Map();
                  params[orderId] = widget.orderItem.id;
                  params[listingId] = widget.orderItem.orderProduct.id;
                  params[rating] = currentRating;
                  params[review] = _reviewController.text;
                  widget.viewModel.submitReview(params);
                },
              )
            ],
          ),
        ),
      ),
    );
  }

  showAlertDialog(BuildContext context, String title, String message,
      String btnCancel, String btnOk, VoidCallback pCallBack) {
    // set up the buttons
    Widget cancelButton = FlatButton(
      child: Text(btnCancel),
      onPressed: () {
        Navigator.of(context, rootNavigator: true).pop('dialog');
      },
    );
    Widget continueButton = FlatButton(
      child: Text(btnOk),
      onPressed: pCallBack,
    );

    // set up the AlertDialog
    AlertDialog alert = AlertDialog(
      title: Text(title),
      content: Text(message),
      actions: [
        cancelButton,
        continueButton,
      ],
    );

    // show the dialog
    newMethod(context, alert);
  }

  Future newMethod(BuildContext context, AlertDialog alert) {
    return showDialog(
      context: context,
      builder: (BuildContext context) {
        return alert;
      },
    );
  }
}
