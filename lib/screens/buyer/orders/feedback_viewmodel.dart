import 'package:flutter/material.dart';
import 'package:rent2buy/di/service_locator.dart';
import 'package:rent2buy/models/common_repository.dart';
import 'package:rent2buy/models/product.dart';
import 'package:rent2buy/networking/network_response.dart';
import 'package:rent2buy/networking/network_responselist.dart';
import 'package:rent2buy/utils/base_change_notifier.dart';

class FeedBackViewModel extends BaseChangeNotifier {
  final CommonRepository _commonRepository = serviceLocator<CommonRepository>();

  ValueNotifier<NetworkResponse<String>> submitValue =
      ValueNotifier(NetworkResponse.idle());

  ValueNotifier<NetworkResponse<String>> disputeValue =
      ValueNotifier(NetworkResponse.idle());

  ValueNotifier<NetworkResponse<Product>> productValue =
      ValueNotifier(NetworkResponse.idle());

  void submitReview(Map<String, dynamic> params) {
    submitValue.value = NetworkResponse.loading();
    _commonRepository.submitReview(params).then((response) {
      submitValue.value =
          NetworkResponse.successWithMsg(null, response.message);
    }).catchError((e) {
      submitValue.value = NetworkResponse.error(parseError(e));
    });
  }

  void submitFeedback(Map<String, dynamic> params) {
    submitValue.value = NetworkResponse.loading();
    _commonRepository.submitFeedback(params).then((response) {
      submitValue.value =
          NetworkResponse.successWithMsg(null, response.message);
    }).catchError((e) {
      submitValue.value = NetworkResponse.error(parseError(e));
    });
  }

  void getFeedback(String id) {
    productValue.value = NetworkResponse.loading();
    _commonRepository.getFeedback(id).then((response) {
      productValue.value = NetworkResponse.success(response?.response?.first);
    }).catchError((e) {
      productValue.value = NetworkResponse.error(parseError(e));
    });
  }

  void disputeProduct(String listingId) {
    disputeValue.value = NetworkResponse.loading();
    _commonRepository.markDispute(listingId).then((response) {
      disputeValue.value =
          NetworkResponse.successWithMsg(null, response.message);
    }).catchError((e) {
      disputeValue.value = NetworkResponse.error(parseError(e));
    });
  }
}
