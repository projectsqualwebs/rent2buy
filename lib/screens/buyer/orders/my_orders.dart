import 'dart:convert';
import 'dart:io';

import 'package:flutter/material.dart';
import 'package:rent2buy/di/service_locator.dart';
import 'package:rent2buy/models/order.dart';
import 'package:rent2buy/models/order_item.dart';
import 'package:rent2buy/models/product_model.dart';
import 'package:rent2buy/networking/network_responselist.dart';
import 'package:rent2buy/networking/network_status.dart';
import 'package:rent2buy/networking/standard_response_list.dart';
import 'package:rent2buy/screens/buyer/orders/feedback.dart';
import 'package:rent2buy/screens/buyer/orders/my_orderviewmodel.dart';
import 'package:rent2buy/screens/common/error_screen.dart';
import 'package:rent2buy/screens/common/progress_view.dart';
import 'package:rent2buy/services/apiProvider.dart';
import 'package:rent2buy/utils/design_utils.dart';
import 'package:rent2buy/utils/utils.dart';
import 'package:rent2buy/widgets/shared_widgets.dart';
import 'package:shared_preferences/shared_preferences.dart';
import 'package:http/http.dart' as http;

class MyOrders extends StatefulWidget {
  final MyOrderViewModel viewModel = serviceLocator<MyOrderViewModel>();
  static final routeName = '/my_orders';
  @override
  _MyOrdersState createState() => _MyOrdersState();
}

class _MyOrdersState extends State<MyOrders> with WidgetsBindingObserver {
  List<OrderItem> myOrders = List();
  List<String> orderDates = List();

  var _keyList = GlobalKey<AnimatedListState>();

  @override
  void initState() {
    WidgetsBinding.instance.addObserver(this);
    getOrders();
    super.initState();
  }

  void getOrders() {
    widget.viewModel.getOrders();
  }

  @override
  void didChangeAppLifecycleState(AppLifecycleState state) {
    // TODO: implement didChangeAppLifecycleState
    super.didChangeAppLifecycleState(state);
    if (state == AppLifecycleState.resumed) {
      getOrders();
    }
  }

  @override
  void dispose() {
    WidgetsBinding.instance.removeObserver(this);
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: BaseAppBar(
        title: 'My Orders',
        automaticallyImplyLeading: true,
        titleColor: Color(0xFF97080E),
        iconTheme: Color(0xFF97080E),
        widgets: [],
        appBar: AppBar(),
      ),
      body: ValueListenableBuilder(
        valueListenable: widget.viewModel.orderValue,
        builder: (context, value, child) {
          var response = value as NetworkResponseList<Order>;
          switch (response.status) {
            case NetworkStatus.LOADING:
              return CircularProgress();
            case NetworkStatus.ERROR:
              return ErrorScreen(response.message);
            case NetworkStatus.SUCCESS:
              addItems(response.response);
              return AnimatedList(
                  key: _keyList,
                  initialItemCount: myOrders.length,
                  itemBuilder:
                      (BuildContext context, int index, Animation animation) {
                    return _buildItem(index, myOrders[index]);
                  });
          }
          return Container();
        },
      ),
    );
  }

  void addItems(List<Order> response) {
    WidgetsBinding.instance.addPostFrameCallback((_) {
      myOrders.clear();
      for (Order order in response) {
        for (OrderItem orderItem in order?.orderItems) {
          orderItem.date = Utils.getDate(order.createdAt);
          orderItem.id = order.sId;
        }
        myOrders.addAll(order?.orderItems);
      }

      for (int i = 0; i < myOrders.length; i++) {
        _keyList.currentState.insertItem(i);
      }
    });
  }

  Widget _buildItem(index, OrderItem orderItem) {
    try {
      return Column(
        children: [
          Padding(
            padding: const EdgeInsets.all(12.0),
            child: ListTile(
              leading: Image.network(
                orderItem.orderProduct.images.path.isNotEmpty
                    ? orderItem.orderProduct.images.path[0]
                    : "",
                fit: BoxFit.fitHeight,
                width: 100,
                height: 100,
              ),
              title: Text(
                orderItem.orderProduct.name,
                style: DesignUtils.textApperanceRegular(fontSize: 16),
              ),
              subtitle: Padding(
                padding: const EdgeInsets.only(top: 8.0),
                child: Text(
                  orderItem.date,
                  style: TextStyle(fontSize: 14),
                ),
              ),
              trailing: Column(
                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                children: [
                  Text(
                    Utils.getPriceInDollar(orderItem
                        .orderProduct.purchaseActions.first.discountPrice),
                    style: DesignUtils.textApperanceRegular(fontSize: 16),
                  ),
                  Text(
                    'Bought',
                    style: TextStyle(color: Colors.green[600], fontSize: 14),
                  )
                ],
              ),
              onTap: () {
                Navigator.push(
                    context,
                    MaterialPageRoute(
                        builder: (context) => FeedBack(
                              orderItem: orderItem,
                            )));
              },
            ),
          ),
          Divider(
            color: Color(0xFFF7A74D),
            height: 5.0,
            thickness: 3,
          ),
        ],
      );
    } catch (e) {}
  }
}
