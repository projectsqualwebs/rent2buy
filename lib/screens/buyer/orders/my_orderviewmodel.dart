import 'package:flutter/material.dart';
import 'package:rent2buy/di/service_locator.dart';
import 'package:rent2buy/models/order.dart';
import 'package:rent2buy/networking/network_responselist.dart';
import 'package:rent2buy/networking/repository/order_repository.dart';
import 'package:rent2buy/networking/standard_response.dart';
import 'package:rent2buy/networking/standard_response_list.dart';
import 'package:rent2buy/utils/base_change_notifier.dart';

class MyOrderViewModel extends BaseChangeNotifier {
  final OrderRepository _orderRepository = serviceLocator<OrderRepository>();
  ValueNotifier<NetworkResponseList<Order>> orderValue =
      ValueNotifier(NetworkResponseList.idle());

  void getOrders() {
    orderValue.value = NetworkResponseList.loading();
    _orderRepository.getOrders().then((response) {
      print(response.response.toList());
      orderValue.value = NetworkResponseList.success(response.response);
    }).catchError((e) {
      orderValue.value = NetworkResponseList.error(parseError(e));
    });
  }
}
