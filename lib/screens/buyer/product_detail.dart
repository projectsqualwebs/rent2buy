import 'dart:convert';
import 'dart:io';
import 'package:carousel_pro/carousel_pro.dart';
import 'package:flutter/material.dart';
import 'package:fluttertoast/fluttertoast.dart';
import 'package:intl/intl.dart';
import 'package:rent2buy/di/service_locator.dart';
import 'package:rent2buy/models/bundle.dart';
import 'package:rent2buy/models/product.dart';
import 'package:rent2buy/models/purchase_actions.dart';
import 'package:rent2buy/networking/network_response.dart';
import 'package:badges/badges.dart';
import 'package:rent2buy/networking/network_status.dart';
import 'package:rent2buy/screens/buyer/cart/my_cart.dart';
import 'package:rent2buy/screens/common/error_screen.dart';
import 'package:rent2buy/screens/common/login/login.dart';
import 'package:rent2buy/screens/common/progress_view.dart';
import 'package:rent2buy/screens/payment/payment_screen.dart';
import 'package:rent2buy/screens/seller/listing/product_detail_viewmodel.dart';
import 'package:rent2buy/services/apiProvider.dart';
import 'package:rent2buy/utils/cart_manager.dart';
import 'package:rent2buy/utils/colors.dart';
import 'package:rent2buy/utils/constants.dart';
import 'package:rent2buy/utils/data_manager.dart';
import 'package:rent2buy/utils/design_utils.dart';
import 'package:rent2buy/utils/dimens.dart';
import 'package:rent2buy/utils/strings.dart';
import 'package:rent2buy/utils/time_utils.dart';
import 'package:rent2buy/utils/utils.dart';
import 'package:rent2buy/widgets/shared_widgets.dart';
import 'package:http/http.dart' as http;
import 'package:rent2buy/utils/constants.dart' as Constants;
import 'package:shared_preferences/shared_preferences.dart';
import 'package:smooth_star_rating/smooth_star_rating.dart';

class ProductDetails extends StatefulWidget {
  final CartManager cartManager = serviceLocator<CartManager>();
  static const routeName = '/product_detail';
  final ProductDetailViewModel viewModel =
      serviceLocator<ProductDetailViewModel>();

  ProductDetails({Key key}) : super(key: key);

  @override
  _ProductDetailsState createState() => _ProductDetailsState();
}

class _ProductDetailsState extends State<ProductDetails> {
  int quantity = 1;
  int index = 0;
  Bundle id;
  GlobalKey<ScaffoldState> _scaffoldState = GlobalKey<ScaffoldState>();

  bool loading = false;
  bool isAddedToCart = false;
  String fromDate = Strings.txtPickUpDate, toDate = Strings.txtDropOffDate;
  String currentPurchaseAction;
  int currentItemCount = 0;

  String dropOffTime = Strings.txtDropOffTime,
      pickUpTime = Strings.txtPickUpTime;

  bool isBuy = false, isCart = false;

  @override
  void initState() {
    super.initState();

    Future.delayed(Duration.zero, () async {
      id = ModalRoute.of(context).settings.arguments;
      currentPurchaseAction = id.purchaseAction;
      widget.viewModel.getSingleProduct(id.id);
      getCart();
    });

    widget.viewModel.cartItemsValue.addListener(() {
      var response = widget.viewModel.cartItemsValue.value;
      if (response.status == NetworkStatus.SUCCESS) {
        String cartId = response.response.items.isNotEmpty
            ? response.response.items.first.cart.id
            : "";
        widget.cartManager.saveCartListingId(cartId);
        setState(() {
          currentItemCount = response.response.items.length;
        });
      }
    });

    widget.viewModel.clearValue.addListener(() {
      var response = widget.viewModel.clearValue.value;
      if (response.status == NetworkStatus.SUCCESS) {
        _scaffoldState.currentState
            .showSnackBar(showMessage(response.response));
        getCart();
      }
    });

    widget.viewModel.cartValue.addListener(() {
      var response = widget.viewModel.cartValue.value;
      switch (response.status) {
        case NetworkStatus.LOADING:
          loading = true;
          break;
        case NetworkStatus.ERROR:
          loading = false;
          _scaffoldState.currentState
              .showSnackBar(showMessage(response.message));
          break;
        case NetworkStatus.SUCCESS:
          loading = false;
          getCart();
          if (response.statuscode == 201) {
            //Alread other items ,clear cart
            showAlertDialog(
                context,
                Strings.msgClearCart,
                Strings.txtAlreadyItems,
                Strings.cancel,
                Strings.msgClearCart, () async {
              String cartId = await widget.cartManager.getCartListingId();
              widget.viewModel.clearCart(cartId);
              Navigator.of(context, rootNavigator: true).pop('dialog');
            });
          } else {
            widget.cartManager.saveCartId(response.response.cartUniqueId);
            _scaffoldState.currentState
                .showSnackBar(showMessage(response.message));
            isAddedToCart = true;

            if (isBuy) {
              goToCart();
            }
          }

          break;
      }
      setState(() {
        this.loading = loading;
        this.isAddedToCart = isAddedToCart;
      });
    });
  }

  showAlertDialog(BuildContext context, String title, String message,
      String btnCancel, String btnOk, VoidCallback pCallBack) {
    // set up the buttons
    Widget cancelButton = FlatButton(
      child: Text(btnCancel),
      onPressed: () {
        Navigator.of(context, rootNavigator: true).pop('dialog');
      },
    );
    Widget continueButton = FlatButton(
      child: Text(btnOk),
      onPressed: pCallBack,
    );

    // set up the AlertDialog
    AlertDialog alert = AlertDialog(
      title: Text(title),
      content: Text(message),
      actions: [
        cancelButton,
        continueButton,
      ],
    );

    // show the dialog
    newMethod(context, alert);
  }

  Future newMethod(BuildContext context, AlertDialog alert) {
    return showDialog(
      context: context,
      builder: (BuildContext context) {
        return alert;
      },
    );
  }

  @override
  void dispose() {
    super.dispose();
    widget.viewModel.dispose();
  }

  @override
  Widget build(BuildContext context) {
    Product product;
    return Scaffold(
      key: _scaffoldState,
      appBar: BaseAppBar(
        title: '',
        titleColor: Color(0xFF97080E),
        iconTheme: Color(0xFF97080E),
        automaticallyImplyLeading: true,
        widgets: [
          InkWell(
            onTap: () async{
              if (await widget.viewModel.isGuestUser()) {
              Navigator.pushNamed(context, Login.routeName);
              showMessage(Strings.please_login_to_your_account);
              return;
              }
              Navigator.pushNamed(context, MyCart.routeName);
            },
            child: Container(
              margin: EdgeInsets.only(right: margin_large),
              child: Badge(
                badgeColor: colorAccent,
                animationType: BadgeAnimationType.scale,
                badgeContent: Text(currentItemCount.toString()),
                child: Icon(Icons.shopping_cart),
              ),
            ),
          ),
        ],
        appBar: AppBar(),
      ),
      body: Stack(
        children: [
          ValueListenableBuilder(
            valueListenable: widget.viewModel.productValue,
            builder: (context, value, child) {
              var response = value as NetworkResponse<Product>;
              switch (response.status) {
                case NetworkStatus.LOADING:
                  return new CircularProgress();
                case NetworkStatus.ERROR:
                  return new ErrorScreen(response.message);
                case NetworkStatus.SUCCESS:
                  product = response.response;
                  currentPurchaseAction =
                      Utils.getPurchaseActions(product.purchaseActions);
                  List<NetworkImage> productImages = List<NetworkImage>();

                  for (String url in product.images.path) {
                    productImages.add(NetworkImage(url));
                  }

                  return SingleChildScrollView(
                    child: Column(
                      mainAxisAlignment: MainAxisAlignment.start,
                      crossAxisAlignment: CrossAxisAlignment.start,
                      children: [
                        GestureDetector(
                          onTap: () {
                            Navigator.pushNamed(context, '/image_slider');
                          },
                          child: SizedBox(
                            width: MediaQuery.of(context).size.width,
                            height: 250,
                            child: Carousel(
                              images: productImages,
                              dotSize: 4.0,
                              dotBgColor: Colors.transparent,
                              dotSpacing: 12.0,
                              dotColor: colorAccentLight,
                              dotIncreasedColor: colorAccent,
                            ),
                          ),
                        ),
                        Container(
                          padding: EdgeInsets.all(16),
                          child: Row(
                            mainAxisAlignment: MainAxisAlignment.spaceBetween,
                            children: [
                              Column(
                                children: [
                                  Visibility(
                                    visible: getSellVisibility(
                                        product.purchaseActions),
                                    child: Text(
                                        Utils.getPriceInDollar(product
                                            .purchaseActions.first.price),
                                        style: TextStyle(
                                            color: Color(0xFF97080E),
                                            fontSize: 24)),
                                  ),
                                  Visibility(
                                    visible: getRentPriceVisibility(
                                        product.purchaseActions),
                                    child: Text(
                                        getRentPricePerDay(
                                            product.purchaseActions),
                                        style: TextStyle(
                                            color: Color(0xFF97080E),
                                            fontSize: 24)),
                                  ),
                                  Visibility(
                                    visible: getRentPriceVisibility(
                                        product.purchaseActions),
                                    child: Text(
                                        getRentPricePerHour(
                                            product.purchaseActions),
                                        style: TextStyle(
                                            color: Color(0xFF97080E),
                                            fontSize: 24)),
                                  ),
                                ],
                              ),
                              Column(
                                mainAxisAlignment:
                                    MainAxisAlignment.spaceAround,
                                children: [
                                  Text(
                                      getPurchaseAction(
                                          product.purchaseActions),
                                      style: TextStyle(
                                          color: Color(0xFF97080E),
                                          fontSize: 24)),
                                  Text('Available',
                                      style: TextStyle(
                                          color: Colors.green, fontSize: 22)),
                                ],
                              )
                            ],
                          ),
                        ),
                        Container(
                          padding: EdgeInsets.only(left: 12, right: 12),
                          child: Column(
                            crossAxisAlignment: CrossAxisAlignment.start,
                            children: [
                              Text(
                                product.name,
                                style: TextStyle(fontSize: 20),
                              ),
                              SizedBox(height: 10),
                              Text(
                                product.description,
                                style: TextStyle(fontSize: 20),
                              ),
                              Visibility(
                                visible:
                                    getSellVisibility(product.purchaseActions),
                                child: Row(
                                  children: [
                                    Text('Quantity :',
                                        style: TextStyle(fontSize: 20)),
                                    CustomStepper(
                                      lowerLimit: 1,
                                      upperLimit: 6,
                                      stepValue: 1,
                                      iconSize: 24,
                                      value: 1,
                                      onChanged: (value) {
                                        quantity = value;
                                      },
                                    ),
                                  ],
                                ),
                              ),
                              Row(children: [
                                Text('Customer Ratings :',
                                    style: TextStyle(fontSize: 20)),
                                Padding(
                                  padding: const EdgeInsets.only(left: 8.0),
                                  child: SmoothStarRating(
                                    size: 24,
                                    isReadOnly: true,
                                    color: Color(0xFF97080E),
                                    rating: product.rating == null
                                        ? 0
                                        : product.rating,
                                    allowHalfRating: false,
                                    borderColor: Color(0xFF97080E),
                                    starCount: 5,
                                  ),
                                ),
                              ]),
                              SizedBox(
                                height: 40,
                              ),
                              Visibility(
                                visible: currentPurchaseAction == ACTION_RENT ||
                                    currentPurchaseAction == ACTION_BOTH,
                                child: Padding(
                                  padding: const EdgeInsets.symmetric(
                                      horizontal: 12.0),
                                  child: Row(
                                    mainAxisAlignment:
                                        MainAxisAlignment.spaceEvenly,
                                    children: [
                                      SizedBox(
                                        height: 40,
                                        child: OutlineButton.icon(
                                          textColor: Color(0xFF97080E),
                                          onPressed: () async {
                                            final DateTime toDateVal =
                                                await showDatePicker(
                                                    context: context,
                                                    initialDate: DateTime.now(),
                                                    firstDate: DateTime.now(),
                                                    lastDate: DateTime(
                                                        DateTime.now().year +
                                                            1));
                                            fromDate = DateFormat('yyyy/MM/dd')
                                                .format(toDateVal);
                                            setState(() {});
                                            print(fromDate);
                                          },
                                          icon: Icon(Icons.date_range),
                                          label: Text(
                                            fromDate,
                                            style: TextStyle(
                                                fontSize: textSizeLarge,
                                                height: 1.1),
                                          ),
                                          shape: DesignUtils
                                              .getDefaultRoundedBorder(),
                                        ),
                                      ),
                                      SizedBox(
                                        height: 40,
                                        child: OutlineButton.icon(
                                          onPressed: () async {
                                            TimeOfDay time =
                                                await showTimePicker(
                                                    initialTime:
                                                        TimeOfDay.now(),
                                                    context: context,
                                                    builder:
                                                        (BuildContext context,
                                                            Widget child) {
                                                      return MediaQuery(
                                                          data: MediaQuery.of(
                                                                  context)
                                                              .copyWith(
                                                                  alwaysUse24HourFormat:
                                                                      true),
                                                          child: child);
                                                    });
                                            setState(() {
                                              pickUpTime =
                                                  TimeUtils.getTimeInPeriod(
                                                      time);
                                            });
                                          },
                                          icon: Icon(
                                            Icons.access_time,
                                            color: Color(0xFF97080E),
                                          ),
                                          label: Text(
                                            pickUpTime,
                                            style: TextStyle(
                                                color: Color(0xFF97080E),
                                                fontSize: textSizeLarge),
                                          ),
                                          shape: DesignUtils
                                              .getDefaultRoundedBorder(),
                                        ),
                                      )
                                    ],
                                  ),
                                ),
                              ),
                              SizedBox(
                                height: 20,
                              ),
                              Visibility(
                                visible: currentPurchaseAction == ACTION_RENT ||
                                    currentPurchaseAction == ACTION_BOTH,
                                child: Padding(
                                  padding: const EdgeInsets.symmetric(
                                      horizontal: 12.0),
                                  child: Row(
                                    mainAxisAlignment:
                                        MainAxisAlignment.spaceEvenly,
                                    children: [
                                      SizedBox(
                                        height: 40,
                                        child: OutlineButton.icon(
                                          textColor: Color(0xFF97080E),
                                          onPressed: () async {
                                            final DateTime toDateVal =
                                                await showDatePicker(
                                                    context: context,
                                                    initialDate: DateTime.now(),
                                                    firstDate: DateTime.now(),
                                                    lastDate: DateTime(
                                                        DateTime.now().year +
                                                            1));
                                            toDate = DateFormat('yyyy/MM/dd')
                                                .format(toDateVal);
                                            setState(() {});
                                            print(toDate);
                                          },
                                          icon: Icon(Icons.date_range),
                                          label: Text(
                                            toDate,
                                            style: TextStyle(
                                                fontSize: textSizeLarge,
                                                height: 1.1),
                                          ),
                                          shape: DesignUtils
                                              .getDefaultRoundedBorder(),
                                        ),
                                      ),
                                      SizedBox(
                                        height: 40,
                                        child: OutlineButton.icon(
                                          onPressed: () async {
                                            TimeOfDay time =
                                                await showTimePicker(
                                                    initialTime:
                                                        TimeOfDay.now(),
                                                    context: context,
                                                    builder:
                                                        (BuildContext context,
                                                            Widget child) {
                                                      return MediaQuery(
                                                          data: MediaQuery.of(
                                                                  context)
                                                              .copyWith(
                                                                  alwaysUse24HourFormat:
                                                                      true),
                                                          child: child);
                                                    });

                                            setState(() {
                                              dropOffTime =
                                                  TimeUtils.getTimeInPeriod(
                                                      time);
                                            });
                                          },
                                          icon: Icon(
                                            Icons.access_time,
                                            color: Color(0xFF97080E),
                                          ),
                                          label: Text(
                                            dropOffTime,
                                            style: TextStyle(
                                                color: Color(0xFF97080E),
                                                fontSize: textSizeLarge),
                                          ),
                                          shape: DesignUtils
                                              .getDefaultRoundedBorder(),
                                        ),
                                      )
                                    ],
                                  ),
                                ),
                              ),
                            ],
                          ),
                        ),
                      ],
                    ),
                  );
              }

              return Container();
            },
          ),
          Visibility(visible: loading, child: CircularProgress()),
        ],
      ),
      bottomNavigationBar: Padding(
        padding: const EdgeInsets.only(bottom: 12.0),
        child: Row(
          mainAxisAlignment: MainAxisAlignment.spaceAround,
          children: [
            FlatButton(
              child: Text(
                isAddedToCart ? Strings.goToCart : Strings.addToCart,
                style: TextStyle(
                    color: Color(0xFFF7A74D),
                    fontSize: 18,
                    fontWeight: FontWeight.bold),
              ),
              onPressed: () {
                isCart = true;
                isBuy = false;
                print(quantity);
                if (isAddedToCart) {
                  goToCart();
                } else {
                  if (currentPurchaseAction == ACTION_BOTH) {
                    showOptions(context, () {
                      //On Buy
                      addToCart(product, ACTION_SELL);
                    }, () {
                      addToCart(product, ACTION_RENT);
                    });
                    return;
                  }
                  addToCart(product, currentPurchaseAction);
                }
              },
            ),
            MyButton(
                text: Strings.buyNow,
                color: Color(0xFF97080E),
                onPressed: () {
                  isBuy = true;
                  isCart = false;
                  if (isAddedToCart) {
                    goToCart();
                  } else {
                    if (currentPurchaseAction == ACTION_BOTH) {
                      showOptions(context, () {
                        //On Buy
                        addToCart(product, ACTION_SELL);
                      }, () {
                        addToCart(product, ACTION_RENT);
                      });
                      return;
                    }
                    addToCart(product, currentPurchaseAction);
                  }
                }),
          ],
        ),
      ),
    );
  }

  void addForRent(Product product) async {
    if (await widget.viewModel.isGuestUser()) {
      Navigator.pushNamed(context, Login.routeName);
      showMessage(Strings.please_login_to_your_account);
      return;
    }
    Map<String, Object> params = Map();
    params[Constants.listingId] = product.id;
    params[Constants.actionType] = Constants.ACTION_RENT;
    params[Constants.quanity] = quantity.toString();
    String cartId = await widget.cartManager.getCartId();
    widget.viewModel.addToCart(params, cartId);
  }

  addToCart(Product product, String action) async {
    if (await widget.viewModel.isGuestUser()) {
      Navigator.pushNamed(context, Login.routeName);
      showMessage(Strings.please_login_to_your_account);
      return;
    }
    Map<String, Object> params = Map();
    if (action == ACTION_RENT) {
      if (fromDate == Strings.txtPickUpDate) {
        Fluttertoast.showToast(msg: Strings.msgSelectPickupDate);
        return;
      } else if (pickUpTime == Strings.txtPickUpTime) {
        Fluttertoast.showToast(msg: Strings.msgSelectPickupTime);
        return;
      } else if (toDate == Strings.txtDropOffDate) {
        Fluttertoast.showToast(msg: Strings.msgSelectDropOffDate);
        return;
      } else if (dropOffTime == Strings.txtDropOffTime) {
        Fluttertoast.showToast(msg: Strings.msgSelectDropOffTime);
        return;
      }
      params[Constants.pickDate] = fromDate;
      params[Constants.pickTime] = pickUpTime;
      params[Constants.dropDate] = toDate;
      params[Constants.dropTime] = dropTime;
    }

    params[Constants.listingId] = product.id;
    params[Constants.actionType] = action;
    if (action == ACTION_SELL) {
      params[Constants.quanity] = quantity.toString();
    }
    String cartId = await widget.cartManager.getCartId();
    widget.viewModel.addToCart(params, cartId);
  }

  SnackBar showMessage(String message) {
    return SnackBar(content: Text(message));
  }

  void goToCart() async {
    String cartId = await widget.cartManager.getCartId();
    if (await widget.viewModel.isGuestUser()) {
      Navigator.pushNamed(context, Login.routeName);
      showMessage(Strings.please_login_to_your_account);
      return;
    }
    Navigator.push(_scaffoldState.currentContext,
        MaterialPageRoute(builder: (context) => MyCart(cart_id: cartId)));
  }

  getSellVisibility(List<PurchaseActions> purchaseActions) {
    if (purchaseActions.length == 2) return true;
    if (purchaseActions.first.name == Constants.ACTION_SELL) return true;
    return false;
  }

  String getRentPricePerDay(List<PurchaseActions> purchaseActions) {
    if (purchaseActions.length == 2)
      return Utils.getPriceInDollar(purchaseActions.last.price[perDay]) +
          " for 1 day";
    if (currentPurchaseAction == ACTION_SELL) return "";
    return Utils.getPriceInDollar(purchaseActions.first.price[perDay]) +
        " for 1 day";
  }

  String getRentPricePerHour(List<PurchaseActions> purchaseActions) {
    if (purchaseActions.length == 2)
      return Utils.getPriceInDollar(purchaseActions.last.price[perHour]) +
          " for 1 hour";
    if (currentPurchaseAction == ACTION_SELL) return "";
    return Utils.getPriceInDollar(purchaseActions.first.price[perDay]) +
        " for 1 hour";
  }

  getRentPriceVisibility(List<PurchaseActions> purchaseActions) {
    if (purchaseActions.length == 2) return true;
    if (purchaseActions.first.name == Constants.ACTION_RENT) return true;
    return false;
  }

  String getPurchaseAction(List<PurchaseActions> purchaseActions) {
    if (purchaseActions.length == 2) return "Sell and Rent";
    if (purchaseActions.first.name == ACTION_SELL) {
      return "Sell Only";
    } else {
      return "Rent Only";
    }
  }

  void showOptions(BuildContext context, Function onBuy, Function onRent) {
    showDialog(
      context: context,
      builder: (context) {
        return SimpleDialog(
          title: Text(Strings.msgSelectOption,
              style: DesignUtils.textApperanceTitle()),
          children: [
            SimpleDialogOption(
              child: Text(Strings.buy),
              onPressed: onBuy,
            ),
            SimpleDialogOption(
              child: Text(Strings.rent),
              onPressed: onRent,
            ),
          ],
        );
      },
    );
  }

  void getCart() async {
    String cartUniqueId = await widget.cartManager.getCartId();
    widget.viewModel.getCartItems(cartUniqueId);
  }
}
