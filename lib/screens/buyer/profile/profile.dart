import 'dart:convert';
import 'dart:io';
import 'package:flutter/material.dart';
import 'package:flutter_datetime_picker/flutter_datetime_picker.dart';
import 'package:flutter_google_places/flutter_google_places.dart';
import 'package:fluttertoast/fluttertoast.dart';
import 'package:rent2buy/di/service_locator.dart';
import 'package:rent2buy/models/User/profileUpdate.dart';
import 'package:rent2buy/models/User/updateProfile.dart';
import 'package:rent2buy/models/User/userAddress.dart';
import 'package:rent2buy/models/address.dart';
import 'package:rent2buy/models/userinfo.dart';
import 'package:rent2buy/networking/network_response.dart';
import 'package:rent2buy/networking/network_status.dart';
import 'package:rent2buy/screens/buyer/profile/profile_viewmodel.dart';
import 'package:rent2buy/screens/common/error_screen.dart';
import 'package:rent2buy/screens/common/progress_view.dart';
import 'package:rent2buy/screens/common/reset/reset_password.dart';
import 'package:rent2buy/services/apiProvider.dart';
import 'package:rent2buy/utils/data_manager.dart';
import 'package:rent2buy/utils/strings.dart';
import 'package:rent2buy/widgets/shared_widgets.dart';
import 'package:http/http.dart' as http;
import 'package:shared_preferences/shared_preferences.dart';
import 'package:google_maps_webservice/places.dart';
import 'package:geocoder/geocoder.dart';
import 'package:rent2buy/models/address.dart' as UserAddress;
import 'package:rent2buy/utils/constants.dart' as Constants;

const kGoogleApiKey = "AIzaSyCOPAn9bpwFSq7s2Tj0datcxhofCEunPZA";

class Profile extends StatefulWidget {
  final ProfileViewModel viewModel = serviceLocator<ProfileViewModel>();
  final DataManager dataManager = serviceLocator<DataManager>();
  @override
  _ProfileState createState() => _ProfileState();
}

class _ProfileState extends State<Profile> {
  GoogleMapsPlaces _places = GoogleMapsPlaces(apiKey: kGoogleApiKey);
  String showLocation = 'Location';
  String dob;
  String _date = "  " + "Select DOB";
  String fname;
  String lname;
  double lat;
  double long;
  ProfileUpdate profile = ProfileUpdate();
  TextEditingController first_name = TextEditingController();
  TextEditingController last_name = TextEditingController();
  TextEditingController current_location = TextEditingController();
  var date_of_birth = TextEditingController();
  Userinfo currentUserInfo;
  Map<String, dynamic> addressParams = Map();

  var _keyForm = GlobalKey<FormState>();

  bool isLoading = false;

  @override
  void initState() {
    super.initState();
    widget.viewModel.getProfile();
    widget.viewModel.udpateValue.addListener(() {
      var response = widget.viewModel.udpateValue.value;
      WidgetsBinding.instance.addPostFrameCallback((_) async {
        switch (response.status) {
          case NetworkStatus.LOADING:
            isLoading = true;
            break;
          case NetworkStatus.ERROR:
            isLoading = false;
            Fluttertoast.showToast(msg: response.message);
            break;
          case NetworkStatus.SUCCESS:
            isLoading = false;
            widget.viewModel.getProfile();
            break;
        }
      });
    });
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        backgroundColor: Color(0xFFF7A74D),
        title: Text('R2B',
            style: TextStyle(
                fontFamily: 'Rammetto One',
                color: Color(0xFF97080E),
                fontSize: 24)),
        centerTitle: true,
        automaticallyImplyLeading: true,
      ),
      body: ValueListenableBuilder(
        valueListenable: widget.viewModel.userinfoValue,
        builder: (context, value, _) {
          var response = value as NetworkResponse<Userinfo>;
          switch (response.status) {
            case NetworkStatus.LOADING:
              return CircularProgress();
            case NetworkStatus.ERROR:
              return ErrorScreen(response.message);
            case NetworkStatus.SUCCESS:
              WidgetsBinding.instance.addPostFrameCallback((_) async {
                currentUserInfo = response.response;
                Userinfo oldUserInfo =
                    Userinfo.fromJson(await widget.dataManager.getUser());

                var defaultAddress = currentUserInfo.address.where((x) {
                  return x.defaultAddress == 1;
                })?.first;

                first_name.text = currentUserInfo.firstName;
                last_name.text = currentUserInfo.lastName;
                current_location.text = defaultAddress.location;
                date_of_birth.text = currentUserInfo.dob;
                addressParams = defaultAddress.toJson();

                //Save New Profile
                var token = oldUserInfo.token;
                currentUserInfo.token = token;
                widget.dataManager.saveUser(currentUserInfo);
              });

              return SingleChildScrollView(
                child: Container(
                  padding: EdgeInsets.all(20),
                  child: Column(
                    mainAxisSize: MainAxisSize.min,
                    mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                    children: [
                      SizedBox(height: 20),
                      Text(
                        'Profile',
                        style: TextStyle(
                            fontWeight: FontWeight.w700, fontSize: 28),
                      ),
                      SizedBox(height: 30),
                      Form(
                        key: _keyForm,
                        child: Column(
                          children: [
                            AuthTextField(
                                validator: (String value) {
                                  if (value.isEmpty) {
                                    return Strings.fieldRequired;
                                  }
                                  return null;
                                },
                                hinttext: 'First Name',
                                prefixIcon: Icon(Icons.person_outline),
                                isPassword: false,
                                color: Color(0xFFF7A74D),
                                textEditingController: first_name,
                                isNumber: false,
                                suffixIcon: Icon(Icons.edit)),
                            SizedBox(height: 10),
                            AuthTextField(
                                validator: (String value) {
                                  if (value.isEmpty) {
                                    return Strings.fieldRequired;
                                  }
                                  return null;
                                },
                                hinttext: 'Last Name',
                                prefixIcon: Icon(Icons.person_outline),
                                isPassword: false,
                                color: Color(0xFFF7A74D),
                                textEditingController: last_name,
                                isNumber: false,
                                suffixIcon: Icon(Icons.edit)),
                            SizedBox(height: 10),
                            TextFormField(
                              validator: (String value) {
                                if (value.isEmpty) {
                                  return Strings.fieldRequired;
                                }
                                return null;
                              },
                              controller: date_of_birth,
                              decoration: InputDecoration(
                                  prefixIcon: Icon(Icons.date_range),
                                  hintText: Strings.dateOfBirth),
                              keyboardType: TextInputType.text,
                              onTap: () {
                                DatePicker.showDatePicker(context,
                                    theme: DatePickerTheme(
                                      containerHeight: 210.0,
                                    ),
                                    showTitleActions: true,
                                    minTime: DateTime(1965, 1, 1),
                                    maxTime: DateTime(2015, 12, 31),
                                    onConfirm: (date) {
                                  print('confirm $date');
                                  _date = '  ' +
                                      '${date.year}-${date.month}-${date.day}';
                                  dob =
                                      '${date.year}-${date.month}-${date.day}';
                                  date_of_birth.text =
                                      '${date.year}-${date.month}-${date.day}';
                                },
                                    currentTime: DateTime.now(),
                                    locale: LocaleType.en);
                              },
                            ),
                            SizedBox(height: 10),
                            AuthTextField(
                                validator: (String value) {
                                  if (value.isEmpty) {
                                    return Strings.fieldRequired;
                                  }
                                  return null;
                                },
                                hinttext: Strings.location,
                                prefixIcon: Icon(Icons.location_on),
                                isPassword: false,
                                color: Color(0xFFF7A74D),
                                textEditingController: current_location,
                                isNumber: false,
                                onTap: () async {
                                  Prediction p = await PlacesAutocomplete.show(
                                      context: context,
                                      apiKey: kGoogleApiKey,
                                      mode: Mode.overlay);
                                  displayPrediction(p, context);
                                },
                                suffixIcon: Icon(Icons.edit)),
                          ],
                        ),
                      ),
                      Column(
                        children: [
                          Row(
                            mainAxisAlignment: MainAxisAlignment.end,
                            children: [
                              FlatButton(
                                  onPressed: () {
                                    // Navigator.pushNamed(
                                    //     context, "/search_results");

                                    Navigator.pushNamed(
                                        context, ResetPasswordScreen.routeName);
                                  },
                                  materialTapTargetSize:
                                      MaterialTapTargetSize.shrinkWrap,
                                  child: Text(
                                    'Reset Password?',
                                    style: TextStyle(color: Colors.grey[500]),
                                  )),
                            ],
                          ),
                        ],
                      ),
                      Padding(
                        padding: const EdgeInsets.all(8.0),
                        child: MyButton(
                            text: 'Save Changes',
                            color: Color(0xFF97080E),
                            onPressed: () {
                              if (_keyForm.currentState.validate()) {
                                Map<String, dynamic> params = Map();
                                List<UserAddress.Address> addressList = List();
                                params[Constants.firstName] = first_name.text;
                                params[Constants.lastName] = last_name.text;
                                params[Constants.dateOfBirth] =
                                    date_of_birth.text;
                                addressList.add(UserAddress.Address.fromJson(
                                    addressParams));
                                params[Constants.address] = addressList;
                                widget.viewModel.updateProfile(params);
                              }
                            }),
                      ),
                      Padding(
                        padding: const EdgeInsets.all(16.0),
                        child: GestureDetector(
                          onTap: () => Navigator.pop(context),
                          child: Text(
                            'Cancel',
                            style: TextStyle(
                                fontSize: 16, color: Color(0xFFF7A74D)),
                          ),
                        ),
                      ),
                    ],
                  ),
                ),
              );
          }
          return Container();
        },
      ),
    );
  }

  Future<Null> displayPrediction(Prediction p, BuildContext context) async {
    if (p != null) {
      PlacesDetailsResponse detail =
          await _places.getDetailsByPlaceId(p.placeId);

      var placeId = p.placeId;
      lat = detail.result.geometry.location.lat;
      long = detail.result.geometry.location.lng;

      var address = await Geocoder.local.findAddressesFromQuery(p.description);
      setState(() {
        showLocation = p.description;
        addressParams[Constants.location] = p.description;
        addressParams[Constants.latitude] = lat;
        addressParams[Constants.longitude] = long;
      });
      print(p.description);
      print(lat);
      print(long);
    }
  }
}
