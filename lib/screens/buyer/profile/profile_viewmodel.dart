import 'package:flutter/material.dart';
import 'package:rent2buy/di/service_locator.dart';
import 'package:rent2buy/models/userinfo.dart';
import 'package:rent2buy/networking/network_response.dart';
import 'package:rent2buy/networking/network_responselist.dart';
import 'package:rent2buy/networking/repository/account_repository.dart';
import 'package:rent2buy/utils/base_change_notifier.dart';

class ProfileViewModel extends BaseChangeNotifier {
  final AccountRepository _accountRepository =
      serviceLocator<AccountRepository>();

  ValueNotifier<NetworkResponse<Userinfo>> userinfoValue =
      ValueNotifier(NetworkResponse.idle());

  ValueNotifier<NetworkResponse<Userinfo>> udpateValue =
      ValueNotifier(NetworkResponse.idle());

  void getProfile() {
    userinfoValue.value = NetworkResponse.loading();
    _accountRepository.getUserProfile().then((response) {
      userinfoValue.value = NetworkResponse.success(response.response);
    }).catchError((e) {
      userinfoValue.value = NetworkResponse.error(parseError(e));
    });
  }

  void updateProfile(Map<String, dynamic> params) {
    udpateValue.value = NetworkResponse.loading();
    _accountRepository.updateProfile(params).then((response) {
      udpateValue.value =
          NetworkResponse.successWithMsg(response.response, response.message);
    }).catchError((e) {
      udpateValue.value = NetworkResponse.error(parseError(e));
    });
  }
}
