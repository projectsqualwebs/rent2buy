import 'package:flutter/material.dart';
import 'package:rent2buy/widgets/shared_widgets.dart';
import 'package:font_awesome_flutter/font_awesome_flutter.dart';

class Qrcode extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: BaseAppBar(
        isCentered: true,
        title: 'QR Code',
        titleColor: Color(0xFF97080E),
        iconTheme: Color(0xFF97080E),
        widgets: [],
        automaticallyImplyLeading: true,
        appBar: AppBar(),
      ),
      body: Column(
        mainAxisAlignment: MainAxisAlignment.center,
        children: [
          Spacer(),
          Stack(children: [
            Center(
              child: Icon(
                Icons.qr_code_scanner,
                size: 192,
                color: Colors.grey,
              ),
            ),
            Padding(
              padding: const EdgeInsets.only(top: 20.0),
              child: Center(
                child: RawMaterialButton(
                  padding: EdgeInsets.all(32),
                  fillColor: Color(0xFFF7A74D),
                  shape: CircleBorder(),
                  child: Icon(
                    Icons.lock,
                    size: 80,
                    color: Color(0xFF97080E),
                  ),
                  onPressed: () {},
                ),
              ),
            )
          ]),
          Text(
            "Scan and Pay with any App",
            style: TextStyle(
                fontSize: 20,
                fontWeight: FontWeight.w600,
                color: Color(0xFF97080E)),
          ),
          SizedBox(height: 20),
          Container(
            padding: EdgeInsets.symmetric(horizontal: 20),
            child: Row(
              mainAxisAlignment: MainAxisAlignment.spaceAround,
              children: [
                MaterialButton(
                    shape: CircleBorder(),
                    child: Column(
                      children: [
                        FaIcon(
                          FontAwesomeIcons.paypal,
                          color: Colors.blue[700],
                        ),
                        Row(
                          mainAxisAlignment: MainAxisAlignment.center,
                          children: [
                            Text(
                              'Pay',
                              style: TextStyle(
                                  color: Colors.blue[700],
                                  fontWeight: FontWeight.bold,
                                  fontStyle: FontStyle.italic),
                            ),
                            Text('Pal',
                                style: TextStyle(
                                    color: Colors.blue,
                                    fontWeight: FontWeight.bold,
                                    fontStyle: FontStyle.italic))
                          ],
                        )
                      ],
                    ),
                    onPressed: () {}),
                MaterialButton(
                    padding: EdgeInsets.symmetric(vertical: 2, horizontal: 6),
                    shape: CircleBorder(),
                    child: FaIcon(
                      FontAwesomeIcons.ccAmex,
                      color: Colors.lightBlue,
                      size: 36,
                    ),
                    onPressed: () {}),
                MaterialButton(
                    color: Colors.white,
                    shape: CircleBorder(),
                    padding: EdgeInsets.all(6),
                    child: FaIcon(
                      FontAwesomeIcons.applePay,
                      color: Colors.black,
                      size: 36,
                    ),
                    onPressed: () {}),
                MaterialButton(
                    color: Colors.white,
                    shape: CircleBorder(),
                    padding: EdgeInsets.all(6),
                    child: Image.network(
                      'https://cdn.freebiesupply.com/logos/thumbs/2x/walmart-logo.png',
                      width: 60,
                      height: 40,
                      fit: BoxFit.fill,
                    ),
                    onPressed: () {})
              ],
            ),
          ),
          SizedBox(height: 20),
          Text(
            'Above logos are property of respective trademark owners.',
            style: TextStyle(color: Colors.grey[600]),
          ),
          Spacer(),
          Padding(
            padding: const EdgeInsets.only(bottom: 12.0),
            child: MyButton(text: 'Select', color: Color(0xFF97080E),onPressed: () {
              Navigator.pushNamed(context, '/payable');
            }),
          )
        ],
      ),
    );
  }
}
