import 'package:flutter/material.dart';
import 'package:rent2buy/widgets/shared_widgets.dart';

class RentConfirmation extends StatefulWidget {
  @override
  _RentConfirmationState createState() => _RentConfirmationState();
}

class _RentConfirmationState extends State<RentConfirmation> {
  String _pickUpTime = '8:00 AM';
  String _selectedlocation;
  static const _location = <String>[];
  final List<DropdownMenuItem<String>> _dropDownMenuItems = _location
      .map(
        (String value) => DropdownMenuItem<String>(
          value: value,
          child: Text(value,
              style: TextStyle(fontWeight: FontWeight.w600, fontSize: 20)),
        ),
      )
      .toList();

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: Container(
        padding: EdgeInsets.all(16),
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: [
            SizedBox(height: 30),
            DropdownButtonFormField(
              decoration: InputDecoration(
                enabled: true,
                enabledBorder: OutlineInputBorder(
                    borderSide:
                        BorderSide(color: Color(0xFF97080E), width: 4),
                    borderRadius: BorderRadius.all(Radius.circular(12))),
                focusedBorder: OutlineInputBorder(
                    borderSide:
                        BorderSide(color: Color(0xFF97080E), width: 4),
                    borderRadius: BorderRadius.all(Radius.circular(12))),
                contentPadding: EdgeInsets.fromLTRB(36, 24, 24, 24),
              ),
              icon: Icon(
                Icons.keyboard_arrow_down,
                color: Color(0xFF97080E),
              ),
              value: _selectedlocation,
              items: _dropDownMenuItems,
              hint: Text(
                'Pick-up Location',
                style: TextStyle(
                    fontWeight: FontWeight.w600,
                    fontSize: 20,
                    height: 1.1,
                    color: Color(0xFF97080E)),
              ),
              onChanged: ((String val) {
                setState(() {
                  _selectedlocation = val;
                });
              }),
            ),
            SizedBox(
              height: 20,
            ),
            Row(
              mainAxisAlignment: MainAxisAlignment.spaceBetween,
              children: [
                SizedBox(
                  height: 60,
                  child: OutlineButton.icon(
                    textColor: Color(0xFF97080E),
                    onPressed: () {},
                    icon: Icon(Icons.date_range),
                    label: Text(
                      'Pick-up Date',
                      style: TextStyle(fontSize: 24, height: 1.1),
                    ),
                    shape: RoundedRectangleBorder(
                        side: BorderSide(color: Colors.black, width: 4.0),
                        borderRadius: BorderRadius.circular(6)),
                  ),
                ),
                SizedBox(
                  width: 150,
                  child: DropdownButtonFormField(
                    decoration: InputDecoration(
                      enabled: true,
                      enabledBorder: OutlineInputBorder(
                          borderSide:
                              BorderSide(color: Color(0xFF97080E), width: 4),
                          borderRadius:
                              BorderRadius.all(Radius.circular(12))),
                      focusedBorder: OutlineInputBorder(
                          borderSide:
                              BorderSide(color: Color(0xFF97080E), width: 4),
                          borderRadius:
                              BorderRadius.all(Radius.circular(12))),
                    ),
                    icon: Icon(
                      Icons.keyboard_arrow_down,
                      color: Color(0xFF97080E),
                    ),
                    value: _selectedlocation,
                    items: _dropDownMenuItems,
                    hint: Text(
                      _pickUpTime,
                      style: TextStyle(
                          fontWeight: FontWeight.w600,
                          fontSize: 20,
                          color: Color(0xFF97080E)),
                    ),
                    onChanged: ((String val) {
                      setState(() {
                        _selectedlocation = val;
                      });
                    }),
                  ),
                )
              ],
            ),
            SizedBox(height: 20),
            Row(
              mainAxisAlignment: MainAxisAlignment.spaceBetween,
              children: [
                SizedBox(
                  height: 60,
                  child: OutlineButton.icon(
                    textColor: Color(0xFF97080E),
                    onPressed: () {},
                    icon: Icon(Icons.date_range),
                    label: Text(
                      'Drop-off Date',
                      style: TextStyle(fontSize: 24, height: 1.1),
                    ),
                    shape: RoundedRectangleBorder(
                        side: BorderSide(color: Colors.black, width: 4.0),
                        borderRadius: BorderRadius.circular(6)),
                  ),
                ),
                SizedBox(
                  width: 150,
                  child: DropdownButtonFormField(
                    decoration: InputDecoration(
                      enabled: true,
                      enabledBorder: OutlineInputBorder(
                          borderSide:
                              BorderSide(color: Color(0xFF97080E), width: 4),
                          borderRadius:
                              BorderRadius.all(Radius.circular(12))),
                      focusedBorder: OutlineInputBorder(
                          borderSide:
                              BorderSide(color: Color(0xFF97080E), width: 4),
                          borderRadius:
                              BorderRadius.all(Radius.circular(12))),
                    ),
                    icon: Icon(
                      Icons.keyboard_arrow_down,
                      color: Color(0xFF97080E),
                    ),
                    value: _selectedlocation,
                    items: _dropDownMenuItems,
                    hint: Text(
                      _pickUpTime,
                      style: TextStyle(
                          fontWeight: FontWeight.w600,
                          fontSize: 20,
                          color: Color(0xFF97080E)),
                    ),
                    onChanged: ((String val) {
                      setState(() {
                        _selectedlocation = val;
                      });
                    }),
                  ),
                )
              ],
            ),
            Container(
              padding: EdgeInsets.all(18),
              child: Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                children: [
                  Text(
                    'PRICE DETAILS',
                    style: TextStyle(fontSize: 18, color: Color(0xFF97080E)),
                  ),
                  SizedBox(
                    height: 10,
                  ),
                  Row(
                    mainAxisAlignment: MainAxisAlignment.spaceBetween,
                    children: [
                      Text('Price (1 Item)', style: TextStyle(fontSize: 20)),
                      Text(
                        '\$663.30',
                        style: TextStyle(fontSize: 20, color: Colors.grey),
                      ),
                    ],
                  ),
                  Row(
                    mainAxisAlignment: MainAxisAlignment.spaceBetween,
                    children: [
                      Text('Delivery Fee', style: TextStyle(fontSize: 20)),
                      Text(
                        '\$2.00',
                        style: TextStyle(fontSize: 20, color: Colors.grey),
                      ),
                    ],
                  ),
                  Row(
                    mainAxisAlignment: MainAxisAlignment.spaceBetween,
                    children: [
                      Text('Taxes', style: TextStyle(fontSize: 20)),
                      Text(
                        '\$2.00',
                        style: TextStyle(fontSize: 20, color: Colors.grey),
                      ),
                    ],
                  ),
                  SizedBox(
                    height: 10,
                  ),
                  Row(
                    mainAxisAlignment: MainAxisAlignment.spaceBetween,
                    children: [
                      Text('Total Amount', style: TextStyle(fontSize: 20)),
                      Text(
                        '\$667.30',
                        style: TextStyle(fontSize: 20),
                      ),
                    ],
                  )
                ],
              ),
            ),
            Spacer(),
            FlatButton(
                onPressed: () {},
                child: Text(
                  'Chat with Dealer',
                  style: TextStyle(
                      color: Color(0xFF97080E),
                      fontSize: 18,
                      fontWeight: FontWeight.bold),
                )),
            Spacer()
          ],
        ),
      ),
      bottomNavigationBar: Container(
        height: 150,
        child: Column(
          children: [
            Divider(
              color: Color(0xFFF7A74D),
              height: 5.0,
              thickness: 3,
            ),
            Padding(
              padding:
                  const EdgeInsets.symmetric(vertical: 8.0, horizontal: 20),
              child: Row(
                mainAxisAlignment: MainAxisAlignment.spaceAround,
                children: [
                  Text(
                    '\$663.30',
                    style: TextStyle(fontSize: 32, color: Color(0xFF97080E)),
                  ),
                  MyButton(
                    text: 'Pay Now',
                    color: Color(0xFF97080E),
                    onPressed: () {
                      Navigator.pushNamed(context, '/qr_code');
                    },
                  )
                ],
              ),
            ),
            Divider(
              color: Color(0xFFF7A74D),
              height: 5.0,
              thickness: 3,
            ),
            FlatButton(
                onPressed: () {Navigator.pop(context);},
                child: Text(
                  'Cancel',
                  style: TextStyle(
                      color: Color(0xFFF7A74D),
                      fontSize: 24,
                      fontWeight: FontWeight.bold),
                ))
          ],
        ),
      ),
    );
  }
}
