import 'dart:convert';
import 'dart:io';
import 'package:http/http.dart' as http;
import 'package:carousel_pro/carousel_pro.dart';
import 'package:flutter/material.dart';
import 'package:intl/intl.dart';
import 'package:rent2buy/services/apiProvider.dart';
import 'package:rent2buy/widgets/shared_widgets.dart';
import 'package:shared_preferences/shared_preferences.dart';
import 'package:smooth_star_rating/smooth_star_rating.dart';

class RentProductDetail extends StatefulWidget {
  Map<String, dynamic> product = Map();

  RentProductDetail({Key key, this.product}) : super(key: key);

  @override
  _RentProductDetailState createState() => _RentProductDetailState();
}

class _RentProductDetailState extends State<RentProductDetail> {
  String id;
  String fromDateTitle = 'Pick-Up Date';
  String toDateTitle = 'DropOff Date';
  String pickUpTitle = 'Pick-Up Time';
  String dropoffTitle = 'DropOff Time';
  String fromDate;
  String toDate;

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: BaseAppBar(
        title: '',
        titleColor: Color(0xFF97080E),
        iconTheme: Color(0xFF97080E),
        automaticallyImplyLeading: true,
        widgets: [
          IconButton(
              icon: Icon(Icons.shopping_cart),
              onPressed: () {
                Navigator.pushNamed(context, '/my_cart');
              })
        ],
        appBar: AppBar(),
      ),
      body: SingleChildScrollView(
        child: Column(
          mainAxisAlignment: MainAxisAlignment.start,
          crossAxisAlignment: CrossAxisAlignment.start,
          children: [
            GestureDetector(
              onTap: () {
                Navigator.pushNamed(context, '/image_slider');
              },
              child: Stack(children: <Widget>[
                SizedBox(
                  width: MediaQuery.of(context).size.width,
                  height: 250,
                  child: Carousel(
                    images: [
                      NetworkImage(
                        widget.product['images']['path'][0],
                      ),
                      Image.network(widget.product['images']['path'][0],fit: BoxFit.fill,)
                    ],
                    dotSize: 4.0,
                    dotBgColor: Colors.transparent,
                    dotSpacing: 12.0,
                    dotColor: Colors.transparent,
                  ),
                ),
                Positioned(
                  right: 10,
                  child: IconButton(
                      icon: Icon(
                        Icons.info,
                        color: Colors.blueAccent,
                        size: 36,
                      ),
                      onPressed: () {
                        showModalBottomSheet(
                            context: context,
                            builder: (context) => Container(
                              color: Colors.yellow.shade50,
                                  height: 250,
                                  child: Row(
                                    mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                                    children: [
                                      Column(
                                        mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                                        children: [
                                          Text('Discount',style: TextStyle(fontWeight: FontWeight.bold,fontSize: 16)),
                                          Text("More than 3 Days : "+widget.product["purchase_actions"][0]["discount_percent"]['more_than_3_days'].toString()+'%'),
                                          Text("More than 1 Week : "+widget.product["purchase_actions"][0]["discount_percent"]['more_than_1_week'].toString()+'%'),
                                          Text("More than 1 Month : "+widget.product["purchase_actions"][0]["discount_percent"]['more_than_1_month'].toString()+'%')
                                        ],
                                      ),
                                      VerticalDivider(thickness: 3,),
                                      Column(
                                        //mainAxisAlignment: MainAxisAlignment.spaceAround,
                                        children: [
                                          SizedBox(height: 35),
                                          Text('Availability',style: TextStyle(fontWeight: FontWeight.bold,fontSize: 16)),
                                          SizedBox(height: 35),
                                          Text("From : "+widget.product["purchase_actions"][0]["availability"]['available_from'].toString()),
                                          SizedBox(height: 35),
                                          Text("To : "+widget.product["purchase_actions"][0]["availability"]['available_to'].toString()),
                                        ],
                                      ),
                                    ],
                                  )
                                ));
                      }),
                )
              ]),
            ),
            Container(
              padding: EdgeInsets.all(16),
              child: Row(
                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                children: [
                  Column(
                    crossAxisAlignment:CrossAxisAlignment.start,
                    children: [
                      Text(
                          'Price : ' +
                              '\u0024' +
                              widget.product['purchase_actions'][0]['price']
                                      ['per_hour']
                                  .toString() +
                              '/hour',
                          style:
                              TextStyle(color: Color(0xFF97080E), fontSize: 24)),
                      Text(
                          'Price : ' +
                              '\u0024' +
                              widget.product['purchase_actions'][0]['price']
                                      ['per_day']
                                  .toString() +
                              '/day',
                          style:
                              TextStyle(color: Color(0xFF97080E), fontSize: 24)),
                    ],
                  ),
                  Column(
                    crossAxisAlignment: CrossAxisAlignment.end,
                    mainAxisAlignment: MainAxisAlignment.spaceAround,
                    children: [
                      Text('Rent',
                          style:
                              TextStyle(color: Color(0xFF97080E), fontSize: 24)),
                      Text('Available',
                          style: TextStyle(color: Colors.green, fontSize: 22)),
                    ],
                  )
                ],
              ),
            ),
            Container(
              padding: EdgeInsets.only(left: 12, right: 12),
              child: Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                children: [
                  Text(
                    widget.product["name"],
                    style: TextStyle(fontSize: 20),
                  ),
                  SizedBox(height: 10),
                  Text(
                    widget.product["description"],
                    style: TextStyle(fontSize: 20),
                  ),
                  SizedBox(height: 10),
                  Row(
                      children:[
                        Text('Customer Ratings :',style: TextStyle(fontSize: 20)),
                        Padding(
                          padding: const EdgeInsets.only(left:8.0),
                          child: SmoothStarRating(
                            isReadOnly: true,
                            size: 24,
                            color: Color(0xFF97080E),
                            rating: 3,
                            allowHalfRating: false,
                            borderColor: Color(0xFF97080E),
                            starCount: 5,

                          ),
                        ),
                      ]
                  )
                ],
              ),
            ),
            SizedBox(
              height:40,
            ),
            Padding(
              padding: const EdgeInsets.symmetric(horizontal:12.0),
              child: Row(
                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                children: [
                  SizedBox(
                    height: 40,
                    child: OutlineButton.icon(
                      textColor: Color(0xFF97080E),
                      onPressed: () async {
                        final DateTime toDateVal = await showDatePicker(
                            context: context,
                            initialDate: DateTime.now(),
                            firstDate: DateTime.now(),
                            lastDate: DateTime(2022));
                        fromDate = DateFormat('yyyy/MM/dd').format(toDateVal);
                        setState(() {
                          fromDateTitle = fromDate;
                        });
                        print(fromDate);
                      },
                      icon: Icon(Icons.date_range),
                      label: Text(
                        fromDateTitle,
                        style: TextStyle(fontSize: 24, height: 1.1),
                      ),
                      shape: RoundedRectangleBorder(
                          side: BorderSide(color: Colors.black, width: 4.0),
                          borderRadius: BorderRadius.circular(6)),
                    ),
                  ),
                  SizedBox(
                    height: 40,
                    child: OutlineButton.icon(
                        onPressed: () async {
                          TimeOfDay time = await showTimePicker(
                              initialTime: TimeOfDay.now(),
                              context: context,
                              builder: (BuildContext context, Widget child) {
                                return MediaQuery(
                                    data: MediaQuery.of(context)
                                        .copyWith(alwaysUse24HourFormat: true),
                                    child: child);
                              });
                          setState(() {
                            pickUpTitle = time.toString().substring(10, 15);
                          });
//                      DateTime temp = DateFormat("hh:mm").parse(time.format(context));
//                      print(time);
//                      print(DateFormat('HH:mm').format(temp));
                        },
                        icon: Icon(
                          Icons.access_time,
                          color: Color(0xFF97080E),
                        ),
                        label: Text(
                          pickUpTitle,
                          style: TextStyle(color: Color(0xFF97080E), fontSize: 24),
                        )),
                  )
                ],
              ),
            ),
            SizedBox(
              height: 20,
            ),
            Padding(
              padding: const EdgeInsets.symmetric(horizontal:12.0),
              child: Row(
                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                children: [
                  SizedBox(
                    height: 40,
                    child: OutlineButton.icon(
                      textColor: Color(0xFF97080E),
                      onPressed: () async {
                        final DateTime toDateVal = await showDatePicker(
                            context: context,
                            initialDate: DateTime.now(),
                            firstDate: DateTime.now(),
                            lastDate: DateTime(DateTime.now().year + 5));
                        toDate = DateFormat('yyyy/MM/dd').format(toDateVal);
                        setState(() {
                          toDateTitle = toDate;
                        });
                        print(toDate);
                      },
                      icon: Icon(Icons.date_range),
                      label: Text(
                        toDateTitle,
                        style: TextStyle(fontSize: 24, height: 1.1),
                      ),
                      shape: RoundedRectangleBorder(
                          side: BorderSide(color: Colors.black, width: 4.0),
                          borderRadius: BorderRadius.circular(6)),
                    ),
                  ),
                  SizedBox(
                    height: 40,
                    child: OutlineButton.icon(
                      onPressed: () async {
                        TimeOfDay time = await showTimePicker(
                            initialTime: TimeOfDay.now(),
                            context: context,
                            builder: (BuildContext context, Widget child) {
                              return MediaQuery(
                                  data: MediaQuery.of(context)
                                      .copyWith(alwaysUse24HourFormat: true),
                                  child: child);
                            });
//                      DateTime temp = DateFormat("hh:mm").parse(time.format(context));
//                      print(time);
//                      print(DateFormat('HH:mm').format(temp));
                        setState(() {
                          dropoffTitle = time.toString().substring(10, 15);
                          print(dropoffTitle);
                        });
                      },
                      icon: Icon(
                        Icons.access_time,
                        color: Color(0xFF97080E),
                      ),
                      label: Text(
                        dropoffTitle,
                        style: TextStyle(color: Color(0xFF97080E), fontSize: 24),
                      ),
                    ),
                  )
                ],
              ),
            ),
          ],
        ),
      ),
      bottomNavigationBar: Padding(
        padding: const EdgeInsets.only(bottom: 12.0),
        child: Row(
          mainAxisAlignment: MainAxisAlignment.spaceAround,
          children: [
            FlatButton(
              child: Text(
                'Add To Cart',
                style: TextStyle(
                    color: Color(0xFFF7A74D),
                    fontSize: 18,
                    fontWeight: FontWeight.bold),
              ),
              onPressed: () {
                addToCart();
              },
            ),
            MyButton(
                text: 'Buy Now!',
                color: Color(0xFF97080E),
                onPressed: () {
                  Navigator.pushNamed(context, '/qr_code');
                }),
          ],
        ),
      ),
    );
  }

  addToCart() async {
    SharedPreferences prefs = await SharedPreferences.getInstance();
    String token = prefs.getString('token');
    http.Response response =
        await http.post('${ApiProvider.addToCart}', headers: {
      HttpHeaders.authorizationHeader: 'Bearer $token'
    }, body: {
      'listing_id': widget.product["_id"],
      'action_type': widget.product['purchase_actions'][0]['name'],
      "pickup_date": '2020/12/20',
      "dropOff_date": '2021/01/30',
      "pickup_time": '12:00',
      "dropOff_time": '.1:00'
    }).then((value) {
      print(value.body);
      Map<String, dynamic> map = jsonDecode(value.body)['response'];
      id = map['cart_unique_id'];
      print(id);
      //Navigator.push(context, MaterialPageRoute(builder: (context) => MyCart(cart_id: id)));
    }).catchError((e) {
      print(e);
    });
  }
}
