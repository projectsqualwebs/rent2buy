import 'package:flutter/material.dart';
import 'package:rent2buy/services/User/forget_pwd.dart';
import 'package:rent2buy/widgets/shared_widgets.dart';

class ResetPassword extends StatefulWidget {
  @override
  _ResetPasswordState createState() => _ResetPasswordState();
}

class _ResetPasswordState extends State<ResetPassword> {
  TextEditingController _currentPwd = TextEditingController();
  TextEditingController _newPwd = TextEditingController();
  TextEditingController _confirmPwd = TextEditingController();

  @override
  Widget build(BuildContext context) {
    return SafeArea(
      child: Scaffold(
        appBar: appBar(Color(0xFF97080E)),
        body: SingleChildScrollView(
          child: Container(
            padding: EdgeInsets.symmetric(horizontal: 40),
            child: Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              children: [
                SizedBox(height: 60),
                Padding(
                  padding: EdgeInsets.only(left: 12.0),
                  child: Text('Reset Password',
                      style: TextStyle(
                          fontFamily: 'Dosis',
                          fontWeight: FontWeight.w700,
                          fontSize: 24)),
                ),
                SizedBox(height: 40),
                CustomTextField(
                  textEditingController: _currentPwd,
                    hinttext: 'Email Address',
                    borderColor: Color(0xFF97080E),
                    isPassword: false,
                    isNumber: false),
                SizedBox(height: 40),
                CustomTextField(
                  textEditingController: _newPwd,
                    hinttext: 'New Password',
                    borderColor: Color(0xFF97080E),
                    isPassword: true,
                    isNumber: false),
                SizedBox(height: 40),
                CustomTextField(
                  textEditingController: _confirmPwd,
                    hinttext: 'Confirm Password',
                    borderColor: Color(0xFF97080E),
                    isPassword: true,
                    isNumber: false),
                SizedBox(height: 60),
                Center(
                    child: MyButton(
                        text: 'Reset',
                        color: Color(0xFF97080E),
                        onPressed: () {
                          forgetPwd(_currentPwd.text, _newPwd.text, _confirmPwd.text,context);
                        }))
              ],
            ),
          ),
        ),
      ),
    );
  }
}
