import 'dart:convert';
import 'package:flutter_google_places/flutter_google_places.dart';
import 'package:geocoder/geocoder.dart';
import 'package:google_maps_webservice/places.dart';
import 'package:http/http.dart' as http;
import 'package:flutter/material.dart';
import 'package:intl/intl.dart';
import 'package:rent2buy/data.dart';
import 'package:rent2buy/di/service_locator.dart';
import 'package:rent2buy/models/User/userAddress.dart';
import 'package:rent2buy/models/address.dart' as UserAddress;
import 'package:rent2buy/models/category.dart';
import 'package:rent2buy/screens/buyer/search/search_screen.dart';
import 'package:rent2buy/screens/buyer/search_results.dart';
import 'package:rent2buy/screens/buyer/temp_search.dart';
import 'package:rent2buy/screens/common/loading_dialog.dart';
import 'package:rent2buy/services/apiProvider.dart';
import 'package:rent2buy/utils/constants.dart' as Constants;
import 'package:rent2buy/utils/design_utils.dart';
import 'package:rent2buy/utils/dimens.dart';
import 'package:rent2buy/utils/strings.dart';
import 'package:rent2buy/widgets/shared_widgets.dart';
import 'package:syncfusion_flutter_sliders/sliders.dart';
import 'package:syncfusion_flutter_core/theme.dart';

class Search extends StatefulWidget {
  @override
  _SearchState createState() => _SearchState();
}

class _SearchState extends State<Search> {
  final _bigFont = const TextStyle(
      fontWeight: FontWeight.w700, fontSize: 24, color: Color(0xff757575));
  String _selectedtype;
  String _selectedlocation;
  double _value = 0.0;
  List<ListingCategory> categories = List();
  GoogleMapsPlaces _places = GoogleMapsPlaces(apiKey: Constants.kGoogleApiKey);
  Map<String, dynamic> params = Map();
  static const _type = <String>[
    'Rent',
    'Buy',
  ];
  static const _location = <String>[
    'India',
    'USA',
    'Australia',
  ];
  Map<String, dynamic> regionMap = {
    "India": "IN",
    "USA": "US",
    "Autralia": "AU"
  };
  final List<DropdownMenuItem<String>> _dropDownMenuItemType = _type
      .map(
        (String value) => DropdownMenuItem<String>(
          value: value,
          child: Text(value,
              style: TextStyle(fontWeight: FontWeight.w600, fontSize: 20)),
        ),
      )
      .toList();
  final List<DropdownMenuItem<String>> _dropDownMenuItemLocation = _location
      .map(
        (String value) => DropdownMenuItem<String>(
          value: value,
          child: Text(value,
              style: TextStyle(fontWeight: FontWeight.w600, fontSize: 20)),
        ),
      )
      .toList();

  var keywordController = TextEditingController();
  var _keyForm = GlobalKey<FormState>();

  var _keyLoader = GlobalKey<State>();

  var _addressController = TextEditingController();

  var _isRent = false;
  var _startDateController = TextEditingController();
  var _endDateController = TextEditingController();

  DateTime fromDate;

  ListingCategory _selectedCategory;

  getCategory() async {
    var res = await http.get('${ApiProvider.getCategories}');
    Map<String, dynamic> map = json.decode(res.body);
    categories
        .add(ListingCategory(id: null, name: 'All Categories', images: null));
    categories.addAll(List<ListingCategory>.from(
        map['response'].map((x) => ListingCategory.fromJson(x))));
    setState(() {});
  }

  @override
  void initState() {
    this.getCategory();
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: BaseAppBar(
        titleColor: Color(0xFF97080E),
        iconTheme: Color(0xFF97080E),
        title: 'Search Here',
        automaticallyImplyLeading: true,
        appBar: AppBar(),
        widgets: [
          Padding(
            padding: const EdgeInsets.only(right: 12.0),
            child: Icon(Icons.refresh),
          )
        ],
      ),
      body: SingleChildScrollView(
        child: Container(
            padding: EdgeInsets.symmetric(horizontal: 30),
            child: Form(
              key: _keyForm,
              child: Column(
                mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                crossAxisAlignment: CrossAxisAlignment.start,
                children: [
                  SizedBox(height: 30),
                  CustomTextField(
                      validator: (String value) {
                        if (value.isEmpty) {
                          return Strings.fieldRequired;
                        }
                        return null;
                      },
                      textEditingController: keywordController,
                      hinttext: 'Search',
                      borderColor: Color(0xFF97080E),
                      isPassword: false,
                      isNumber: false),
                  SizedBox(height: 20),
                  Padding(
                    padding: EdgeInsets.only(left: 12.0),
                    child: Text(
                      'Categories',
                      style: _bigFont,
                    ),
                  ),
                  DropdownButtonFormField(
                      decoration: InputDecoration(
                        enabled: true,
                        enabledBorder: OutlineInputBorder(
                            borderSide:
                                BorderSide(color: Color(0xFF97080E), width: 4),
                            borderRadius:
                                BorderRadius.all(Radius.circular(12))),
                        focusedBorder: OutlineInputBorder(
                            borderSide:
                                BorderSide(color: Color(0xFF97080E), width: 4),
                            borderRadius:
                                BorderRadius.all(Radius.circular(12))),
                        contentPadding: EdgeInsets.fromLTRB(36, 24, 24, 24),
                      ),
                      icon: Icon(
                        Icons.keyboard_arrow_down,
                        color: Color(0xFF97080E),
                      ),
                      value: _selectedCategory,
                      items: categories.map((item) {
                        return DropdownMenuItem(
                          child: Text(item.name),
                          value: item,
                        );
                      }).toList(),
                      hint: Text(
                        'All Categories',
                        style: TextStyle(
                            fontWeight: FontWeight.w600,
                            fontSize: 20,
                            height: 1.1),
                      ),
                      onChanged: (val) {
                        setState(() {
                          _selectedCategory = val;
                        });
                      }),
                  SizedBox(height: 20),
                  Padding(
                    padding: EdgeInsets.only(left: 12.0),
                    child: Text(
                      'Rent or Buy',
                      style: _bigFont,
                    ),
                  ),
                  DropdownButtonFormField(
                    decoration: InputDecoration(
                      enabled: true,
                      enabledBorder: OutlineInputBorder(
                          borderSide:
                              BorderSide(color: Color(0xFF97080E), width: 4),
                          borderRadius: BorderRadius.all(Radius.circular(12))),
                      focusedBorder: OutlineInputBorder(
                          borderSide:
                              BorderSide(color: Color(0xFF97080E), width: 4),
                          borderRadius: BorderRadius.all(Radius.circular(12))),
                      contentPadding: EdgeInsets.fromLTRB(36, 24, 24, 24),
                    ),
                    icon: Icon(
                      Icons.keyboard_arrow_down,
                      color: Color(0xFF97080E),
                    ),
                    value: _selectedtype,
                    items: _dropDownMenuItemType,
                    hint: Text(
                      'Select Option',
                      style: TextStyle(
                          fontWeight: FontWeight.w600,
                          fontSize: 20,
                          height: 1.1),
                    ),
                    onChanged: ((String val) {
                      setState(() {
                        _isRent = val == 'Rent';
                        _selectedtype = val;
                      });
                    }),
                  ),
                  SizedBox(
                    height: margin_large,
                  ),
                  Visibility(
                    visible: _isRent,
                    child: Row(
                      children: [
                        Flexible(
                          flex: 1,
                          child: TextFormField(
                            controller: _startDateController,
                            onTap: () async {
                              final DateTime toDateVal = await showDatePicker(
                                context: context,
                                initialDate: DateTime.now(),
                                firstDate: DateTime.now(),
                                lastDate: DateTime(DateTime.now().year + 1),
                              );

                              setState(() {
                                _startDateController.text =
                                    DateFormat('yyyy/MM/dd').format(toDateVal);
                                fromDate = toDateVal;
                              });
                            },
                            decoration: DesignUtils.textInputLayoutOutLined(
                                Strings.startDate),
                            keyboardType: TextInputType.text,
                          ),
                        ),
                        SizedBox(
                          width: margin_large,
                        ),
                        Flexible(
                          flex: 1,
                          child: TextFormField(
                            onTap: () async {
                              final DateTime toDateVal = await showDatePicker(
                                context: context,
                                initialDate: fromDate,
                                firstDate: fromDate,
                                lastDate: DateTime(fromDate.year + 1),
                              );
                              _endDateController.text =
                                  DateFormat('yyyy/MM/dd').format(toDateVal);
                            },
                            controller: _endDateController,
                            decoration: DesignUtils.textInputLayoutOutLined(
                                Strings.endDate),
                            keyboardType: TextInputType.text,
                          ),
                        ),
                      ],
                    ),
                  ),
                  SizedBox(height: 20),
                  Padding(
                    padding: EdgeInsets.only(left: 12.0),
                    child: Text(
                      'Location',
                      style: _bigFont,
                    ),
                  ),
                  DropdownButtonFormField(
                    decoration: InputDecoration(
                      enabled: true,
                      enabledBorder: OutlineInputBorder(
                          borderSide:
                              BorderSide(color: Color(0xFF97080E), width: 4),
                          borderRadius: BorderRadius.all(Radius.circular(12))),
                      focusedBorder: OutlineInputBorder(
                          borderSide:
                              BorderSide(color: Color(0xFF97080E), width: 4),
                          borderRadius: BorderRadius.all(Radius.circular(12))),
                      contentPadding: EdgeInsets.fromLTRB(36, 24, 24, 24),
                    ),
                    icon: Icon(
                      Icons.keyboard_arrow_down,
                      color: Color(0xFF97080E),
                    ),
                    value: _selectedlocation,
                    items: _dropDownMenuItemLocation,
                    hint: Text(
                      'Select Option',
                      style: TextStyle(
                          fontWeight: FontWeight.w600,
                          fontSize: 20,
                          height: 1.1),
                    ),
                    onChanged: ((String val) {
                      setState(() {
                        _selectedlocation = val;
                      });
                    }),
                  ),
                  SizedBox(height: 20),
                  CustomTextField(
                    textEditingController: _addressController,
                    hinttext: 'Address',
                    isNumber: false,
                    borderColor: Color(0xFF97080E),
                    isPassword: false,
                    onTap: () async {
                      Prediction p = await PlacesAutocomplete.show(
                          context: context,
                          region: regionMap[_selectedlocation],
                          apiKey: Constants.kGoogleApiKey,
                          mode: Mode.overlay);
                      displayPrediction(p, context);
                    },
                  ),
                  Padding(
                    padding: EdgeInsets.only(left: 12.0),
                    child: Text(
                      'SELECT DISTANCE [KM]',
                      style: _bigFont,
                    ),
                  ),
                  SfSliderTheme(
                    data: SfSliderThemeData(
                        activeTrackColor: Color(0xFF97080E),
                        inactiveTrackColor: Color(0xFFF7A74D),
                        activeDivisorRadius: 5,
                        inactiveDivisorRadius: 5,
                        activeDivisorColor: Color(0xFF97080E),
                        inactiveDivisorColor: Color(0xFFF7A74D),
                        thumbColor: Color(0xFF97080E)),
                    child: SfSlider(
                      min: 0.0,
                      max: 250.0,
                      value: _value,
                      interval: 50,
                      stepSize: 50,
                      showLabels: true,
                      showDivisors: true,
                      onChanged: (dynamic value) {
                        setState(() {
                          _value = value;
                        });
                      },
                    ),
                  ),
                ],
              ),
            )),
      ),
      bottomNavigationBar: Padding(
        padding: const EdgeInsets.only(left: 100, right: 100, bottom: 10),
        child: MyButton(
            text: 'Search Now',
            color: Color(0xFF97080E),
            onPressed: () {
              if (_keyForm.currentState.validate()) {
                Navigator.push(
                    context,
                    MaterialPageRoute(
                        builder: (context) => SearchScreen(
                              keyword: keywordController.text,
                              category: _selectedCategory?.id,
                              location: UserAddress.Address.fromJson(params),
                            )));
              }
              // Navigator.push(context, MaterialPageRoute(builder: (context) => TempSearch(category_id: category_id,radius: _value),));
            }),
      ),
    );
  }

  Future<Null> displayPrediction(Prediction p, BuildContext context) async {
    if (p != null) {
      Dialogs.showLoadingDialog(
          context, _keyLoader, Strings.txtGettingLocation);
      PlacesDetailsResponse detail =
          await _places.getDetailsByPlaceId(p.placeId);

      var placeId = p.placeId;
      var lat = detail.result.geometry.location.lat;
      var long = detail.result.geometry.location.lng;

      var a = await Geocoder.local.findAddressesFromQuery(p.description);

      setState(() {
        _addressController.text = p.description;
        params[Constants.location] = p.description;
        params[Constants.latitude] = lat;
        params[Constants.longitude] = long;
        Navigator.of(_keyLoader.currentContext).pop();
      });
    }
  }
}
