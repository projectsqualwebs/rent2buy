import 'package:flutter/material.dart';
import 'package:fluttertoast/fluttertoast.dart';
import 'package:geolocator/geolocator.dart';
import 'package:rent2buy/di/service_locator.dart';
import 'package:rent2buy/models/address.dart';
import 'package:rent2buy/models/bundle.dart';
import 'package:rent2buy/models/product.dart';
import 'package:rent2buy/models/userinfo.dart';
import 'package:rent2buy/networking/network_response.dart';
import 'package:rent2buy/networking/network_responselist.dart';
import 'package:rent2buy/networking/network_status.dart';
import 'package:rent2buy/screens/buyer/product_detail.dart';
import 'package:rent2buy/screens/buyer/search/search_viewmodel.dart';
import 'package:rent2buy/screens/common/base_app_bar.dart';
import 'package:rent2buy/screens/common/progress_view.dart';
import 'package:rent2buy/utils/colors.dart';
import 'package:rent2buy/utils/constants.dart';
import 'package:rent2buy/utils/data_manager.dart';
import 'package:rent2buy/utils/design_utils.dart';
import 'package:rent2buy/utils/strings.dart';

class SearchScreen extends StatefulWidget {
  static var routeName = "/search_screen";
  final String category;
  final String action;
  final Address location;
  final keyword;
  final DataManager dataManager = serviceLocator<DataManager>();
  final SearchViewModel viewModel = serviceLocator<SearchViewModel>();

  SearchScreen({
    this.keyword,
    this.category,
    this.action,
    this.location,
  });

  @override
  State<StatefulWidget> createState() {
    return _SearchScreenState();
  }
}

class _SearchScreenState extends State<SearchScreen>
    with SingleTickerProviderStateMixin {
  String searchKeyword;
  final List<Product> listings = List();

  TabController _tabController;

  int _selectedTab = 0;

  Map<String, dynamic> locationParams = Map();

  var _titleController = TextEditingController();

  @override
  void initState() {
    // TODO: implement initState
    _titleController.text = widget.keyword;
    super.initState();
    initLocationParams();

    _tabController = TabController(length: 2, vsync: this);
    _tabController.addListener(() {
      if (_tabController.indexIsChanging) {
        _selectedTab = _tabController.index;
        if (_selectedTab == 0) {
          fetchSelfListing();
        } else {
          fetchDeliveryListing();
        }
      }
    });
    fetchSelfListing();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
        appBar: AppBar(
          title: TextField(
            decoration: InputDecoration(border: InputBorder.none),
            onTap: () {
              Navigator.pop(context);
            },
            controller: _titleController,
          ),
          actions: [
            IconButton(
                icon: Icon(Icons.dashboard_rounded),
                onPressed: () {
                  Navigator.pushNamed(context, '/home');
                }),
            IconButton(
                icon: Icon(Icons.location_on),
                onPressed: () {
                  Navigator.pushNamed(context, '/set_location');
                })
          ],
        ),
        body: Column(
          children: [
            TabBar(
              indicatorColor: colorAccent,
              controller: _tabController,
              tabs: [
                Tab(
                  child: Row(
                    children: [
                      Icon(
                        Icons.location_on,
                        color: colorAccent,
                      ),
                      Text(
                        Strings.txtPickUp,
                        style: DesignUtils.textApperanceMedium(),
                      ),
                    ],
                  ),
                ),
                Tab(
                  child: Row(
                    children: [
                      Icon(
                        Icons.local_library_outlined,
                        color: colorAccent,
                      ),
                      Text(
                        Strings.shipping,
                        style: DesignUtils.textApperanceMedium(),
                      ),
                    ],
                  ),
                )
              ],
            ),
            ValueListenableBuilder(
              valueListenable: widget.viewModel.searchValue,
              builder: (context, value, _) {
                var response = value as NetworkResponseList<Product>;
                switch (response.status) {
                  case NetworkStatus.LOADING:
                    return Expanded(
                      child: Container(
                          width: double.infinity,
                          height: double.infinity,
                          child: CircularProgress()),
                    );
                  case NetworkStatus.ERROR:
                    Fluttertoast.showToast(msg: response.message);
                    break;
                  case NetworkStatus.SUCCESS:
                    WidgetsBinding.instance.addPostFrameCallback((_) {
                      setState(() {
                        listings.clear();
                        listings.addAll(response.response);
                      });
                    });
                    return Expanded(
                      child: listings.isEmpty
                          ? _emptyView()
                          : TabBarView(
                              physics: NeverScrollableScrollPhysics(),
                              controller: _tabController,
                              children: [
                                searchPickUp(),
                                searchShipping(),
                              ],
                            ),
                    );
                }
                return Expanded(
                  child: _emptyView(),
                );
              },
            )
          ],
        ));
  }

  searchPickUp() {
    return GridView.builder(
        physics: ScrollPhysics(),
        scrollDirection: Axis.vertical,
        shrinkWrap: true,
        itemCount: listings.length,
        gridDelegate: SliverGridDelegateWithFixedCrossAxisCount(
            crossAxisCount: 2, crossAxisSpacing: 6, mainAxisSpacing: 10),
        itemBuilder: (BuildContext context, int index) {
          return _buildItem(index, listings[index]);
        });
  }

  searchShipping() {
    return GridView.builder(
        physics: ScrollPhysics(),
        scrollDirection: Axis.vertical,
        shrinkWrap: true,
        itemCount: listings.length,
        gridDelegate: SliverGridDelegateWithFixedCrossAxisCount(
            crossAxisCount: 2, crossAxisSpacing: 6, mainAxisSpacing: 10),
        itemBuilder: (BuildContext context, int index) {
          return _buildItem(index, listings[index]);
        });
  }

  _buildItem(int index, Product listing) {
    try {
      return InkWell(
        onTap: () {
          Navigator.pushNamed(context, ProductDetails.routeName,
              arguments: Bundle(listing.id));
        },
        child: Image.network(
          listing.images.path.isNotEmpty ? listing.images.path.first : "",
          fit: BoxFit.contain,
        ),
      );
    } catch (e) {
      debugPrint(e);
    }
  }

  void fetchSelfListing() async {
    String deliveryTypeID = await widget.dataManager.loadSelfPickupId();
    Map<String, dynamic> params = Map();
    params[category] = widget.category;
    params[deliveryType] = deliveryTypeID;
    params[keywords] = widget.keyword;

    widget.viewModel.getListings(params, locationParams);
  }

  void fetchDeliveryListing() async {
    String deliveryTypeID = await widget.dataManager.loadDeliveryId();
    Map<String, dynamic> params = Map();
    params[category] = widget.category;
    params[deliveryType] = deliveryTypeID;
    params[keywords] = widget.keyword;
    widget.viewModel.getListings(params, locationParams);
  }

  void initLocationParams() async {
    bool isGuestUser = await widget.dataManager.isGuestUser();

    if (isGuestUser) {
      getLocation();
    } else {
      Userinfo userinfo = Userinfo.fromJson(await widget.dataManager.getUser());

      var currentAddress = userinfo.address.firstWhere((x) {
        return x.defaultAddress == 1;
      }, orElse: () => null);

      if (currentAddress == null) {
        currentAddress = widget.location;
      }
      locationParams[lat] = currentAddress.latitude;
      locationParams[long] = currentAddress.longitude;
    }
  }

  _emptyView() {
    return Container(
      alignment: Alignment.center,
      child: Text(
        Strings.msgNoListingFound,
        style: DesignUtils.textApperanceRegular(),
      ),
    );
  }

  getLocation() async {
    LocationPermission permission = await Geolocator.requestPermission();
    Position position = await Geolocator.getCurrentPosition(
        desiredAccuracy: LocationAccuracy.high);
    setState(() {
      locationParams[lat] = position.latitude;
      locationParams[long] = position.longitude;
    });
  }
}
