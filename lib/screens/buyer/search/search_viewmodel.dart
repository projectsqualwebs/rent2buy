import 'package:flutter/material.dart';
import 'package:rent2buy/di/service_locator.dart';
import 'package:rent2buy/models/common_repository.dart';
import 'package:rent2buy/models/delivery_type.dart';
import 'package:rent2buy/models/product.dart';
import 'package:rent2buy/networking/network_responselist.dart';
import 'package:rent2buy/utils/base_change_notifier.dart';

class SearchViewModel extends BaseChangeNotifier {
  final CommonRepository commonRepository = serviceLocator<CommonRepository>();

  ValueNotifier<NetworkResponseList<Product>> searchValue =
      ValueNotifier(NetworkResponseList.idle());



  void getListings(
      Map<String, dynamic> params, Map<String, dynamic> queryParams) {
    searchValue.value = NetworkResponseList.loading();
    commonRepository.getListings(params, queryParams).then((response) {
      searchValue.value = NetworkResponseList.success(response.response);
    }).catchError((e) {
      searchValue.value = NetworkResponseList.error(parseError(e));
    });
  }

 
}
