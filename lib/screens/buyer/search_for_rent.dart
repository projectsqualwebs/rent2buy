import 'package:flutter/material.dart';
import 'package:geolocator/geolocator.dart';
import 'package:rent2buy/screens/buyer/search_for_rent_results.dart';
import 'package:rent2buy/widgets/shared_widgets.dart';
import 'package:syncfusion_flutter_sliders/sliders.dart';
import 'package:syncfusion_flutter_core/theme.dart';

class SearchForRent extends StatefulWidget {
  @override
  _SearchForRentState createState() => _SearchForRentState();
}

class _SearchForRentState extends State<SearchForRent> {
  double lat;
  double long;
  String start_date;
  String end_date;
  int distance;
  dynamic _value = 0.0;
  String btnname = "Select Duration";

  final _bigFont = const TextStyle(
      fontWeight: FontWeight.w700, fontSize: 24, color: Color(0xFF97080E));

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: BaseAppBar(
        isCentered: true,
        title: 'Search For Rent',
        titleColor: Color(0xFF97080E),
        iconTheme: Color(0xFF97080E),
        automaticallyImplyLeading: true,
        widgets: [
          Padding(
            padding: const EdgeInsets.all(8.0),
            child: Icon(Icons.refresh),
          )
        ],
        appBar: AppBar(),
      ),
      body: SingleChildScrollView(
        child: Container(
          padding: EdgeInsets.symmetric(horizontal: 30),
          child: Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: [
              SizedBox(height: 30),
              Center(
                child: SizedBox(
                  width: 300,
                  child: MyButton(
                    text: btnname,
                    color: Color(0xFF97080E),
                    onPressed: () async {
                      DateTimeRange date = await showDateRangePicker(firstDate: DateTime.now(),lastDate: DateTime(2022),context: context);
                      // duration = date.duration.inDays;
                      start_date = date.start.toString().substring(0,10);
                      end_date = date.end.toString().substring(0,10);
                      btnname = '$start_date' +' - '+ '$end_date';

                      setState(() {
                      });
                    },
                  ),
                ),
              ),
              SizedBox(height: 20),
              CustomIconButton(
                textColor: Color(0xFF97080E),
                hinttext: 'Get my Location',
                icon: Icon(Icons.location_pin, color: Color(0xFF97080E)),
                onPressed: () {
                  getLocation();
                },
              ),
              SizedBox(height: 30),
              Padding(
                padding: EdgeInsets.only(left: 12.0),
                child: Text(
                  'SELECT DISTANCE [KM]',
                  style: _bigFont,
                ),
              ),
              SfSliderTheme(
                data: SfSliderThemeData(
                    activeTrackColor: Color(0xFF97080E),
                    inactiveTrackColor: Color(0xFFF7A74D),
                    activeDivisorRadius: 5,
                    inactiveDivisorRadius: 5,
                    activeDivisorColor: Color(0xFF97080E),
                    inactiveDivisorColor: Color(0xFFF7A74D),
                    thumbColor: Color(0xFF97080E)),
                child: SfSlider(
                  min: 0.0,
                  max: 250.0,
                  value: _value,
                  interval: 50.0,
                  stepSize: 50.0,
                  showLabels: true,
                  showDivisors: true,
                  onChanged: (dynamic value) {
                    setState(() {
                      _value = value;
                      distance = _value.toInt();
                    });
                  },
                ),
              ),
            ],
          ),
        ),
      ),
      bottomNavigationBar: Padding(
        padding: const EdgeInsets.only(left: 140, right: 140, bottom: 30),
        child: MyButton(
            text: 'View',
            color: Color(0xFF97080E),
            onPressed: () async{
              print(start_date);
              print(end_date);
              Navigator.push(context, MaterialPageRoute(builder: (context) => RentSearchResults(start_date: start_date,end_date: end_date,lat: lat,long: long,radius: distance)));
            }),
      ),
    );
  }
  getLocation() async {
    LocationPermission permission = await Geolocator.requestPermission();
    Position position = await Geolocator.getCurrentPosition(desiredAccuracy: LocationAccuracy.high);
    lat = position.latitude;
    long = position.longitude;
    print(lat);
    print(long);
  }
}
