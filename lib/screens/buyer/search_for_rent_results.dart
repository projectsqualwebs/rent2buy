import 'dart:convert';

import 'package:flutter/material.dart';
import 'package:rent2buy/models/bundle.dart';
import 'package:rent2buy/screens/buyer/product_detail.dart';
import 'package:rent2buy/screens/buyer/rent_product_detail.dart';
import 'package:rent2buy/services/apiProvider.dart';
import 'package:rent2buy/widgets/shared_widgets.dart';
import 'package:http/http.dart' as http;

class RentSearchResults extends StatefulWidget {
  String start_date;
  String end_date;
  int radius;
  double lat;
  double long;

  RentSearchResults(
      {Key key,
      this.start_date,
      this.end_date,
      this.lat,
      this.long,
      this.radius})
      : super(key: key);

  @override
  _RentSearchResultsState createState() => _RentSearchResultsState();
}

class _RentSearchResultsState extends State<RentSearchResults> {
  List<dynamic> products = List();

  getResults() async {
    http.Response response =
        await http.post('${ApiProvider.searchForRent}', body: {
      'start_date': widget.start_date,
      'end_date': widget.end_date,
      'latitude': '22.719568',
      'longitude': '75.857727',
      'radius': widget.radius.toString()
    });
    products = jsonDecode(response.body)['response'];
    setState(() {});
  }

  @override
  void initState() {
    getResults();
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: BaseAppBar(
        title: 'Search Results',
        titleColor: Color(0xFF97080E),
        iconTheme: Color(0xFF97080E),
        isCentered: false,
        appBar: AppBar(),
        automaticallyImplyLeading: true,
        widgets: [],
      ),
      body: products == null
          ? Center(
              child: Text(
              'No Results Found!',
              style: TextStyle(fontSize: 24),
            ))
          : ListView.builder(
              reverse: true,
              itemCount: products.length,
              itemBuilder: (BuildContext context, int index) {
                return Column(
                  children: [
                    ListTile(
                      contentPadding: EdgeInsets.all(8),
                      leading: Image.network(
                        'https://apinizer.com/wp-content/uploads/revslider/the7-web-master-hero-image/laptop.png',
                        height: 60,
                      ),
                      title: Text(
                        products[index]["name"],
                        style: TextStyle(fontSize: 22),
                      ),
                      subtitle: Text(
                        products[index]['description'],
                        maxLines: 2,
                        overflow: TextOverflow.ellipsis,
                      ),
                      trailing: Padding(
                        padding: const EdgeInsets.only(right: 8.0),
                        child: Text(
                          products[index]['purchase_actions'][0]['name'],
                          style: TextStyle(
                              color: Color(0xFF97080E),
                              fontWeight: FontWeight.w600,
                              fontSize: 22),
                        ),
                      ),
                      onTap: () {
                        if (products[index]['purchase_actions'][0]['name'] ==
                            'sell') {
                          Navigator.pushNamed(context, ProductDetails.routeName,
                              arguments: Bundle(products[index].id));
                        } else {
                          Navigator.push(
                              context,
                              MaterialPageRoute(
                                  builder: (context) => RentProductDetail(
                                      product: products[index])));
                        }
                      },
                    ),
                    Divider(
                      color: Color(0xFFF7A74D),
                      height: 5.0,
                      thickness: 3,
                    ),
                  ],
                );
              }),
    );
  }
}
