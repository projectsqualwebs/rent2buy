import 'package:rent2buy/models/User/userRegister.dart';

class RegisterEvent {
  final UserRegister userRegister;

  const RegisterEvent(this.userRegister);
}
