import 'dart:collection';

import 'package:dio/dio.dart';
import 'package:flutter/cupertino.dart';
import 'package:rent2buy/di/service_locator.dart';
import 'package:rent2buy/networking/network_exceptions.dart';
import 'package:rent2buy/networking/network_response.dart';
import 'package:rent2buy/networking/network_status.dart';
import 'package:rent2buy/networking/repository/account_repository.dart';
import 'package:rent2buy/utils/strings.dart';

class RegisterViewModel extends ChangeNotifier {
  NetworkResponse<String> registerEvent;
  final AccountRepository _accountRepository =
      serviceLocator<AccountRepository>();

  RegisterViewModel() {
    setRegisterState(NetworkResponse.idle());
  }

  T tryCast<T>(dynamic x, {T fallback}) {
    try {
      return (x as T);
    } on CastError catch (e) {
      print('CastError when trying to cast $x to $T!');
      return fallback;
    }
  }

  void setRegisterState(NetworkResponse<String> response) {
    registerEvent = response;
    notifyListeners();
  }

  Future<String> register(Map<String, dynamic> params) {
    setRegisterState(NetworkResponse.loading());
    try {
      _accountRepository
          .registerUser(params)
          .then((value) =>
              setRegisterState(NetworkResponse.success(value.message)))
          .catchError((e) {
        if (e is DioError) {
          if (e.type == DioErrorType.RESPONSE) {
            setRegisterState(NetworkResponse.error(e.response.data['message']));
          } else {
            setRegisterState(NetworkResponse.error(
                NetworkExceptions.getErrorMessage(
                    NetworkExceptions.getDioException(e))));
          }
        } else {
          setRegisterState(NetworkResponse.error(Strings.somethingWentWrong));
        }
      });
    } catch (exception) {
      setRegisterState(NetworkResponse.error(exception.toString()));
    }
  }
}
