import 'package:flutter/gestures.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:flutter_datetime_picker/flutter_datetime_picker.dart';
import 'package:flutter_google_places/flutter_google_places.dart';
import 'package:font_awesome_flutter/font_awesome_flutter.dart';
import 'package:geocoder/geocoder.dart';
import 'package:geocoding/geocoding.dart';
import 'package:geolocator/geolocator.dart';
import 'package:google_maps_webservice/places.dart';
import 'package:provider/provider.dart';
import 'package:rent2buy/di/service_locator.dart';
import 'package:rent2buy/models/Seller/companyModel.dart';
import 'package:rent2buy/models/User/userAddress.dart';
import 'package:rent2buy/models/User/userRegister.dart';
import 'package:rent2buy/networking/network_status.dart';
import 'package:rent2buy/screens/buyer/signup/register_viewmodel.dart';
import 'package:rent2buy/screens/common/error_screen.dart';
import 'package:rent2buy/screens/common/loading.dart';
import 'package:rent2buy/screens/common/loading_dialog.dart';
import 'package:rent2buy/screens/common/verification/otp_verify.dart';
import 'package:rent2buy/services/User/auth.dart';
import 'package:rent2buy/services/User/fblogin.dart';
import 'package:rent2buy/utils/constants.dart' as Constants;
import 'package:rent2buy/utils/constants.dart';
import 'package:rent2buy/utils/data_manager.dart';
import 'package:rent2buy/utils/strings.dart';
import 'package:rent2buy/widgets/shared_widgets.dart';

class SignUp extends StatefulWidget {
  String name;
  String email;
  final RegisterViewModel viewModel = serviceLocator<RegisterViewModel>();
  final DataManager dataManager = serviceLocator<DataManager>();

  SignUp({Key key, this.name, this.email}) : super(key: key);

  @override
  _SignUpState createState() => _SignUpState();
}

class _SignUpState extends State<SignUp> {
  TextEditingController nameController = TextEditingController();
  TextEditingController emailController = TextEditingController();
  TextEditingController addressController = TextEditingController();

  GoogleMapsPlaces _places = GoogleMapsPlaces(apiKey: kGoogleApiKey);
  String _date = "  " + "Select DOB";
  bool _value = false;
  Company company = Company();
  final _key = GlobalKey<FormState>();
  final GlobalKey<ScaffoldState> scafoldKey = GlobalKey<ScaffoldState>();
  final GlobalKey<State> _keyLoader = new GlobalKey<State>();
  UserRegister userRegister = UserRegister();
  Map<String, dynamic> params = Map();
  Map<String, dynamic> companyParams = Map();
  UserAddress address = UserAddress();
  List<UserAddress> userAddresses = List();
  String showAddress = "Enter Address";
  String showLocation = 'Take my current location';
  double lat;
  double long;
  String adrs;
  Placemark currentPosition;
  String dob;
  int is_company = 0;

  bool _isCompany = false;
  bool _isTaxable = false;

  void _isCompanyChanges(bool value) {
    setState(() {
      _isCompany = value;
      if (_isCompany) {
        params[Constants.isCompany] = 1;
        is_company = 1;
      } else {
        params[Constants.isCompany] = 0;
        is_company = 0;
      }
      print(is_company);
    });
  }

  void _isTaxableChanges(bool value) {
    setState(() {
      _isTaxable = value;
    });
  }

  @override
  void initState() {
    nameController = TextEditingController()..text = widget.name ?? '';
    emailController = TextEditingController()..text = widget.email ?? '';
    params[Constants.isCompany] = is_company;
    super.initState();
  }

  @override
  void dispose() {
    // TODO: implement dispose
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return ChangeNotifierProvider<RegisterViewModel>(
      create: (BuildContext context) => widget.viewModel,
      child: Consumer<RegisterViewModel>(
        builder: (context, value, _) {
          return Scaffold(
            key: scafoldKey,
            appBar: appBar(Color(0xFF97080E)),
            body: Stack(
              children: [
                SingleChildScrollView(
                  scrollDirection: Axis.vertical,
                  child: Container(
                    child: Column(
                      children: [
                        SizedBox(height: 20),
                        Text(
                          "Register With Us!",
                          style: TextStyle(
                              fontSize: 40, fontWeight: FontWeight.w800),
                        ),
                        SizedBox(height: 20),
                        Form(
                          key: _key,
                          child: Padding(
                            padding: EdgeInsets.only(left: 20, right: 20),
                            child: Column(
                              children: [
                                AuthTextField(
                                  textEditingController: nameController,
                                  hinttext: 'First Name',
                                  isNumber: false,
                                  prefixIcon: Icon(Icons.person_outline),
                                  isPassword: false,
                                  color: Color(0xFFF7A74D),
                                  validator: (String value) {
                                    if (value.isEmpty) {
                                      return 'Please enter Full Name';
                                    }
                                    return null;
                                  },
                                  onSaved: (String value) {
                                    params[Constants.firstName] = value;
                                    userRegister.first_name = value;
                                  },
                                ),
                                SizedBox(height: 10),
                                AuthTextField(
                                  hinttext: 'Last Name',
                                  prefixIcon: Icon(Icons.person_outline),
                                  isPassword: false,
                                  color: Color(0xFFF7A74D),
                                  isNumber: false,
                                  validator: (String value) {
                                    if (value.isEmpty) {
                                      return 'Please enter Last Name';
                                    }
                                    return null;
                                  },
                                  onSaved: (String value) {
                                    params[Constants.lastName] = value;
                                    userRegister.last_name = value;
                                  },
                                ),
                                SizedBox(height: 10),
                                AuthTextField(
                                  textEditingController: emailController,
                                  hinttext: 'Email Address',
                                  isNumber: false,
                                  prefixIcon: Icon(Icons.email_outlined),
                                  isPassword: false,
                                  color: Color(0xFFF7A74D),
                                  validator: (String value) {
                                    Pattern pattern =
                                        r'^(([^<>()[\]\\.,;:\s@\"]+(\.[^<>()[\]\\.,;:\s@\"]+)*)|(\".+\"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$';
                                    RegExp regexp = RegExp(pattern);
                                    if (value.isEmpty) {
                                      return 'Please enter Email';
                                    } else if (!regexp.hasMatch(value)) {
                                      return 'Please enter valid Email';
                                    }
                                    return null;
                                  },
                                  onSaved: (String value) {
                                    params[Constants.email] = value;
                                    userRegister.email = value;
                                  },
                                ),
                                SizedBox(height: 10),
                                AuthTextField(
                                  inputFormatters: [
                                    LengthLimitingTextInputFormatter(10)
                                  ],
                                  hinttext: 'Phone Number',
                                  prefixIcon: Icon(Icons.smartphone),
                                  isPassword: false,
                                  color: Color(0xFFF7A74D),
                                  isNumber: true,
                                  validator: (String value) {
                                    if (value.isEmpty) {
                                      return 'Please enter Phone Number';
                                    } else if (value.length < 9) {
                                      return 'Phone number should be 10 digit';
                                    }
                                    return null;
                                  },
                                  onSaved: (String value) {
                                    params[Constants.phoneNumber] = value;
                                    userRegister.phone = value;
                                  },
                                ),
                                SizedBox(height: 10),
                                RaisedButton(
                                  shape: RoundedRectangleBorder(
                                    borderRadius: BorderRadius.circular(5.0),
                                  ),
                                  elevation: 0.0,
                                  onPressed: () {
                                    DatePicker.showDatePicker(context,
                                        theme: DatePickerTheme(
                                          containerHeight: 210.0,
                                        ),
                                        showTitleActions: true,
                                        minTime: DateTime(1965, 1, 1),
                                        maxTime: DateTime(2020, 12, 31),
                                        onConfirm: (date) {
                                      print('confirm $date');
                                      _date = '  ' +
                                          '${date.year}-${date.month}-${date.day}';
                                      dob =
                                          '${date.year}-${date.month}-${date.day}';
                                      print(_date);
                                      print(dob);
                                      params[Constants.dateOfBirth] = dob;
                                      setState(() {});
                                    },
                                        currentTime: DateTime.now(),
                                        locale: LocaleType.en);
                                  },
                                  child: Container(
                                    alignment: Alignment.center,
                                    height: 50.0,
                                    child: Row(
                                      mainAxisAlignment:
                                          MainAxisAlignment.spaceBetween,
                                      children: <Widget>[
                                        Row(
                                          children: <Widget>[
                                            Container(
                                              child: Row(
                                                mainAxisAlignment:
                                                    MainAxisAlignment.start,
                                                children: <Widget>[
                                                  Icon(
                                                    Icons.date_range,
                                                    size: 22.0,
                                                    color: Color(0xFFF7A74D),
                                                  ),
                                                  Text(
                                                    " $_date",
                                                    style: TextStyle(
                                                        color: Colors.grey[600],
                                                        fontWeight:
                                                            FontWeight.bold,
                                                        fontSize: 18.0),
                                                  ),
                                                ],
                                              ),
                                            )
                                          ],
                                        ),
                                      ],
                                    ),
                                  ),
                                  color: Colors.yellow[100],
                                ),
                                AuthTextField(
                                  hinttext: 'Password',
                                  isNumber: false,
                                  prefixIcon: Icon(Icons.lock_outline),
                                  color: Color(0xFFF7A74D),
                                  isPassword: true,
                                  validator: (String value) {
                                    if (value.isEmpty) {
                                      return 'Please enter Password';
                                    } else if (value.length < 7) {
                                      return 'Password should be greater than 8 digit';
                                    }
                                    _key.currentState.save();
                                    return null;
                                  },
                                  onSaved: (String value) {
                                    params[Constants.password] = value;
                                    userRegister.password = value;
                                  },
                                ),
                                SizedBox(height: 10),
                                AuthTextField(
                                  hinttext: 'Confirm Password',
                                  isNumber: false,
                                  prefixIcon: Icon(Icons.lock_outline),
                                  color: Color(0xFFF7A74D),
                                  isPassword: true,
                                  validator: (String value) {
                                    if (value.isEmpty) {
                                      return 'Please enter Password';
                                    } else if (value.length < 7) {
                                      return 'Password should be greater than 8 digit';
                                    } else if (value != userRegister.password) {
                                      return 'Password not Matched!';
                                    }
                                    return null;
                                  },
                                  onSaved: (String value) {
                                    params[Constants.confirmPassword] = value;
                                    userRegister.confirm_password = value;
                                  },
                                ),
                                SizedBox(height: 10),
                                SizedBox(
                                  height: 10,
                                ),
                                Row(
                                  children: [
                                    SizedBox(width: 15),
                                    Icon(
                                      Icons.business_rounded,
                                      color: Colors.grey[500],
                                    ),
                                    SizedBox(
                                      width: 15,
                                    ),
                                    Text(
                                      'Are You a Company?',
                                      style: TextStyle(
                                          color: Colors.grey[700],
                                          fontSize: 16),
                                    ),
                                  ],
                                ),
                                SizedBox(height: 10),
                                Row(
                                  mainAxisAlignment: MainAxisAlignment.start,
                                  children: [
                                    SizedBox(
                                      width: 120,
                                      child: ListTile(
                                        title: Text('Yes'),
                                        leading: Radio(
                                            value: true,
                                            groupValue: _isCompany,
                                            onChanged: _isCompanyChanges),
                                      ),
                                    ),
                                    SizedBox(
                                      width: 120,
                                      child: ListTile(
                                        title: Text('No'),
                                        leading: Radio(
                                            value: false,
                                            groupValue: _isCompany,
                                            onChanged: _isCompanyChanges),
                                      ),
                                    ),
                                  ],
                                ),
                                Visibility(
                                  visible: _isCompany,
                                  child: Column(
                                    children: [
                                      AuthTextField(
                                        hinttext: 'Company Details',
                                        color: Color(0xFFF7A74D),
                                        isPassword: false,
                                        isNumber: false,
                                        validator: (String value) {
                                          if (value.isEmpty) {
                                            return 'Please enter Company Details';
                                          }
                                          return null;
                                        },
                                        onSaved: (String value) {
                                          company.company_name = value;
                                          companyParams[Constants.companyName] =
                                              value;
                                        },
                                      ),
                                      SizedBox(height: 10),
                                      AuthTextField(
                                        hinttext: 'VAT',
                                        color: Color(0xFFF7A74D),
                                        isPassword: false,
                                        isNumber: true,
                                        validator: (String value) {
                                          if (value.isEmpty) {
                                            return 'Please enter VAT';
                                          }
                                          return null;
                                        },
                                        onSaved: (String value) {
                                          company.vat_number = value;
                                          companyParams[Constants.vatNumber] =
                                              value;
                                        },
                                      ),
                                      SizedBox(
                                        height: 20,
                                      ),
                                      Row(
                                        children: [
                                          SizedBox(width: 15),
                                          Icon(
                                            Icons.attach_money_outlined,
                                            color: Colors.grey[500],
                                          ),
                                          SizedBox(
                                            width: 15,
                                          ),
                                          Text(
                                            'Taxable?',
                                            style: TextStyle(
                                                color: Colors.grey[700],
                                                fontSize: 16),
                                          ),
                                        ],
                                      ),
                                      SizedBox(height: 10),
                                      Row(
                                        mainAxisAlignment:
                                            MainAxisAlignment.start,
                                        children: [
                                          SizedBox(
                                            width: 120,
                                            child: ListTile(
                                              title: Text('Yes'),
                                              leading: Radio(
                                                  value: true,
                                                  groupValue: _isTaxable,
                                                  onChanged: _isTaxableChanges),
                                            ),
                                          ),
                                          SizedBox(
                                            width: 120,
                                            child: ListTile(
                                              title: Text('No'),
                                              leading: Radio(
                                                  value: false,
                                                  groupValue: _isTaxable,
                                                  onChanged: _isTaxableChanges),
                                            ),
                                          ),
                                        ],
                                      ),
                                    ],
                                  ),
                                ),
                                SizedBox(height: 10),
                                Align(
                                  alignment: Alignment.center,
                                  child: IntrinsicWidth(
                                    child: TextFormField(
                                      validator: (value) {
                                        if (value.isEmpty) {
                                          return Strings.msgEnterAddress;
                                        }
                                        return null;
                                      },
                                      controller: addressController,
                                      onTap: () async {
                                        Prediction p =
                                            await PlacesAutocomplete.show(
                                                context: context,
                                                apiKey: kGoogleApiKey,
                                                mode: Mode.overlay);
                                        displayPrediction(p, context);
                                      },
                                      cursorColor: Colors.black,
                                      maxLines: null,
                                      keyboardType: TextInputType.multiline,
                                      decoration: new InputDecoration(
                                          border: InputBorder.none,
                                          focusedBorder: InputBorder.none,
                                          enabledBorder: InputBorder.none,
                                          errorBorder: InputBorder.none,
                                          disabledBorder: InputBorder.none,
                                          contentPadding: EdgeInsets.only(
                                              left: 15,
                                              bottom: 11,
                                              top: 11,
                                              right: 15),
                                          hintText: Strings.enterAddress),
                                    ),
                                  ),
                                ),
                                SizedBox(height: 10),
                                Text('OR',
                                    style: TextStyle(
                                      fontWeight: FontWeight.bold,
                                      color: Colors.grey[600],
                                    )),
                                SizedBox(
                                  height: 10,
                                ),
                                FlatButton(
                                    onPressed: () {
                                      getLocation();
                                    },
                                    child: Row(
                                      mainAxisAlignment:
                                          MainAxisAlignment.center,
                                      children: [
                                        Text(
                                          showLocation,
                                          style: TextStyle(
                                              color: Colors.grey[600],
                                              fontSize: 16),
                                        ),
                                        Icon(
                                          Icons.location_on_outlined,
                                          color: Colors.grey[600],
                                        )
                                      ],
                                    ))
                              ],
                            ),
                          ),
                        ),
                        Padding(
                          padding: EdgeInsets.only(left: 20),
                          child: Row(
                            children: [
                              Checkbox(
                                value: _value,
                                onChanged: (bool newValue) {
                                  setState(() {
                                    _value = newValue;
                                  });
                                },
                              ),
                              SizedBox(width: 15),
                              Text(Strings.txtAgreeWithTerms,
                                  style: TextStyle(
                                      fontSize: 14, color: Colors.black87)),
                            ],
                          ),
                        ),
                        SizedBox(height: 20),
                        MyButton(
                            text: "Register",
                            color: Color(0xFF97080E),
                            onPressed: () async {
                              if (_key.currentState.validate()) {
                                _key.currentState.save();
                                if (!_value) {
                                  scafoldKey.currentState.showSnackBar(
                                      showMessage(Strings.msgAcceptTerms));
                                } else {
                                  userRegister = UserRegister(
                                    first_name: userRegister.first_name,
                                    last_name: userRegister.last_name,
                                    email: userRegister.email,
                                    phone: userRegister.phone,
                                    address: address,
                                    is_company: is_company.toString(),
                                    company: company,
                                    dob: dob,
                                    password: userRegister.password,
                                    confirm_password:
                                        userRegister.confirm_password,
                                  );
                                  params[Constants.company] = companyParams;
                                  params[Constants.deviceToken] =
                                      await widget.dataManager.getFCMToken();
                                  widget.viewModel.register(params);
                                }
                              }
                            }),
                        SizedBox(height: 20),
                        Text('OR',
                            style: TextStyle(
                              fontWeight: FontWeight.bold,
                              color: Colors.grey[600],
                            )),
                        SizedBox(height: 20),
                        Row(
                          mainAxisAlignment: MainAxisAlignment.center,
                          children: [
                            RawMaterialButton(
                                child: FaIcon(FontAwesomeIcons.facebookF,
                                    size: 24, color: Colors.white),
                                fillColor: Colors.blue[900],
                                shape: CircleBorder(),
                                onPressed: () {
                                  initiateFacebookLogin(context);
                                }),
                            SizedBox(width: 20),
                            RawMaterialButton(
                                child: FaIcon(FontAwesomeIcons.googlePlusG,
                                    size: 24, color: Colors.white),
                                fillColor: Colors.red,
                                shape: CircleBorder(),
                                onPressed: () {
                                  signInUserWithGoogle(context);
                                }),
                          ],
                        ),
                        SizedBox(height: 20)
                      ],
                    ),
                  ),
                ),
                observeSignUp(value, context),
              ],
            ),
            bottomNavigationBar: Container(
              height: 50,
              child: Column(
                children: [
                  Row(
                    children: [
                      Expanded(
                        child: Divider(
                          color: Color(0xFFF7A74D),
                          height: 5.0,
                          thickness: 5,
                        ),
                      ),
                    ],
                  ),
                  SizedBox(height: 10),
                  Padding(
                    padding: const EdgeInsets.only(bottom: 10.0),
                    child: RichText(
                      text: TextSpan(
                        children: <TextSpan>[
                          TextSpan(
                              text: 'Already Have an Account? ',
                              style: TextStyle(
                                  fontFamily: 'Dosis',
                                  fontSize: 18,
                                  color: Color(0xFF97080E),
                                  fontWeight: FontWeight.bold)),
                          TextSpan(
                              text: 'Login Here',
                              style: TextStyle(
                                  fontFamily: 'Dosis',
                                  color: Color(0xFFF7A74D),
                                  fontSize: 18,
                                  fontWeight: FontWeight.bold),
                              recognizer: TapGestureRecognizer()
                                ..onTap = () {
                                  Navigator.pushNamed(context, '/login');
                                }),
                        ],
                      ),
                    ),
                  ),
                ],
              ),
            ),
          );
        },
      ),
    );
  }

  getLocation() async {
    LocationPermission permission = await Geolocator.requestPermission();
    Position position = await Geolocator.getCurrentPosition(
        desiredAccuracy: LocationAccuracy.high);
    lat = position.latitude;
    long = position.longitude;
    List<Placemark> placemarks = await placemarkFromCoordinates(lat, long);
    currentPosition = placemarks[0];
    setState(() {
      userAddresses.clear();
      showAddress = currentPosition.name +
          " " +
          currentPosition.subLocality +
          " " +
          currentPosition.locality +
          " " +
          currentPosition.postalCode;
      adrs = currentPosition.name;
      address.location = showAddress;
      address.latitude = lat;
      address.longitude = long;

      userAddresses.add(address);
      addressController.text = showAddress;
      params[Constants.address] = userAddresses;
    });
  }

  Future<Null> displayPrediction(Prediction p, BuildContext context) async {
    FocusManager.instance.primaryFocus.unfocus();
    if (p != null) {
      Dialogs.showLoadingDialog(context, _keyLoader, "Getting Location...");
      PlacesDetailsResponse detail =
          await _places.getDetailsByPlaceId(p.placeId);

      var placeId = p.placeId;
      lat = detail.result.geometry.location.lat;
      long = detail.result.geometry.location.lng;

      print(lat);
      print(long);
      var a = await Geocoder.local.findAddressesFromQuery(p.description);

      setState(() {
        userAddresses.clear();
        showAddress = p.description;
        adrs = p.description;
        address.location = showAddress;
        address.latitude = lat;
        address.longitude = long;

        userAddresses.add(address);
        addressController.text = showAddress;
        params[Constants.address] = userAddresses;
      });
    }
    Navigator.of(_keyLoader.currentContext, rootNavigator: true).pop();
  }

  Widget observeSignUp(RegisterViewModel value, BuildContext context) {
    switch (widget.viewModel.registerEvent.status) {
      case NetworkStatus.LOADING:
        return LoadingScreen(isCustom: false);
      case NetworkStatus.ERROR:
        return ErrorScreen(value.registerEvent.message);
      case NetworkStatus.SUCCESS:
        WidgetsBinding.instance.addPostFrameCallback((_) {
          Navigator.push(
              context,
              MaterialPageRoute(
                  builder: (context) => OtpVerify(email: userRegister.email)));
        });
        break;
    }

    return Container();
  }

  SnackBar showMessage(String msg) {
    return SnackBar(
      content: Text(msg),
      duration: Duration(milliseconds: 800),
    );
  }
}
