import 'package:flutter/material.dart';
import 'package:rent2buy/screens/buyer/categories/products_by_category.dart';
import 'package:rent2buy/services/apiProvider.dart';
import 'package:rent2buy/widgets/shared_widgets.dart';
import 'dart:convert';
import 'package:http/http.dart' as http;

class SubCategory extends StatefulWidget {
  final List<dynamic> category;

  SubCategory({Key key, @required this.category}) : super(key: key);

  @override
  _SubCategoryState createState() => _SubCategoryState();
}

class _SubCategoryState extends State<SubCategory> {
  List<dynamic> subCategories = List();

  getSubCategories() async {
    http.Response response = await http.get(
        '${ApiProvider.getSubCategories}' + '${widget.category[0]["_id"]}');
    setState(() {
      subCategories = jsonDecode(response.body)['response'];
    });
    print(subCategories[0]['name']);
    return jsonDecode(response.body)['response'];
  }

  @override
  void initState() {
    getSubCategories();
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: BaseAppBar(
        title: widget.category[0]["name"],
        automaticallyImplyLeading: true,
        titleColor: Color(0xFF97080E),
        iconTheme: Color(0xFF97080E),
        widgets: [],
        appBar: AppBar(),
      ),
      body: ListView.builder(
          itemCount: subCategories.length,
          itemBuilder: (BuildContext context, int index) {
            return Column(
              children: [
                ListTile(
                  contentPadding: EdgeInsets.symmetric(horizontal: 50),
                  title: Text(
                    subCategories[index]['name'],
                    style: TextStyle(fontWeight: FontWeight.w600, fontSize: 20),
                  ),
                  onTap: () {
                    Navigator.push(
                        context,
                        MaterialPageRoute(
                          builder: (context) => ProductsByCategory(
                              categoryName: widget.category[0]['name'],
                              category: widget.category[0]["_id"],
                              subCategory: ''),
                        ));
                  },
                ),
                Divider(
                  color: Color(0xFFF7A74D),
                  height: 5.0,
                  thickness: 3,
                ),
              ],
            );
          }),
    );
  }
}
