import 'package:flutter/material.dart';
import 'package:rent2buy/services/apiProvider.dart';
import 'package:rent2buy/widgets/shared_widgets.dart';
import 'package:http/http.dart' as http;
import 'dart:convert';
class TempSearch extends StatefulWidget {
  String category_id;
  double radius;
  TempSearch({Key key,this.category_id,this.radius}): super(key:key);
  @override
  _TempSearchState createState() => _TempSearchState();
}

class _TempSearchState extends State<TempSearch> {
  List<dynamic> searchResults = List();
  search() async {
    http.Response response =
    await http.post('${ApiProvider.searchSellListings}',
        body: {
          'category_id': widget.category_id,
          'action_type': 'sell',
          'latitude':'22.253433',
          'longitude':'76.039673',
          'radius':widget.radius.toString()
        });
    setState(() {
      searchResults = jsonDecode(response.body)['response'];
    });
  }

  @override
  void initState() {
    search();
    super.initState();
  }
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: BaseAppBar(
        title: 'Search Results',
        automaticallyImplyLeading: true,
        titleColor: Color(0xFF97080E),
        iconTheme: Color(0xFF97080E),
        widgets: [],
        appBar: AppBar(),
      ),
      body: ListView.builder(
        reverse: true,
          itemCount: searchResults.length,
          itemBuilder: (BuildContext context,int index){
            return Column(
              children: [
                ListTile(
                  contentPadding: EdgeInsets.all(8),
                  leading: Image.network('https://apinizer.com/wp-content/uploads/revslider/the7-web-master-hero-image/laptop.png',height: 60,),
                  title: Text(searchResults[index]["name"],style: TextStyle(fontSize: 22),),
                  subtitle: Text(searchResults[index]['description'],maxLines: 2,overflow: TextOverflow.ellipsis,),
                  trailing: Padding(
                    padding: const EdgeInsets.only(right:8.0),
                    child: Text(searchResults[index]['purchase_actions'][0]['name'],style: TextStyle(color: Color(0xFF97080E),fontWeight: FontWeight.w600,fontSize: 22),),
                  ),
                  onTap: (){
                //    Navigator.push(context, MaterialPageRoute(builder: (context) => ProductDetails(product: [products[index]],)));
                  },
                ),
                Divider(
                  color: Color(0xFFF7A74D),
                  height: 5.0,
                  thickness: 3,
                ),
              ],
            );
          }),
    );
  }
}
