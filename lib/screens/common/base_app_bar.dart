import 'package:flutter/material.dart';
import 'package:rent2buy/utils/colors.dart';
import 'package:rent2buy/utils/design_utils.dart';

class CommonAppBar extends StatelessWidget implements PreferredSizeWidget {
  final String title;
  final AppBar appBar;
  final List<Widget> widgets;
  bool centerTitle = false;
  bool automaticallyImplyLeading = false;

  /// you can add more fields that meet your needs

  CommonAppBar(
      {Key key,
      this.title,
      this.centerTitle,
      this.automaticallyImplyLeading,
      this.appBar,
      this.widgets})
      : super(key: key);

  @override
  Widget build(BuildContext context) {
    return AppBar(
      title: Text(
        title,
        style: DesignUtils.textApperanceHeader(),
      ),
      centerTitle: centerTitle,
      automaticallyImplyLeading:
          automaticallyImplyLeading == null ? true : false,
      actions: widgets,
    );
  }

  @override
  Size get preferredSize => new Size.fromHeight(appBar.preferredSize.height);
}
