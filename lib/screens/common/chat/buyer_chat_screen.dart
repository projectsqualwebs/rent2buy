import 'package:firebase_core/firebase_core.dart';
import 'package:firebase_database/firebase_database.dart';
import 'package:flutter/material.dart';
import 'package:fluttertoast/fluttertoast.dart';
import 'package:rent2buy/di/service_locator.dart';
import 'package:rent2buy/models/chat_message.dart';
import 'package:rent2buy/models/message_model.dart';
import 'package:rent2buy/models/userinfo.dart';
import 'package:rent2buy/networking/network_status.dart';
import 'package:rent2buy/screens/common/base_app_bar.dart';
import 'package:rent2buy/screens/common/chat/chat_viewmodel.dart';
import 'package:rent2buy/utils/chat_manager.dart';
import 'package:rent2buy/utils/colors.dart';
import 'package:rent2buy/utils/constants.dart';
import 'package:rent2buy/utils/data_manager.dart';
import 'package:rent2buy/utils/design_utils.dart';
import 'package:rent2buy/utils/dimens.dart';
import 'package:rent2buy/utils/strings.dart';
import 'package:rent2buy/utils/time_utils.dart';

class BuyerChatScreen extends StatefulWidget {
  final String receiverName;
  final String listingId;
  final String chatId;
  final ChatViewModel viewModel = serviceLocator<ChatViewModel>();
  final ChatManager chatManager = serviceLocator<ChatManager>();
  final DataManager dataManager = serviceLocator<DataManager>();
  BuyerChatScreen({this.receiverName, this.chatId, this.listingId});

  @override
  State<StatefulWidget> createState() {
    return _BuyerChatScreenState();
  }
}

class _BuyerChatScreenState extends State<BuyerChatScreen> {
  final List<ChatMessage> chatList = List();
  FirebaseDatabase _firebaseDatabase;
  DatabaseReference _databaseReference;

  String chatId;

  var _keyList = GlobalKey<AnimatedListState>();
  Userinfo currentUser;
  bool isInitChat = false;

  var _messageController = TextEditingController();
  var _listController = ScrollController();

  var _isToDelete = false;
  int deleteCount = 0;

  @override
  void initState() {
    initChatList();
    super.initState();
  }

  @override
  void dispose() {
    // TODO: implement dispose
    super.dispose();
    widget.viewModel.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: CommonAppBar(
        appBar: AppBar(),
        title: widget.receiverName,
        widgets: [
          Visibility(
            visible: _isToDelete,
            child: InkWell(
                onTap: () {
                  for (ChatMessage chatMessage in chatList) {
                    if (chatMessage.isChecked) {
                      _databaseReference
                          .child(chatMessage.key)
                          .child('status')
                          .set(1);
                    }
                  }
                  setState(() {
                    _isToDelete = false;
                  });
                },
                child: Icon(Icons.delete)),
          ),
          PopupMenuButton<String>(
            onSelected: handleMenuClick,
            itemBuilder: (BuildContext context) {
              return {'Pay Now', 'Details'}.map((String choice) {
                return PopupMenuItem<String>(
                  value: choice,
                  child: Text(choice),
                );
              }).toList();
            },
          ),
        ],
      ),
      body: Container(
        padding: EdgeInsets.all(margin_large),
        child: Column(
          children: [
            Expanded(
              child: AnimatedList(
                controller: _listController,
                key: _keyList,
                shrinkWrap: true,
                scrollDirection: Axis.vertical,
                initialItemCount: chatList?.length,
                itemBuilder: (context, index, animation) {
                  return _buildItem(index, chatList[index]);
                },
              ),
            ),
            Align(
              alignment: Alignment.bottomCenter,
              child: Container(
                padding: EdgeInsets.all(padding_large),
                width: double.infinity,
                child: TextFormField(
                  controller: _messageController,
                  validator: (value) {
                    if (value.isEmpty) {
                      return "";
                    }
                    return null;
                  },
                  keyboardType: TextInputType.multiline,
                  decoration: DesignUtils.textInputLayoutOutLined(
                      Strings.messageTo + widget.receiverName,
                      suffixIcon: IconButton(
                        icon: Icon(Icons.send),
                        onPressed: () {
                          if (_messageController.text.isNotEmpty) {
                            sendMessage();
                          }
                        },
                      ),
                      prefixIcon: Container(
                        padding: EdgeInsets.all(padding_large),
                        child: Image(
                          width: 4,
                          height: 4,
                          color: Colors.grey,
                          image: AssetImage(
                            'assets/speech-bubbles.png',
                          ),
                        ),
                      )),
                ),
              ),
            )
          ],
        ),
      ),
    );
  }

  void handleMenuClick(String value) {}

  Widget _buildItem(int index, ChatMessage chatMessage) {
    try {
      int currentMessageType =
          chatMessage.sender.sId.compareTo(currentUser.sId) == 0
              ? TYPE_SENT
              : TYPE_RECEIVED;

      return InkWell(
        onTap: () {
          setState(() {
            if (chatMessage.isChecked) {
              chatMessage.isChecked = false;
              deleteCount--;
              if (deleteCount <= 0) {
                _isToDelete = false;
                deleteCount = 0;
              }
            }
          });
        },
        onLongPress: () {
          deleteChat(chatMessage);
        },
        child: Container(
          padding: EdgeInsets.only(left: 14, right: 14, top: 10, bottom: 10),
          child: Align(
            alignment: (currentMessageType == TYPE_RECEIVED
                ? Alignment.topLeft
                : Alignment.topRight),
            child: Container(
              decoration: BoxDecoration(
                borderRadius: BorderRadius.circular(20),
                color: chatMessage.isChecked
                    ? Colors.black26
                    : (currentMessageType == TYPE_RECEIVED
                        ? colorSeller
                        : colorPrimary),
              ),
              padding: EdgeInsets.all(16),
              child: Row(
                mainAxisSize: MainAxisSize.min,
                children: [
                  Flexible(
                    child: Text(
                      chatMessage.status == 0
                          ? chatMessage.message
                          : String.fromCharCode(0xe6a1) +
                              Strings.txtMessageIsDeleted,
                      style: TextStyle(
                          fontSize: 15,
                          color: chatMessage.status == 0
                              ? Colors.black
                              : Colors.black54),
                    ),
                  ),
                  SizedBox(
                    width: margin_medium,
                  ),
                  Text(
                    TimeUtils.getHourMinuteFromEpoch(
                        chatMessage.send_timestamp),
                    style: DesignUtils.textApperanceRegular(
                        color: Colors.grey, fontSize: 12),
                  )
                ],
              ),
            ),
          ),
        ),
      );
    } catch (e) {
      debugPrint(e.toString());
    }
  }

  void deleteChat(ChatMessage chatMessage) {
    setState(() {
      chatMessage.isChecked = true;
      _isToDelete = true;
      deleteCount++;
    });
  }

  void initChatList() async {
    currentUser = Userinfo.fromJson(await widget.dataManager.getUser());

    _firebaseDatabase = FirebaseDatabase.instance;
    Map<String, dynamic> params = Map();
    params[listingId] = widget.listingId;

    if (widget.chatId == null) {
      chatId = await widget.chatManager.getChatId(widget.listingId);
    } else {
      chatId = widget.chatId;
    }

    if (chatId == null) {
      //Get ChatId
      widget.viewModel.createThread(params);
      widget.viewModel.createValue.addListener(() {
        var response = widget.viewModel.createValue.value;
        if (response.status == NetworkStatus.ERROR) {
          Fluttertoast.showToast(msg: response.message);
        } else if (response.status == NetworkStatus.SUCCESS) {
          chatId = response.response.id;
          widget.chatManager.saveChatId(widget.listingId, chatId);
          addListener();
        }
      });
    } else {
      //Listen To ChatId
      addListener();
    }
  }

  void addListener() {
    _databaseReference =
        _firebaseDatabase.reference().child('chat_line').child(chatId);

    _databaseReference.onChildAdded.listen((event) {
      if (isInitChat) {
        var snapshot = event.snapshot;
        ChatMessage chatMessage = ChatMessage.fromJson(snapshot.value);
        chatList.add(chatMessage);
        _keyList.currentState.insertItem(chatList.length - 1);
        scrollToEnd();
      }
    });

    _databaseReference.once().then((DataSnapshot dataSnapshot) {
      int i = 0;
      Map<dynamic, dynamic> chatSnapShot = dataSnapshot.value;
      //To Order Chat
      var mapEntries = chatSnapShot.entries.toList()
        ..sort((a, b) {
          ChatMessage aMessage = ChatMessage.fromJson(a.value);
          ChatMessage bMessage = ChatMessage.fromJson(b.value);

          return aMessage.send_timestamp.compareTo(bMessage.send_timestamp);
        });

      chatSnapShot
        ..clear()
        ..addEntries(mapEntries);

      chatSnapShot.forEach((key, value) {
        ChatMessage chatMessage = ChatMessage.fromJson(value);
        chatList.add(chatMessage);
        _keyList.currentState.insertItem(i);
        i++;
      });
      isInitChat = true;
      scrollToEnd();
    }).catchError((e) {
      debugPrint(e.toString());
    });
  }

  void scrollToEnd() {
    _listController.animateTo(
      _listController.position.maxScrollExtent,
      duration: Duration(seconds: 1),
      curve: Curves.fastOutSlowIn,
    );
  }

  void sendMessage() {
    Map<String, dynamic> params = Map();
    params['chat_id'] = chatId;
    params['message'] = _messageController.text;
    widget.viewModel.sendMessage(params);
    _messageController.text = "";
  }
}
