import 'package:flutter/material.dart';
import 'package:rent2buy/di/service_locator.dart';
import 'package:rent2buy/models/order.dart';
import 'package:rent2buy/models/thread_response.dart';
import 'package:rent2buy/networking/network_response.dart';
import 'package:rent2buy/networking/network_responselist.dart';
import 'package:rent2buy/networking/repository/message_repository.dart';
import 'package:rent2buy/networking/repository/seller_repository.dart';
import 'package:rent2buy/utils/base_change_notifier.dart';

class ChatViewModel extends BaseChangeNotifier {
  final MessageRepository messageRepository =
      serviceLocator<MessageRepository>();
  final SellerRepository _sellerRepository = serviceLocator<SellerRepository>();

  ValueNotifier<NetworkResponse<ThreadResponse>> createValue =
      ValueNotifier(NetworkResponse.idle());
  ValueNotifier<NetworkResponse<String>> sendValue =
      ValueNotifier(NetworkResponse.idle());
  ValueNotifier<NetworkResponse<String>> updateValue =
      ValueNotifier(NetworkResponse.idle());

  ValueNotifier<NetworkResponse<Order>> orderValue =
      ValueNotifier(NetworkResponse.idle());

  void createThread(Map<String, dynamic> params) {
    createValue.value = NetworkResponse.loading();
    messageRepository.createThread(params).then((response) {
      createValue.value = NetworkResponse.success(response.response);
    }).catchError((e) {
      createValue.value = NetworkResponse.error(parseError(e));
    });
  }

  void sendMessage(Map<String, dynamic> params) {
    sendValue.value = NetworkResponse.loading();
    messageRepository.sendMessage(params).then((response) {
      sendValue.value =
          NetworkResponse.successWithMsg(null, response.message);
    }).catchError((e) {
      sendValue.value = NetworkResponse.error(parseError(e));
    });
  }

  void updateOrderStatus(Map<String, dynamic> params, String orderId) {
    updateValue.value = NetworkResponse.loading();
    _sellerRepository.updateOrderStatus(params, orderId).then((response) {
      updateValue.value =
          NetworkResponse.successWithMsg(null, response.message);
    }).catchError((e) {
      updateValue.value = NetworkResponse.error(parseError(e));
    });
  }

  void getOrderInfo(String id) {
    orderValue.value = NetworkResponse.loading();
    _sellerRepository.getOrderInfo(id).then((response) {
      orderValue.value = NetworkResponse.success(response.response);
    }).catchError((e) {
      orderValue.value = NetworkResponse.error(parseError(e));
    });
  }
}
