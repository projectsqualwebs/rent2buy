import 'package:flutter/material.dart';
import 'package:rent2buy/screens/common/base_app_bar.dart';

class CommonScreen extends StatefulWidget {
  static final String routeName = "/common_setting_screen";

  @override
  State<StatefulWidget> createState() {
    // TODO: implement createState
    return _CommonScreenState();
  }
}

class _CommonScreenState extends State<CommonScreen> {
  String args;
  @override
  void initState() {
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    args = ModalRoute.of(context).settings.arguments as String;
    return Scaffold(
        appBar: CommonAppBar(
          appBar: AppBar(),
          title: args,
        ),
        body: Center(child: Text("Test")));
  }
}
