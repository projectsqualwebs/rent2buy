import 'package:flutter/material.dart';
import 'package:fluttertoast/fluttertoast.dart';
import 'package:rent2buy/di/service_locator.dart';
import 'package:rent2buy/networking/network_status.dart';
import 'package:rent2buy/screens/buyer/orders/feedback_viewmodel.dart';
import 'package:rent2buy/screens/common/base_app_bar.dart';
import 'package:rent2buy/theme/custom_theme.dart';
import 'package:rent2buy/utils/constants.dart';
import 'package:rent2buy/utils/design_utils.dart';
import 'package:rent2buy/utils/dimens.dart';
import 'package:rent2buy/utils/strings.dart';
import 'package:rent2buy/widgets/shared_widgets.dart';

class FeedbackScreen extends StatefulWidget {
  static final String routeName = "/feedback_screen";
  final FeedBackViewModel viewModel = serviceLocator<FeedBackViewModel>();

  @override
  State<StatefulWidget> createState() {
    // TODO: implement createState
    return _FeedbackScreenState();
  }
}

class _FeedbackScreenState extends State<FeedbackScreen> {
  TextEditingController _feedbackController = TextEditingController();
  bool isLoading = false;
  @override
  void initState() {
    WidgetsBinding.instance.addPostFrameCallback((_) {
      widget.viewModel.submitValue.addListener(() {
        var response = widget.viewModel.submitValue.value;
        switch (response.status) {
          case NetworkStatus.LOADING:
            isLoading = true;
            break;
          case NetworkStatus.ERROR:
            isLoading = false;
            Fluttertoast.showToast(msg: response.message);
            break;
          case NetworkStatus.SUCCESS:
            isLoading = false;
            Fluttertoast.showToast(msg: response.message);
            break;
        }
      });
    });
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    // TODO: implement build
    return Theme(
      data: CustomTheme.buyerTheme,
      child: Scaffold(
          appBar: CommonAppBar(
            appBar: AppBar(),
            title: Strings.feedBack,
          ),
          body: Container(
            margin: EdgeInsets.all(margin_large),
            child: Column(
              children: [
                Container(
                  height: 96.0,
                  child: TextFormField(
                    validator: (value) {
                      if (value.isEmpty) {
                        return Strings.fieldRequired;
                      }
                      return null;
                    },
                    maxLines: 8,
                    controller: _feedbackController,
                    decoration: InputDecoration(
                      hintText: Strings.enterMessage,
                    ),
                    keyboardType: TextInputType.multiline,
                  ),
                ),
                SizedBox(
                  height: margin_large,
                ),
                Center(
                    child: isLoading
                        ? DesignUtils.getProgressButton()
                        : MyButton(
                            text: 'Submit',
                            onPressed: () {
                              Map<String, dynamic> params = Map();
                              params[rating] = 0.0;
                              params[review] = _feedbackController.text;
                              widget.viewModel.submitFeedback(params);
                            })),
              ],
            ),
          )),
    );
  }
}
