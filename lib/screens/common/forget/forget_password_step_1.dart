import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:fluttertoast/fluttertoast.dart';
import 'package:rent2buy/di/service_locator.dart';
import 'package:rent2buy/networking/network_status.dart';
import 'package:rent2buy/screens/common/base_app_bar.dart';
import 'package:rent2buy/screens/common/forget/forget_password_step_2.dart';
import 'package:rent2buy/screens/common/forget/forget_password_viewmodel.dart';
import 'package:rent2buy/theme/custom_theme.dart';
import 'package:rent2buy/utils/colors.dart';
import 'package:rent2buy/utils/constants.dart';
import 'package:rent2buy/utils/data_manager.dart';
import 'package:rent2buy/utils/design_utils.dart';
import 'package:rent2buy/utils/dimens.dart';
import 'package:rent2buy/utils/strings.dart';
import 'package:rent2buy/widgets/shared_widgets.dart';

class ForgetPasswordStep1Screeen extends StatefulWidget {
  static final String routeName = "/forget_password_step_1";
  final ForgetPasswordViewModel viewModel =
      serviceLocator<ForgetPasswordViewModel>();
  final DataManager dataManager = serviceLocator<DataManager>();
  @override
  _ForgetPasswordState createState() => _ForgetPasswordState();
}

class _ForgetPasswordState extends State<ForgetPasswordStep1Screeen> {
  TextEditingController _email = TextEditingController();
  var _otpController = TextEditingController();

  int currentRole = 0;

  bool isLoading = false;

  bool isSentOTP = false;

  var _keyForm = GlobalKey<FormState>();

  @override
  void initState() {
    getRole();
    super.initState();
    WidgetsBinding.instance.addPostFrameCallback((_) {
      widget.viewModel.resendValue.addListener(() {
        var response = widget.viewModel.resendValue.value;
        switch (response.status) {
          case NetworkStatus.LOADING:
            setLoading(true);
            break;
          case NetworkStatus.ERROR:
            setLoading(false);
            Fluttertoast.showToast(msg: response.message);
            break;
          case NetworkStatus.SUCCESS:
            setLoading(false);
            Fluttertoast.showToast(msg: response.message);
            setState(() {
              isSentOTP = true;
            });
            break;
        }
      });

      widget.viewModel.verifyValue.addListener(() {
        var response = widget.viewModel.verifyValue.value;
        switch (response.status) {
          case NetworkStatus.LOADING:
            setLoading(true);
            break;
          case NetworkStatus.ERROR:
            setLoading(false);
            Fluttertoast.showToast(msg: response.message);
            break;
          case NetworkStatus.SUCCESS:
            setLoading(false);
            Fluttertoast.showToast(msg: Strings.txtOTPVerified);
            Navigator.pushNamed(context, ForgetPasswordStep2Screen.routeName);
            break;
        }
      });
    });
  }

  void setLoading(bool value) {
    FocusManager.instance.primaryFocus.unfocus();
    if (this.mounted) {
      setState(() {
        isLoading = value;
      });
    }
  }

  void getRole() async {
    currentRole = await widget.dataManager.getUserRole();
  }

  @override
  Widget build(BuildContext context) {
    return Theme(
      data: CustomTheme.buyerTheme,
      child: Scaffold(
        appBar: CommonAppBar(
          title: Strings.appName,
          appBar: AppBar(),
          centerTitle: true,
          automaticallyImplyLeading: false,
        ),
        body: Builder(
          builder: (context) => Container(
            height: MediaQuery.of(context).size.height,
            padding: EdgeInsets.symmetric(horizontal: 40),
            child: Form(
              key: _keyForm,
              child: Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                children: [
                  SizedBox(height: 60),
                  Padding(
                    padding: EdgeInsets.only(left: 12.0),
                    child: Text(
                      'Forget Password',
                      style:
                          TextStyle(fontWeight: FontWeight.w700, fontSize: 24),
                    ),
                  ),
                  SizedBox(height: 20),
                  isSentOTP ? _buildVerifyOTPField() : _buildSendOTPField(),
                  SizedBox(height: 30),
                  isLoading
                      ? DesignUtils.getProgressButton()
                      : Center(
                          child: MyButton(
                            color: colorAccent,
                              text: 'Submit',
                              onPressed: () {
                                if (_keyForm.currentState.validate()) {
                                  //if otp sent
                                  Map<String, dynamic> params = Map();
                                  if (isSentOTP) {
                                    params[email] = _email.text;
                                    params[otp] =
                                        int.parse(_otpController.text);
                                    widget.viewModel.verifyOTP(params);
                                  } else {
                                    sendOTP();
                                  }
                                }
                              })),
                  SizedBox(
                    height: margin_extra_large,
                  ),
                  Center(
                    child: InkWell(
                      onTap: () {
                        Navigator.pop(context);
                      },
                      child: Text(
                        Strings.cancel,
                        style: DesignUtils.textApperanceRegular(),
                      ),
                    ),
                  )
                ],
              ),
            ),
          ),
        ),
      ),
    );
  }

  _buildSendOTPField() {
    return Column(
      children: [
        TextFormField(
          validator: (value) {
            if (value.isEmpty) {
              return Strings.fieldRequired;
            }
            return null;
          },
          decoration: InputDecoration(hintText: 'Enter Email'),
          keyboardType: TextInputType.emailAddress,
          controller: _email,
        ),
        SizedBox(height: 20),
        Padding(
          padding: const EdgeInsets.only(left: 12.0),
          child: Text('Otp will be shared to your email to verify',
              style: TextStyle(
                  color: Colors.grey[700],
                  fontWeight: FontWeight.w500,
                  fontSize: 16)),
        ),
      ],
    );
  }

  _buildVerifyOTPField() {
    return Column(
      children: [
        TextFormField(
          validator: (value) {
            if (value.isEmpty) {
              return Strings.fieldRequired;
            }
            return null;
          },
          inputFormatters: [LengthLimitingTextInputFormatter(5)],
          decoration: InputDecoration(hintText: 'Enter OTP'),
          keyboardType: TextInputType.number,
          controller: _otpController,
        ),
        SizedBox(
          height: margin_extra_large,
        ),
        Row(
          children: [
            Text(
              Strings.txtOTPNotReceived,
              style: DesignUtils.textApperanceRegular(),
            ),
            InkWell(
              onTap: () {
                sendOTP();
              },
              child: Text(
                Strings.sendAgain,
                style: DesignUtils.textApperanceRegular(),
              ),
            )
          ],
        )
      ],
    );
  }

  void sendOTP() {
    Map<String, dynamic> params = Map();
    params[email] = _email.text;
    widget.viewModel.resendOTP(params);
  }
}
