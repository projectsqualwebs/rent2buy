import 'package:flutter/material.dart';
import 'package:rent2buy/di/service_locator.dart';
import 'package:rent2buy/models/Verification/verifyOtp.dart';
import 'package:rent2buy/models/common_repository.dart';
import 'package:rent2buy/networking/network_response.dart';
import 'package:rent2buy/networking/repository/account_repository.dart';
import 'package:rent2buy/utils/base_change_notifier.dart';

class ForgetPasswordViewModel extends BaseChangeNotifier {
  final AccountRepository _accountRepository =
      serviceLocator<AccountRepository>();

  ValueNotifier<NetworkResponse<String>> resendValue =
      ValueNotifier(NetworkResponse.idle());

  ValueNotifier<NetworkResponse<String>> forgotValue =
      ValueNotifier(NetworkResponse.idle());

  ValueNotifier<NetworkResponse<String>> verifyValue =
      ValueNotifier(NetworkResponse.idle());

  void resendOTP(Map<String, dynamic> params) {
    resendValue.value = NetworkResponse.loading();
    _accountRepository.resendOTP(params).then((response) {
      resendValue.value =
          NetworkResponse.successWithMsg(null, response.message);
    }).catchError((e) {
      resendValue.value = NetworkResponse.error(parseError(e));
    });
  }

  void verifyOTP(Map<String, dynamic> params) {
    verifyValue.value = NetworkResponse.loading();
    try {
      _accountRepository.verifyOTP(params).then((value) {
        verifyValue.value = NetworkResponse.successWithMsg(null, value.message);
      }).catchError((e) {
        verifyValue.value = NetworkResponse.error(parseError(e));
      });
    } catch (exception) {
      verifyValue.value = NetworkResponse.error(parseError(exception));
    }
  }

  void forgotPassword(Map<String, dynamic> params) {
    forgotValue.value = NetworkResponse.loading();
    _accountRepository.forgotPassword(params).then((response) {
      forgotValue.value =
          NetworkResponse.successWithMsg(null, response.message);
    }).catchError((e) {
      forgotValue.value = NetworkResponse.error(parseError(e));
    });
  }
}
