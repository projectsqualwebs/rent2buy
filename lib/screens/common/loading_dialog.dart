import 'package:flutter/material.dart';
import 'package:rent2buy/utils/colors.dart';

class Dialogs {
  static Future<void> showLoadingDialog(
      BuildContext context, GlobalKey key, String text) async {
    FocusManager.instance.primaryFocus.unfocus();
    return showDialog<void>(
        context: context,
        barrierDismissible: false,
        builder: (BuildContext context) {
          return new WillPopScope(
              onWillPop: () async => false,
              child: SimpleDialog(
                  key: key,
                  backgroundColor: Colors.white,
                  children: <Widget>[
                    Column(mainAxisSize: MainAxisSize.min, children: [
                      CircularProgressIndicator(
                        backgroundColor: null,
                        valueColor: null,
                      ),
                      SizedBox(
                        height: 10,
                      ),
                      text != null
                          ? Text(
                              text,
                              style: TextStyle(color: colorAccent),
                            )
                          : null
                    ])
                  ]));
        });
  }
}
