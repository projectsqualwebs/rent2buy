import 'dart:ui';
import 'package:provider/provider.dart';
import 'package:rent2buy/di/service_locator.dart';
import 'package:rent2buy/networking/network_status.dart';
import 'package:rent2buy/screens/common/forget/forget_password_step_1.dart';
import 'package:rent2buy/screens/common/verification/otp_verify.dart';
import 'package:rent2buy/services/User/auth.dart';
import 'package:flutter/gestures.dart';
import 'package:flutter/material.dart';
import 'package:rent2buy/models/User/userLogin.dart';
import 'package:rent2buy/services/User/fblogin.dart';
import 'package:rent2buy/utils/data_manager.dart';
import 'package:rent2buy/widgets/shared_widgets.dart';
import 'package:font_awesome_flutter/font_awesome_flutter.dart';
import '../error_screen.dart';
import '../loading.dart';
import 'login_viewmodel.dart';
import "package:rent2buy/utils/constants.dart" as Constants;

class Login extends StatefulWidget {
  final LoginViewModel viewModel = serviceLocator<LoginViewModel>();
  final DataManager dataManager = serviceLocator<DataManager>();
  static final String routeName = "/login";

  @override
  _LoginState createState() => _LoginState();
}

class _LoginState extends State<Login> {
  final globalKey = GlobalKey<ScaffoldState>();
  final _key = GlobalKey<FormState>();
  final Map<String, dynamic> params = Map();
  UserLogin userLogin = UserLogin();

  @override
  Widget build(BuildContext context) {
    return ChangeNotifierProvider<LoginViewModel>(
      create: (context) => widget.viewModel,
      child: Consumer<LoginViewModel>(
        builder: (context, value, _) {
          return Scaffold(
            resizeToAvoidBottomInset: true,
            body: Builder(
              builder: (context) => Stack(
                children: [
                  SingleChildScrollView(
                    child: Container(
                      child: Column(
                        children: [
                          Container(
                            height: 200.0,
                            color: Colors.yellow[100],
                            child: Center(
                              child: Padding(
                                padding:
                                    const EdgeInsets.fromLTRB(28.0, 48, 28, 0),
                                child: Image.asset('assets/Rent2S.png'),
                              ),
                            ),
                          ),
                          SizedBox(height: 20),
                          Text(
                            "Welcome Back!",
                            style: TextStyle(
                                fontSize: 40, fontWeight: FontWeight.w800),
                          ),
                          SizedBox(height: 30),
                          Form(
                            key: _key,
                            child: Padding(
                              padding:
                                  const EdgeInsets.only(left: 20.0, right: 20),
                              child: Column(
                                children: [
                                  AuthTextField(
                                    hinttext: 'Email Address',
                                    prefixIcon: Icon(Icons.email_outlined),
                                    isPassword: false,
                                    isNumber: false,
                                    color: Color(0xFFF7A74D),
                                    validator: (String value) {
                                      Pattern pattern =
                                          r'^(([^<>()[\]\\.,;:\s@\"]+(\.[^<>()[\]\\.,;:\s@\"]+)*)|(\".+\"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$';
                                      RegExp regexp = RegExp(pattern);
                                      if (value.isEmpty) {
                                        return 'Please enter email';
                                      } else if (!regexp.hasMatch(value)) {
                                        return 'Please enter valid Email';
                                      }
                                      return null;
                                    },
                                    onSaved: (String value) {
                                      params[Constants.email] = value;
                                      userLogin.email = value;
                                    },
                                  ),
                                  SizedBox(height: 10),
                                  AuthTextField(
                                    hinttext: 'Password',
                                    prefixIcon: Icon(Icons.lock_outline),
                                    isPassword: true,
                                    isNumber: false,
                                    color: Color(0xFFF7A74D),
                                    validator: (String value) {
                                      if (value.isEmpty) {
                                        return 'Please enter password';
                                      } else if (value.length <= 7) {
                                        return 'Password should be greater than 8';
                                      }
                                      return null;
                                    },
                                    onSaved: (String value) {
                                      params[Constants.password] = value;
                                      userLogin.password = value;
                                    },
                                  )
                                ],
                              ),
                            ),
                          ),
                          Padding(
                            padding: const EdgeInsets.only(right: 20),
                            child: Row(
                              mainAxisAlignment: MainAxisAlignment.end,
                              children: [
                                FlatButton(
                                    onPressed: () {
                                      Navigator.pushNamed(context,
                                          ForgetPasswordStep1Screeen.routeName);
                                    },
                                    child: Text(
                                      'Forget Password?',
                                      style: TextStyle(color: Colors.grey[500]),
                                    )),
                              ],
                            ),
                          ),
                          MyButton(
                              text: "Submit",
                              color: Color(0xFF97080E),
                              onPressed: () async {
                                if (_key.currentState.validate()) {
                                  _key.currentState.save();
                                  params['device_token'] =
                                      await widget.dataManager.getFCMToken();
                                  widget.viewModel.login(params);
                                }
                              }),
                          SizedBox(height: 20),
                          Text('OR',
                              style: TextStyle(fontWeight: FontWeight.bold)),
                          SizedBox(height: 20),
                          Row(
                            mainAxisAlignment: MainAxisAlignment.center,
                            children: [
                              RawMaterialButton(
                                  child: FaIcon(FontAwesomeIcons.facebookF,
                                      size: 24, color: Colors.white),
                                  fillColor: Colors.blue[900],
                                  shape: CircleBorder(),
                                  onPressed: () {
                                    initiateFacebookLogin(context);
                                  }),
                              SizedBox(width: 20),
                              RawMaterialButton(
                                  child: FaIcon(FontAwesomeIcons.googlePlusG,
                                      size: 24, color: Colors.white),
                                  fillColor: Colors.red,
                                  shape: CircleBorder(),
                                  onPressed: () {
                                    signInUserWithGoogle(context);
                                  }),
                            ],
                          ),
                          SizedBox(height: 20),
                          MyButton(
                            text: 'Guest Login',
                            color: Color(0xFF97080E),
                            onPressed: () {
                              Navigator.pushNamed(context, '/home');
                            },
                          ),
                        ],
                      ),
                    ),
                  ),
                  observeLogin(value, context),
                ],
              ),
            ),
            bottomNavigationBar: Container(
              height: 50,
              child: Column(
                children: [
                  Row(
                    children: [
                      Expanded(
                        child: Divider(
                          color: Color(0xFFF7A74D),
                          height: 5.0,
                          thickness: 5,
                        ),
                      ),
                    ],
                  ),
                  SizedBox(height: 10),
                  Padding(
                    padding: const EdgeInsets.only(bottom: 10.0),
                    child: RichText(
                      text: TextSpan(
                        children: <TextSpan>[
                          TextSpan(
                              text: 'Not a Member Yet? ',
                              style: TextStyle(
                                  fontSize: 18,
                                  fontFamily: 'Dosis',
                                  color: Color(0xFF97080E),
                                  fontWeight: FontWeight.bold)),
                          TextSpan(
                              text: 'Register with us',
                              style: TextStyle(
                                  fontFamily: 'Dosis',
                                  fontSize: 18,
                                  color: Color(0xFFF7A74D),
                                  fontWeight: FontWeight.bold),
                              recognizer: TapGestureRecognizer()
                                ..onTap = () {
                                  Navigator.pushNamed(context, '/signup');
                                }),
                        ],
                      ),
                    ),
                  ),
                ],
              ),
            ),
          );
        },
      ),
    );
  }

  Widget observeLogin(LoginViewModel value, BuildContext context) {
    switch (value.loginEvent.status) {
      case NetworkStatus.LOADING:
        return LoadingScreen(isCustom: false);
      case NetworkStatus.ERROR:
        if (value.loginEvent.message == "Email is not verified") {
          WidgetsBinding.instance.addPostFrameCallback((_) {
            Navigator.push(
                context,
                MaterialPageRoute(
                    builder: (context) => OtpVerify(email: params['email'])));
          });
        }
        return ErrorScreen(value.loginEvent.message);
      case NetworkStatus.SUCCESS:
        //Save User
        widget.dataManager.saveUser(value.loginEvent.response);
        WidgetsBinding.instance.addPostFrameCallback((_) {
          if (value.loginEvent.response.isSeller == 0) {
            widget.dataManager.saveUserRole(Constants.BUYER);
            Navigator.pushReplacementNamed(context, '/home');
          } else {
            widget.dataManager.saveUserRole(Constants.SELLER);
            print("seller home");
            Navigator.pushReplacementNamed(context, '/seller_home');
          }
        });
        break;
    }
    return Container();
  }
}
