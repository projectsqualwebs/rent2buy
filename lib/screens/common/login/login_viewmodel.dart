import 'package:dio/dio.dart';
import 'package:flutter/foundation.dart';
import 'package:rent2buy/di/service_locator.dart';
import 'package:rent2buy/models/user_model.dart';
import 'package:rent2buy/models/userinfo.dart';
import 'package:rent2buy/networking/network_exceptions.dart';
import 'package:rent2buy/networking/network_response.dart';
import 'package:rent2buy/networking/repository/account_repository.dart';
import 'package:rent2buy/utils/base_change_notifier.dart';
import 'package:rent2buy/utils/strings.dart';

class LoginViewModel extends BaseChangeNotifier {
  NetworkResponse<Userinfo> loginEvent;
  final AccountRepository _accountRepository =
      serviceLocator<AccountRepository>();

  LoginViewModel() {
    setLoginState(NetworkResponse.idle());
  }

  void setLoginState(NetworkResponse<Userinfo> response) {
    loginEvent = response;
    notifyListeners();
  }

  Future<String> login(Map<String, dynamic> params) {
    setLoginState(NetworkResponse.loading());
    try {
      _accountRepository
          .loginUser(params)
          .then(
              (value) => setLoginState(NetworkResponse.success(value.response)))
          .catchError((e) {
        print(e);
        setLoginState(NetworkResponse.error(parseError(e)));
      });
    } catch (exception) {
      print(exception);
      setLoginState(NetworkResponse.error(exception.toString()));
    }
  }
}
