import 'package:flutter/material.dart';
import 'package:rent2buy/di/service_locator.dart';
import 'package:rent2buy/models/common_repository.dart';
import 'package:rent2buy/models/notification_response.dart';
import 'package:rent2buy/networking/network_responselist.dart';
import 'package:rent2buy/utils/base_change_notifier.dart';

class NotificationViewModel extends BaseChangeNotifier {
  final CommonRepository paymentRepository = serviceLocator<CommonRepository>();

  ValueNotifier<NetworkResponseList<NotificationResponse>> notificationValue =
      ValueNotifier(NetworkResponseList.idle());

  void getNotifications() {
    notificationValue.value = NetworkResponseList.loading();
    paymentRepository.getNotifications().then((response) {
      notificationValue.value = NetworkResponseList.success(response.response);
    }).catchError((e) {
      notificationValue.value = NetworkResponseList.error(parseError(e));
    });
  }
}
