import 'package:flutter/material.dart';
import 'package:rent2buy/utils/colors.dart';

class CircularProgress extends StatelessWidget {
  CircularProgress({Key key}) : super(key: key);
  @override
  Widget build(BuildContext context) {
    FocusManager.instance.primaryFocus.unfocus();
    return Container(
      alignment: Alignment.center,
      child: CircularProgressIndicator(
        value: null,
        backgroundColor: null,
      ),
    );
  }
}
