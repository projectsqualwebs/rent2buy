import 'package:flutter/material.dart';
import 'package:fluttertoast/fluttertoast.dart';
import 'package:rent2buy/di/service_locator.dart';
import 'package:rent2buy/networking/network_status.dart';
import 'package:rent2buy/screens/common/base_app_bar.dart';
import 'package:rent2buy/screens/common/reset/reset_password_viewmodel.dart';
import 'package:rent2buy/screens/splashscreen.dart';
import 'package:rent2buy/services/User/forget_pwd.dart';
import 'package:rent2buy/theme/custom_theme.dart';
import 'package:rent2buy/utils/colors.dart';
import 'package:rent2buy/utils/constants.dart';
import 'package:rent2buy/utils/data_manager.dart';
import 'package:rent2buy/utils/design_utils.dart';
import 'package:rent2buy/utils/dimens.dart';
import 'package:rent2buy/utils/strings.dart';
import 'package:rent2buy/widgets/shared_widgets.dart';

class ResetPasswordScreen extends StatefulWidget {
  static final String routeName = "/reset_password";
  final ResetPasswordViewModel viewModel =
      serviceLocator<ResetPasswordViewModel>();
  final DataManager dataManager = serviceLocator<DataManager>();

  @override
  State<StatefulWidget> createState() {
    // TODO: implement createState
    return _ResetPasswordState();
  }
}

class _ResetPasswordState extends State<ResetPasswordScreen> {
  TextEditingController _currentPwd = TextEditingController();
  TextEditingController _newPwd = TextEditingController();
  TextEditingController _confirmPwd = TextEditingController();
  bool isLoading = false;

  var _keyForm = GlobalKey<FormState>();
  int currentRole = 0;
  @override
  void initState() {
    // TODO: implement initState
    super.initState();
    getRole();
    WidgetsBinding.instance.addPostFrameCallback((_) {
      widget.viewModel.resetValue.addListener(() {
        var response = widget.viewModel.resetValue.value;
        switch (response.status) {
          case NetworkStatus.LOADING:
            setLoading(true);
            break;
          case NetworkStatus.ERROR:
            setLoading(false);
            Fluttertoast.showToast(msg: response.message);
            break;
          case NetworkStatus.SUCCESS:
            setLoading(false);
            Fluttertoast.showToast(msg: response.message);
            Navigator.of(context).pushNamedAndRemoveUntil(
                SplashScreen.routeName, (Route<dynamic> route) => false);
            break;
        }
      });
    });
  }

  void setLoading(bool bool) {
    FocusManager.instance.primaryFocus.unfocus();
    if (this.mounted) {
      setState(() {
        isLoading = bool;
      });
    }
  }

  @override
  Widget build(BuildContext context) {
    return Theme(
      data: currentRole == BUYER
          ? CustomTheme.buyerTheme
          : CustomTheme.sellerTheme,
      child: SafeArea(
        child: Scaffold(
          appBar: CommonAppBar(
            appBar: AppBar(),
            title: Strings.appName,
            automaticallyImplyLeading: false,
            centerTitle: true,
          ),
          body: SingleChildScrollView(
            child: Container(
              width: double.infinity,
              padding: EdgeInsets.symmetric(horizontal: 40),
              child: Form(
                key: _keyForm,
                child: Column(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: [
                    SizedBox(height: 60),
                    Padding(
                      padding: EdgeInsets.only(left: 12.0),
                      child: Text('Reset Password',
                          style: TextStyle(
                              fontFamily: 'Dosis',
                              fontWeight: FontWeight.w700,
                              fontSize: 24)),
                    ),
                    SizedBox(height: 40),
                    TextFormField(
                      validator: (value) {
                        if (value.isEmpty) {
                          return Strings.fieldRequired;
                        }
                        return null;
                      },
                      controller: _currentPwd,
                      obscureText: true,
                      decoration: InputDecoration(
                        hintText: Strings.currentPassword,
                      ),
                      keyboardType: TextInputType.visiblePassword,
                    ),
                    SizedBox(height: 40),
                    TextFormField(
                      validator: (value) {
                        if (value.isEmpty) {
                          return Strings.fieldRequired;
                        }
                        return null;
                      },
                      controller: _newPwd,
                      obscureText: true,
                      decoration:
                          InputDecoration(hintText: Strings.newPassword),
                      keyboardType: TextInputType.visiblePassword,
                    ),
                    SizedBox(height: 40),
                    TextFormField(
                      validator: (value) {
                        if (value.isEmpty) {
                          return Strings.fieldRequired;
                        }
                        return null;
                      },
                      controller: _confirmPwd,
                      obscureText: true,
                      decoration:
                          InputDecoration(hintText: Strings.confirmPassword),
                      keyboardType: TextInputType.visiblePassword,
                    ),
                    SizedBox(height: 60),
                    Center(
                        child: isLoading
                            ? DesignUtils.getProgressButton()
                            : MyButton(
                                text: 'Reset',
                                onPressed: () {
                                  if (_keyForm.currentState.validate()) {
                                    Map<String, dynamic> params = Map();
                                    params[currentPassword] = _currentPwd.text;
                                    params[newPassword] = _newPwd.text;
                                    params[confirmPassword] = _confirmPwd.text;
                                    widget.viewModel.resetPassword(params);
                                  }
                                })),
                    SizedBox(
                      height: margin_extra_large,
                    ),
                    Center(
                      child: InkWell(
                        onTap: () {
                          Navigator.pop(context);
                        },
                        child: Text(
                          Strings.cancel,
                          style: DesignUtils.textApperanceRegular(),
                        ),
                      ),
                    )
                  ],
                ),
              ),
            ),
          ),
        ),
      ),
    );
  }

  void getRole() async {
    currentRole = await widget.dataManager.getUserRole();
  }
}
