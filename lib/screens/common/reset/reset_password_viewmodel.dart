import 'package:flutter/material.dart';
import 'package:rent2buy/di/service_locator.dart';
import 'package:rent2buy/models/common_repository.dart';
import 'package:rent2buy/networking/network_response.dart';
import 'package:rent2buy/utils/base_change_notifier.dart';

class ResetPasswordViewModel extends BaseChangeNotifier {
  final CommonRepository _commonRepository = serviceLocator<CommonRepository>();

  ValueNotifier<NetworkResponse<String>> resetValue =
      ValueNotifier(NetworkResponse.idle());

  void resetPassword(Map<String, dynamic> params) {
    resetValue.value = NetworkResponse.loading();
    _commonRepository.resetPassword(params).then((response) {
      resetValue.value = NetworkResponse.successWithMsg(null, response.message);
    }).catchError((e) {
      resetValue.value = NetworkResponse.error(parseError(e));
    });
  }
}
