import 'package:flutter/material.dart';
import 'package:rent2buy/screens/buyer/home/home.dart';
import 'package:rent2buy/screens/buyer/orders/my_orders.dart';
import 'package:rent2buy/screens/common/base_app_bar.dart';
import 'package:rent2buy/utils/design_utils.dart';
import 'package:rent2buy/utils/dimens.dart';
import 'package:rent2buy/utils/strings.dart';

class SuccessScreen extends StatefulWidget {
  static final String routeName = '/success_screen';
  @override
  State<StatefulWidget> createState() {
    return _SuccessScreenState();
  }
}

class _SuccessScreenState extends State<SuccessScreen> {
  final _keyScafold = GlobalKey<ScaffoldState>();

  @override
  void initState() {
    // TODO: implement initState
    super.initState();
    Future.delayed(Duration(seconds: 2), () {
      Navigator.of(_keyScafold.currentContext)
          .pushReplacementNamed(Home.routeName);
      Navigator.of(_keyScafold.currentContext).pushNamed(MyOrders.routeName);
    });
  }

  @override
  Widget build(BuildContext context) {
    return WillPopScope(
      onWillPop: () async => false,
      child: Scaffold(
          key: _keyScafold,
          appBar: AppBar(
              centerTitle: true,
              leading: null,
              automaticallyImplyLeading: false,
              title: Text(
                'R2B',
                style: DesignUtils.textApperanceHeader(),
              )),
          body: Container(
            alignment: Alignment.center,
            child: Column(
              mainAxisAlignment: MainAxisAlignment.center,
              crossAxisAlignment: CrossAxisAlignment.center,
              children: [
                Image(
                  height: 48,
                  width: 48,
                  image: AssetImage('assets/icons/checked.png'),
                ),
                SizedBox(
                  height: margin_large,
                ),
                Text(
                  Strings.msgOrderPlaced,
                  style: DesignUtils.textApperanceLarge(),
                )
              ],
            ),
          )),
    );
  }
}
