import 'package:flutter/material.dart';
import 'package:rent2buy/di/service_locator.dart';
import 'package:rent2buy/models/mutable_live_data.dart';
import 'package:rent2buy/models/userinfo.dart';
import 'package:rent2buy/networking/network_response.dart';
import 'package:rent2buy/networking/network_responselist.dart';
import 'package:rent2buy/networking/repository/account_repository.dart';
import 'package:rent2buy/utils/base_change_notifier.dart';

class OTPVerifyViewModel extends BaseChangeNotifier {
  MutableLiveData<NetworkResponse<String>> verifyLiveData;
  NetworkResponse<String> verifyEvent;
  AccountRepository _accountRepository = serviceLocator<AccountRepository>();
  ValueNotifier<NetworkResponse<String>> resendValue =
      ValueNotifier(NetworkResponse.idle());

  ValueNotifier<NetworkResponse<Userinfo>> verifyValue =
      ValueNotifier(NetworkResponse.idle());

  OTPVerifyViewModel() {
    setVerifyState(NetworkResponse.idle());
  }

  void setVerifyState(NetworkResponse<String> response) {
    verifyEvent = response;
    notifyListeners();
  }

  void verifyOTP(Map<String, dynamic> params) {
//    verifyLiveData.postValue(value)

    verifyValue.value = NetworkResponse.loading();
    try {
      _accountRepository.verifyOTP(params).then((value) {
        verifyValue.value = NetworkResponse.successWithMsg(value.response,value.message);
      }).catchError((e) {
        verifyValue.value = NetworkResponse.error(parseError(e));
      });
    } catch (exception) {
      verifyValue.value = NetworkResponse.error(parseError(exception));
    }
  }

  void resendOTP(Map<String, dynamic> params) {
//    verifyLiveData.postValue(value)
    resendValue.value = NetworkResponse.loading();
    try {
      _accountRepository.resendOTP(params).then((value) {
        resendValue.value = NetworkResponse.success(value.message);
      }).catchError((e) {
        resendValue.value = NetworkResponse.error(parseError(e));
      });
    } catch (exception) {
      resendValue.value = NetworkResponse.error(parseError(exception));
    }
  }
}
