import 'package:flutter/material.dart';
import 'package:fluttertoast/fluttertoast.dart';
import 'package:rent2buy/di/service_locator.dart';
import 'package:rent2buy/models/Verification/verifyOtp.dart';
import 'package:rent2buy/models/userinfo.dart';
import 'package:rent2buy/networking/network_response.dart';
import 'package:rent2buy/networking/network_status.dart';
import 'package:rent2buy/screens/buyer/home/home.dart';
import 'package:rent2buy/screens/common/base_app_bar.dart';
import 'package:rent2buy/screens/common/error_screen.dart';
import 'package:rent2buy/screens/common/loading.dart';
import 'package:rent2buy/screens/common/loading_dialog.dart';
import 'package:rent2buy/screens/common/verification/OTPVerifyViewModel.dart';
import 'package:rent2buy/screens/splashscreen.dart';
import 'package:rent2buy/services/User/verification.dart';
import 'package:rent2buy/utils/colors.dart';
import 'package:rent2buy/utils/constants.dart';
import 'package:rent2buy/utils/data_manager.dart';
import 'package:rent2buy/utils/design_utils.dart';
import 'package:rent2buy/utils/strings.dart';
import 'package:rent2buy/utils/utils.dart';
import 'package:rent2buy/widgets/shared_widgets.dart';

class OtpVerify extends StatefulWidget {
  final String email;
  DataManager dataManager = serviceLocator<DataManager>();
  OTPVerifyViewModel viewModel = serviceLocator<OTPVerifyViewModel>();

  OtpVerify({Key key, @required this.email});

  @override
  State<StatefulWidget> createState() {
    // TODO: implement createState
    return _OTPVerifyState();
  }
}

class _OTPVerifyState extends State<OtpVerify> {
  TextEditingController _otp = TextEditingController();
  VerifyOtp verifyOtp = VerifyOtp();
  GlobalKey<ScaffoldState> _scaffoldKey = GlobalKey<ScaffoldState>();
  GlobalKey<State> _keyLoader = GlobalKey<State>();

  @override
  void initState() {
    // TODO: implement initState
    super.initState();
    Map<String, Object> params = Map();
    params['email'] = widget.email;
    widget.viewModel.resendOTP(params);
    widget.viewModel.resendValue.addListener(() {
      var response = widget.viewModel.resendValue;
      if (response.value.status == NetworkStatus.SUCCESS) {
        
      }
    });
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      key: _scaffoldKey,
      appBar: CommonAppBar(
        appBar: AppBar(),
        title: Strings.appName,
        automaticallyImplyLeading: false,
        centerTitle: true,
      ),
      body: Container(
        padding: EdgeInsets.symmetric(horizontal: 40),
        child: ValueListenableBuilder(
          valueListenable: widget.viewModel.verifyValue,
          builder: (context, value, _) {
            var response = value as NetworkResponse<Userinfo>;
            if (response.status == NetworkStatus.LOADING) {
              WidgetsBinding.instance.addPostFrameCallback((_) {
                Dialogs.showLoadingDialog(
                    context, _keyLoader, Strings.msgConfirmingOTP);
              });
            } else if (response.status == NetworkStatus.ERROR) {
              WidgetsBinding.instance.addPostFrameCallback((_) {
                Navigator.of(_keyLoader.currentContext, rootNavigator: true)
                    .pop();
              });
              Fluttertoast.showToast(msg: Strings.txtOTPNotReceived);
            } else if (response.status == NetworkStatus.SUCCESS) {
              WidgetsBinding.instance.addPostFrameCallback((_) {
                Navigator.of(_keyLoader.currentContext, rootNavigator: true)
                    .pop();
                Fluttertoast.showToast(msg: response.message);
                widget.dataManager.saveUser(response.response);
                if (response.response.isSeller == 0) {
                  Navigator.of(context).pushNamedAndRemoveUntil(
                      Home.routeName, (Route<dynamic> route) => false);
                } else {
                  Navigator.of(context).pushNamedAndRemoveUntil(
                      '/seller_home', (Route<dynamic> route) => false);
                }
              });
              return _buildForm();
            }
            return _buildForm();
          },
        ),
      ),
    );
  }

  void sendOTP() {
    Map<String, dynamic> params = Map();
    params[email] = widget.email;
    widget.viewModel.resendOTP(params);
  }

  Widget _buildForm() {
    return Column(
      children: [
        SizedBox(height: 60),
        CustomTextField(
            hinttext: Strings.enterOTP,
            isPassword: false,
            borderColor: Color(0xFF97080E),
            isNumber: true,
            textEditingController: _otp),
        SizedBox(height: 40),
        Row(
          children: [
            Text(
              Strings.txtOTPNotReceived,
              style: DesignUtils.textApperanceRegular(),
            ),
            InkWell(
              onTap: () {
                sendOTP();
              },
              child: Text(
                Strings.sendAgain,
                style: DesignUtils.textApperanceRegular(),
              ),
            )
          ],
        ),
        SizedBox(height: 40),
        Center(
          child: MyButton(
            text: Strings.verify,
            color: Color(0xFF97080E),
            onPressed: () {
              verifyOtp.email = widget.email;
              verifyOtp.otp = int.parse(_otp.text);
              print(verifyOtp.email);
              print(verifyOtp.otp);
              Map<String, dynamic> params = Map();
              params['email'] = widget.email;
              params['otp'] = int.parse(_otp.text);
              widget.viewModel.verifyOTP(params);
            },
          ),
        ),
      ],
    );
  }
}
