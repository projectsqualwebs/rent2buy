import 'package:flutter/material.dart';
import 'package:rent2buy/utils/utils.dart';

class RoundedBorderSpinner extends StatelessWidget {
  final List<String> items;
  final ValueChanged<String> _valueChanged;

  RoundedBorderSpinner(this.items, this._valueChanged);

  @override
  Widget build(BuildContext context) {
    return Center(
      child: Container(
          padding: EdgeInsets.symmetric(horizontal: 10.0),
          decoration: BoxDecoration(
            borderRadius: BorderRadius.circular(15.0),
            border: Border.all(
                color: Colors.red, style: BorderStyle.solid, width: 0.80),
          ),
          child: new DropdownButton<String>(
            underline: SizedBox(),
            items: items.map((x) {
              return new DropdownMenuItem<String>(
                value: x,
                child: Text(Utils.getQty(x)),
              );
            }).toList(),
            onChanged: _valueChanged,
          )),
    );
  }
}
