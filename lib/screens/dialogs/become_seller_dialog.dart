import 'package:flutter/material.dart';
import 'package:flutter/scheduler.dart';
import 'package:fluttertoast/fluttertoast.dart';
import 'package:rent2buy/di/service_locator.dart';
import 'package:rent2buy/models/common_item.dart';
import 'package:rent2buy/models/userinfo.dart';
import 'package:rent2buy/networking/network_status.dart';
import 'package:rent2buy/screens/custom/progress_button.dart';
import 'package:rent2buy/screens/dialogs/become_seller_viewmodel.dart';
import 'package:rent2buy/screens/splashscreen.dart';
import 'package:rent2buy/utils/colors.dart';
import 'package:rent2buy/utils/constants.dart';
import 'package:rent2buy/utils/data_manager.dart';
import 'package:rent2buy/utils/dimens.dart';
import 'package:rent2buy/utils/strings.dart';

class BecomeSellerDialog extends StatefulWidget {
  final BecomeSellerViewModel viewModel =
      serviceLocator<BecomeSellerViewModel>();
  final DataManager dataManager = serviceLocator<DataManager>();

  @override
  State<StatefulWidget> createState() {
    return _BecomeSellerState();
  }
}

class _BecomeSellerState extends State<BecomeSellerDialog> {
  TextEditingController _companyController = TextEditingController();
  List<CommonItem> _items = [CommonItem(1, 'Yes'), CommonItem(2, 'No')];
  int _isTaxable = 0;
  String radioItemHolder;
  final _formKey = GlobalKey<FormState>();

  var isLoading = false;
  TextEditingController _vatController = TextEditingController();
  Userinfo userinfo;

  @override
  void initState() {
    // TODO: implement initState
    super.initState();
    getUser();

    widget.viewModel.updateValue.addListener(() {
      var response = widget.viewModel.updateValue.value;
      switch (response.status) {
        case NetworkStatus.LOADING:
          isLoading = true;
          break;
        case NetworkStatus.ERROR:
          isLoading = false;
          Fluttertoast.showToast(
              msg: response.message,
              toastLength: Toast.LENGTH_SHORT,
              gravity: ToastGravity.CENTER,
              timeInSecForIosWeb: 1,
              backgroundColor: Colors.grey,
              textColor: Colors.white,
              fontSize: 16.0);
          break;
        case NetworkStatus.SUCCESS:
          isLoading = false;
          Navigator.of(context, rootNavigator: true).pop('dialog');
          SchedulerBinding.instance.addPostFrameCallback((_) {
            Navigator.of(context).pushNamedAndRemoveUntil(
                SplashScreen.routeName, (Route<dynamic> route) => false);
          });
          break;
      }
      setState(() {});
    });
    widget.viewModel.becomeValue.addListener(() {
      var response = widget.viewModel.becomeValue.value;
      if (response.status == NetworkStatus.SUCCESS) {
        callUpdateSeller();
      } else if (response.status == NetworkStatus.ERROR) {
        isLoading = false;
        Fluttertoast.showToast(
            msg: response.message,
            toastLength: Toast.LENGTH_SHORT,
            gravity: ToastGravity.CENTER,
            timeInSecForIosWeb: 1,
            backgroundColor: Colors.grey,
            textColor: Colors.white,
            fontSize: 16.0);
      } else if (response.status == NetworkStatus.LOADING) {
        isLoading = true;
      }
      setState(() {});
    });
  }

  @override
  Widget build(BuildContext context) {
    return Dialog(
      shape: RoundedRectangleBorder(
        borderRadius: BorderRadius.circular(default_radius),
      ),
      elevation: 0,
      backgroundColor: Colors.transparent,
      child: contentBox(context),
    );
  }

  contentBox(context) {
    return Stack(
      children: <Widget>[
        Container(
          padding: EdgeInsets.all(padding_large),
          margin: EdgeInsets.only(top: avatarRadius),
          decoration: BoxDecoration(
              shape: BoxShape.rectangle,
              color: Colors.white,
              borderRadius: BorderRadius.circular(padding_medium),
              boxShadow: [
                BoxShadow(
                    color: Colors.black, offset: Offset(0, 10), blurRadius: 10),
              ]),
          child: SingleChildScrollView(
            child: Column(
              mainAxisSize: MainAxisSize.min,
              children: <Widget>[
                Text(
                  'Company Details',
                  style: TextStyle(fontSize: 22, fontWeight: FontWeight.w600),
                ),
                SizedBox(
                  height: margin_large,
                ),
                Text(
                  'We need few more details to create your seller profile.',
                  style: TextStyle(fontSize: 14, fontWeight: FontWeight.w400),
                ),
                SizedBox(
                  height: 15,
                ),
                Form(
                  key: _formKey,
                  child: Column(
                    children: [
                      TextFormField(
                        validator: (value) {
                          if (value.isEmpty) {
                            return Strings.fieldRequired;
                          }
                          return null;
                        },
                        controller: _companyController,
                        decoration: InputDecoration(
                          hintText: Strings.companyDetails,
                        ),
                        keyboardType: TextInputType.text,
                      ),
                      SizedBox(
                        height: margin_medium,
                      ),
                      TextFormField(
                        validator: (value) {
                          if (value.isEmpty) {
                            return Strings.fieldRequired;
                          }
                          return null;
                        },
                        controller: _vatController,
                        decoration: InputDecoration(
                          hintText: Strings.vat,
                        ),
                        keyboardType: TextInputType.number,
                      ),
                    ],
                  ),
                ),
                SizedBox(
                  height: margin_medium,
                ),
                Text(
                  Strings.taxable,
                  style: TextStyle(fontSize: 14),
                  textAlign: TextAlign.start,
                ),
                Row(
                  mainAxisAlignment: MainAxisAlignment.start,
                  children: [
                    SizedBox(
                      width: 120,
                      child: ListTile(
                        title: Text('Yes'),
                        leading: Radio(
                            value: 1,
                            groupValue: _isTaxable,
                            onChanged: _isTaxableChanges),
                      ),
                    ),
                    SizedBox(
                      width: 120,
                      child: ListTile(
                        title: Text('No'),
                        leading: Radio(
                            value: 0,
                            groupValue: _isTaxable,
                            onChanged: _isTaxableChanges),
                      ),
                    ),
                  ],
                ),
                AnimatedCrossFade(
                  crossFadeState: isLoading
                      ? CrossFadeState.showSecond
                      : CrossFadeState.showFirst,
                  duration: Duration(milliseconds: 250),
                  firstChild: ElevatedButton(
                    style: ElevatedButton.styleFrom(
                        elevation: 0, primary: colorPrimary),
                    child: Text(Strings.becomeSeller,
                        style: TextStyle(
                            fontSize: 14, fontWeight: FontWeight.w700)),
                    onPressed: () {
                      if (_formKey.currentState.validate()) {
                        widget.viewModel.becomeSeller();
                      }
                    },
                  ),
                  secondChild: SizedBox(
                      height: minButtonHeight,
                      width: minButtonHeight, // needs to be a square container
                      child: Padding(
                        padding: EdgeInsets.all(8),
                        child: CircularProgressIndicator(
                          valueColor:
                              AlwaysStoppedAnimation<Color>(colorAccent),
                          strokeWidth: 3,
                        ),
                      )),
                )
              ],
            ),
          ),
        ),
      ],
    );
  }

  void _isTaxableChanges(int value) {
    setState(() {
      _isTaxable = value;
    });
  }

  void callUpdateSeller() {
    Map<String, dynamic> companyParams = Map();
    companyParams[companyName] = _companyController.text;
    companyParams[vatNumber] = _vatController.text;

    Map<String, dynamic> params = Map();
    params[firstName] = userinfo.firstName;
    params[lastName] = userinfo.lastName;
    params[isCompany] = 1;
    params[dateOfBirth] = userinfo.dob;
    params[company] = companyParams;
    params[address] = userinfo.address.firstWhere((x) {
      return x.defaultAddress == 1;
    }, orElse: () {
      return null;
    });

    widget.viewModel.updateSeller(params);
  }

  void getUser() async {
    userinfo = Userinfo.fromJson(await widget.dataManager.getUser());
  }
}
