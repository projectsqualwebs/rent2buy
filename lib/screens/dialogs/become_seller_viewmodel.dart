import 'package:flutter/material.dart';
import 'package:rent2buy/networking/network_response.dart';
import 'package:rent2buy/networking/repository/seller_repository.dart';
import 'package:rent2buy/utils/base_change_notifier.dart';
import 'package:flutter/material.dart';
import 'package:rent2buy/di/service_locator.dart';
import 'package:rent2buy/utils/base_change_notifier.dart';

class BecomeSellerViewModel extends BaseChangeNotifier {
  ValueNotifier<NetworkResponse<String>> updateValue =
      ValueNotifier(NetworkResponse.idle());

  ValueNotifier<NetworkResponse<String>> becomeValue =
      ValueNotifier(NetworkResponse.idle());

  SellerRepository _commonRepository = serviceLocator<SellerRepository>();

  void becomeSeller() {
    becomeValue.value = NetworkResponse.loading();
    _commonRepository.becomeSeller().then((response) {
      becomeValue.value = NetworkResponse.success(response.message);
    }).catchError((e) {
      becomeValue.value = NetworkResponse.error(parseError(e));
    });
  }

  void updateSeller(Map<String,dynamic> params) {
    updateValue.value = NetworkResponse.loading();
    _commonRepository.updateSeller(params).then((response) {
      updateValue.value = NetworkResponse.success(response.message);
    }).catchError((e) {
      updateValue.value = NetworkResponse.error(parseError(e));
    });
  }
}
