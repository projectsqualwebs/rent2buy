import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:fluttertoast/fluttertoast.dart';
import 'package:rent2buy/di/service_locator.dart';
import 'package:rent2buy/networking/network_response.dart';
import 'package:rent2buy/networking/network_status.dart';
import 'package:rent2buy/screens/common/error_screen.dart';
import 'package:rent2buy/screens/custom/payment_card.dart';
import 'package:rent2buy/screens/custom/progress_dialog_builder.dart';
import 'package:rent2buy/screens/payment/add_card_viewmodel.dart';
import 'package:rent2buy/screens/payment/payment_screen.dart';
import 'package:rent2buy/utils/constants.dart';
import 'package:rent2buy/utils/design_utils.dart';
import 'package:rent2buy/utils/dimens.dart';
import 'package:rent2buy/utils/strings.dart';
import 'package:rent2buy/utils/colors.dart';

class AddCard extends StatefulWidget {
  static var routeName = '/add_card';
  final AddCardViewModel viewModel = serviceLocator<AddCardViewModel>();

  @override
  State<StatefulWidget> createState() {
    return _AddCardState();
  }
}

class _AddCardState extends State<AddCard> {
  var _cardHolderController = TextEditingController();
  final _formKey = GlobalKey<FormState>();
  final _scaffoldKey = GlobalKey<ScaffoldState>();

  var _cardNumberController = TextEditingController();

  var _monthController = TextEditingController();

  var _yearController = TextEditingController();

  var _cvvController = TextEditingController();

  bool isLoading = false;

  @override
  void initState() {
    // TODO: implement initState
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      key: _scaffoldKey,
      appBar: AppBar(
        title: Text(
          'Add Card',
          style: TextStyle(color: colorAccent, fontWeight: FontWeight.w700),
        ),
      ),
      body: SingleChildScrollView(
        child: Container(
          padding: EdgeInsets.all(margin_large),
          child: ValueListenableBuilder(
            valueListenable: widget.viewModel.addCardValue,
            builder: (ctx, value, _) {
              var response = value as NetworkResponse<String>;
              WidgetsBinding.instance.addPostFrameCallback((_) {
                switch (response.status) {
                  case NetworkStatus.LOADING:
                    setLoading(true);
                    break;
                  case NetworkStatus.ERROR:
                    setLoading(false);
                    Fluttertoast.showToast(msg: response.message);
                    break;
                  case NetworkStatus.SUCCESS:
                    setLoading(false);
                    Navigator.popAndPushNamed(context, PaymentScreen.routeName);
                    // Navigator.pop(
                    //     _scaffoldKey.currentContext, {resultCode: RESULT_OK});
                    break;
                }
              });

              return Form(
                key: _formKey,
                child: Column(
                  children: [
                    TextFormField(
                      validator: (value) {
                        if (value.isEmpty) {
                          return Strings.fieldRequired;
                        }
                        return null;
                      },
                      controller: _cardHolderController,
                      decoration: InputDecoration(
                          hintText: "Card Holder Name",
                          focusedErrorBorder: DesignUtils.getDefaultBorder(),
                          errorBorder: DesignUtils.getDefaultBorder(),
                          focusedBorder: DesignUtils.getDefaultBorder(),
                          enabledBorder: DesignUtils.getDefaultBorder()),
                      keyboardType: TextInputType.text,
                    ),
                    SizedBox(
                      height: margin_large,
                    ),
                    TextFormField(
                      validator: (value) {
                        if (value.isEmpty) {
                          return Strings.fieldRequired;
                        }
                        return null;
                      },
                      controller: _cardNumberController,
                      inputFormatters: [
                        FilteringTextInputFormatter.digitsOnly,
                        LengthLimitingTextInputFormatter(maxCardLength),
                        CardNumberInputFormatter(),
                      ],
                      decoration: InputDecoration(
                          hintText: "Card Number",
                          focusedErrorBorder: DesignUtils.getDefaultBorder(),
                          errorBorder: DesignUtils.getDefaultBorder(),
                          focusedBorder: DesignUtils.getDefaultBorder(),
                          enabledBorder: DesignUtils.getDefaultBorder()),
                      keyboardType: TextInputType.number,
                    ),
                    SizedBox(
                      height: margin_large,
                    ),
                    Row(
                      children: [
                        Flexible(
                          flex: 1,
                          child: TextFormField(
                            validator: (value) {
                              if (value.isEmpty) {
                                return Strings.fieldRequired;
                              }
                              return null;
                            },
                            controller: _monthController,
                            decoration: InputDecoration(
                                hintText: "MM",
                                focusedErrorBorder:
                                    DesignUtils.getDefaultBorder(),
                                errorBorder: DesignUtils.getDefaultBorder(),
                                focusedBorder: DesignUtils.getDefaultBorder(),
                                enabledBorder: DesignUtils.getDefaultBorder()),
                            keyboardType: TextInputType.number,
                            inputFormatters: [
                              LengthLimitingTextInputFormatter(maxLengthMonth),
                            ],
                          ),
                        ),
                        SizedBox(
                          width: margin_large,
                        ),
                        Flexible(
                          flex: 1,
                          child: TextFormField(
                            validator: (value) {
                              if (value.isEmpty) {
                                return Strings.fieldRequired;
                              }
                              return null;
                            },
                            controller: _yearController,
                            decoration: InputDecoration(
                                hintText: "YY",
                                focusedErrorBorder:
                                    DesignUtils.getDefaultBorder(),
                                errorBorder: DesignUtils.getDefaultBorder(),
                                focusedBorder: DesignUtils.getDefaultBorder(),
                                enabledBorder: DesignUtils.getDefaultBorder()),
                            keyboardType: TextInputType.number,
                            inputFormatters: [
                              LengthLimitingTextInputFormatter(maxLengthMonth),
                            ],
                          ),
                        ),
                        SizedBox(
                          width: margin_large,
                        ),
                        Flexible(
                          flex: 1,
                          child: TextFormField(
                            validator: (value) {
                              if (value.isEmpty) {
                                return Strings.fieldRequired;
                              }
                              return null;
                            },
                            controller: _cvvController,
                            decoration: InputDecoration(
                                hintText: "CVV",
                                focusedErrorBorder:
                                    DesignUtils.getDefaultBorder(),
                                errorBorder: DesignUtils.getDefaultBorder(),
                                focusedBorder: DesignUtils.getDefaultBorder(),
                                enabledBorder: DesignUtils.getDefaultBorder()),
                            keyboardType: TextInputType.number,
                            obscureText: true,
                            inputFormatters: [
                              LengthLimitingTextInputFormatter(3),
                            ],
                          ),
                        ),
                      ],
                    ),
                    SizedBox(
                      height: margin_large,
                    ),
                    isLoading
                        ? DesignUtils.getProgressButton()
                        : ElevatedButton(
                            style: ElevatedButton.styleFrom(
                                minimumSize: Size(defaultSize, defaultSize),
                                shape: RoundedRectangleBorder(
                                    borderRadius: BorderRadius.circular(
                                        defaultBtnCorner)),
                                primary: colorPrimary),
                            child: Text(Strings.proceed,
                                style: TextStyle(
                                    fontSize: textSizeLarge,
                                    color: colorAccent,
                                    fontWeight: FontWeight.w700)),
                            onPressed: () {
                              if (_formKey.currentState.validate()) {
                                Map<String, dynamic> params = Map();
                                params[cardNumber] = CardUtils.getCleanedNumber(
                                    _cardNumberController.text);
                                params[cardHolder] = _cardHolderController.text;
                                params[expiryMonth] = _monthController.text;
                                params[expiryYear] = _yearController.text;
                                params[cvc] = _cvvController.text;
                                widget.viewModel.addCard(params);
                              }
                            },
                          ),
                  ],
                ),
              );
            },
          ),
        ),
      ),
    );
  }

  void setLoading(bool bool) {
    WidgetsBinding.instance.addPostFrameCallback((_) {
      FocusManager.instance.primaryFocus.unfocus();
      if (this.mounted) {
        setState(() {
          isLoading = bool;
        });
      }
    });
  }
}

class CardNumberInputFormatter extends TextInputFormatter {
  @override
  TextEditingValue formatEditUpdate(
      TextEditingValue oldValue, TextEditingValue newValue) {
    var text = newValue.text;

    if (newValue.selection.baseOffset == 0) {
      return newValue;
    }

    var buffer = new StringBuffer();
    for (int i = 0; i < text.length; i++) {
      buffer.write(text[i]);
      var nonZeroIndex = i + 1;
      if (nonZeroIndex % 4 == 0 && nonZeroIndex != text.length) {
        buffer.write('  '); // Add double spaces.
      }
    }

    var string = buffer.toString();
    return newValue.copyWith(
        text: string,
        selection: new TextSelection.collapsed(offset: string.length));
  }
}
