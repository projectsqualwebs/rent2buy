import 'package:flutter/material.dart';
import 'package:rent2buy/di/service_locator.dart';
import 'package:rent2buy/networking/network_response.dart';
import 'package:rent2buy/networking/repository/payment_repository.dart';
import 'package:rent2buy/utils/base_change_notifier.dart';

class AddCardViewModel extends BaseChangeNotifier {

  final PaymentRepository paymentRepository =
      serviceLocator<PaymentRepository>();
  ValueNotifier<NetworkResponse<String>> addCardValue =
      ValueNotifier(NetworkResponse.idle());

  void addCard(Map<String, dynamic> params) {
    addCardValue.value = NetworkResponse.loading();
    paymentRepository.addCard(params).then((response) {
      addCardValue.value = NetworkResponse.success(response.message);
    }).catchError((e) {
      addCardValue.value = NetworkResponse.error(parseError(e));
    });
  }
}
