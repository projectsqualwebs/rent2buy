import 'package:flutter/material.dart';
import 'package:rent2buy/di/service_locator.dart';
import 'package:rent2buy/models/payment_card.dart';
import 'package:rent2buy/networking/network_response.dart';
import 'package:rent2buy/networking/network_responselist.dart';
import 'package:rent2buy/networking/repository/payment_repository.dart';
import 'package:rent2buy/utils/base_change_notifier.dart';

class CardViewModel extends BaseChangeNotifier {
  final PaymentRepository paymentRepository =
      serviceLocator<PaymentRepository>();

  ValueNotifier<NetworkResponseList<PaymentCard>> cardValue =
      ValueNotifier(NetworkResponseList.idle());
  ValueNotifier<NetworkResponse<String>> removeValue =
      ValueNotifier(NetworkResponse.idle());

  void getCards() {
    cardValue.value = NetworkResponseList.loading();
    paymentRepository.getCards().then((response) {
      cardValue.value = NetworkResponseList.success(response.response);
    }).catchError((e) {
      cardValue.value = NetworkResponseList.error(parseError(e));
    });
  }

  void markAsDefault(String cardId) {
    removeValue.value = NetworkResponse.loading();
    paymentRepository.markAsDefault(cardId).then((response) {
      removeValue.value = NetworkResponse.success(response.message);
    }).catchError((e) {
      removeValue.value = NetworkResponse.error(parseError(e));
    });
  }
}
