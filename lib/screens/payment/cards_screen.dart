import 'package:flutter/material.dart';
import 'package:fluttertoast/fluttertoast.dart';
import 'package:rent2buy/di/service_locator.dart';
import 'package:rent2buy/models/payment_card.dart';
import 'package:rent2buy/networking/network_response.dart';
import 'package:rent2buy/networking/network_responselist.dart';
import 'package:rent2buy/networking/network_status.dart';
import 'package:rent2buy/screens/common/base_app_bar.dart';
import 'package:rent2buy/screens/common/progress_view.dart';
import 'package:rent2buy/screens/payment/add_card.dart';
import 'package:rent2buy/screens/payment/card_viewmodel.dart';
import 'package:rent2buy/utils/colors.dart';
import 'package:rent2buy/utils/constants.dart';
import 'package:rent2buy/utils/design_utils.dart';
import 'package:rent2buy/utils/dimens.dart';
import 'package:rent2buy/utils/strings.dart';
import 'package:rent2buy/utils/utils.dart';
import 'package:rent2buy/widgets/shared_widgets.dart';

class CardsScreen extends StatefulWidget {
  static final String routeName = '/cards_screens';
  final CardViewModel viewModel = serviceLocator<CardViewModel>();

  @override
  State<StatefulWidget> createState() {
    return _CardScreenState();
  }
}

class _CardScreenState extends State<CardsScreen> with WidgetsBindingObserver {
  final _keyList = GlobalKey<AnimatedListState>();
  List<PaymentCard> paymentCards = new List();
  @override
  void initState() {
    // TODO: implement initState
    widget.viewModel.getCards();
    WidgetsBinding.instance.addObserver(this);
    super.initState();
  }

  @override
  void dispose() {
    WidgetsBinding.instance.removeObserver(this);
    super.dispose();
  }

  @override
  void didChangeAppLifecycleState(AppLifecycleState state) {
    if (state == AppLifecycleState.resumed) {
      widget.viewModel.getCards();
    }
    super.didChangeAppLifecycleState(state);
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: CommonAppBar(
        title: 'My Cards',
        appBar: AppBar(),
      ),
      body: ValueListenableBuilder(
        valueListenable: widget.viewModel.cardValue,
        builder: (context, value, child) {
          var response = value as NetworkResponseList<PaymentCard>;
          switch (response.status) {
            case NetworkStatus.LOADING:
              return CircularProgress();
            case NetworkStatus.ERROR:
              Fluttertoast.showToast(msg: response.message);
              break;
            case NetworkStatus.SUCCESS:
              addItems(response.response);
              return AnimatedCrossFade(
                  duration: Duration(milliseconds: 500),
                  crossFadeState: response.response.isEmpty
                      ? CrossFadeState.showSecond
                      : CrossFadeState.showFirst,
                  firstChild: AnimatedList(
                      key: _keyList,
                      scrollDirection: Axis.vertical,
                      shrinkWrap: true,
                      initialItemCount: paymentCards.length,
                      itemBuilder: (BuildContext context, int index,
                          Animation animation) {
                        return _buildItem(paymentCards[index]);
                      }),
                  secondChild: emptyView());
          }
          return emptyView();
        },
      ),
    );
  }

  void addItems(List<PaymentCard> response) {
    WidgetsBinding.instance.addPostFrameCallback((_) {
      paymentCards.clear();
      paymentCards.addAll(response);
      for (int i = 0; i < response.length; i++) {
        _keyList.currentState.insertItem(i);
      }
    });
  }

  Widget _buildItem(PaymentCard item) {
    String subString = item.cardNumber.substring(0, 2);
    String path;
    if (VISA.hasMatch(subString)) {
      path = 'assets/icons/visa.png';
    } else if (MASTERCARD.hasMatch(subString)) {
      path = 'assets/icons/mastercard.png';
    } else if (AMEX.hasMatch(subString)) {
      path = 'assets/icons/american_express.png';
    } else if (DISCOVER.hasMatch(subString)) {
      path = 'assets/icons/discover.png';
    }
    return Card(
        margin: EdgeInsets.only(
            left: margin_large, right: margin_large, top: margin_large),
        color: colorPrimary,
        child: Container(
          padding: EdgeInsets.all(padding_small),
          child: ListTile(
            leading: Image.asset(path),
            title: Text(
              item.cardHolder,
              style: DesignUtils.textApperanceMedium(),
            ),
            subtitle: Text(
              item.cardNumber,
              style: DesignUtils.textApperanceRegular(),
            ),
            trailing: PopupMenuButton<String>(
              onSelected: (String value) {
                if (value == Strings.markAsDefault) {
                  widget.viewModel.markAsDefault(item.id);
                }
              },
              itemBuilder: (BuildContext context) {
                return {Strings.markAsDefault, Strings.removeCard}
                    .map((String choice) {
                  return PopupMenuItem<String>(
                    value: choice,
                    child: Text(choice),
                  );
                }).toList();
              },
            ),
          ),
        ));
  }

  Widget emptyView() {
    return Center(
        child: MyButton(
            text: 'Add Card',
            color: Color(0xFF97080E),
            onPressed: () {
              Navigator.pushNamed(context, AddCard.routeName);
            }));
  }

  void handleMenuClick(String value) {}
}
