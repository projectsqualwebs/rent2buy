import 'dart:collection';

import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:fluttertoast/fluttertoast.dart';
import 'package:rent2buy/di/service_locator.dart';
import 'package:rent2buy/models/address.dart';
import 'package:rent2buy/models/cart.dart';
import 'package:rent2buy/models/cart_item.dart';
import 'package:rent2buy/models/payment_card.dart';
import 'package:rent2buy/networking/network_response.dart';
import 'package:rent2buy/networking/network_status.dart';
import 'package:rent2buy/screens/address/shipping_address_screen.dart';
import 'package:rent2buy/screens/common/base_app_bar.dart';
import 'package:rent2buy/screens/common/error_screen.dart';
import 'package:rent2buy/screens/common/loading_dialog.dart';
import 'package:rent2buy/screens/common/progress_view.dart';
import 'package:rent2buy/screens/common/success_screen.dart';
import 'package:rent2buy/screens/payment/add_card.dart';
import 'package:rent2buy/screens/payment/payment_viewmodel.dart';
import 'package:rent2buy/utils/cart_manager.dart';
import 'package:rent2buy/utils/colors.dart';
import 'package:rent2buy/utils/constants.dart';
import 'package:rent2buy/utils/data_manager.dart';
import 'package:rent2buy/utils/design_utils.dart';
import 'package:rent2buy/utils/dimens.dart';
import 'package:rent2buy/utils/strings.dart';
import 'package:rent2buy/utils/utils.dart';
import 'package:rent2buy/widgets/shared_widgets.dart';
import 'package:rent2buy/models/address.dart';

class PaymentScreen extends StatefulWidget {
  final PaymentViewModel viewModel = serviceLocator<PaymentViewModel>();
  final CartManager cartManager = serviceLocator<CartManager>();
  final DataManager dataManager = serviceLocator<DataManager>();
  static String routeName = "/payment_screen";

  @override
  State<StatefulWidget> createState() {
    return _PaymentScreen();
  }
}

class _PaymentScreen extends State<PaymentScreen> with WidgetsBindingObserver {
  List<PaymentCard> paymentCards = List();
  List<CartItem> cartItems = List();
  final _listKey = GlobalKey<AnimatedListState>();
  final _keyScafold = GlobalKey<ScaffoldState>();
  final _keyLoader = GlobalKey<State>();
  Map<String, dynamic> addressParams = Map();
  String currentCartUniqueId, currentCartId;
  Address currentAddress;
  String deliveryId, selfPickUpId;

  double cartTotal = 0.0;

  var _selectedDeliveryMethod;

  var _selectedPaymentMethod;

  @override
  void initState() {
    WidgetsBinding.instance.addObserver(this);
    super.initState();
    getDeliveryMethods();
    getCart();
    getCards();
    placeOrder();
  }

  @override
  void dispose() {
    WidgetsBinding.instance.removeObserver(this);
    super.dispose();
  }

  @override
  void didChangeAppLifecycleState(AppLifecycleState state) {
    if (state == AppLifecycleState.resumed) {
      getDeliveryMethods();
      getCart();
      getCards();
    }
    super.didChangeAppLifecycleState(state);
  }

  void placeOrder() {
    widget.viewModel.orderValue.addListener(() {
      var response = widget.viewModel.orderValue.value;
      WidgetsBinding.instance.addPostFrameCallback((_) {
        switch (response.status) {
          case NetworkStatus.LOADING:
            Dialogs.showLoadingDialog(_keyScafold.currentContext, _keyLoader,
                Strings.txtPlacingOrder);
            break;
          case NetworkStatus.ERROR:
            Navigator.of(_keyLoader.currentContext).pop();
            Fluttertoast.showToast(msg: response.message);
            break;
          case NetworkStatus.SUCCESS:
            Navigator.of(_keyScafold.currentContext).pushNamedAndRemoveUntil(
                SuccessScreen.routeName, (dynaic) => false);
            break;
        }
      });
    });
  }

  void getDeliveryMethods() async {
    final cartListingId = await widget.cartManager.getCartListingId();
    print(cartListingId);
    widget.viewModel.getDeliveryMethods(cartListingId);
    widget.viewModel.deliveryMethodsValue.addListener(() {
      var response = widget.viewModel.deliveryMethodsValue.value;
      if (response.status == NetworkStatus.SUCCESS) {
        if (response.response.isNotEmpty) {
          WidgetsBinding.instance.addPostFrameCallback((_) {
            setState(() {
              deliveryId = response.response.firstWhere(
                (x) {
                  return x.name == ACTION_DELIVERY;
                },
                orElse: () {
                  return null;
                },
              )?.id;
              selfPickUpId = response.response
                  .firstWhere(
                    (x) => x.name == ACTION_SELF,
                    orElse: () => null,
                  )
                  ?.id;
              _selectedDeliveryMethod =
                  deliveryId == null ? SELF_PICKUP : DELIVERY;
            });
          });
        }
      }
    });
  }

  void getCart() async {
    currentCartUniqueId = await widget.cartManager.getCartId();
    widget.viewModel.getCartItems(currentCartUniqueId);
  }

  void getCards() async {
    widget.viewModel.getCards();
    widget.viewModel.cardsValue.addListener(() {
      var response = widget.viewModel.cardsValue.value;
      if (response.status == NetworkStatus.SUCCESS) {
        WidgetsBinding.instance.addPostFrameCallback((_) {
          setState(() {
            paymentCards.clear();
            paymentCards.addAll(response.response);
          });
        });
      }
    });
  }

  @override
  Widget build(BuildContext context) {
    // TODO: implement build
    return WillPopScope(
      onWillPop: () async => false,
      child: Scaffold(
        key: _keyScafold,
        body: ValueListenableBuilder(
          valueListenable: widget.viewModel.cartItemsValue,
          builder: (ctx, value, child) {
            var response = value as NetworkResponse<Cart>;
            switch (response.status) {
              case NetworkStatus.LOADING:
                return CircularProgress();
              case NetworkStatus.ERROR:
                return ErrorScreen(response.message);
              case NetworkStatus.SUCCESS:
                var cart = response.response;
                WidgetsBinding.instance.addPostFrameCallback((_) {
                  setState(() {
                    cartTotal = cart.orderTotalAmount;
                    currentCartId =
                        cart.items.isNotEmpty ? cart.items.first.cart.id : "";
                    cartItems.clear();
                    cartItems.addAll(cart.items);
                  });

                  for (int offset = 0; offset < cart.items.length; offset++) {
                    _listKey.currentState.insertItem(offset);
                  }
                });

                return SafeArea(
                  child: CustomScrollView(
                    slivers: [
                      SliverList(
                        delegate: SliverChildListDelegate([
                          Container(
                            margin: EdgeInsets.only(
                                left: margin_large, right: margin_large),
                            height: 60,
                            child: GestureDetector(
                              onTap: () async {
                                addressParams = await Navigator.push(
                                    context,
                                    MaterialPageRoute(
                                        builder: (context) =>
                                            ShippinAddressScreen()));
                                currentAddress =
                                    Address.fromJson(addressParams);
                              },
                              child: Row(
                                children: [
                                  Expanded(
                                    child: Container(
                                      height: 24.0,
                                      child: Text(
                                        currentAddress == null
                                            ? 'Select Address'
                                            : currentAddress.location,
                                        style: TextStyle(
                                            color: Color(0xFF97080E),
                                            fontWeight: FontWeight.w600,
                                            fontSize: 20),
                                      ),
                                    ),
                                  ),
                                  Align(
                                    alignment: Alignment.centerRight,
                                    child: Icon(
                                      Icons.arrow_forward_ios_sharp,
                                      color: Color(0xFF97080E),
                                    ),
                                  )
                                ],
                              ),
                            ),
                          ),
                          Divider(
                            color: Color(0xFFF7A74D),
                            height: 5.0,
                            thickness: 3,
                          ),
                          AnimatedList(
                            key: _listKey,
                            itemBuilder: (context, index, animation) {
                              if (index < cartItems.length) {
                                return _buildCartItem(cartItems[index]);
                              }
                              return Container();
                            },
                            shrinkWrap: true,
                            initialItemCount: cartItems.length,
                          ),
                          SizedBox(
                            height: margin_large,
                          ),
                          Container(
                            margin: EdgeInsets.only(left: margin_large),
                            child: Text(
                              'Delivery Method',
                              style: DesignUtils.textApperanceMedium(),
                            ),
                          ),
                          SizedBox(
                            height: margin_large,
                          ),
                          Container(
                            margin: EdgeInsets.only(right: margin_large),
                            child: Row(
                              mainAxisAlignment: MainAxisAlignment.spaceBetween,
                              children: [
                                Radio(
                                  groupValue: _selectedDeliveryMethod,
                                  value: DELIVERY,
                                  onChanged: deliveryId == null
                                      ? null
                                      : (value) {
                                          _selectedDeliveryMethod = value;
                                        },
                                ),
                                Text(Strings.delivery),
                                Radio(
                                  groupValue: _selectedDeliveryMethod,
                                  value: SELF_PICKUP,
                                  onChanged: selfPickUpId == null
                                      ? null
                                      : (value) {
                                          _selectedDeliveryMethod = value;
                                        },
                                ),
                                Text(Strings.selfPickup),
                              ],
                            ),
                          ),
                          SizedBox(
                            height: margin_large,
                          ),
                          Container(
                            margin: EdgeInsets.only(
                                left: margin_large, right: margin_large),
                            child: Row(
                              children: [
                                Expanded(
                                  child: Text(
                                    'Payment Method',
                                    style: DesignUtils.textApperanceMedium(),
                                  ),
                                ),
                                InkWell(
                                    onTap: () {
                                      // Navigator.push(
                                      //     context,
                                      //     MaterialPageRoute(
                                      //         builder: (context) => AddCard()));
                                      Navigator.pushNamed(
                                          context, AddCard.routeName);
                                    },
                                    child: Icon(Icons.add)),
                              ],
                            ),
                          ),
                          SizedBox(
                            height: margin_large,
                          ),
                          ListView.builder(
                            itemBuilder: (context, index) {
                              return _buildItem(paymentCards[index]);
                            },
                            scrollDirection: Axis.vertical,
                            shrinkWrap: true,
                            itemCount: paymentCards.length,
                          )
                        ]),
                      ),
                      SliverFillRemaining(
                        hasScrollBody: false,
                        child: Container(
                            margin: EdgeInsets.only(bottom: margin_large),
                            alignment: Alignment.bottomCenter,
                            child: Column(
                              mainAxisSize: MainAxisSize.min,
                              children: [
                                Container(
                                  padding: EdgeInsets.all(padding_large),
                                  decoration: BoxDecoration(
                                      border: Border(
                                          top: BorderSide(
                                              color: colorPrimary,
                                              width: defaultBorderWidth),
                                          bottom: BorderSide(
                                              color: colorPrimary,
                                              width: defaultBorderWidth))),
                                  child: Row(
                                    mainAxisAlignment:
                                        MainAxisAlignment.spaceAround,
                                    children: [
                                      Text(
                                        Utils.getPriceInDollar(cartTotal),
                                        style: TextStyle(
                                            fontSize: 32,
                                            color: Color(0xFF97080E)),
                                      ),
                                      MyButton(
                                        text: 'Place Order',
                                        color: Color(0xFF97080E),
                                        onPressed: () async {
                                          String token = await widget
                                              .dataManager
                                              .loadToken();
                                          if (token == null) {
                                            Fluttertoast.showToast(
                                                msg:
                                                    'You have to login first.');
                                            Navigator.pushNamed(
                                                context, '/login');
                                            return;
                                          } else {
                                            if (currentAddress == null) {
                                              Fluttertoast.showToast(
                                                  msg:
                                                      Strings.msgSelectAddress);
                                              return;
                                            } else if (_selectedDeliveryMethod ==
                                                null) {
                                              Fluttertoast.showToast(
                                                  msg: Strings
                                                      .msgSelectDeliveryMethod);
                                              return;
                                            } else if (_selectedPaymentMethod ==
                                                null) {
                                              Fluttertoast.showToast(
                                                  msg: Strings
                                                      .msgSelectPaymentMethod);
                                              return;
                                            }
                                            Map<String, dynamic> params = Map();
                                            params[deliveryType] =
                                                _selectedDeliveryMethod ==
                                                        DELIVERY
                                                    ? deliveryId
                                                    : selfPickUpId;
                                            params[shippingAddressId] =
                                                currentAddress.id;
                                            params[billingAddressId] =
                                                currentAddress.id;
                                            params[isShippingSame] = 1;
                                            params[stripeCardId] =
                                                _selectedPaymentMethod;
                                            widget.viewModel.placeOrder(params);
                                          }
                                        },
                                      )
                                    ],
                                  ),
                                ),
                                SizedBox(
                                  height: padding_large,
                                ),
                                InkWell(
                                  onTap: () {
                                    Navigator.pop(context);
                                  },
                                  child: Text(
                                    Strings.cancel,
                                    style: DesignUtils.textApperanceMedium(),
                                  ),
                                )
                              ],
                            )),
                      )
                    ],
                  ),
                );
            }
            return Container();
          },
        ),
      ),
    );
  }

  Widget _buildItem(PaymentCard paymentCard) {
    try {
      String subString = paymentCard.cardNumber.substring(0, 2);
      String path;
      if (VISA.hasMatch(subString)) {
        path = 'assets/icons/visa.png';
      } else if (MASTERCARD.hasMatch(subString)) {
        path = 'assets/icons/mastercard.png';
      } else if (AMEX.hasMatch(subString)) {
        path = 'assets/icons/american_express.png';
      } else if (DISCOVER.hasMatch(subString)) {
        path = 'assets/icons/discover.png';
      }
      return Container(
        margin: EdgeInsets.only(
            left: margin_large, right: margin_large, bottom: margin_medium),
        child: Row(
          mainAxisAlignment: MainAxisAlignment.spaceBetween,
          children: [
            Image(
              width: 32,
              height: 32,
              image: AssetImage(path),
            ),
            SizedBox(
              width: margin_large,
            ),
            Expanded(
              child: Text(paymentCard.cardNumber),
            ),
            Radio(
              groupValue: _selectedPaymentMethod,
              value: paymentCard.id,
              onChanged: (value) {
                setState(() {
                  _selectedPaymentMethod = value;
                });
              },
            )
          ],
        ),
      );
    } catch (e) {}
  }
}

Widget _buildCartItem(CartItem cartItem) {
  try {
    return Container(
      height: 196,
      padding: EdgeInsets.symmetric(horizontal: 12),
      child: Row(
        mainAxisAlignment: MainAxisAlignment.spaceEvenly,
        children: [
          Expanded(
            flex: 1,
            child: Column(
              mainAxisSize: MainAxisSize.min,
              crossAxisAlignment: CrossAxisAlignment.start,
              mainAxisAlignment: MainAxisAlignment.start,
              children: [
                Text(
                  cartItem.listing.name,
                  style: TextStyle(fontWeight: FontWeight.w600, fontSize: 24),
                  overflow: TextOverflow.ellipsis,
                ),
                Text(
                  cartItem.listing.description,
                  style: TextStyle(fontWeight: FontWeight.w600, fontSize: 24),
                  overflow: TextOverflow.ellipsis,
                ),
                Text(
                  'Seller: ' +
                      Utils.getFullName(cartItem.listing.seller.firstName,
                          cartItem.listing.seller.lastName),
                  style: DesignUtils.getTextStyle(),
                ),
                SizedBox(
                  height: 20,
                ),
                Text(
                  Utils.getPriceInDollar(cartItem.price.toDouble()),
                  style: TextStyle(fontSize: 28),
                ),
              ],
            ),
          ),
          Expanded(
            flex: 1,
            child: Column(
              mainAxisSize: MainAxisSize.min,
              children: [
                Image.network(
                  cartItem.listing.images.path.isNotEmpty
                      ? cartItem.listing.images.path[0]
                      : "",
                  width: 120,
                  height: 96,
                ),
              ],
            ),
          )
        ],
      ),
    );
  } catch (exceptoin) {}
}
