import 'package:flutter/material.dart';
import 'package:rent2buy/di/service_locator.dart';
import 'package:rent2buy/models/cart.dart';
import 'package:rent2buy/models/common_repository.dart';
import 'package:rent2buy/models/delivery_type.dart';
import 'package:rent2buy/models/payment_card.dart';
import 'package:rent2buy/networking/network_response.dart';
import 'package:rent2buy/networking/network_responselist.dart';
import 'package:rent2buy/networking/repository/payment_repository.dart';
import 'package:rent2buy/utils/base_change_notifier.dart';

class PaymentViewModel extends BaseChangeNotifier {
  final PaymentRepository paymentRepository =
      serviceLocator<PaymentRepository>();

  final CommonRepository _commonRepository = serviceLocator<CommonRepository>();

  ValueNotifier<NetworkResponseList<PaymentCard>> cardsValue =
      ValueNotifier(NetworkResponseList.idle());

  ValueNotifier<NetworkResponse<Cart>> cartItemsValue =
      ValueNotifier(NetworkResponse.idle());

  ValueNotifier<NetworkResponseList<DeliveryType>> deliveryMethodsValue =
      ValueNotifier(NetworkResponseList.idle());

  ValueNotifier<NetworkResponse<String>> orderValue =
      ValueNotifier(NetworkResponse.idle());

  void getCards() {
    cardsValue.value = NetworkResponseList.loading();
    paymentRepository.getCards().then((response) {
      cardsValue.value = NetworkResponseList.success(response.response);
    }).catchError((e) {
      cardsValue.value = NetworkResponseList.error(parseError(e));
    });
  }

  void getCartItems(String cartId) {
    cartItemsValue.value = NetworkResponse.loading();
    _commonRepository.getCartItems(cartId).then((response) {
      cartItemsValue.value = NetworkResponse.success(response.response);
    }).catchError((e) {
      cartItemsValue.value = NetworkResponse.error(parseError(e));
    });
  }

  void getDeliveryMethods(String cartId) {
    deliveryMethodsValue.value = NetworkResponseList.loading();
    _commonRepository.getDeliveryMethods(cartId).then((response) {
      deliveryMethodsValue.value =
          NetworkResponseList.success(response.response);
    }).catchError((e) {
      deliveryMethodsValue.value = NetworkResponseList.error(parseError(e));
    });
  }

  void placeOrder(Map<String, dynamic> params) {
    orderValue.value = NetworkResponse.loading();
    _commonRepository.placeOrder(params).then((response) {
      orderValue.value = NetworkResponse.successWithMsg(null, response.message);
    }).catchError((e) {
      orderValue.value = NetworkResponse.error(parseError(e));
    });
  }
}
