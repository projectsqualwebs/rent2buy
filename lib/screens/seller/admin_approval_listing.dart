import 'package:flutter/material.dart';
import 'package:rent2buy/screens/seller/approved_listing.dart';
import 'package:rent2buy/screens/seller/pending_listing.dart';
import 'package:rent2buy/widgets/shared_widgets.dart';
import '../../theme.dart';

class AdminApprovalListing extends StatefulWidget {
  @override
  _AdminApprovalListingState createState() => _AdminApprovalListingState();
}

class _AdminApprovalListingState extends State<AdminApprovalListing> {
  Color activeButtonColor;
  Color inactiveButtonColor = Color(0xFFE8F5FA);
  int elevation;
  int index = 0;

  List<Widget> listings = [
    ApprovedListing(),
    PendingListing()
  ];

  @override
  Widget build(BuildContext context) {
    return Theme(
      data: sellerTheme,
      child: Scaffold(
        appBar: BaseAppBar(
          appBar: AppBar(),
          title: 'All Listing',
          isCentered: true,
          automaticallyImplyLeading: true,
          iconTheme: Color(0xFF003585),
          titleColor: Color(0xFF003585),
        ),
        body: listings[index],
        bottomNavigationBar: Padding(
          padding: const EdgeInsets.all(8.0),
          child: Row(
            mainAxisAlignment: MainAxisAlignment.center,
            children: [
              RaisedButton(
                  elevation: 0,
                  color: activeButtonColor,
                  child: Text('Approved',style: TextStyle(fontSize: 16, fontWeight: FontWeight.w800,color:Color(0xFF003585))),
                  onPressed: () {
                    activeButtonColor = Color(0xFF71C7E3);
                    inactiveButtonColor = Color(0xFFE8F5FA);
                    index = 0;
                    setState(() {
                    });
                  }
              ),
              SizedBox(width: 10),
              RaisedButton(
                  elevation: 0,
                  color: inactiveButtonColor,
                  child: Text('Pending',style: TextStyle(fontSize: 16, fontWeight: FontWeight.w800,color:Color(0xFF003585))),
                  onPressed: () {
                    activeButtonColor = Color(0xFFE8F5FA);
                    inactiveButtonColor = Color(0xFF71C7E3);
                    index = 1;
                    setState(() {
                    });
                  }
              ),
            ],
          ),
        ),
      ),
    );
  }
}
