import 'dart:convert';
import 'dart:io';

import 'package:flutter/material.dart';
import 'package:fluttertoast/fluttertoast.dart';
import 'package:rent2buy/di/service_locator.dart';
import 'package:rent2buy/models/bundle.dart';
import 'package:rent2buy/models/product.dart';
import 'package:rent2buy/models/product_model.dart';
import 'package:rent2buy/models/purchase_actions.dart';
import 'package:rent2buy/networking/network_responselist.dart';
import 'package:rent2buy/networking/network_status.dart';
import 'package:rent2buy/screens/common/progress_view.dart';
import 'package:rent2buy/screens/seller/listing/listing_viewmodel.dart';
import 'package:rent2buy/screens/seller/listing/product_detail.dart';
import 'package:rent2buy/services/apiProvider.dart';
import 'package:rent2buy/utils/constants.dart';
import 'package:rent2buy/utils/strings.dart';
import 'package:rent2buy/utils/time_utils.dart';
import 'package:rent2buy/widgets/shared_widgets.dart';
import 'package:shared_preferences/shared_preferences.dart';
import 'package:http/http.dart' as http;

class ApprovedListing extends StatefulWidget {
  final ListingViewModel viewModel = serviceLocator<ListingViewModel>();
  @override
  _ApprovedListingState createState() => _ApprovedListingState();
}

class _ApprovedListingState extends State<ApprovedListing> {
  List<Product> listings = List();

  @override
  void initState() {
    WidgetsBinding.instance.addPostFrameCallback((_) {
      Map<String, dynamic> params = Map();
      params[status] = 1;
      widget.viewModel.getListing(params);
    });
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return ValueListenableBuilder(
      valueListenable: widget.viewModel.listingValue,
      builder: (context, value, _) {
        var response = value as NetworkResponseList<Product>;
        switch (response.status) {
          case NetworkStatus.LOADING:
            return CircularProgress();
          case NetworkStatus.ERROR:
            Fluttertoast.showToast(msg: response.message);
            break;
          case NetworkStatus.SUCCESS:
            WidgetsBinding.instance.addPostFrameCallback((_) {
              setState(() {
                listings.clear();
                listings.addAll(response.response);
              });
            });
            return AnimatedCrossFade(
              duration: Duration(milliseconds: 500),
              crossFadeState: listings.isEmpty
                  ? CrossFadeState.showSecond
                  : CrossFadeState.showFirst,
              firstChild: ListView.builder(
                  shrinkWrap: true,
                  scrollDirection: Axis.vertical,
                  itemCount: listings.length,
                  itemBuilder: (BuildContext context, int index) {
                    return _buildItem(index, listings[index]);
                  }),
              secondChild: Center(
                child: Text(Strings.msgNoListingFound),
              ),
            );
        }
        return Container(
          child: Text(Strings.msgNoListingFound),
        );
      },
    );
  }

  Widget _buildItem(int index, Product product) {
    try {
      return InkWell(
        onTap: () {
          Navigator.pushNamed(context, SellerProductDetail.routeName,
              arguments: Bundle(product.id, isOrder: false));
        },
        child: Column(
          children: [
            Padding(
              padding: const EdgeInsets.all(12.0),
              child: ListTile(
                leading: Image.network(
                  product.images.path[0],
                  fit: BoxFit.fitHeight,
                  width: 100,
                  height: 100,
                ),
                title: Text(
                  product.name,
                  style: TextStyle(fontSize: 24),
                ),
                subtitle: Padding(
                  padding: const EdgeInsets.only(top: 8.0),
                  child: Text(
                    TimeUtils.getMMDDYYY(product.createdAt),
                    style: TextStyle(fontSize: 16),
                  ),
                ),
                trailing: Text(
                  getPurchaseAction(product.purchaseActions),
                  style: TextStyle(color: Colors.green[600], fontSize: 20),
                ),
                onTap: () {},
              ),
            ),
            CustomDivider()
          ],
        ),
      );
    } catch (e) {
      debugPrint(e.toString());
    }
  }

  String getPurchaseAction(List<PurchaseActions> purchaseActions) {
    if (purchaseActions.length == 2) {
      return Strings.sellAndRent;
    } else {
      if (purchaseActions.first.name == ACTION_SELL) {
        return Strings.sellOnly;
      }
      if (purchaseActions.first.name == ACTION_RENT) {
        return Strings.rentOnly;
      }
    }
  }
}
