import 'dart:io';

import 'package:flutter_google_places/flutter_google_places.dart';
import 'package:geocoder/geocoder.dart';
import 'package:google_maps_webservice/places.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:fluttertoast/fluttertoast.dart';
import 'package:intl/intl.dart';
import 'package:rent2buy/di/service_locator.dart';
import 'package:rent2buy/networking/network_status.dart';
import 'package:rent2buy/screens/common/loading_dialog.dart';
import 'package:rent2buy/screens/common/progress_view.dart';
import 'package:rent2buy/screens/seller/bank/bank_viewmodel.dart';
import 'package:rent2buy/services/apiProvider.dart';
import 'package:rent2buy/theme.dart';
import 'package:rent2buy/utils/constants.dart';
import 'package:rent2buy/utils/strings.dart';
import 'package:rent2buy/widgets/shared_widgets.dart';
import 'package:http/http.dart' as http;
import 'package:shared_preferences/shared_preferences.dart';

class AddBankAccount extends StatefulWidget {
  static final String routeName = "/add_bank_details";
  final BankViewModel viewModel = serviceLocator<BankViewModel>();
  @override
  _AddBankAccountState createState() => _AddBankAccountState();
}

class _AddBankAccountState extends State<AddBankAccount> {
  var _dateOfBirthController = TextEditingController();
  GoogleMapsPlaces _places = GoogleMapsPlaces(apiKey: kGoogleApiKey);
  var isLoading = false;

  GlobalKey<State> _keyLoader = GlobalKey<State>();

  addBankAccount() async {
    if (_key.currentState.validate()) {
      Map<String, dynamic> params = Map();
      params[accountNumber] = acc_no_controller.text;
      params[routing] = routing_controller.text;
      params[city] = city_controller.text;
      params[line1] = line1_controller.text;
      params[line2] = line2_controller.text;
      params[postalCode] = postal_code_controller.text;
      params[state] = state_controller.text;
      params[date_Of_Birth] = _dateOfBirthController.text;
      widget.viewModel.addBankAccount(params);
    }
  }

  final _key = GlobalKey<FormState>();
  TextEditingController acc_no_controller = TextEditingController();
  TextEditingController routing_controller = TextEditingController();
  TextEditingController city_controller = TextEditingController();
  TextEditingController line1_controller = TextEditingController();
  TextEditingController line2_controller = TextEditingController();
  TextEditingController postal_code_controller = TextEditingController();
  TextEditingController state_controller = TextEditingController();
  String acc_no;
  DateTime _dob;
  String dob;
  @override
  void initState() {
    // TODO: implement initState
    super.initState();
    widget.viewModel.addValue.addListener(() {
      WidgetsBinding.instance.addPostFrameCallback((_) {
        var response = widget.viewModel.addValue.value;
        switch (response.status) {
          case NetworkStatus.LOADING:
            isLoading = true;
            break;
          case NetworkStatus.ERROR:
            isLoading = false;
            Fluttertoast.showToast(msg: response.message);
            break;
          case NetworkStatus.SUCCESS:
            isLoading = false;
            Fluttertoast.showToast(msg: response.message);
            Navigator.pop(context, RESULT_OK);
            break;
        }
        setState(() {});
      });
    });
  }

  @override
  Widget build(BuildContext context) {
    return Theme(
        data: sellerTheme,
        child: Scaffold(
            appBar: BaseAppBar(
              appBar: AppBar(),
              title: 'Add Bank Details',
              titleColor: Color(0xFF003585),
              iconTheme: Color(0xFF003585),
              automaticallyImplyLeading: true,
            ),
            body: Column(
              children: [
                Expanded(
                  child: SingleChildScrollView(
                    child: Container(
                      padding: EdgeInsets.only(left: 20, right: 20, top: 20),
                      child: Form(
                        key: _key,
                        child: Column(
                          children: [
                            AuthTextField(
                              hinttext: 'Account Number',
                              isNumber: true,
                              textEditingController: acc_no_controller,
                              color: Color(0xFF71C7E3),
                              isPassword: false,
                              validator: (String value) {
                                if (value.isEmpty) {
                                  return 'Please enter Account Number';
                                }
                                return null;
                              },
                              onSaved: (String value) {
                                acc_no = value;
                              },
                            ),
                            SizedBox(height: 20),
                            AuthTextField(
                              hinttext: 'Routing',
                              isNumber: true,
                              color: Color(0xFF71C7E3),
                              textEditingController: routing_controller,
                              isPassword: false,
                              validator: (String value) {
                                if (value.isEmpty) {
                                  return 'Please enter Routing';
                                }
                                return null;
                              },
                            ),
                            SizedBox(height: 20),
                            AuthTextField(
                              hinttext: 'Line 1',
                              isNumber: false,
                              color: Color(0xFF71C7E3),
                              textEditingController: line1_controller,
                              isPassword: false,
                              validator: (String value) {
                                if (value.isEmpty) {
                                  return 'Please enter Line 1';
                                }
                                return null;
                              },
                              onTap: () async {
                                Prediction p = await PlacesAutocomplete.show(
                                    context: context,
                                    apiKey: kGoogleApiKey,
                                    mode: Mode.overlay,
                                    region: "us");
                                displayPrediction(p, context);
                              },
                            ),
                            SizedBox(height: 20),
                            AuthTextField(
                              hinttext: 'Line 2',
                              isNumber: false,
                              color: Color(0xFF71C7E3),
                              textEditingController: line2_controller,
                              isPassword: false,
                              validator: (String value) {
                                if (value.isEmpty) {
                                  return 'Please enter Line 2';
                                }
                                return null;
                              },
                            ),
                            SizedBox(height: 20),
                            AuthTextField(
                              hinttext: 'State',
                              isNumber: false,
                              color: Color(0xFF71C7E3),
                              textEditingController: state_controller,
                              isPassword: false,
                              validator: (String value) {
                                if (value.isEmpty) {
                                  return 'Please enter State';
                                }
                                return null;
                              },
                            ),
                            SizedBox(height: 20),
                            AuthTextField(
                              hinttext: 'City',
                              isNumber: false,
                              color: Color(0xFF71C7E3),
                              textEditingController: city_controller,
                              isPassword: false,
                              validator: (String value) {
                                if (value.isEmpty) {
                                  return 'Please enter City';
                                }
                                return null;
                              },
                            ),
                            SizedBox(height: 20),
                            AuthTextField(
                              inputFormatters: [
                                LengthLimitingTextInputFormatter(6)
                              ],
                              hinttext: 'Postal Code',
                              isNumber: true,
                              color: Color(0xFF71C7E3),
                              textEditingController: postal_code_controller,
                              isPassword: false,
                              validator: (String value) {
                                if (value.isEmpty) {
                                  return 'Please enter Postal Code';
                                }
                                return null;
                              },
                            ),
                            SizedBox(height: 20),
                            AuthTextField(
                              onTap: () async {
                                _dob = await showDatePicker(
                                    context: context,
                                    initialDatePickerMode: DatePickerMode.year,
                                    initialDate: DateTime(1990),
                                    firstDate: DateTime(1990),
                                    lastDate: DateTime(2010));
                                dob = DateFormat('yyyyMMdd').format(_dob);
                                _dateOfBirthController.text = dob;
                              },
                              hinttext: Strings.dateOfBirth,
                              isNumber: false,
                              color: Color(0xFF71C7E3),
                              textEditingController: _dateOfBirthController,
                              isPassword: false,
                              validator: (String value) {
                                if (value.isEmpty) {
                                  return 'Please enter Date of Birth';
                                }
                                return null;
                              },
                            ),
                          ],
                        ),
                      ),
                    ),
                  ),
                ),
                AnimatedSwitcher(
                  duration: Duration(milliseconds: 500),
                  child: isLoading
                      ? CircularProgress(key: UniqueKey())
                      : MyButton(
                          key: UniqueKey(),
                          width: double.infinity,
                          color: Color(0xFF003585),
                          onPressed: () {
                            if (_key.currentState.validate()) {
                              _key.currentState.save();
                              addBankAccount();
                            }
                          },
                          text: 'Submit',
                        ),
                  switchInCurve: Curves.easeIn,
                  switchOutCurve: Curves.easeOut,
                  transitionBuilder: (child, animation) {
                    return ScaleTransition(scale: animation, child: child);
                  },
                )
              ],
            )));
  }

  Future<Null> displayPrediction(Prediction p, BuildContext context) async {
    FocusManager.instance.primaryFocus.unfocus();
    if (p != null) {
      Dialogs.showLoadingDialog(context, _keyLoader, "Getting Location...");
      PlacesDetailsResponse detail =
          await _places.getDetailsByPlaceId(p.placeId);

      var placeId = p.placeId;
//      lat = detail.result.geometry.location.lat;
      //     long = detail.result.geometry.location.lng;

      print(lat);
      print(long);
      var a = await Geocoder.local.findAddressesFromQuery(p.description);

      setState(() {
        var firstAddress = a.first;
        city_controller.text = firstAddress.locality;
        state_controller.text = firstAddress.adminArea;
        line1_controller.text = firstAddress.addressLine;
        line2_controller.text = firstAddress.addressLine;
        print(firstAddress.toString());
        // userAddresses.clear();
        // showAddress = p.description;
        // adrs = p.description;
        // address.location = showAddress;
        // address.latitude = lat;
        // address.longitude = long;

        // userAddresses.add(address);
        // addressController.text = showAddress;
        // params[Constants.address] = userAddresses;
      });
      Navigator.of(_keyLoader.currentContext, rootNavigator: true).pop();
    }
  }
}
