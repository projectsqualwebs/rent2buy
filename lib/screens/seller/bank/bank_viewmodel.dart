import 'package:flutter/material.dart';
import 'package:rent2buy/di/service_locator.dart';
import 'package:rent2buy/models/stripe_account.dart';
import 'package:rent2buy/networking/network_response.dart';
import 'package:rent2buy/networking/network_responselist.dart';
import 'package:rent2buy/networking/repository/payment_repository.dart';
import 'package:rent2buy/utils/base_change_notifier.dart';

class BankViewModel extends BaseChangeNotifier {
  final PaymentRepository paymentRepository =
      serviceLocator<PaymentRepository>();

  ValueNotifier<NetworkResponse<String>> addValue =
      ValueNotifier(NetworkResponse.idle());
        ValueNotifier<NetworkResponse<String>> removeValue =
      ValueNotifier(NetworkResponse.idle());
  ValueNotifier<NetworkResponseList<StripeAccount>> getValue =
      ValueNotifier(NetworkResponseList.idle());

  void addBankAccount(Map<String, dynamic> params) {
    addValue.value = NetworkResponse.loading();
    paymentRepository.addBankAccount(params).then((response) {
      addValue.value = NetworkResponse.successWithMsg(null, response.message);
    }).catchError((e) {
      addValue.value = NetworkResponse.error(parseError(e));
    });
  }

  void removeBankAccount(String id) {
    removeValue.value = NetworkResponse.loading();
    paymentRepository.removeBankAccount(id).then((response) {
      removeValue.value = NetworkResponse.successWithMsg(null, response.message);
    }).catchError((e) {
      removeValue.value = NetworkResponse.error(parseError(e));
    });
  }

  void getBankAccount() {
    getValue.value = NetworkResponseList.loading();
    paymentRepository.getBankAccount().then((response) {
      getValue.value = NetworkResponseList.success(response.response);
    }).catchError((e) {
      getValue.value = NetworkResponseList.error(parseError(e));
    });
  }
}
