import 'dart:convert';
import 'dart:io';

import 'package:flutter/material.dart';
import 'package:fluttertoast/fluttertoast.dart';
import 'package:rent2buy/di/service_locator.dart';
import 'package:rent2buy/models/stripe_account.dart';
import 'package:rent2buy/networking/network_responselist.dart';
import 'package:rent2buy/networking/network_status.dart';
import 'package:rent2buy/screens/common/error_screen.dart';
import 'package:rent2buy/screens/common/loading_dialog.dart';
import 'package:rent2buy/screens/common/progress_view.dart';
import 'package:rent2buy/screens/seller/bank/bank_viewmodel.dart';
import 'package:rent2buy/services/apiProvider.dart';
import 'package:rent2buy/theme.dart';
import 'package:rent2buy/utils/dimens.dart';
import 'package:rent2buy/utils/strings.dart';
import 'package:rent2buy/widgets/shared_widgets.dart';
import 'package:http/http.dart' as http;
import 'package:shared_preferences/shared_preferences.dart';

class BankAccount extends StatefulWidget {
  static final String routeName = "/bank_account_details";
  final BankViewModel viewModel = serviceLocator<BankViewModel>();

  @override
  _BankAccountState createState() => _BankAccountState();
}

class _BankAccountState extends State<BankAccount> {
  final GlobalKey<State> _keyLoader = GlobalKey<State>();
  List<StripeAccount> accounts = List();
  final _keyList = GlobalKey<AnimatedListState>();
  getSellerBankAcc() async {
    widget.viewModel.getBankAccount();
  }

  @override
  void initState() {
    getSellerBankAcc();
    WidgetsBinding.instance.addPostFrameCallback((_) {
      widget.viewModel.removeValue.addListener(() {
        var response = widget.viewModel.removeValue.value;
        switch (response.status) {
          case NetworkStatus.LOADING:
            Dialogs.showLoadingDialog(context, _keyLoader, null);
            break;
          case NetworkStatus.ERROR:
            Navigator.of(_keyLoader.currentContext, rootNavigator: true).pop();
            Fluttertoast.showToast(msg: response.message);
            break;
          case NetworkStatus.SUCCESS:
            Navigator.of(_keyLoader.currentContext, rootNavigator: true).pop();
            Fluttertoast.showToast(msg: response.message);
            getSellerBankAcc();
            break;
        }
      });
    });
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return Theme(
        data: sellerTheme,
        child: Scaffold(
            appBar: BaseAppBar(
              appBar: AppBar(),
              title: 'Bank Account Details',
              isCentered: false,
              automaticallyImplyLeading: true,
              iconTheme: Color(0xFF003585),
              titleColor: Color(0xFF003585),
            ),
            body: ValueListenableBuilder(
                valueListenable: widget.viewModel.getValue,
                builder: (context, value, child) {
                  var response = value as NetworkResponseList<StripeAccount>;
                  switch (response.status) {
                    case NetworkStatus.LOADING:
                      return CircularProgress();
                    case NetworkStatus.ERROR:
                      Fluttertoast.showToast(msg: response.message);
                      break;
                    case NetworkStatus.SUCCESS:
                      addItems(response.response);
                      return AnimatedCrossFade(
                        duration: Duration(milliseconds: 500),
                        crossFadeState: response.response.isEmpty
                            ? CrossFadeState.showSecond
                            : CrossFadeState.showFirst,
                        firstChild: _buildFirstChild(),
                        secondChild: _buildEmptyItem(),
                      );
                  }
                  return _buildEmptyItem();
                })));
  }

  void addItems(List<StripeAccount> response) {
    WidgetsBinding.instance.addPostFrameCallback((_) {
      accounts.clear();
      accounts.addAll(response);
      for (int i = 0; i < response.length; i++) {
        _keyList.currentState.insertItem(i);
      }
    });
  }

  Widget _buildEmptyItem() {
    return Container(
      padding: EdgeInsets.fromLTRB(20, 30, 20, 20),
      child: Column(
        children: [
          Row(
            children: [
              Expanded(
                  child: MyButton(
                text: 'Add Bank Account',
                onPressed: () {
                  Navigator.pushNamed(context, '/add_bank_details');
                },
                color: Color(0xFF003585),
              ))
            ],
          )
        ],
      ),
    );
  }

  Widget _buildItem(int index, StripeAccount account) {
    return Card(
      margin: EdgeInsets.all(margin_large),
      shape: RoundedRectangleBorder(borderRadius: BorderRadius.circular(16)),
      color: Color(0xFF71C7E3),
      child: Container(
        height: 125,
        child: ListTile(
          contentPadding: EdgeInsets.only(top: 12, left: 12, right: 12),
          leading: Padding(
            padding: const EdgeInsets.only(left: 6.0, top: 8),
            child: Image.asset(
              'assets/icons/museum.png',
              fit: BoxFit.fill,
            ),
          ),
          title: Column(
            mainAxisSize: MainAxisSize.min,
            crossAxisAlignment: CrossAxisAlignment.start,
            children: [
              Text(
                account.accountBank,
                style: TextStyle(
                    color: Colors.black,
                    fontSize: 24,
                    fontWeight: FontWeight.w600),
              ),
              Text(
                account.accountHolder,
                style: TextStyle(
                    color: Colors.black,
                    fontSize: 24,
                    fontWeight: FontWeight.w600),
              ),
            ],
          ),
          subtitle: Padding(
              padding: const EdgeInsets.only(top: 12.0),
              child: Text(
                account.accountNumber,
              )),
          trailing: Icon(
            Icons.check_circle_outline,
            size: 36,
            color: Colors.green.shade700,
          ),
        ),
      ),
    );
  }

  _buildFirstChild() {
    return Column(
      children: [
        AnimatedList(
            key: _keyList,
            scrollDirection: Axis.vertical,
            shrinkWrap: true,
            initialItemCount: accounts.length,
            itemBuilder:
                (BuildContext context, int index, Animation animation) {
              return _buildItem(index, accounts[index]);
            }),
        Expanded(
            child: MyButton(
          text: 'Remove Account',
          onPressed: () {
            widget.viewModel.removeBankAccount(accounts.first.id);
          },
          color: Color(0xFF003585),
        ))
      ],
    );
  }
}
