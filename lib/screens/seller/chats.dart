import 'dart:convert';
import 'dart:io';

import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:rent2buy/models/user_model.dart';
import 'package:rent2buy/screens/seller/chat_screen.dart';
import 'package:rent2buy/services/apiProvider.dart';
import 'package:rent2buy/theme.dart';
import 'package:rent2buy/widgets/shared_widgets.dart';
import 'package:shared_preferences/shared_preferences.dart';
import 'package:http/http.dart' as http;

class SellerChats extends StatefulWidget {
  @override
  _SellerChatsState createState() => _SellerChatsState();
}

class _SellerChatsState extends State<SellerChats> {
  bool isSelected = false;
  List<dynamic> threads = List();

  getThreads() async {
    SharedPreferences prefs = await SharedPreferences.getInstance();
    String token = prefs.getString('token');
    http.Response response = await http.get('${ApiProvider.thread}',
        headers: {HttpHeaders.authorizationHeader: 'Bearer $token'});

    setState(() {
      threads = jsonDecode(response.body)['response'];
    });
  }

  deleteThread(String id) async {
    SharedPreferences prefs = await SharedPreferences.getInstance();
    String token = prefs.getString('token');
    http.Response response = await http.delete(
        '${ApiProvider.thread}' + '/' + id,
        headers: {HttpHeaders.authorizationHeader: 'Bearer $token'});
    print(response.body.toString());
    getThreads();
  }

  @override
  void initState() {
    getThreads();
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return Theme(
      data: sellerTheme,
      child: Scaffold(
        appBar: BaseAppBar(
          title: 'Chats',
          automaticallyImplyLeading: true,
          titleColor: Color(0xFF003585),
          iconTheme: Color(0xFF003585),
          widgets: [IconButton(icon: Icon(Icons.delete), onPressed: () {})],
          appBar: AppBar(),
        ),
        body: threads.isEmpty
            ? Row(
                mainAxisAlignment: MainAxisAlignment.center,
                children: [
                  Text('No Chats', style: TextStyle(fontSize: 24)),
                ],
              )
            : ListView.builder(
                itemCount: threads.length,
                itemBuilder: (BuildContext context, int index) {
                  return Column(
                    children: [
                      Padding(
                        padding: const EdgeInsets.all(12.0),
                        child: ListTile(
                          selected: isSelected,
                          leading: CircleAvatar(
                            child: Image.network(
                                'https://encrypted-tbn0.gstatic.com/images?q=tbn%3AANd9GcQlR_mhU1bn_sa7j_M5UeMmIbKmR3UI1evxNw&usqp=CAU'),
                            radius: 30,
                            backgroundColor: Colors.transparent,
                          ),
                          title: Padding(
                            padding: const EdgeInsets.only(left:6.0,bottom: 8),
                            child: Text(
                              threads[index]['user']['full_name'],
                              style: TextStyle(
                                  fontSize: 24, fontWeight: FontWeight.w700),
                            ),
                          ),
                          subtitle: Row(
                            mainAxisAlignment: MainAxisAlignment.start,
                            children: [
                              Icon(
                                Icons.location_on,
                                color: Color(0xFF656565),
                              ),
                              Text(
                                'Location',
                                style: TextStyle(color: Colors.grey, fontSize: 16),
                              )
                            ],
                          ),
                          trailing: Visibility(
                            visible: isSelected,
                            child: IconButton(
                                icon: Icon(Icons.delete),
                                onPressed: () {
                                  deleteThread(threads[index]['_id']);
                                }),
                          ),
                          onLongPress: () {
                            print(threads[index]['_id']);
                            setState(() {
                              isSelected = true;
                            });
                          },
                          onTap: () {
                            Navigator.push(
                              context,
                              MaterialPageRoute(
                                builder: (context) =>
                                    SellerChatScreen(id: threads[index]['_id']),
                              ),
                            );
                          },
                        ),
                      ),
                      Divider(
                        color: Color(0xFF71C7E3),
                        height: 5.0,
                        thickness: 3,
                      ),
                    ],
                  );
                }),
      ),
    );
  }
}
