import 'package:flutter/material.dart';
import 'package:provider/provider.dart';
import 'package:rent2buy/di/service_locator.dart';
import 'package:rent2buy/models/bundle.dart';
import 'package:rent2buy/models/product.dart';
import 'package:rent2buy/networking/network_status.dart';
import 'package:rent2buy/screens/buyer/product_detail.dart';
import 'package:rent2buy/screens/common/error_screen.dart';
import 'package:rent2buy/screens/common/loading.dart';
import 'package:rent2buy/screens/seller/listing/product_detail.dart';
import 'package:rent2buy/services/apiProvider.dart';
import 'package:rent2buy/theme.dart';
import 'package:rent2buy/utils/data_manager.dart';
import 'package:rent2buy/utils/strings.dart';
import 'package:rent2buy/widgets/shared_widgets.dart';
import '../../../data.dart';
import 'seller_home_viewmodel.dart';

class SellerHome extends StatefulWidget {
  static String routeName = "/seller_home";
  final DataManager dataManager = serviceLocator<DataManager>();
  final SellerHomeViewModel viewModel = serviceLocator<SellerHomeViewModel>();

  @override
  _SellerHomeState createState() => _SellerHomeState();
}

class _SellerHomeState extends State<SellerHome> with WidgetsBindingObserver {
  Map<String, dynamic> params = Map();
  List<Product> products = List<Product>();

  @override
  void initState() {
    // TODO: implement initState
    WidgetsBinding.instance.addObserver(this);
    widget.viewModel.getProducts(params);
    super.initState();
    WidgetsBinding.instance.addPostFrameCallback((_) {
      //getActions();
      getDeliveryTypes();
    });
  }

  // getActions() {
  //   widget.viewModel.getActions();
  //   widget.viewModel.actions.addListener(() {
  //     var value = widget.viewModel.actions.value;
  //     if (value.status == NetworkStatus.SUCCESS && value.response.isNotEmpty) {
  //       currentActionSellID = value.response.first.id;
  //       currentActionRentID = value.response.last.id;
  //       print(currentActionSellID);
  //     }
  //   });
  // }

  getDeliveryTypes() {
    widget.viewModel.getDeliveryTypes();
    widget.viewModel.actions.addListener(() {
      var value = widget.viewModel.deliveryTypes.value;
      if (value.status == NetworkStatus.SUCCESS && value.response.isNotEmpty) {
        widget.dataManager.saveDeliveryId(value.response.first.id);
        widget.dataManager.saveSelfPickUpId(value.response.last.id);
      }
    });
  }

  @override
  void dispose() {
    WidgetsBinding.instance.removeObserver(this);
    super.dispose();
  }

  @override
  void didChangeAppLifecycleState(AppLifecycleState state) {
    if (state == AppLifecycleState.resumed) {
      widget.viewModel.getProducts(params);
    }
    super.didChangeAppLifecycleState(state);
  }

  @override
  Widget build(BuildContext context) {
    return SafeArea(
      child: Theme(
        data: sellerTheme,
        child: Scaffold(
          appBar: AppBar(
            title: Text(
              'Rent2S',
              style: TextStyle(
                  fontSize: 24,
                  fontWeight: FontWeight.bold,
                  color: Color(0xFF003585)),
            ),
            actions: [
              IconButton(
                  icon: Icon(
                    Icons.search,
                  ),
                  onPressed: () {}),
              IconButton(
                  icon: Icon(
                    Icons.location_pin,
                  ),
                  onPressed: () {})
            ],
          ),
          drawer: SellerDrawer(),
          body: ChangeNotifierProvider<SellerHomeViewModel>(
            create: (context) => widget.viewModel,
            child: Consumer<SellerHomeViewModel>(
              builder: (context, value, _) {
                return Stack(
                  children: [
                    SingleChildScrollView(
                      child: Container(
                        width: MediaQuery.of(context).size.width,
                        child: Column(
                          mainAxisAlignment: MainAxisAlignment.start,
                          children: [
                            Stack(
                              children: [
                                Image.network(
                                  'https://www.breathintravel.com/wp-content/uploads/2020/09/best-backpacking-binoculars-1150x766.jpg',
                                  width: MediaQuery.of(context).size.width,
                                ),
                                Positioned(
                                  top: 30,
                                  left: 20,
                                  child: Text(
                                    'Search from \nThousands of \nAds',
                                    style: TextStyle(
                                        color: Color(0xFF003585),
                                        fontWeight: FontWeight.w700,
                                        fontSize: 30),
                                  ),
                                ),
                                Positioned(
                                  top: 150,
                                  left: 20,
                                  child: Text(
                                    'Here you can Search',
                                    style: TextStyle(
                                        color: Color(0xFF003585),
                                        fontWeight: FontWeight.w700,
                                        fontSize: 16),
                                  ),
                                ),
                                Positioned(
                                  top: 180,
                                  width: MediaQuery.of(context).size.width,
                                  child: Padding(
                                    padding: const EdgeInsets.symmetric(
                                        horizontal: 20.0),
                                    child: SuffixedIconTextField(
                                      filled: true,
                                      fillColor: Colors.white54,
                                      borderColor: Color(0xFF003585),
                                      hinttext: 'Search by Keyword',
                                      textAlign: TextAlign.justify,
                                      isNumber: false,
                                      suffixIcon: IconButton(
                                          icon: Icon(
                                            Icons.search,
                                            color: Color(0xFF003585),
                                          ),
                                          onPressed: () {}),
                                    ),
                                  ),
                                )
                              ],
                            ),
                            Container(
                              padding: EdgeInsets.all(16),
                              child: products.isEmpty
                                  ? Text(Strings.msgNoListingFound)
                                  : GridView.builder(
                                      physics: ScrollPhysics(),
                                      scrollDirection: Axis.vertical,
                                      shrinkWrap: true,
                                      itemCount: products.length,
                                      gridDelegate:
                                          SliverGridDelegateWithFixedCrossAxisCount(
                                              crossAxisCount: 3,
                                              crossAxisSpacing: 6,
                                              mainAxisSpacing: 12),
                                      itemBuilder:
                                          (BuildContext context, int index) {
                                        Product product = products[index];
                                        String path =
                                            (product.images.path.isNotEmpty
                                                ? product.images.path[0]
                                                : "");

                                        return GestureDetector(
                                          onTap: () {
                                            Navigator.pushNamed(context,
                                                SellerProductDetail.routeName,
                                                arguments: Bundle(product.id,
                                                    isOrder: false));
                                          },
                                          child: Container(
                                              decoration: BoxDecoration(
                                                  border: Border.all(
                                                      width: 3,
                                                      color:
                                                          Color(0xFF71C7E3))),
                                              child: Image.network(
                                                path,
                                                fit: BoxFit.fitWidth,
                                              )),
                                        );
                                      }),
                            ),
                          ],
                        ),
                      ),
                    ),
                    observeListing(value, context),
                  ],
                );
              },
            ),
          ),
        ),
      ),
    );
  }

  Widget observeListing(SellerHomeViewModel value, BuildContext context) {
    switch (value.productsEvent.status) {
      case NetworkStatus.LOADING:
        return LoadingScreen(isCustom: false);
      case NetworkStatus.ERROR:
        return ErrorScreen(value.productsEvent.message);
      case NetworkStatus.SUCCESS:
        WidgetsBinding.instance.addPostFrameCallback((_) {
          // Add Your Code here.
          updateList(value.productsEvent.response);
        });

        break;
    }
    return Container();
  }

  void updateList(List<Product> response) {
    setState(() {
      products.clear();
      products.addAll(response);
    });
  }
}
