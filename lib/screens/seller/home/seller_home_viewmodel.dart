import 'package:flutter/material.dart';
import 'package:rent2buy/di/service_locator.dart';
import 'package:rent2buy/models/delivery_type.dart';
import 'package:rent2buy/models/product.dart';
import 'package:rent2buy/models/purchase_actions.dart';
import 'package:rent2buy/networking/network_response.dart';
import 'package:rent2buy/networking/network_responselist.dart';
import 'package:rent2buy/networking/repository/seller_repository.dart';
import 'package:rent2buy/utils/base_change_notifier.dart';

class SellerHomeViewModel extends BaseChangeNotifier {
  NetworkResponseList<Product> productsEvent;
  final SellerRepository _sellerRepository = serviceLocator<SellerRepository>();
  NetworkResponse<String> listEvent;

  ValueNotifier<NetworkResponseList<PurchaseActions>> actions =
      ValueNotifier(NetworkResponseList.idle());

  ValueNotifier<NetworkResponseList<DeliveryType>> deliveryTypes =
      ValueNotifier(NetworkResponseList.idle());

  SellerHomeViewModel() {
    setProduct(NetworkResponseList.idle());
  }

  void setProduct(NetworkResponseList<Product> response) {
    productsEvent = response;
    notifyListeners();
  }

  void getProducts(Map<String, dynamic> params) {
    setProduct(NetworkResponseList.loading());
    _sellerRepository
        .getProducts(params)
        .then(
            (value) => setProduct(NetworkResponseList.success(value.response)))
        .catchError((e) {
      setProduct(NetworkResponseList.error(parseError(e)));
    });
  }

  void getDeliveryTypes() {
    deliveryTypes.value = NetworkResponseList.loading();
    _sellerRepository
        .getDeliveryTypes()
        .then((value) =>
            deliveryTypes.value = NetworkResponseList.success(value.response))
        .catchError((e) {
      deliveryTypes.value = NetworkResponseList.error(parseError(e));
    });
  }

  void getActions() {
    actions.value = NetworkResponseList.loading();
    _sellerRepository
        .getActions()
        .then((value) =>
            actions.value = NetworkResponseList.success(value.response))
        .catchError((e) {
      actions.value = NetworkResponseList.error(parseError(e));
    });
  }
}
