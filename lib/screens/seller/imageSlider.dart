import 'package:carousel_slider/carousel_slider.dart';
import 'package:flutter/material.dart';
import 'package:rent2buy/widgets/shared_widgets.dart';
import '../../theme.dart';

class ImageSlider extends StatefulWidget {
  List<dynamic> imgList = List();
  ImageSlider({Key key, this.imgList}) : super(key: key);
  @override
  _ImageSliderState createState() => _ImageSliderState();
}

class _ImageSliderState extends State<ImageSlider> {
  @override
  Widget build(BuildContext context) {
    return Theme(
      data: sellerTheme,
      child: Scaffold(
        appBar: BaseAppBar(
          title: '',
          iconTheme: Color(0xFF003585),
          automaticallyImplyLeading: true,
          widgets: [],
          appBar: AppBar(),
        ),
        body: Center(
          child: CarouselSlider(
              items: widget.imgList
                  .map((item) => Container(
                child: Image.network(
                  item,
                  fit: BoxFit.fitHeight,
                  width: MediaQuery.of(context).size.width,
                ),
              ))
                  .toList(),
              options: CarouselOptions(
                enableInfiniteScroll: false,
              )),
        ),
      ),
    );
  }
}
