import 'package:flutter/material.dart';
import 'package:rent2buy/di/service_locator.dart';
import 'package:rent2buy/models/thread_response.dart';
import 'package:rent2buy/networking/network_response.dart';
import 'package:rent2buy/networking/network_responselist.dart';
import 'package:rent2buy/networking/repository/message_repository.dart';
import 'package:rent2buy/utils/base_change_notifier.dart';

class InBoxViewModel extends BaseChangeNotifier {
  final MessageRepository messageRepository =
      serviceLocator<MessageRepository>();

  ValueNotifier<NetworkResponseList<ThreadResponse>> threadsValue =
      ValueNotifier(NetworkResponseList.idle());

  ValueNotifier<NetworkResponse<String>> deleteValue =
      ValueNotifier(NetworkResponse.idle());

  void getThreads(Map<String, dynamic> params) {
    threadsValue.value = NetworkResponseList.loading();
    messageRepository.getThreads(params).then((response) {
      threadsValue.value = NetworkResponseList.success(response.response);
    }).catchError((e) {
      threadsValue.value = NetworkResponseList.error(parseError(e));
    });
  }

  void deleteThread(String threadId) {
    deleteValue.value = NetworkResponse.loading();
    messageRepository.deleteThread(threadId).then((response) {
      deleteValue.value =
          NetworkResponse.successWithMsg(null, response.message);
    }).catchError((e) {
      deleteValue.value = NetworkResponse.error(parseError(e));
    });
  }
}
