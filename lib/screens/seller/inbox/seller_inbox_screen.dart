import 'package:flutter/material.dart';
import 'package:fluttertoast/fluttertoast.dart';
import 'package:rent2buy/di/service_locator.dart';
import 'package:rent2buy/models/thread_response.dart';
import 'package:rent2buy/networking/network_status.dart';
import 'package:rent2buy/screens/common/chat/seller_chat_screen.dart';
import 'package:rent2buy/screens/common/progress_view.dart';
import 'package:rent2buy/screens/seller/inbox/inbox_viewmodel.dart';
import 'package:rent2buy/theme.dart';
import 'package:rent2buy/utils/strings.dart';
import 'package:rent2buy/widgets/shared_widgets.dart';

class SellerInBoxScreen extends StatefulWidget {
  final InBoxViewModel viewModel = serviceLocator<InBoxViewModel>();

  static String routeName = '/seller_inbox_screen';

  @override
  State<StatefulWidget> createState() {
    // TODO: implement createState
    return _SellerInboxScreenState();
  }
}

class _SellerInboxScreenState extends State<SellerInBoxScreen> {
  List<ThreadResponse> threads = List();
  bool isSelected = false;

  @override
  void initState() {
    // TODO: implement initState
    super.initState();
    Map<String, dynamic> params = Map();
    widget.viewModel.getThreads(params);
  }

  @override
  Widget build(BuildContext context) {
    return Theme(
      data: sellerTheme,
      child: Scaffold(
          appBar: BaseAppBar(
            title: 'Chats',
            automaticallyImplyLeading: true,
            titleColor: Color(0xFF003585),
            iconTheme: Color(0xFF003585),
            widgets: [IconButton(icon: Icon(Icons.delete), onPressed: () {})],
            appBar: AppBar(),
          ),
          body: ValueListenableBuilder(
            valueListenable: widget.viewModel.threadsValue,
            builder: (context, value, _) {
              var response = widget.viewModel.threadsValue.value;
              switch (response.status) {
                case NetworkStatus.LOADING:
                  return CircularProgress();
                case NetworkStatus.ERROR:
                  Fluttertoast.showToast(msg: response.message);
                  break;
                case NetworkStatus.SUCCESS:
                  WidgetsBinding.instance.addPostFrameCallback((_) {
                    setState(() {
                      threads.clear();
                      threads.addAll(response.response);
                    });
                  });
                  return ListView.builder(
                      itemCount: threads.length,
                      itemBuilder: (BuildContext context, int index) {
                        return _buildItem(index);
                      });
              }
              return Row(
                mainAxisAlignment: MainAxisAlignment.center,
                children: [
                  Text('No Chats', style: TextStyle(fontSize: 24)),
                ],
              );
            },
          )),
    );
  }

  Widget _buildItem(int index) {
    try {
      ThreadResponse item = threads[index];
      return Column(
        children: [
          Padding(
            padding: const EdgeInsets.all(12.0),
            child: ListTile(
              selected: isSelected,
              leading: CircleAvatar(
                child: Image.network(
                    'https://encrypted-tbn0.gstatic.com/images?q=tbn%3AANd9GcQlR_mhU1bn_sa7j_M5UeMmIbKmR3UI1evxNw&usqp=CAU'),
                radius: 30,
                backgroundColor: Colors.transparent,
              ),
              title: Padding(
                padding: const EdgeInsets.only(left: 6.0, bottom: 8),
                child: Text(
                  item.user.firstName,
                  style: TextStyle(fontSize: 24, fontWeight: FontWeight.w700),
                ),
              ),
              subtitle: Row(
                mainAxisAlignment: MainAxisAlignment.start,
                children: [
                  Icon(
                    Icons.location_on,
                    color: Color(0xFF656565),
                  ),
                  Text(
                    '',
                    style: TextStyle(color: Colors.grey, fontSize: 16),
                  )
                ],
              ),
              trailing: Visibility(
                visible: isSelected,
                child: IconButton(icon: Icon(Icons.delete), onPressed: () {}),
              ),
              onLongPress: () {
                setState(() {
                  isSelected = true;
                });
              },
              onTap: () {
                Navigator.push(
                  context,
                  MaterialPageRoute(
                    builder: (context) => SellerChatScreen(
                        chatId: item.id,
                        receiverName: item.user.firstName,
                        receiverUserId: item.user.sId),
                  ),
                );
              },
            ),
          ),
          Divider(
            color: Color(0xFF71C7E3),
            height: 5.0,
            thickness: 3,
          ),
        ],
      );
    } catch (e) {}
  }

  showAlertDialog(BuildContext context, String title, String message,
      String btnCancel, String btnOk, VoidCallback pCallBack) {
    // set up the buttons
    Widget cancelButton = FlatButton(
      child: Text("Cancel"),
      onPressed: () {
        Navigator.of(context, rootNavigator: true).pop('dialog');
      },
    );
    Widget continueButton = FlatButton(
      child: Text("Continue"),
      onPressed: pCallBack,
    );

    // set up the AlertDialog
    AlertDialog alert = AlertDialog(
      title: Text(title),
      content: Text(message),
      actions: [
        cancelButton,
        continueButton,
      ],
    );

    // show the dialog
    newMethod(context, alert);
  }

  Future newMethod(BuildContext context, AlertDialog alert) {
    return showDialog(
      context: context,
      builder: (BuildContext context) {
        return alert;
      },
    );
  }
}
