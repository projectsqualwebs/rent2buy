import 'package:flutter/material.dart';
import 'package:rent2buy/theme.dart';
import 'package:rent2buy/widgets/shared_widgets.dart';

class ItemAdded extends StatelessWidget {
  final Map item;
  ItemAdded({Key key,@required this.item}): super(key:key);

  @override
  Widget build(BuildContext context) {
    return Theme(
      data: sellerTheme,
      child: Scaffold(
        appBar: BaseAppBar(
          appBar: AppBar(),
          title: 'Item Added',
          isCentered: true,
          automaticallyImplyLeading: false,
          iconTheme: Color(0xFF003585),
          titleColor: Color(0xFF003585),
        ),
        body: Container(
          padding: const EdgeInsets.all(20.0),
          child:
              Column(crossAxisAlignment: CrossAxisAlignment.start, children: [
                Image.network(
               item['url']??'null'),
                Row(
                  mainAxisAlignment: MainAxisAlignment.spaceBetween,
                  children: [
                    Text('\u0024' + item['price'] ?? 'null',
                    style: TextStyle(
                        color: Color(0xFF003585),
                        fontWeight: FontWeight.w600,
                        fontSize: 36)),
                    Text(item['sellType'] ?? 'null',
                    style: TextStyle(
                        color: Colors.green,
                        fontWeight: FontWeight.w600,
                        fontSize: 36))
              ],
            ),
                Text(item['itemname'] ?? 'null',
                style: TextStyle(
                    color: Color(0xFF003585),
                    fontWeight: FontWeight.w600,
                    fontSize: 36)),
                Row(
                  mainAxisAlignment: MainAxisAlignment.spaceBetween,
                  children: [
                  Text('Date Of Listing',
                    style: TextStyle(color: Color(0xFF003585), fontSize: 24)),
                  Text('26/11/2020',
                    style: TextStyle(color: Color(0xFF003585), fontSize: 24))
              ],
            ),
                SizedBox(
              height: 40,
            ),
               FlatButton(
                onPressed: () {},
                child: Text('Get Approval from Admin',
                    style: TextStyle(
                        color: Color(0xFF003585),
                        fontWeight: FontWeight.w600,
                        fontSize: 28)))
          ]),
        ),
      ),
    );
  }
}
