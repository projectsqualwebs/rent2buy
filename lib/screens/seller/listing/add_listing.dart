import 'dart:io';
import 'dart:ui';
import 'package:flutter/material.dart';
import 'package:flutter/scheduler.dart';
import 'package:fluttertoast/fluttertoast.dart';
import 'package:http/http.dart';
import 'package:image_cropper/image_cropper.dart';
import 'package:intl/intl.dart';
import 'package:provider/provider.dart';
import 'package:rent2buy/di/service_locator.dart';
import 'package:rent2buy/models/Seller/addListings.dart';
import 'package:rent2buy/models/Seller/availability.dart';
import 'package:rent2buy/models/Seller/rentDiscountPercent.dart';
import 'package:rent2buy/models/Seller/rentPrice.dart';
import 'package:rent2buy/models/bundle.dart';
import 'package:rent2buy/models/category.dart';
import 'package:rent2buy/models/delivery_type.dart';
import 'package:rent2buy/models/product.dart';
import 'package:rent2buy/models/purchase_actions.dart';
import 'package:rent2buy/networking/network_response.dart';
import 'package:rent2buy/networking/network_status.dart';
import 'package:rent2buy/screens/common/error_screen.dart';
import 'package:rent2buy/screens/common/loading.dart';
import 'package:rent2buy/screens/common/loading_dialog.dart';
import 'package:rent2buy/screens/common/progress_view.dart';
import 'package:rent2buy/screens/seller/imageSlider.dart';
import 'package:rent2buy/screens/seller/listing/add_listing_viewmodel.dart';
import 'package:image_picker/image_picker.dart';
import 'package:rent2buy/services/apiProvider.dart';
import 'package:rent2buy/theme.dart';
import 'package:rent2buy/utils/colors.dart';
import 'package:rent2buy/utils/constants.dart';
import 'package:rent2buy/utils/data_manager.dart';
import 'package:rent2buy/utils/design_utils.dart';
import 'package:rent2buy/utils/dimens.dart';
import 'package:rent2buy/utils/strings.dart';
import 'package:rent2buy/widgets/shared_widgets.dart';
import 'package:http/http.dart' as http;
import 'dart:convert';
import 'package:file_picker/file_picker.dart';

class AddListing extends StatefulWidget {
  static final String routeName = '/add_listing';
  final DataManager dataManager = serviceLocator<DataManager>();
  final AddListingViewModel viewModel = serviceLocator<AddListingViewModel>();

  @override
  _AddListingState createState() => _AddListingState();
}

class _AddListingState extends State<AddListing> {
  AddListings addListings = AddListings();
  RentPrice rentPrice = RentPrice();
  RentDiscountPercent rentDiscountPercent = RentDiscountPercent();
  Availability availability = Availability();
  GlobalKey<ScaffoldState> _keyScafold = GlobalKey<ScaffoldState>();
  List<ListingCategory> categories = List();
  List<ListingCategory> subCategories = List();
  final _key = GlobalKey<FormState>();
  String name;
  int sellPrice;
  int sellDiscount;
  int rentPricePerHour;
  int rentPricePerDay;
  int discountFor3Day;
  int discountFor1Week;
  int discountFor1Month;
  String desc;
  String url;
  String fromDate = 'From Date';
  String toDate = 'To Date';
  DateTime fromDateVal;
  bool _isSelfPickup = false;
  bool _isDelivery = false;
  bool _isSell = false;
  bool _isRent = false;
  List<String> purchase_action = List();
  List<String> delivery_type = List();
  List<dynamic> imageUrl = List();
  List<File> imageFiles = List();
  List<String> paths = List();
  bool loading = false;
  bool showImages = false;
  bool uploadingImage = false;
  String deliveryTypeID;
  String selfPickUpID;

  GlobalKey<State> _keyLoader = GlobalKey<State>();

  var _keyList = GlobalKey<State>();

  var _keyImageLoader = GlobalKey<State>();

  bool isEdit = false;

  TextEditingController _itemNameController = TextEditingController();

  var _descriptionController = TextEditingController();

  var _sellPriceController = TextEditingController();

  var _sellDiscountController = TextEditingController();

  var _rentPricePerHourController = TextEditingController();

  var _rentPricePerDayController = TextEditingController();

  Bundle bundle;

  String _selectedCategory, _selectedSubCategory;

  var _isLoading = false;

  getActions() {
    widget.viewModel.getActions();
    widget.viewModel.actions.addListener(() {
      var value = widget.viewModel.actions.value;
      if (value.status == NetworkStatus.SUCCESS && value.response.isNotEmpty) {
        widget.dataManager.saveSellId(value.response.first.id);
        widget.dataManager.saveRentId(value.response.last.id);
      }
    });
  }

  getDeliveryTypes() {
    widget.viewModel.getDeliveryTypes();
    widget.viewModel.actions.addListener(() {
      var value = widget.viewModel.deliveryTypes.value;
      if (value.status == NetworkStatus.SUCCESS && value.response.isNotEmpty) {
        widget.dataManager.saveDeliveryId(value.response.first.id);
        widget.dataManager.saveSelfPickUpId(value.response.last.id);
        print(deliveryTypeID);
      }
    });
  }

  getCategory() async {
    var res = await http.get(ApiProvider.baseUrl + 'categories');
    Map<String, dynamic> map = json.decode(res.body);

    // categories.clear();
    // categories.addAll(List<ListingCategory>.from(
    //     map['response'].map((x) => ListingCategory.fromJson(x))));

    setState(() {
      categories = List<ListingCategory>.from(map['response'].map((x) {
        ListingCategory category = ListingCategory.fromJson(x);
        return category;
      }));
    });
  }

  getSubCategory(String id) async {
    var res = await http.get(ApiProvider.baseUrl + 'sub-categories/' + '$id');
    Map<String, dynamic> map = json.decode(res.body);
    subCategories.clear();
    subCategories.addAll(List<ListingCategory>.from(
        map['response'].map((x) => ListingCategory.fromJson(x))));
    setState(() {});
  }

  getImageFromCamera() async {
    final pickedFile = await ImagePicker().getImage(source: ImageSource.camera);

    setState(() {
      if (pickedFile != null) {
        cropImage(pickedFile.path);
      } else {
        print('No image selected.');
      }
    });
  }

  getImages() async {
    FilePickerResult result = await FilePicker.platform.pickFiles(
      allowMultiple: false,
      type: FileType.custom,
      allowedExtensions: ['jpg', 'png', 'gif'],
    );

    if (result != null) {
      var path = result.paths.first;
      cropImage(path);
    }
  }

  cropImage(String path) async {
    File croppedFile = await ImageCropper.cropImage(
        sourcePath: path,
        aspectRatioPresets: Platform.isAndroid
            ? [
                CropAspectRatioPreset.square,
                CropAspectRatioPreset.ratio3x2,
                CropAspectRatioPreset.original,
                CropAspectRatioPreset.ratio4x3,
                CropAspectRatioPreset.ratio16x9
              ]
            : [
                CropAspectRatioPreset.original,
                CropAspectRatioPreset.square,
                CropAspectRatioPreset.ratio3x2,
                CropAspectRatioPreset.ratio4x3,
                CropAspectRatioPreset.ratio5x3,
                CropAspectRatioPreset.ratio5x4,
                CropAspectRatioPreset.ratio7x5,
                CropAspectRatioPreset.ratio16x9
              ],
        androidUiSettings: AndroidUiSettings(
            toolbarTitle: 'Cropper',
            toolbarColor: Colors.deepOrange,
            toolbarWidgetColor: Colors.white,
            initAspectRatio: CropAspectRatioPreset.original,
            lockAspectRatio: false),
        iosUiSettings: IOSUiSettings(
          title: 'Cropper',
        ));
    if (croppedFile != null) {
      paths.clear();
      paths.add(croppedFile.path);
      uploadImage();
    }
  }

  uploadImage() async {
    setState(() {
      uploadingImage = true;
      showImages = true;
    });

    List<MultipartFile> newList = List();
    var url = Uri.parse('${ApiProvider.uploadImage}');
    http.MultipartRequest req = http.MultipartRequest('POST', url);
    req.fields['type'] = 'listing_images';
    for (var file in paths) {
      http.MultipartFile multipartFile =
          await http.MultipartFile.fromPath('images[]', file);
      newList.add(multipartFile);
    }
    req.files.addAll(newList);
    var response = await req.send();

    if (response.statusCode == 200) {
      print("uploaded");
      setState(() {
        uploadingImage = false;
      });
    } else {
      print("not uploaded");
    }
    response.stream.transform(utf8.decoder).listen((value) {
      List<dynamic> uploadedImages = jsonDecode(value)['response']['path'];
      imageUrl.addAll(uploadedImages);
      print(imageUrl);
    });
  }

  @override
  void initState() {
    super.initState();
    WidgetsBinding.instance.addPostFrameCallback((_) {
      widget.viewModel.submitValue.addListener(() {
        var response = widget.viewModel.submitValue.value;
        switch (response.status) {
          case NetworkStatus.LOADING:
            Dialogs.showLoadingDialog(
                _keyScafold.currentContext, _keyLoader, Strings.addingProduct);
            break;
          case NetworkStatus.ERROR:
            Navigator.of(_keyLoader.currentContext, rootNavigator: true)
                .pop('dialog');
            Fluttertoast.showToast(msg: response.message);
            break;
          case NetworkStatus.SUCCESS:
            Navigator.of(_keyLoader.currentContext, rootNavigator: true)
                .pop('dialog');
            Navigator.pushReplacementNamed(context, '/seller_home');
            break;
        }
        return Container();
      });

      widget.viewModel.productValue.addListener(() {
        var response = widget.viewModel.productValue.value;
        switch (response.status) {
          case NetworkStatus.LOADING:
            _isLoading = true;
            break;
          case NetworkStatus.ERROR:
            _isLoading = false;
            Fluttertoast.showToast(msg: response.message);
            break;
          case NetworkStatus.SUCCESS:
            _isLoading = false;
            var product = response.response;
            _itemNameController.text = product.name;
            _descriptionController.text = product.description;
            setState(() {
              _isSelfPickup = checkSelfPickUp(product.deliveryType);
              _isDelivery = checkDelivery(product.deliveryType);
              _isSell = checkSell(product.purchaseActions);
              _isRent = checkRent(product.purchaseActions);
              imageUrl.clear();
              imageUrl.addAll(product.images.path);

              _selectedCategory = product.listingCategory.id;
              _selectedSubCategory = product.listingSubCategory.id;

              if (_isSell) {
                _sellPriceController.text =
                    product.purchaseActions.first.price.toString();
                _sellDiscountController.text =
                    product.purchaseActions.first.discountPercent.toString();
              }
              if (_isRent) {
                _rentPricePerHourController.text =
                    product.purchaseActions.first.price.toString();
              }
              this.getCategory();
              this.getSubCategory(_selectedCategory);
              getActions();
              getDeliveryTypes();
              showImages = true;
            });
            break;
        }
      });
    });

    Future.delayed(Duration.zero, () {
      bundle = ModalRoute.of(context).settings.arguments;
      if (bundle != null) {
        widget.viewModel.getSingleProduct(bundle.id);
        isEdit = bundle != null;
      } else {
        this.getCategory();
        getActions();
        getDeliveryTypes();
      }
    });
  }

  @override
  Widget build(BuildContext context) {
    return Theme(
      data: sellerTheme,
      child: Scaffold(
        key: _keyScafold,
        appBar: BaseAppBar(
          appBar: AppBar(),
          title: isEdit ? Strings.txtEditListing : Strings.txtAddListing,
          isCentered: true,
          automaticallyImplyLeading: true,
          iconTheme: Color(0xFF003585),
          titleColor: Color(0xFF003585),
        ),
        body: AnimatedCrossFade(
          duration: Duration(milliseconds: 500),
          crossFadeState:
              _isLoading ? CrossFadeState.showFirst : CrossFadeState.showSecond,
          firstChild: CircularProgress(),
          secondChild: _buildForm(),
        ),
      ),
    );
  }

  Widget _buildForm() {
    return SingleChildScrollView(
      child: Container(
        padding: EdgeInsets.symmetric(
          horizontal: 30,
        ),
        child: Form(
          key: _key,
          child: Column(
            children: [
              SizedBox(height: 20),
              AuthTextField(
                textEditingController: _itemNameController,
                hinttext: 'Item Name',
                color: Color(0xFF71C7E3),
                isNumber: false,
                isPassword: false,
                validator: (String val) {
                  if (val.isEmpty) {
                    return 'Enter Item Name';
                  }
                  return null;
                },
                onSaved: (String value) {
                  name = value;
                },
              ),
              SizedBox(height: 20),
              Row(
                children: [
                  Expanded(
                    child: DropdownButton(
                        hint: Text('Choose Category'),
                        dropdownColor: Color(0xFFE8F5FA),
                        isExpanded: true,
                        items: categories.map((item) {
                          return DropdownMenuItem(
                            child: Text(item.name),
                            value: item.id,
                          );
                        }).toList(),
                        value: _selectedCategory,
                        onChanged: (val) {
                          setState(() {
                            _selectedCategory = val;
                            getSubCategory(_selectedCategory);
                          });
                        }),
                  ),
                ],
              ),
              Row(
                children: [
                  Expanded(
                    child: DropdownButton(
                        hint: Text('Choose Sub Category'),
                        dropdownColor: Color(0xFFE8F5FA),
                        isExpanded: true,
                        items: subCategories.map((item) {
                          return DropdownMenuItem(
                            child: Text(item.name),
                            value: item.id,
                          );
                        }).toList(),
                        value: _selectedSubCategory,
                        onChanged: (val) {
                          setState(() {
                            _selectedSubCategory = val;
                          });
                        }),
                  ),
                ],
              ),
              SizedBox(height: 20),
              TextFormField(
                controller: _descriptionController,
                decoration: InputDecoration(
                    enabled: true,
                    enabledBorder: UnderlineInputBorder(
                        borderSide:
                            BorderSide(width: 3, color: Color(0xFF71C7E3))),
                    hintText: 'Description'),
                validator: (String val) {
                  if (val.isEmpty) {
                    return 'Enter Description';
                  }
                  return null;
                },
                onSaved: (String value) {
                  desc = value;
                },
                maxLines: null,
                keyboardType: TextInputType.multiline,
              ),
              SizedBox(height: 20),
              Row(
                children: [
                  Expanded(
                    child: ListTile(
                      title: Text('Self Pickup',
                          style: TextStyle(color: Colors.black54)),
                      leading: Checkbox(
                        value: _isSelfPickup,
                        onChanged: (value) {
                          setState(() {
                            _isSelfPickup = value;
                          });
                        },
                      ),
                    ),
                  ),
                  Expanded(
                    child: ListTile(
                      title: Text(
                        'Delivery',
                        style: TextStyle(color: Colors.black54),
                      ),
                      leading: Checkbox(
                        value: _isDelivery,
                        onChanged: (value) {
                          setState(() {
                            _isDelivery = value;
                          });
                        },
                      ),
                    ),
                  )
                ],
              ),
              SizedBox(height: 20),
              Row(
                children: [
                  Expanded(
                    child: ListTile(
                      title:
                          Text('Sell', style: TextStyle(color: Colors.black54)),
                      leading: Checkbox(
                        value: _isSell,
                        onChanged: (value) {
                          setState(() {
                            _isSell = value;
                          });
                        },
                      ),
                    ),
                  ),
                  Expanded(
                    child: ListTile(
                      title:
                          Text('Rent', style: TextStyle(color: Colors.black54)),
                      leading: Checkbox(
                        value: _isRent,
                        onChanged: (value) {
                          setState(() {
                            _isRent = value;
                          });
                        },
                      ),
                    ),
                  )
                ],
              ),
              Visibility(
                visible: _isSell,
                maintainSize: false,
                child: Row(
                  children: [
                    Expanded(
                      child: AuthTextField(
                        textEditingController: _sellPriceController,
                        hinttext: 'Sell Price',
                        color: Color(0xFF71C7E3),
                        isNumber: true,
                        isPassword: false,
                        validator: (String val) {
                          if (val.isEmpty) {
                            return 'Enter Sell Price';
                          }
                          return null;
                        },
                        onSaved: (String value) {
                          sellPrice = int.parse(value);
                        },
                      ),
                    ),
                    SizedBox(width: 30),
                    Expanded(
                      child: AuthTextField(
                        textEditingController: _sellDiscountController,
                        hinttext: 'Sell Discount (%)',
                        color: Color(0xFF71C7E3),
                        isNumber: true,
                        isPassword: false,
                        validator: (String val) {
                          if (val.isEmpty) {
                            return 'Enter Sell Disc.';
                          }
                          return null;
                        },
                        onSaved: (String value) {
                          sellDiscount = int.parse(value);
                        },
                      ),
                    ),
                  ],
                ),
              ),
              Visibility(
                visible: _isRent,
                maintainSize: false,
                child: Column(
                  children: [
                    Row(
                      children: [
                        Expanded(
                          child: AuthTextField(
                            textEditingController: _rentPricePerHourController,
                            hinttext: 'Rent Price (per hour)',
                            color: Color(0xFF71C7E3),
                            isNumber: true,
                            isPassword: false,
                            validator: (String val) {
                              if (val.isEmpty) {
                                return 'Enter Price / Your Bid';
                              }
                              return null;
                            },
                            onSaved: (String value) {
                              rentPrice.per_hour = int.parse(value) ?? '';
                            },
                          ),
                        ),
                        SizedBox(
                          width: 30,
                        ),
                        Expanded(
                          child: AuthTextField(
                            textEditingController: _rentPricePerDayController,
                            hinttext: 'Rent Price (per day)',
                            color: Color(0xFF71C7E3),
                            isNumber: true,
                            isPassword: false,
                            validator: (String val) {
                              if (val.isEmpty) {
                                return 'Enter Rent Price';
                              }
                              return null;
                            },
                            onSaved: (String value) {
                              rentPrice.per_day = int.parse(value) ?? '';
                            },
                          ),
                        ),
                      ],
                    ),
                    SizedBox(height: 10),
                    Column(
                      children: [
                        AuthTextField(
                          hinttext: 'Disc.(%) more than 3 Days',
                          color: Color(0xFF71C7E3),
                          isNumber: true,
                          isPassword: false,
                          validator: (String val) {
                            if (val == null) {
                              return 'Enter Disc.';
                            }
                            return null;
                          },
                          onSaved: (String value) {
                            rentDiscountPercent.more_than_3_days =
                                int.parse(value) ?? '';
                          },
                        ),
                        SizedBox(height: 10),
                        AuthTextField(
                          hinttext: 'Disc.(%) more than 1 Week',
                          color: Color(0xFF71C7E3),
                          isNumber: true,
                          isPassword: false,
                          validator: (String val) {
                            if (val == null) {
                              return 'Enter Disc.';
                            }
                            return null;
                          },
                          onSaved: (String value) {
                            rentDiscountPercent.more_than_1_week =
                                int.parse(value) ?? '';
                          },
                        ),
                        SizedBox(height: 10),
                        AuthTextField(
                          hinttext: 'Disc.(%) more than 1 Month',
                          color: Color(0xFF71C7E3),
                          isNumber: true,
                          isPassword: false,
                          validator: (String val) {
                            if (val == null) {
                              return 'Enter Price / Your Bid';
                            }
                            return null;
                          },
                          onSaved: (String value) {
                            rentDiscountPercent.more_than_1_month =
                                int.parse(value) ?? '';
                          },
                        ),
                        SizedBox(
                          height: 10,
                        ),
                        Text(Strings.setAvailability,
                            style: DesignUtils.textApperanceRegular()),
                        Row(
                          mainAxisAlignment: MainAxisAlignment.spaceAround,
                          children: [
                            FlatButton.icon(
                                color: Color(0xFFE8F5FA),
                                icon: Icon(Icons.date_range,
                                    color: Color(0xFF71C7E3)),
                                label: Text(
                                  fromDate,
                                  style: TextStyle(color: Colors.black54),
                                ),
                                onPressed: () async {
                                  fromDateVal = await showDatePicker(
                                      context: context,
                                      initialDate: DateTime.now(),
                                      firstDate: DateTime.now(),
                                      lastDate: DateTime(2022));
                                  fromDate = DateFormat('yyyy/MM/dd')
                                      .format(fromDateVal);
                                  print(fromDate);
                                  availability.available_from = fromDate ?? '';
                                  setState(() {});
                                }),
                            FlatButton.icon(
                                color: Color(0xFFE8F5FA),
                                icon: Icon(Icons.date_range,
                                    color: Color(0xFF71C7E3)),
                                label: Text(
                                  toDate,
                                  style: TextStyle(color: Colors.black54),
                                ),
                                onPressed: () async {
                                  final DateTime toDateVal =
                                      await showDatePicker(
                                          context: context,
                                          initialDate: fromDateVal,
                                          firstDate: fromDateVal,
                                          lastDate: DateTime(2022));
                                  toDate = DateFormat('yyyy/MM/dd')
                                      .format(toDateVal);
                                  print(toDate);
                                  availability.available_to = toDate ?? '';
                                  setState(() {});
                                }),
                          ],
                        )
                      ],
                    ),
                  ],
                ),
              ),
              SizedBox(height: 30),
              Visibility(
                key: _keyImageLoader,
                visible: showImages,
                child: (uploadingImage
                    ? CircularProgress()
                    : Container(
                        height: 64.0,
                        child: Row(
                          children: [
                            Flexible(
                              child: ListView.builder(
                                key: _keyList,
                                shrinkWrap: true,
                                scrollDirection: Axis.horizontal,
                                itemCount: imageUrl.length,
                                itemBuilder: (BuildContext context, int index) {
                                  return GestureDetector(
                                    onTap: () {
                                      Navigator.push(
                                          context,
                                          MaterialPageRoute(
                                              builder: (context) => ImageSlider(
                                                  imgList: imageUrl)));
                                    },
                                    child: Container(
                                      width: 96,
                                      height: 96,
                                      child: Stack(
                                        children: [
                                          Image.network(imageUrl[index],
                                              width: 90, height: 90),
                                          InkWell(
                                              onTap: () {
                                                imageUrl.removeAt(index);
                                                setState(() {
                                                  if (imageUrl.isEmpty) {
                                                    showImages = false;
                                                  }
                                                });
                                              },
                                              child: Align(
                                                alignment: Alignment.topRight,
                                                child: Icon(
                                                  Icons.delete,
                                                  color: colorSellerDark,
                                                ),
                                              ))
                                        ],
                                      ),
                                    ),
                                  );
                                },
                              ),
                            ),
                            SizedBox(
                              width: margin_large,
                            ),
                            InkWell(
                                onTap: () {
                                  // getImages();
                                  showImageAndfileChooser();
                                },
                                child: Icon(Icons.add)),
                          ],
                        ),
                      )),
              ),
              SizedBox(height: 10),
              Visibility(
                visible: imageUrl.isEmpty,
                child: MyButton(
                    color: Color(0xFF003585),
                    text: 'Upload Image',
                    onPressed: () {
                      showImageAndfileChooser();
                    }),
              ),
              SizedBox(height: 10),
              Visibility(
                  child: Row(
                    mainAxisAlignment: MainAxisAlignment.center,
                    children: [
                      SizedBox(width: 8),
                      CircularProgressIndicator(
                        strokeWidth: 3,
                      ),
                      SizedBox(width: 8),
                      Text('Uploading...')
                    ],
                  ),
                  visible: false),
              SizedBox(height: 10),
              MyButton(
                color: Color(0xFF003585),
                text: isEdit ? Strings.txtUpdate : Strings.txtSubmit,
                onPressed: () async {
                  if (_key.currentState.validate()) {
                    _key.currentState.save();
                    delivery_type.clear();
                    purchase_action.clear();

                    if (_isSelfPickup) {
                      delivery_type
                          .add(await widget.dataManager.loadSelfPickupId());
                    }
                    if (_isDelivery) {
                      delivery_type
                          .add(await widget.dataManager.loadDeliveryId());
                    }

                    if (_isSell) {
                      purchase_action
                          .add(await widget.dataManager.loadSellId());
                    }
                    if (_isRent) {
                      purchase_action
                          .add(await widget.dataManager.loadRentId());
                    }

                    if (_selectedCategory == null) {
                      Fluttertoast.showToast(msg: Strings.msgSelectCategory);
                      return;
                    }
                    if (delivery_type.isEmpty) {
                      Fluttertoast.showToast(
                          msg: Strings.msgSelectDeliveryMethod);
                      return;
                    } else if (purchase_action.isEmpty) {
                      Fluttertoast.showToast(
                          msg: Strings.msgSelectPurchaseAction);
                      return;
                    }

                    addListings = AddListings(
                        name: name,
                        category_id: _selectedCategory,
                        sub_category_id: _selectedSubCategory,
                        description: desc,
                        delivery_type: delivery_type,
                        purchase_action: purchase_action,
                        sell_price: sellPrice,
                        sell_discount_percent: sellDiscount,
                        rent_price: rentPrice,
                        rent_discount_percent: rentDiscountPercent,
                        availability: availability,
                        images: imageUrl);
                    //print(addListings.toJson());
                    if (isEdit) {
                      widget.viewModel.editListing(addListings, bundle?.id);
                    } else {
                      widget.viewModel.addListing(addListings);
                    }
                  }
                },
              )
            ],
          ),
        ),
      ),
    );
  }

  bool checkSelfPickUp(List<DeliveryType> deliveryTypes) {
    if (deliveryTypes.length == 2) return true;
    return deliveryTypes.first.name == ACTION_SELF;
  }

  bool checkDelivery(List<DeliveryType> deliveryTypes) {
    if (deliveryTypes.length == 2) return true;
    return deliveryTypes.first.name == ACTION_DELIVERY;
  }

  bool checkSell(List<PurchaseActions> purchaseActions) {
    if (purchaseActions.length == 2) return true;
    return purchaseActions.first.name == ACTION_SELL;
  }

  bool checkRent(List<PurchaseActions> purchaseActions) {
    if (purchaseActions.length == 2) return true;
    return purchaseActions.first.name == ACTION_RENT;
  }

  showImageAndfileChooser() {
    showDialog(
        context: context,
        barrierDismissible: true,
        builder: (BuildContext context) {
          return WillPopScope(
            child: AlertDialog(
                shape: RoundedRectangleBorder(
                  borderRadius:
                      BorderRadius.all(Radius.circular(default_radius)),
                ),
                backgroundColor: Colors.white,
                content: Column(
                  mainAxisSize: MainAxisSize.min,
                  children: [
                    ListTile(
                      title: Text(Strings.camera),
                      leading: Image.asset(
                        "assets/icons/camera.png",
                        width: 24,
                        height: 24,
                      ),
                      onTap: () {
                        Navigator.pop(context);
                        getImageFromCamera();
                      },
                    ),
                    ListTile(
                      title: Text(Strings.file),
                      leading: Image.asset("assets/icons/image.png",
                          width: 24, height: 24),
                      onTap: () {
                        Navigator.pop(context);
                        getImages();
                      },
                    )
                  ],
                )),
            onWillPop: () async => true,
          );
        });
  }
}
