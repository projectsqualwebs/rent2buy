import 'package:flutter/cupertino.dart';
import 'package:rent2buy/di/service_locator.dart';
import 'package:rent2buy/models/Seller/addListings.dart';
import 'package:rent2buy/models/delivery_type.dart';
import 'package:rent2buy/models/product.dart';
import 'package:rent2buy/models/purchase_actions.dart';
import 'package:rent2buy/networking/network_response.dart';
import 'package:rent2buy/networking/network_responselist.dart';
import 'package:rent2buy/networking/repository/seller_repository.dart';
import 'package:rent2buy/utils/base_change_notifier.dart';

class AddListingViewModel extends BaseChangeNotifier {
  SellerRepository _sellerRepository = serviceLocator<SellerRepository>();
  NetworkResponse<String> listEvent;

  ValueNotifier<NetworkResponseList<PurchaseActions>> actions =
      ValueNotifier(NetworkResponseList.idle());

  ValueNotifier<NetworkResponseList<DeliveryType>> deliveryTypes =
      ValueNotifier(NetworkResponseList.idle());

  ValueNotifier<NetworkResponse<Product>> productValue =
      ValueNotifier(NetworkResponse.idle());

  ValueNotifier<NetworkResponse<String>> submitValue =
      ValueNotifier(NetworkResponse.idle());

  AddListingViewModel() {
    setListing(NetworkResponse.idle());
  }

  void setListing(NetworkResponse<String> response) {
    listEvent = response;
    notifyListeners();
  }

  void getSingleProduct(String id) {
    productValue.value = NetworkResponse.loading();
    _sellerRepository.getSingleProduct(id).then((response) {
      productValue.value = NetworkResponse.success(response.response);
    }).catchError((e) {
      productValue.value = NetworkResponse.error(parseError(e));
    });
  }

  void addListing(AddListings params) {
    submitValue.value = NetworkResponse.loading();
    _sellerRepository.addListing(params).then((response) {
      submitValue.value =
          NetworkResponse.successWithMsg(null, response.message);
    }).catchError((e) {
      submitValue.value = NetworkResponse.error(parseError(e));
    });
  }

  void editListing(AddListings params, String id) {
    submitValue.value = NetworkResponse.loading();
    _sellerRepository.editListing(params, id).then((response) {
      submitValue.value =
          NetworkResponse.successWithMsg(null, response.message);
    }).catchError((e) {
      submitValue.value = NetworkResponse.error(parseError(e));
    });
  }

  void getDeliveryTypes() {
    deliveryTypes.value = NetworkResponseList.loading();
    _sellerRepository
        .getDeliveryTypes()
        .then((value) =>
            deliveryTypes.value = NetworkResponseList.success(value.response))
        .catchError((e) {
      deliveryTypes.value = NetworkResponseList.error(parseError(e));
    });
  }

  void getActions() {
    actions.value = NetworkResponseList.loading();
    _sellerRepository
        .getActions()
        .then((value) =>
            actions.value = NetworkResponseList.success(value.response))
        .catchError((e) {
      actions.value = NetworkResponseList.error(parseError(e));
    });
  }
}
