import 'package:flutter/material.dart';
import 'package:rent2buy/di/service_locator.dart';
import 'package:rent2buy/models/product.dart';
import 'package:rent2buy/networking/network_response.dart';
import 'package:rent2buy/networking/network_responselist.dart';
import 'package:rent2buy/networking/repository/seller_repository.dart';
import 'package:rent2buy/utils/base_change_notifier.dart';

class ListingViewModel extends BaseChangeNotifier {
  final SellerRepository _sellerRepository = serviceLocator<SellerRepository>();

  ValueNotifier<NetworkResponseList<Product>> listingValue =
      ValueNotifier(NetworkResponseList.idle());

  ValueNotifier<NetworkResponse<String>> updateValue =
      ValueNotifier(NetworkResponse.idle());

  void getListing(Map<String, dynamic> params) {
    listingValue.value = NetworkResponseList.loading();
    _sellerRepository.getProducts(params).then((response) {
      listingValue.value = NetworkResponseList.success(response.response);
    }).catchError((e) {
      listingValue.value = NetworkResponseList.error(parseError(e));
    });
  }

  void updateListingStatus(Map<String, dynamic> params, String listingId) {
    updateValue.value = NetworkResponse.loading();
    _sellerRepository.updateListingStatus(params, listingId).then((response) {
      updateValue.value =
          NetworkResponse.successWithMsg(null, response.message);
    }).catchError((e) {
      updateValue.value = NetworkResponse.error(parseError(e));
    });
  }
}
