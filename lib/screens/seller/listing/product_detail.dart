import 'package:carousel_pro/carousel_pro.dart';
import 'package:flutter/material.dart';
import 'package:rent2buy/data.dart';
import 'package:rent2buy/di/service_locator.dart';
import 'package:rent2buy/models/bundle.dart';
import 'package:rent2buy/models/product.dart';
import 'package:rent2buy/models/purchase_actions.dart';
import 'package:rent2buy/networking/network_response.dart';
import 'package:rent2buy/networking/network_status.dart';
import 'package:rent2buy/screens/common/error_screen.dart';
import 'package:rent2buy/screens/common/loading.dart';
import 'package:rent2buy/screens/seller/listing/add_listing.dart';
import 'package:rent2buy/screens/seller/listing/product_detail_viewmodel.dart';
import 'package:rent2buy/screens/seller/rent_product_details.dart';
import 'package:rent2buy/theme.dart';
import 'package:rent2buy/utils/colors.dart';
import 'package:rent2buy/utils/constants.dart';
import 'package:rent2buy/utils/strings.dart';
import 'package:rent2buy/utils/constants.dart' as Constants;
import 'package:rent2buy/utils/utils.dart';
import 'package:rent2buy/widgets/shared_widgets.dart';

class SellerProductDetail extends StatefulWidget {
  static const routeName = '/seller_product_detail';

  final ProductDetailViewModel viewModel =
      serviceLocator<ProductDetailViewModel>();

  @override
  _SellerProductDetailState createState() => _SellerProductDetailState();
}

class _SellerProductDetailState extends State<SellerProductDetail> {
  int index = 0;
  Bundle id;
  Product product;
  String currentPurchaseAction;
  bool _isOrder = false;

  @override
  void initState() {
    super.initState();
    Future.delayed(Duration.zero, () {
      id = ModalRoute.of(context).settings.arguments;
      widget.viewModel.getSingleProduct(id.id);
      setState(() {
        _isOrder = id.isOrder;
      });
    });
  }

  @override
  void dispose() {
    // TODO: implement dispose
    widget.viewModel.dispose();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return Theme(
      data: sellerTheme,
      child: Scaffold(
        appBar: BaseAppBar(
          title: '',
          iconTheme: Color(0xFF003585),
          automaticallyImplyLeading: true,
          widgets: [
            Visibility(
              visible: _isOrder,
              child: IconButton(
                  icon: Image.asset(
                    'assets/dispute.png',
                    height: 32,
                    color: Color(0xFF003585),
                  ),
                  onPressed: () {
                    Navigator.pushNamed(context, '/seller_dispute');
                  }),
            ),
            IconButton(
                icon: Icon(Icons.edit),
                onPressed: () {
                  Navigator.pushNamed(context, AddListing.routeName,
                      arguments: id);
                })
          ],
          appBar: AppBar(),
        ),
        body: ValueListenableBuilder(
          valueListenable: widget.viewModel.productValue,
          builder: (context, value, _) {
            var response = value as NetworkResponse<Product>;
            switch (response.status) {
              case NetworkStatus.LOADING:
                return new LoadingScreen(isCustom: false);
              case NetworkStatus.ERROR:
                return new ErrorScreen(response.message);
              case NetworkStatus.SUCCESS:
                product = response.response;

                List<NetworkImage> images = List<NetworkImage>();
                for (String url in response.response.images.path) {
                  images.add(NetworkImage(url));
                }
                print(response.response.purchaseActions.toString());

                currentPurchaseAction =
                    Utils.getPurchaseActions(product.purchaseActions);

                var sellAction = response.response.purchaseActions.first;
                var purchaseType = 1;
                var sellPrice, rentPrice;

                if (response.response.purchaseActions.length > 1) {
                  purchaseType = 3;
                  sellPrice = response.response.purchaseActions.first.price;
                  rentPrice =
                      response.response.purchaseActions.last.price['per_day'];
                } else {
                  purchaseType =
                      response.response.purchaseActions.first.name == "sell"
                          ? 1
                          : 2;
                  if (purchaseType == 1) {
                    sellPrice = sellPrice =
                        response.response.purchaseActions.first.price;
                  } else {
                    rentPrice =
                        response.response.purchaseActions.last.price['per_day'];
                  }
                }

                return Column(
                  mainAxisAlignment: MainAxisAlignment.start,
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: [
                    SizedBox(
                      width: MediaQuery.of(context).size.width,
                      height: 250,
                      child: Carousel(
                        autoplay: false,
                        images: images,
                        dotSize: 4.0,
                        dotBgColor: Colors.transparent,
                        dotSpacing: 12.0,
                        dotColor: colorAccentLight,
                        dotIncreasedColor: colorAccent,
                      ),
                    ),
                    Container(
                      padding: EdgeInsets.all(16),
                      child: Row(
                        mainAxisAlignment: MainAxisAlignment.spaceBetween,
                        children: [
                          Column(
                            mainAxisAlignment: MainAxisAlignment.start,
                            children: [
                              Visibility(
                                visible:
                                    getSellVisibility(product.purchaseActions),
                                child: Text(
                                    Utils.getPriceInDollar(
                                        product.purchaseActions.first.price),
                                    style: TextStyle(
                                        color: Color(0xFF97080E),
                                        fontSize: 24)),
                              ),
                              Visibility(
                                visible: getRentPriceVisibility(
                                    product.purchaseActions),
                                child: Text(
                                    getRentPricePerDay(product.purchaseActions),
                                    style: TextStyle(
                                        color: Color(0xFF97080E),
                                        fontSize: 24)),
                              ),
                              Visibility(
                                visible: getRentPriceVisibility(
                                    product.purchaseActions),
                                child: Text(
                                    getRentPricePerHour(
                                        product.purchaseActions),
                                    style: TextStyle(
                                        color: Color(0xFF97080E),
                                        fontSize: 24)),
                              ),
                            ],
                          ),
                          Column(
                            mainAxisAlignment: MainAxisAlignment.spaceAround,
                            children: [
                              Text(getPurchaseAction(product.purchaseActions),
                                  style: TextStyle(
                                      color: Color(0xFF97080E), fontSize: 24)),
                              Text('Available',
                                  style: TextStyle(
                                      color: Colors.green, fontSize: 22)),
                            ],
                          )
                        ],
                      ),
                    ),
                    Container(
                      padding: EdgeInsets.only(left: 12, right: 12),
                      child: Column(
                        crossAxisAlignment: CrossAxisAlignment.start,
                        children: [
                          Text(disputeList[index].avaliability + ' : In Stock',
                              style: TextStyle(
                                  color: Colors.blue[900], fontSize: 22)),
                          SizedBox(height: 10),
                          Text(
                            response.response.name,
                            style: TextStyle(fontSize: 20),
                          ),
                          SizedBox(height: 10),
                          Text(
                            response.response.description,
                            style: TextStyle(fontSize: 20),
                          ),
                        ],
                      ),
                    ),
                  ],
                );
            }
            return Container();
          },
        ),
      ),
    );
  }

  getSellVisibility(List<PurchaseActions> purchaseActions) {
    if (purchaseActions.length == 2) return true;
    if (purchaseActions.first.name == Constants.ACTION_SELL) return true;
    return false;
  }

  String getRentPricePerDay(List<PurchaseActions> purchaseActions) {
    if (purchaseActions.length == 2)
      return Utils.getPriceInDollar(purchaseActions.last.price[perDay]) +
          " for 1 day";
    if (currentPurchaseAction == ACTION_SELL) return "";
    return Utils.getPriceInDollar(purchaseActions.first.price[perDay]) +
        " for 1 day";
  }

  String getRentPricePerHour(List<PurchaseActions> purchaseActions) {
    if (purchaseActions.length == 2)
      return Utils.getPriceInDollar(purchaseActions.last.price[perHour]) +
          " for 1 hour";
    if (currentPurchaseAction == ACTION_SELL) return "";
    return Utils.getPriceInDollar(purchaseActions.first.price[perDay]) +
        " for 1 hour";
  }

  getRentPriceVisibility(List<PurchaseActions> purchaseActions) {
    if (purchaseActions.length == 2) return true;
    if (purchaseActions.first.name == Constants.ACTION_RENT) return true;
    return false;
  }

  String getPurchaseAction(List<PurchaseActions> purchaseActions) {
    if (purchaseActions.length == 2) return "Sell and Rent";
    if (purchaseActions.first.name == ACTION_SELL) {
      return "Sell Only";
    } else {
      return "Rent Only";
    }
  }
}
