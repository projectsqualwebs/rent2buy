import 'package:flutter/material.dart';
import 'package:rent2buy/di/service_locator.dart';
import 'package:rent2buy/models/cart.dart';
import 'package:rent2buy/models/cart_response.dart';
import 'package:rent2buy/models/common_repository.dart';
import 'package:rent2buy/models/product.dart';
import 'package:rent2buy/networking/network_response.dart';
import 'package:rent2buy/networking/repository/seller_repository.dart';
import 'package:rent2buy/utils/base_change_notifier.dart';
import 'package:rent2buy/utils/data_manager.dart';

class ProductDetailViewModel extends BaseChangeNotifier {
  ValueNotifier<NetworkResponse<Product>> productValue =
      ValueNotifier(NetworkResponse.idle());

  ValueNotifier<NetworkResponse<Cart>> cartItemsValue =
      ValueNotifier(NetworkResponse.idle());

  ValueNotifier<NetworkResponse<CartResponse>> cartValue =
      ValueNotifier(NetworkResponse.idle());

  ValueNotifier<NetworkResponse<String>> clearValue =
      ValueNotifier(NetworkResponse.idle());

  ValueNotifier<NetworkResponse<dynamic>> cartCountValue =
      ValueNotifier(NetworkResponse.idle());

  final SellerRepository _sellerRepository = serviceLocator<SellerRepository>();
  final CommonRepository _commonRepository = serviceLocator<CommonRepository>();
  final DataManager dataManager = serviceLocator<DataManager>();

  void getSingleProduct(String id) {
    productValue.value = NetworkResponse.loading();
    _sellerRepository.getSingleProduct(id).then((response) {
      productValue.value = NetworkResponse.success(response.response);
    }).catchError((e) {
      productValue.value = NetworkResponse.error(parseError(e));
    });
  }

  void addToCart(Map<String, dynamic> params, String cartId) {
    cartValue.value = NetworkResponse.loading();
    _sellerRepository.addToCart(params, cartId).then((response) {
      cartValue.value = NetworkResponse.successWithMsgAndStatus(
          response.response, response.message, response.status);
    }).catchError((e) {
      cartValue.value = NetworkResponse.error(parseError(e));
    });
  }

  void clearCart(String cartId) {
    clearValue.value = NetworkResponse.loading();
    _commonRepository.clearCart(cartId).then((response) {
      clearValue.value = NetworkResponse.success(response.message);
    }).catchError((e) {
      clearValue.value = NetworkResponse.error(parseError(e));
    });
  }

  void getCartItems(String cartId) {
    if (cartId.isEmpty) return;
    cartItemsValue.value = NetworkResponse.loading();
    _commonRepository.getCartItems(cartId).then((response) {
      cartItemsValue.value = NetworkResponse.success(response.response);
    }).catchError((e) {
      cartItemsValue.value = NetworkResponse.error(parseError(e));
    });
  }

  void getCartItemCount(String cartId) {
    if (cartId == null) return;
    cartCountValue.value = NetworkResponse.loading();
    _commonRepository.getCartItemsCount(cartId).then((response) {
      cartCountValue.value = NetworkResponse.success(response.response);
    }).catchError((e) {
      cartCountValue.value = NetworkResponse.error(parseError(e));
    });
  }

  @override
  void dispose() {
    cartItemsValue.dispose();
    clearValue.dispose();
    cartValue.dispose();
    productValue.dispose();
    super.dispose();
  }

  Future<bool> isGuestUser() {
    return dataManager.isGuestUser();
  }
}
