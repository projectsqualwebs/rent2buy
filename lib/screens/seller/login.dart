import 'package:flutter/gestures.dart';
import 'package:flutter/material.dart';
import 'package:font_awesome_flutter/font_awesome_flutter.dart';
import 'package:rent2buy/models/User/userLogin.dart';
import 'package:rent2buy/services/Seller/auth.dart';
import 'package:rent2buy/widgets/shared_widgets.dart';
import '../../theme.dart';

class SellerLogin extends StatefulWidget {
  @override
  _SellerLoginState createState() => _SellerLoginState();
}

class _SellerLoginState extends State<SellerLogin> {
  UserLogin userLogin = UserLogin();
  final _key = GlobalKey<FormState>();

  @override
  Widget build(BuildContext context) {
    return Theme(
      data: sellerTheme,
      child: Scaffold(
        body: SingleChildScrollView(
          child: Container(
            height: MediaQuery.of(context).size.height,
            child: Column(
              children: [
                Container(
                  height: 200.0,
                  color: Color(0xFFE8F5FA),
                  child: Center(
                    child: Padding(
                      padding: const EdgeInsets.fromLTRB(24.0,48,24,0),
                      child: Image.asset('assets/Rent2S.png'),
                    ),
                  ),
                ),
                SizedBox(height: 20),
                Text(
                  "Welcome Back!",
                  style: TextStyle(fontSize: 40, fontWeight: FontWeight.w800),
                ),
                SizedBox(height: 30),
                Form(
                  key: _key,
                  child: Padding(
                    padding: const EdgeInsets.only(left: 20.0, right: 20),
                    child: Column(
                      children: [
                        AuthTextField(
                          hinttext: 'Email Address',
                          prefixIcon: Icon(Icons.email_outlined),
                          isPassword: false,
                          color: Color(0xFF71C7E3),
                          isNumber: false,
                          validator: (String value) {
                            Pattern pattern =  r'^(([^<>()[\]\\.,;:\s@\"]+(\.[^<>()[\]\\.,;:\s@\"]+)*)|(\".+\"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$';
                            RegExp regexp = RegExp(pattern);
                            if (value.isEmpty) {
                              return 'Please enter email';
                            }else if(!regexp.hasMatch(value)){
                              return 'Please enter valid Email';
                            }
                            return null;
                          },
                          onSaved: (String value) {
                            userLogin.email = value;
                          },
                        ),
                        SizedBox(height: 10),
                        AuthTextField(
                          hinttext: 'Password',
                          prefixIcon: Icon(Icons.lock_outline),
                          isPassword: true,
                          color: Color(0xFF71C7E3),
                          isNumber: false,
                            validator: (String value) {
                              if (value.isEmpty) {
                                return 'Please enter password';
                              } else if (value.length < 7) {
                                return 'Password should be greater than 8';
                              }
                              return null;
                            },
                            onSaved: (String value) {
                              userLogin.password = value;
                            }
                        )
                      ],
                    ),
                  ),
                ),
                Padding(
                  padding: const EdgeInsets.only(right: 20),
                  child: Row(
                    mainAxisAlignment: MainAxisAlignment.end,
                    children: [
                      FlatButton(
                          onPressed: () {
                            Navigator.pushNamed(context, "/seller_forget_pwd");
                          },
                          child: Text(
                            'Forget Password?',
                            style: TextStyle(color: Colors.grey[500]),
                          )),
                    ],
                  ),
                ),
                MyButton(
                    text: "Submit",
                    color: Color(0xFF003585),
                    onPressed: () {
                      if (_key.currentState.validate()) {
                        _key.currentState.save();
                        //logIn(_email, _password, '/seller_home', context);
                        postSellerLogin(userLogin,context);
                      }
                    }),
                SizedBox(height: 20),
                Text('OR', style: TextStyle(fontWeight: FontWeight.bold)),
                SizedBox(height: 20),
                Row(
                  mainAxisAlignment: MainAxisAlignment.center,
                  children: [
                    RawMaterialButton(
                        child: FaIcon(FontAwesomeIcons.facebookF,
                            size: 24, color: Colors.white),
                        fillColor: Colors.blue[900],
                        shape: CircleBorder(),
                        onPressed: () {
                          initiateFacebookLoginSeller(context);
                        }),
                    SizedBox(width: 20),
                    RawMaterialButton(
                        child: FaIcon(FontAwesomeIcons.googlePlusG,
                            size: 24, color: Colors.white),
                        fillColor: Colors.red,
                        shape: CircleBorder(),
                        onPressed: () {
                          signInSellerWithGoogle();
                        }),
                  ],
                ),
                SizedBox(height: 20),
                MyButton(
                  text: 'Guest Login',
                  color: Color(0xFF003585),
                  onPressed: () {
                    Navigator.pushNamed(context, '/seller_home');
                  },
                ),
                Spacer(),
                Row(
                  children: [
                    Expanded(
                      child: Divider(
                        color: Color(0xFF71C7E3),
                        height: 5.0,
                        thickness: 5,
                      ),
                    ),
                  ],
                ),
                SizedBox(height: 10),
                Align(
                  alignment: Alignment.bottomCenter,
                  child: Row(
                    mainAxisAlignment: MainAxisAlignment.center,
                    children: [
                      Padding(
                        padding: const EdgeInsets.only(bottom: 20.0),
                        child: RichText(
                          text: TextSpan(
                            children: <TextSpan>[
                              TextSpan(
                                  text: 'Not a Member Yet? ',
                                  style: TextStyle(
                                      fontSize: 18,
                                      fontFamily: 'Dosis',
                                      color: Colors.grey,
                                      fontWeight: FontWeight.bold)),
                              TextSpan(
                                  text: 'Register with us',
                                  style: TextStyle(
                                      fontFamily: 'Dosis',
                                      fontSize: 18,
                                      color: Color(0xFF71C7E3),
                                      fontWeight: FontWeight.bold),
                                  recognizer: TapGestureRecognizer()
                                    ..onTap = () {
                                      Navigator.pushNamed(context, '/seller_signup');
                                    }),
                            ],
                          ),
                        ),
                      ),
                    ],
                  ),
                ),
              ],
            ),
          ),
        ),
      ),

    );
  }
}
