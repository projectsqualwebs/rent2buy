import 'package:flutter/material.dart';
import 'package:rent2buy/di/service_locator.dart';
import 'package:rent2buy/screens/seller/order/seller_order_viewmodel.dart';
import 'package:rent2buy/screens/seller/order_accepted.dart';
import 'package:rent2buy/screens/seller/order_pending.dart';
import 'package:rent2buy/screens/seller/order_rent.dart';
import 'package:rent2buy/screens/seller/order_sold.dart';
import 'package:rent2buy/theme.dart';
import 'package:rent2buy/utils/constants.dart';
import 'package:rent2buy/widgets/shared_widgets.dart';

class SellerOrder extends StatefulWidget {
  final SellerOrderViewModel viewModel = serviceLocator<SellerOrderViewModel>();
  @override
  _SellerOrderState createState() => _SellerOrderState();
}

class _SellerOrderState extends State<SellerOrder> {
  int _selectedIndex = 0;
  List<Widget> _children = [
    OrderPending(),
    OrderAccepted(),
    OrderSold(),
    OrderRent()
  ];

  Map<String, dynamic> params = Map();

  @override
  void initState() {
    // TODO: implement initState
    super.initState();
    params[status] = "placed";
    // getOrders();
  }

  void _onItemTapped(int index) {
    switch (index) {
      case 0:
        params[status] = "placed";
        break;
      case 1:
        params[status] = "shipped";
        break;
      case 2:
        params[status] = "delivered";
        break;
      case 3:
        params[status] = "rent";
        break;
    }
    setState(() {
      _selectedIndex = index;
    });
  }

  @override
  Widget build(BuildContext context) {
    int args = ModalRoute.of(context).settings.arguments as int;
    WidgetsBinding.instance.addPostFrameCallback((_) {
      if (args != null) {
        setState(() {
          _selectedIndex = args;
        });
      }
    });
    return Theme(
      data: sellerTheme,
      child: Scaffold(
        appBar: BaseAppBar(
          appBar: AppBar(),
          title: 'Order',
          isCentered: true,
          automaticallyImplyLeading: true,
          iconTheme: Color(0xFF003585),
          titleColor: Color(0xFF003585),
        ),
        body: _children[_selectedIndex],
        bottomNavigationBar: BottomNavigationBar(
            selectedLabelStyle: TextStyle(fontWeight: FontWeight.w600),
            type: BottomNavigationBarType.shifting,
            selectedItemColor: Color(0xFF003585),
            unselectedItemColor: Colors.grey,
            iconSize: 36,
            onTap: _onItemTapped,
            currentIndex: _selectedIndex,
            items: [
              BottomNavigationBarItem(
                backgroundColor: Color(0xFFE8F5FA),
                activeIcon: Image.asset('assets/pending.png',
                    height: 30, color: (Color(0xFF003585))),
                label: 'Pending',
                icon: Image.asset(
                  'assets/pending.png',
                  height: 30,
                  color: Colors.grey,
                ),
              ),
              BottomNavigationBarItem(
                  backgroundColor: Color(0xFFE8F5FA),
                  label: 'Accepted',
                  icon: Image.asset('assets/accepted.png',
                      height: 30, color: Colors.grey),
                  activeIcon: Image.asset(
                    'assets/accepted.png',
                    height: 30,
                    color: Color(0xFF003585),
                  )),
              BottomNavigationBarItem(
                  backgroundColor: Color(0xFFE8F5FA),
                  label: 'Sold',
                  icon: Image.asset('assets/sold.png', height: 30),
                  activeIcon: Image.asset(
                    'assets/sold.png',
                    height: 30,
                    color: Color(0xFF003585),
                  )),
              BottomNavigationBarItem(
                  backgroundColor: Color(0xFFE8F5FA),
                  label: 'Rent',
                  icon: Image.asset('assets/rent.png', height: 30),
                  activeIcon: Image.asset(
                    'assets/rent.png',
                    height: 30,
                    color: Color(0xFF003585),
                  )),
            ]),
      ),
    );
  }

  void getOrders() {
    widget.viewModel.getOrders(params);
  }
}
