import 'package:flutter/material.dart';
import 'package:rent2buy/di/service_locator.dart';
import 'package:rent2buy/models/common_repository.dart';
import 'package:rent2buy/models/order.dart';
import 'package:rent2buy/models/thread_response.dart';
import 'package:rent2buy/networking/network_responselist.dart';
import 'package:rent2buy/networking/repository/message_repository.dart';
import 'package:rent2buy/networking/repository/seller_repository.dart';
import 'package:rent2buy/utils/base_change_notifier.dart';

class SellerOrderViewModel extends BaseChangeNotifier {
    final MessageRepository messageRepository =
      serviceLocator<MessageRepository>();
  final SellerRepository _sellerRepository = serviceLocator<SellerRepository>();

  ValueNotifier<NetworkResponseList<Order>> orderValue =
      ValueNotifier(NetworkResponseList.idle());
  ValueNotifier<NetworkResponseList<ThreadResponse>> threadsValue =
      ValueNotifier(NetworkResponseList.idle());

  void getOrders(Map<String, dynamic> params) {
    orderValue.value = NetworkResponseList.loading();
    _sellerRepository.getOrders(params).then((response) {
      orderValue.value = NetworkResponseList.success(response.response);
    }).catchError((e) {
      orderValue.value = NetworkResponseList.error(parseError(e));
    });
  }

  void getThreads(Map<String, dynamic> params) {
    threadsValue.value = NetworkResponseList.loading();
    messageRepository.getThreads(params).then((response) {
      threadsValue.value = NetworkResponseList.success(response.response);
    }).catchError((e) {
      threadsValue.value = NetworkResponseList.error(parseError(e));
    });
  }
}
