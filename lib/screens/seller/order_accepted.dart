import 'dart:convert';
import 'dart:io';
import 'package:http/http.dart' as http;
import 'package:flutter/material.dart';
import 'package:rent2buy/di/service_locator.dart';
import 'package:rent2buy/models/order.dart';
import 'package:rent2buy/models/order_item.dart';
import 'package:rent2buy/models/product_model.dart';
import 'package:rent2buy/models/thread_response.dart';
import 'package:rent2buy/networking/network_responselist.dart';
import 'package:rent2buy/networking/network_status.dart';
import 'package:rent2buy/screens/common/chat/seller_chat_screen.dart';
import 'package:rent2buy/screens/common/error_screen.dart';
import 'package:rent2buy/screens/common/progress_view.dart';
import 'package:rent2buy/screens/seller/order/seller_order_viewmodel.dart';
import 'package:rent2buy/services/apiProvider.dart';
import 'package:rent2buy/utils/constants.dart';
import 'package:rent2buy/utils/strings.dart';
import 'package:rent2buy/utils/utils.dart';
import 'package:rent2buy/widgets/shared_widgets.dart';
import 'package:shared_preferences/shared_preferences.dart';

class OrderAccepted extends StatefulWidget {
  final SellerOrderViewModel viewModel = serviceLocator<SellerOrderViewModel>();
  @override
  _OrderAcceptedState createState() => _OrderAcceptedState();
}

class _OrderAcceptedState extends State<OrderAccepted>
    with WidgetsBindingObserver {
  List<OrderItem> orderItems = List();
  final _keyList = GlobalKey<AnimatedListState>();
  Map<String, dynamic> params = Map();
  List<ThreadResponse> threads = List();

  @override
  void didChangeAppLifecycleState(AppLifecycleState state) {
    // TODO: implement didChangeAppLifecycleState
    super.didChangeAppLifecycleState(state);
    if (state == AppLifecycleState.resumed) {
      getOrders();
    }
  }

  @override
  void initState() {
    // TODO: implement initState
    super.initState();
    Map<String, dynamic> map = Map();
    widget.viewModel.getThreads(map);
    WidgetsBinding.instance.addPostFrameCallback((_) {
      widget.viewModel.threadsValue.addListener(() {
        var response = widget.viewModel.threadsValue.value;
        if (response.status == NetworkStatus.SUCCESS) {
          setState(() {
            threads.clear();
            threads.addAll(response.response);
          });
        }
      });
    });
    params[status] = "accepted";
    getOrders();
  }

  void getOrders() {
    widget.viewModel.getOrders(params);
  }

  @override
  Widget build(BuildContext context) {
    return ValueListenableBuilder(
        valueListenable: widget.viewModel.orderValue,
        builder: (context, value, _) {
          var response = value as NetworkResponseList<Order>;
          switch (response.status) {
            case NetworkStatus.LOADING:
              return CircularProgress();
            case NetworkStatus.ERROR:
              return ErrorScreen(response.message);
            case NetworkStatus.SUCCESS:
              addItems(response.response);
              return AnimatedCrossFade(
                duration: Duration(milliseconds: 500),
                crossFadeState: response.response.isEmpty
                    ? CrossFadeState.showSecond
                    : CrossFadeState.showFirst,
                firstChild: AnimatedList(
                    key: _keyList,
                    scrollDirection: Axis.vertical,
                    shrinkWrap: true,
                    initialItemCount: orderItems.length,
                    itemBuilder:
                        (BuildContext context, int index, Animation animation) {
                      return _buildItem(index, orderItems[index]);
                    }),
                secondChild: Utils.getEmptyView(Strings.msgNoOrdersFound),
              );
          }
          return Utils.getEmptyView(Strings.msgNoOrdersFound);
        });
  }

  void addItems(List<Order> response) {
    WidgetsBinding.instance.addPostFrameCallback((_) {
      orderItems.clear();
      for (Order order in response) {
        for (OrderItem orderItem in order?.orderItems) {
          orderItem.date = Utils.getDate(order.createdAt);
          orderItem.user = order.user;
        }
        orderItems.addAll(order?.orderItems);
      }

      for (int i = 0; i < orderItems.length; i++) {
        _keyList.currentState.insertItem(i);
      }
    });
  }

  Widget _buildItem(int index, OrderItem orderItem) {
    try {
      return Column(
        children: [
          Padding(
            padding: const EdgeInsets.all(12.0),
            child: ListTile(
              leading: Image.network(
                orderItem.orderProduct.images.path.isNotEmpty
                    ? orderItem.orderProduct.images.path[0]
                    : "",
                fit: BoxFit.fitHeight,
                width: 100,
                height: 100,
              ),
              title: Text(
                orderItem.orderProduct.name,
                style: TextStyle(fontSize: 24),
              ),
              subtitle: Padding(
                padding: const EdgeInsets.only(top: 8.0),
                child: Text(
                  orderItem.date,
                  style: TextStyle(fontSize: 16),
                ),
              ),
              trailing: Column(
                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                children: [
                  Text(
                    Utils.getPriceInDollar(orderItem.totalPrice),
                    style: TextStyle(color: Color(0xFF97080E), fontSize: 24),
                  ),
                  Text(
                    orderItem.orderProduct.purchaseActions.first.name,
                    style: TextStyle(color: Colors.green[600], fontSize: 16),
                  )
                ],
              ),
              onTap: () {
                ThreadResponse threadId = threads.firstWhere((x) {
                  return x.user.sId == orderItem.user.sId;
                }, orElse: () => null);

                // Navigator.pushNamed(context, '/seller_product_detail');
                Navigator.push(
                  context,
                  MaterialPageRoute(
                    builder: (context) => SellerChatScreen(
                      chatId: threadId?.id,
                      receiverName: orderItem.user.firstName,
                      receiverUserId: orderItem.user.sId,
                      orderItemId: orderItem.id,
                      listingId: orderItem.orderProduct.id,
                    ),
                  ),
                );
              },
            ),
          ),
          CustomDivider()
        ],
      );
    } catch (e) {}
  }
}
