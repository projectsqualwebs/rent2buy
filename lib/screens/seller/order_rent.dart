import 'dart:convert';
import 'dart:io';

import 'package:flutter/material.dart';
import 'package:http/http.dart' as http;
import 'package:rent2buy/services/apiProvider.dart';
import 'package:rent2buy/widgets/shared_widgets.dart';
import 'package:shared_preferences/shared_preferences.dart';
class OrderRent extends StatefulWidget {

  @override
  _OrderRentState createState() => _OrderRentState();
}

class _OrderRentState extends State<OrderRent> {

  List<dynamic> listing = List();

  getOrders()async{
    SharedPreferences prefs = await SharedPreferences.getInstance();
    String token = prefs.getString('token');

    http.Response response = await http.post('${ApiProvider.sellerOrders}',
        headers: {HttpHeaders.authorizationHeader: 'Bearer $token'},
        body: {
          "status":"rent"
        }
    );
    print(response.body);
    setState(() {
      listing = jsonDecode(response.body)["response"];
    });
  }

  @override
  void initState() {
    getOrders();
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return listing.length == null
        ? Text('No Items Yet')
        : ListView.builder(
        itemCount: listing.length,
        itemBuilder: (BuildContext context, int index) {
          return Column(
            children: [
              Padding(
                padding: const EdgeInsets.all(12.0),
                child: ListTile(
                  leading: Image.network(
                    listing[index]['images']['path'][0],
                    fit: BoxFit.fitHeight,
                    width: 100,
                    height: 100,
                  ),
                  title: Text(
                    listing[index]['name'],
                    style: TextStyle(fontSize: 24),
                  ),
                  subtitle: Padding(
                    padding: const EdgeInsets.only(top: 8.0),
                    child: Text(
                      listing[index]['created_at'].toString().substring(0,10),
                      style: TextStyle(fontSize: 16),
                    ),
                  ),
                  trailing: Column(
                    mainAxisAlignment: MainAxisAlignment.spaceBetween,
                    children: [
                      Text(
                        '\u0024' + '300',
                        style: TextStyle(
                            color: Color(0xFF97080E), fontSize: 24),
                      ),
                      Text(
                        listing[index]['purchase_actions'][0]['name'],
                        style: TextStyle(
                            color: Colors.green[600], fontSize: 16),
                      )
                    ],
                  ),
                  onTap: () {
                    Navigator.pushNamed(context, '/seller_product_detail');
                  },
                ),
              ),
              CustomDivider()
            ],
          );
        });
  }
}
