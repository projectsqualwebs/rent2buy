import 'dart:convert';
import 'dart:io';

import 'package:flutter/material.dart';
import 'package:rent2buy/models/product_model.dart';
import 'package:rent2buy/services/apiProvider.dart';
import 'package:rent2buy/widgets/shared_widgets.dart';
import 'package:shared_preferences/shared_preferences.dart';
import 'package:http/http.dart' as http;
class OrderSold extends StatefulWidget {
  @override
  _OrderSoldState createState() => _OrderSoldState();
}

class _OrderSoldState extends State<OrderSold> {
  List<dynamic> listing = List();
  List<ProductModel> _orderSold = [
    ProductModel("Laptop", 'Description', 'https://images-na.ssl-images-amazon.com/images/I/71h6PpGaz9L._AC_SL1500_.jpg', '12/11/2020', 30.0, 'Sell', 'TV',10.0 , 'Buy Only','Avaliable'),
  ];

  getOrders()async{
    SharedPreferences prefs = await SharedPreferences.getInstance();
    String token = prefs.getString('token');
    http.Response response = await http.post('${ApiProvider.sellerOrders}',
        headers: {HttpHeaders.authorizationHeader: 'Bearer $token'},
        body: {
          "status":"sell"
        }
    );
    print(response.body);
    setState(() {
      listing = jsonDecode(response.body)["response"];
    });
  }

  @override
  Widget build(BuildContext context) {
    return listing.length == null
        ? Text('No Items Yet')
        : ListView.builder(
        itemCount: listing.length,
        itemBuilder: (BuildContext context, int index) {
          return Column(
            children: [
              Padding(
                padding: const EdgeInsets.all(12.0),
                child: ListTile(
                  leading: Image.network(
                    listing[index]['images']['path'][0],
                    fit: BoxFit.fitHeight,
                    width: 100,
                    height: 100,
                  ),
                  title: Text(
                    listing[index]['name'],
                    style: TextStyle(fontSize: 24),
                  ),
                  subtitle: Padding(
                    padding: const EdgeInsets.only(top: 8.0),
                    child: Text(
                      listing[index]['created_at'].toString().substring(0,10),
                      style: TextStyle(fontSize: 16),
                    ),
                  ),
                  trailing: Column(
                    mainAxisAlignment: MainAxisAlignment.spaceBetween,
                    children: [
                      Text(
                        '\u0024' + '300',
                        style: TextStyle(
                            color: Color(0xFF97080E), fontSize: 24),
                      ),
                      Text(
                        listing[index]['purchase_actions'][0]['name'],
                        style: TextStyle(
                            color: Colors.green[600], fontSize: 16),
                      )
                    ],
                  ),
                  onTap: () {
                    Navigator.pushNamed(context, '/seller_product_detail');
                  },
                ),
              ),
              CustomDivider()
            ],
          );
        });
  }
}
