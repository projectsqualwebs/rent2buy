import 'package:flutter/material.dart';
import 'package:rent2buy/models/Verification/verifyOtp.dart';
import 'package:rent2buy/services/Seller/verification.dart';
import 'package:rent2buy/theme.dart';
import 'package:rent2buy/widgets/shared_widgets.dart';

class SellerOTPVerify extends StatelessWidget {
  final String email;

  SellerOTPVerify({Key key, @required this.email});

  VerifyOtp verifyOtp = VerifyOtp();
  TextEditingController _otp = TextEditingController();

  @override
  Widget build(BuildContext context) {
    return Theme(
      data: sellerTheme,
      child: Scaffold(
        appBar: appBar(Color(0xFF003585)),
        body: Container(
          padding: EdgeInsets.symmetric(horizontal: 40),
          child: Column(
            children: [
              SizedBox(height: 60),
              CustomTextField(
                hinttext: 'Enter OTP',
                isPassword: false,
                textEditingController: _otp,
                borderColor: Color(0xFF003585),
                isNumber: true,
              ),
              SizedBox(height: 40),
              Center(
                child: MyButton(
                    text: 'Verify',
                    color: Color(0xFF003585),
                    onPressed: () {
                      verifyOtp.email = email;
                      verifyOtp.otp = int.parse(_otp.text);
                      postVerifyOtp(verifyOtp, context);
                    }),
              ),
            ],
          ),
        ),
      ),
    );
  }
}
