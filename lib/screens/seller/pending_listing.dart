import 'dart:convert';
import 'dart:io';

import 'package:flutter/material.dart';
import 'package:fluttertoast/fluttertoast.dart';
import 'package:rent2buy/di/service_locator.dart';
import 'package:rent2buy/models/product.dart';
import 'package:rent2buy/models/product_model.dart';
import 'package:rent2buy/models/purchase_actions.dart';
import 'package:rent2buy/networking/network_responselist.dart';
import 'package:rent2buy/networking/network_status.dart';
import 'package:rent2buy/screens/common/progress_view.dart';
import 'package:rent2buy/screens/seller/listing/listing_viewmodel.dart';
import 'package:rent2buy/services/apiProvider.dart';
import 'package:rent2buy/utils/constants.dart';
import 'package:rent2buy/utils/strings.dart';
import 'package:rent2buy/utils/time_utils.dart';
import 'package:rent2buy/widgets/shared_widgets.dart';
import 'package:shared_preferences/shared_preferences.dart';
import 'package:http/http.dart' as http;

class PendingListing extends StatefulWidget {
  final ListingViewModel viewModel = serviceLocator<ListingViewModel>();

  @override
  _PendingListingState createState() => _PendingListingState();
}

class _PendingListingState extends State<PendingListing> {
  List<Product> listings = List();

  @override
  void initState() {
    WidgetsBinding.instance.addPostFrameCallback((_) {
      Map<String, dynamic> params = Map();
      params[status] = 0;
      widget.viewModel.getListing(params);
    });
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return ValueListenableBuilder(
      valueListenable: widget.viewModel.listingValue,
      builder: (context, value, _) {
        var response = value as NetworkResponseList<Product>;
        switch (response.status) {
          case NetworkStatus.LOADING:
            return CircularProgress();
          case NetworkStatus.ERROR:
            Fluttertoast.showToast(msg: response.message);
            break;
          case NetworkStatus.SUCCESS:
            WidgetsBinding.instance.addPostFrameCallback((_) {
              setState(() {
                listings.clear();
                listings.addAll(response.response);
              });
            });
            return Container(
              child: Column(
                children: [
                  Row(
                    mainAxisAlignment: MainAxisAlignment.spaceAround,
                    children: [
                      FlatButton(
                          onPressed: () {},
                          child: Text('Approve All',
                              style: TextStyle(fontSize: 20))),
                      FlatButton(
                          onPressed: () {},
                          child: Text(
                            'Refuse All',
                            style: TextStyle(fontSize: 20),
                          )),
                    ],
                  ),
                  Row(
                    mainAxisAlignment: MainAxisAlignment.spaceAround,
                    children: [
                      Text('WT Traders',
                          style: TextStyle(
                              color: Color(0xFF003585), fontSize: 24)),
                      FlatButton.icon(
                          onPressed: () {},
                          icon: Icon(Icons.notifications_none,
                              color: Color(0xFF003585)),
                          label: Text('Notify Seller',
                              style: TextStyle(
                                  color: Color(0xFF003585), fontSize: 20))),
                    ],
                  ),
                  Expanded(
                    child: AnimatedCrossFade(
                      duration: Duration(milliseconds: 500),
                      crossFadeState: listings.isEmpty
                          ? CrossFadeState.showSecond
                          : CrossFadeState.showFirst,
                      firstChild: ListView.builder(
                          itemCount: listings.length,
                          shrinkWrap: true,
                          scrollDirection: Axis.vertical,
                          itemBuilder: (BuildContext context, int index) {
                            return _buildItem(index, listings[index]);
                          }),
                      secondChild: Center(
                        child: Text(Strings.msgNoListingFound),
                      ),
                    ),
                  )
                ],
              ),
            );
        }
        return Container(
          child: Text(Strings.msgNoListingFound),
        );
      },
    );
  }

  Widget _buildItem(int index, Product product) {
    try {
      return Column(
        children: [
          Padding(
            padding: const EdgeInsets.all(12.0),
            child: ListTile(
              leading: Image.network(
                product.images.path[0],
                fit: BoxFit.fitHeight,
                width: 100,
                height: 100,
              ),
              title: Text(
                product.name,
                style: TextStyle(fontSize: 24),
              ),
              subtitle: Padding(
                padding: const EdgeInsets.only(top: 8.0),
                child: Text(
                  TimeUtils.getMMDDYYY(product.createdAt),
                  style: TextStyle(fontSize: 16),
                ),
              ),
              trailing: Text(
                getPurchaseAction(product.purchaseActions),
                style: TextStyle(color: Colors.green[600], fontSize: 20),
              ),
              onTap: () {},
            ),
          ),
          CustomDivider()
        ],
      );
    } catch (e) {
      debugPrint(e);
    }
  }

  String getPurchaseAction(List<PurchaseActions> purchaseActions) {
    if (purchaseActions.length == 2) {
      return Strings.sellAndRent;
    } else {
      if (purchaseActions.first.name == ACTION_SELL) {
        return Strings.sellOnly;
      }
      if (purchaseActions.first.name == ACTION_RENT) {
        return Strings.rentOnly;
      }
    }
  }
}
