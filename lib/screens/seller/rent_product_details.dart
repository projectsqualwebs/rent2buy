import 'package:carousel_pro/carousel_pro.dart';
import 'package:flutter/material.dart';
import 'package:rent2buy/widgets/shared_widgets.dart';

import '../../data.dart';
import '../../theme.dart';

class SellerRentProductDetails extends StatefulWidget {
  @override
  _SellerRentProductDetailsState createState() => _SellerRentProductDetailsState();
}

class _SellerRentProductDetailsState extends State<SellerRentProductDetails> {
  int index = 0;
  @override
  Widget build(BuildContext context) {
    return Theme(
      data: sellerTheme,
      child: Scaffold(
        appBar: BaseAppBar(
          title: '',
          iconTheme: Color(0xFF003585),
          automaticallyImplyLeading: true,
          widgets: [
            IconButton(
                icon: Image.asset(
                  'assets/dispute.png',
                  height: 32,
                  color: Color(0xFF003585),
                ),
                onPressed: () {
                  Navigator.pushNamed(context, '/seller_dispute');
                }
            ),
            IconButton(icon: Icon(Icons.edit), onPressed: (){})
          ],
          appBar: AppBar(),
        ),
        body: Column(
          mainAxisAlignment: MainAxisAlignment.start,
          crossAxisAlignment: CrossAxisAlignment.start,
          children: [
            SizedBox(
              width: MediaQuery.of(context).size.width,
              height: 250,
              child: Carousel(
                autoplay: false,
                images: [
                  NetworkImage(
                    "https://images-na.ssl-images-amazon.com/images/I/71h6PpGaz9L._AC_SL1500_.jpg",
                  ),
                  NetworkImage(
                    "https://images-na.ssl-images-amazon.com/images/I/71h6PpGaz9L._AC_SL1500_.jpg",
                  ),
                ],
                dotSize: 4.0,
                dotBgColor: Colors.transparent,
                dotSpacing: 12.0,
                dotColor: Colors.transparent,
              ),
            ),
            Container(
              padding: EdgeInsets.all(16),
              child: Row(
                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                children: [
                  Column(crossAxisAlignment:CrossAxisAlignment.start,
                    children: [
                      Text('\u0024' + disputeList[index].price.toString(),
                          style: TextStyle(color: Color(0xFF97080E), fontSize: 24)),
                      Text('for 1 day',
                          style: TextStyle(color: Color(0xFF97080E), fontSize: 24)),
                    ],
                  ),
                  Text('Rent Only',
                      style:
                      TextStyle(color: Color(0xFF97080E), fontSize: 24))
                ],
              ),
            ),
            Container(
              padding: EdgeInsets.only(left: 12, right: 12),
              child: Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                children: [
                  Text(disputeList[index].avaliability+' : In Stock',
                      style: TextStyle(color: Colors.blue[900], fontSize: 22)),
                  SizedBox(height:10),
                  Text(
                    disputeList[index].name,
                    style: TextStyle(fontSize: 20),
                  ),
                  SizedBox(height: 10),
                  Text(
                    'Introduce the author, the historical period and topic of the book. Tell the reader what genre of History this work belongs to or what approach the author has used.',
                    style: TextStyle(fontSize: 20),
                  ),
                ],
              ),
            ),
          ],
        ),

      ),
    );
  }
}
