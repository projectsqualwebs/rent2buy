import 'dart:io';

import 'package:flutter/material.dart';
import 'package:rent2buy/data.dart';
import 'package:rent2buy/theme.dart';
import 'package:rent2buy/widgets/shared_widgets.dart';
import 'package:pdf/pdf.dart';
import 'package:pdf/widgets.dart' as pw;

class SellingReport extends StatefulWidget {
  @override
  _SellingReportState createState() => _SellingReportState();
}

class _SellingReportState extends State<SellingReport> {
  @override
  Widget build(BuildContext context) {
    return Theme(
      data: sellerTheme,
      child: Scaffold(
        appBar: BaseAppBar(
          appBar: AppBar(),
          title: 'Selling Report',
          isCentered: true,
          automaticallyImplyLeading: true,
          iconTheme: Color(0xFF003585),
          titleColor: Color(0xFF003585),
        ),
        body: ListView.builder(
            itemCount: disputeList.length,
            itemBuilder: (BuildContext context, int index) {
              return Column(
                children: [
                  Padding(
                    padding: const EdgeInsets.all(12.0),
                    child: ListTile(
                      leading: Image.network(
                        disputeList[index].imageUrl,
                        fit: BoxFit.fitHeight,
                        width: 100,
                        height: 100,
                      ),
                      title: Text(
                        disputeList[index].name,
                        style: TextStyle(fontSize: 24),
                      ),
                      subtitle: Padding(
                        padding: const EdgeInsets.only(top: 8.0),
                        child: Text(
                          disputeList[index].date,
                          style: TextStyle(fontSize: 16),
                        ),
                      ),
                      trailing: Column(
                        mainAxisAlignment: MainAxisAlignment.spaceBetween,
                        children: [
                          Text(
                            '\u0024' + disputeList[index].price.toString(),
                            style: TextStyle(
                                color: Color(0xFF97080E), fontSize: 24),
                          ),
                          Text(
                            disputeList[index].status,
                            style: TextStyle(
                                color: Colors.green[600], fontSize: 16),
                          )
                        ],
                      ),
                      onTap: () {},
                    ),
                  ),
                  CustomDivider()
                ],
              );
            }),
        bottomNavigationBar: Padding(
            padding: const EdgeInsets.symmetric(horizontal: 100, vertical: 16),
            child: CustomIconButton(
              hinttext: 'Download PDF',
              onPressed: () {
                pdf();
              },
              textColor: Color(0xFF003585),
              icon: Icon(
                Icons.download_rounded,
                color: Color(0xFF003585),
              ),
            )),
      ),
    );
  }

  pdf() async {
    final pdf = pw.Document();
    pdf.addPage(pw.Page(
        pageFormat: PdfPageFormat.a4,
        build: (pw.Context context) {
          return pw.Center(
            child: pw.Text("Report"),
          ); // Center
        }));
    final file = File("selling_report.pdf");
    await file.writeAsBytes(await pdf.save());
  }
}
