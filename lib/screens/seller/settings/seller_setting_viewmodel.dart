import 'package:flutter/material.dart';
import 'package:rent2buy/di/service_locator.dart';
import 'package:rent2buy/networking/network_response.dart';
import 'package:rent2buy/networking/network_responselist.dart';
import 'package:rent2buy/networking/repository/account_repository.dart';
import 'package:rent2buy/utils/base_change_notifier.dart';

class SellerSettingViewModel extends BaseChangeNotifier{
     final AccountRepository _accountRepository =
        serviceLocator<AccountRepository>();

    ValueNotifier<NetworkResponse<String>> disableValue =
        ValueNotifier(NetworkResponse.idle());

    void disableSeller() {
      disableValue.value = NetworkResponse.loading();
      _accountRepository.disableSeller().then((response) {
        disableValue.value = NetworkResponse.successWithMsg(null,response.message);
      }).catchError((e) {
        disableValue.value = NetworkResponse.error(parseError(e));
      });
    }
}