import 'package:flutter/material.dart';
import 'package:fluttertoast/fluttertoast.dart';
import 'package:rent2buy/di/service_locator.dart';
import 'package:rent2buy/networking/network_status.dart';
import 'package:rent2buy/screens/common/loading_dialog.dart';
import 'package:rent2buy/screens/common/reset/reset_password.dart';
import 'package:rent2buy/screens/seller/settings/seller_setting_viewmodel.dart';
import 'package:rent2buy/screens/splashscreen.dart';
import 'package:rent2buy/theme.dart';
import 'package:rent2buy/utils/data_manager.dart';
import 'package:rent2buy/utils/design_utils.dart';
import 'package:rent2buy/utils/strings.dart';
import 'package:rent2buy/widgets/shared_widgets.dart';

class SellerSettings extends StatefulWidget {
  final DataManager dataManager = serviceLocator<DataManager>();
  final SellerSettingViewModel viewModel =
      serviceLocator<SellerSettingViewModel>();

  @override
  State<StatefulWidget> createState() {
    // TODO: implement createState
    return _SellerSettingState();
  }
}

class _SellerSettingState extends State<SellerSettings> {
  final _bigFont = const TextStyle(fontSize: 20, fontWeight: FontWeight.w700);
  final padding = const EdgeInsets.symmetric(horizontal: 50, vertical: 12);

  var _keyScafold = GlobalKey<ScaffoldState>();

  var _keyLoader = GlobalKey<State>();

  @override
  void initState() {
    // TODO: implement initState
    super.initState();
    WidgetsBinding.instance.addPostFrameCallback((_) {
      widget.viewModel.disableValue.addListener(() {
        var response = widget.viewModel.disableValue.value;
        switch (response.status) {
          case NetworkStatus.LOADING:
            Dialogs.showLoadingDialog(_keyScafold.currentContext, _keyLoader,
                Strings.msgDisableAccount);
            break;
          case NetworkStatus.ERROR:
            Navigator.pop(_keyLoader.currentContext);
            Fluttertoast.showToast(msg: response.message);
            break;
          case NetworkStatus.SUCCESS:
            Navigator.pop(_keyLoader.currentContext);
            Fluttertoast.showToast(msg: response.message);
            widget.dataManager.removeUser();
            Navigator.of(context).pushNamedAndRemoveUntil(
                SplashScreen.routeName, (Route<dynamic> route) => false);
            break;
        }
      });
    });
  }

  @override
  Widget build(BuildContext context) {
    return Theme(
      data: sellerTheme,
      child: Scaffold(
        key: _keyScafold,
        appBar: BaseAppBar(
          appBar: AppBar(),
          title: 'Seller Settings',
          isCentered: true,
          automaticallyImplyLeading: true,
          iconTheme: Color(0xFF003585),
          titleColor: Color(0xFF003585),
        ),
        body: ListView(
          children: [
            ListTile(
              contentPadding: padding,
              leading: Icon(
                Icons.cloud_upload_outlined,
                color: Colors.black,
                size: 32,
              ),
              title: Text(
                'Upload New Items',
                style: _bigFont,
              ),
            ),
            CustomDivider(),
            ListTile(
              contentPadding: padding,
              leading: Image.asset(
                'assets/analytics.png',
                height: 34,
              ),
              title: Text(
                'Obtain Report of sold Items',
                style: _bigFont,
              ),
              onTap: () {
                Navigator.pushNamed(context, '/seller_order', arguments: 2);
              },
            ),
            CustomDivider(),
            ListTile(
              contentPadding: padding,
              leading: Icon(
                Icons.account_balance_outlined,
                color: Colors.black,
                size: 32,
              ),
              title: Text(
                'Remove Bank Account',
                style: _bigFont,
              ),
              onTap: () {
                Navigator.pushNamed(context, '/bank_account');
              },
            ),
            CustomDivider(),
            ListTile(
              onTap: () {
                Navigator.pushNamed(context, ResetPasswordScreen.routeName);
              },
              contentPadding: padding,
              leading: Icon(
                Icons.lock_open_outlined,
                color: Colors.black,
                size: 32,
              ),
              title: Text(
                'Reset Password',
                style: _bigFont,
              ),
            ),
            CustomDivider(),
            ListTile(
              onTap: () {
                showDialog(
                    context: context,
                    builder: (context) {
                      return AlertDialog(
                        title: Text(
                          Strings.txtDeleteAccount,
                          style: DesignUtils.textApperanceTitle(),
                        ),
                        content: Text(
                          Strings.msgRemoveAccount,
                          style: DesignUtils.textApperanceRegular(),
                        ),
                        actions: [
                          new FlatButton(
                            child: Text(Strings.remove),
                            onPressed: () {
                              Navigator.of(context).pop();
                              widget.viewModel.disableSeller();
                            },
                          ),
                          new FlatButton(
                            child: Text(Strings.cancel),
                            onPressed: () {
                              Navigator.of(context).pop();
                            },
                          ),
                        ],
                      );
                    });
              },
              contentPadding: padding,
              leading: Icon(
                Icons.person_remove_alt_1_outlined,
                color: Colors.black,
                size: 32,
              ),
              title: Text(
                'Remove Account',
                style: _bigFont,
              ),
            ),
            CustomDivider(),
          ],
        ),
      ),
    );
  }
}
