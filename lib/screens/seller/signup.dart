import 'package:flutter/gestures.dart';
import 'package:flutter/material.dart';
import 'package:flutter_datetime_picker/flutter_datetime_picker.dart';
import 'package:font_awesome_flutter/font_awesome_flutter.dart';
import 'package:geolocator/geolocator.dart';
import 'package:rent2buy/models/Seller/companyModel.dart';
import 'package:rent2buy/models/Seller/sellerRegister.dart';
import 'package:rent2buy/models/User/address.dart';
import 'package:rent2buy/models/User/userAddress.dart';
import 'package:rent2buy/services/Seller/auth.dart';
import 'package:rent2buy/theme.dart';
import 'package:rent2buy/utils/strings.dart';
import 'package:rent2buy/widgets/shared_widgets.dart';

class SellerSignup extends StatefulWidget {
  @override
  _SellerSignupState createState() => _SellerSignupState();
}

class _SellerSignupState extends State<SellerSignup> {
  final _key = GlobalKey<FormState>();
  String _date = "  " + "Select DOB";
  String dob;
  double lat;
  double long;
  String isCompany = '1';
  bool _value = false;
  SellerRegister sellerRegister = SellerRegister();
  bool _isCompany = false;
  UserAddress address = UserAddress();
  Company companyModel = Company();
  void _isCompanyChanges(bool value) {
    setState(() {
      _isCompany = value;
    });
  }

  @override
  Widget build(BuildContext context) {
    return Theme(
      data: sellerTheme,
      child: Scaffold(
        appBar: appBar(Colors.blue[900]),
        body: Builder(builder: (context)=>  SingleChildScrollView(
            child: Container(
              child: Column(
                children: [
                  SizedBox(height: 20),
                  Text(
                    "Register With Us!",
                    style: TextStyle(fontSize: 40, fontWeight: FontWeight.w800),
                  ),
                  SizedBox(height: 20),
                  Form(
                    key: _key,
                    child: Padding(
                      padding: EdgeInsets.only(left: 20, right: 20),
                      child: Column(
                        children: [
                          AuthTextField(
                            hinttext: 'First Name',
                            isNumber: false,
                            prefixIcon: Icon(Icons.person_outline),
                            color: Color(0xFF71C7E3),
                            isPassword: false,
                            validator: (String value) {
                              if (value.isEmpty) {
                                return 'Please enter Full Name';
                              }
                              return null;
                            },
                            onSaved: (String value) {
                              sellerRegister.first_name = value;
                            },
                          ),
                          SizedBox(height: 10),
                          AuthTextField(
                            hinttext: 'Last Name',
                            isNumber: false,
                            prefixIcon: Icon(Icons.person_outline),
                            color: Color(0xFF71C7E3),
                            isPassword: false,
                            validator: (String value) {
                              if (value.isEmpty) {
                                return 'Please enter Full Name';
                              }
                              return null;
                            },
                            onSaved: (String value) {
                              sellerRegister.last_name = value;
                            },
                          ),
                          SizedBox(height: 10),
                          AuthTextField(
                            hinttext: 'Email Address',
                            isNumber: false,
                            prefixIcon: Icon(Icons.email_outlined),
                            color: Color(0xFF71C7E3),
                            isPassword: false,
                            validator: (String value) {
                              Pattern pattern =
                                  r'^(([^<>()[\]\\.,;:\s@\"]+(\.[^<>()[\]\\.,;:\s@\"]+)*)|(\".+\"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$';
                              RegExp regexp = RegExp(pattern);
                              if (value.isEmpty) {
                                return 'Please enter Email';
                              } else if (!regexp.hasMatch(value)) {
                                return 'Please enter valid Email';
                              }
                              return null;
                            },
                            onSaved: (String value) {
                              sellerRegister.email = value;
                            },
                          ),
                          SizedBox(height: 10),
                          AuthTextField(
                            hinttext: 'Phone Number',
                            prefixIcon: Icon(Icons.smartphone),
                            color: Color(0xFF71C7E3),
                            isPassword: false,
                            isNumber: true,
                            validator: (String value) {
                              if (value.isEmpty) {
                                return 'Please enter Phone Number';
                              } else if (value.length < 9) {
                                return 'Phone number should be 10 digit';
                              }
                              return null;
                            },
                            onSaved: (String value) {
                              sellerRegister.phone = value;
                            },
                          ),
                          SizedBox(height: 10),
                          AuthTextField(
                            hinttext: 'Password',
                            isNumber: false,
                            prefixIcon: Icon(Icons.lock_outline),
                            color: Color(0xFF71C7E3),
                            isPassword: true,
                            validator: (String value) {
                              if (value.isEmpty) {
                                return 'Please enter Password';
                              } else if (value.length < 7) {
                                return 'Password should be greater than 8 digit';
                              }
                              _key.currentState.save();
                              return null;
                            },
                            onSaved: (String value) {
                              sellerRegister.password = value;
                            },
                          ),
                          SizedBox(height: 10),
                          AuthTextField(
                            hinttext: 'Confirm Password',
                            isNumber: false,
                            prefixIcon: Icon(Icons.lock_outline),
                            color: Color(0xFF71C7E3),
                            isPassword: true,
                            validator: (String value) {
                              if (value.isEmpty) {
                                return 'Please enter Password';
                              } else if (value.length < 7) {
                                return 'Password should be greater than 8 digit';
                              } else if (value != sellerRegister.password) {
                                return 'Password not Matched!';
                              }
                              return null;
                            },
                            onSaved: (String value) {
                              sellerRegister.confirm_password = value;
                            },
                          ),
                          SizedBox(height: 10),
                          RaisedButton(
                            shape: RoundedRectangleBorder(
                              borderRadius: BorderRadius.circular(5.0),
                            ),
                            elevation: 0.0,
                            onPressed: () {
                              DatePicker.showDatePicker(context,
                                  theme: DatePickerTheme(
                                    containerHeight: 210.0,
                                  ),
                                  showTitleActions: true,
                                  minTime: DateTime(1965, 1, 1),
                                  maxTime: DateTime(2015, 12, 31),
                                  onConfirm: (date) {
                                _date = "  " +
                                    '${date.year} - ${date.month} - ${date.day}';
                                dob = '${date.year}-${date.month}-${date.day}';
                                setState(() {});

                              },
                                  currentTime: DateTime.now(),
                                  locale: LocaleType.en);
                            },
                            child: Container(
                              alignment: Alignment.center,
                              height: 50.0,
                              child: Row(
                                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                                children: <Widget>[
                                  Row(
                                    children: <Widget>[
                                      Container(
                                        child: Row(
                                          children: <Widget>[
                                            Icon(
                                              Icons.date_range,
                                              size: 24.0,
                                              color: Color(0xFF71C7E3),
                                            ),
                                            Text(
                                              " $_date",
                                              style: TextStyle(
                                                  color: Colors.grey[700],
                                                  fontWeight: FontWeight.bold,
                                                  fontSize: 18.0),
                                            ),
                                          ],
                                        ),
                                      )
                                    ],
                                  ),
                                ],
                              ),
                            ),
                            color: Color(0xFFE8F5FA),
                          ),
                          SizedBox(height: 10),
                          Row(
                            children: [
                              SizedBox(width: 15),
                              Icon(
                                Icons.business_rounded,
                                color: Colors.grey[500],
                              ),
                              SizedBox(
                                width: 15,
                              ),
                              Text(
                                'Are You a Company?',
                                style: TextStyle(
                                    color: Colors.grey[700], fontSize: 16),
                              ),
                            ],
                          ),
                          SizedBox(height: 10),
                          Row(
                            mainAxisAlignment: MainAxisAlignment.start,
                            children: [
                              SizedBox(
                                width: 120,
                                child: ListTile(
                                  title: Text('Yes'),
                                  leading: Radio(
                                      value: true,
                                      groupValue: _isCompany,
                                      onChanged: _isCompanyChanges),
                                ),
                              ),
                              SizedBox(
                                width: 120,
                                child: ListTile(
                                  title: Text('No'),
                                  leading: Radio(
                                      value: false,
                                      groupValue: _isCompany,
                                      onChanged: _isCompanyChanges),
                                ),
                              ),
                            ],
                          ),
                          Visibility(
                            visible: _isCompany,
                            child: Column(
                              children: [
                                AuthTextField(
                                  hinttext: 'Company Details',
                                  color: Color(0xFF71C7E3),
                                  isPassword: false,
                                  isNumber: false,
                                  validator: (String value) {
                                    if (value.isEmpty) {
                                      return 'Please enter Company Details';
                                    }
                                    return null;
                                  },
                                  onSaved: (String value) {
                                    companyModel.company_name = value;
                                  },
                                ),
                                SizedBox(height: 10),
                                AuthTextField(
                                  hinttext: 'VAT',
                                  color: Color(0xFF71C7E3),
                                  isPassword: false,
                                  isNumber: true,
                                  validator: (String value) {
                                    if (value.isEmpty) {
                                      return 'Please enter VAT';
                                    }
                                    return null;
                                  },
                                  onSaved: (String value) {
                                    companyModel.vat_number = value;
                                  },
                                ),
                              ],
                            ),
                          ),
                          SizedBox(height: 10),
                          AuthTextField(
                            hinttext: 'Address',
                            isNumber: false,
                            prefixIcon: Icon(Icons.location_on_outlined),
                            color: Color(0xFF71C7E3),
                            isPassword: false,
                            validator: (String value) {
                              if (value.isEmpty) {
                                return 'Please enter Shop Address';
                              }
                              return null;
                            },
                            onSaved: (String value) {
                              address.location = value ?? '';
                            },
                          ),
                          SizedBox(height: 20),
                          Text('OR', style: TextStyle(fontWeight: FontWeight.bold,color: Colors.grey[600],)),
                          FlatButton(
                              onPressed: () {
                                getLocation();
                              },
                              child: Row(
                                mainAxisAlignment: MainAxisAlignment.center,
                                children: [
                                  Text(
                                    'Take my current location',
                                    style: TextStyle(
                                        color: Colors.grey[600], fontSize: 16),
                                  ),
                                  Icon(
                                    Icons.location_on_outlined,
                                    color: Colors.grey[600],
                                  )
                                ],
                              ))
                        ],
                      ),
                    ),
                  ),
                  Padding(
                    padding: EdgeInsets.only(left: 20),
                    child: Row(
                      children: [
                        Checkbox(
                          value: _value,
                          onChanged: (bool newValue) {
                            setState(() {
                              _value = newValue;
                            });
                          },
                        ),
                        SizedBox(width: 15),
                        Text(Strings.txtAgreeWithTerms,
                            style:
                                TextStyle(fontSize: 14, color: Colors.black87)),
                      ],
                    ),
                  ),
                  SizedBox(height: 20),
                  MyButton(
                    text: "Register",
                    onPressed: () {
                      if (_key.currentState.validate()) {
                        _key.currentState.save();
                        //signUp(_email,_password,_name,_number,'sellers',context,'/seller_home');
                        address.latitude = lat;
                        address.longitude = lat;
                        sellerRegister = SellerRegister(
                            first_name: sellerRegister.first_name,
                            last_name: sellerRegister.last_name,
                            email: sellerRegister.email,
                            phone: sellerRegister.phone,
                            address: address,
                            is_company: isCompany,
                            company:companyModel,
                            dob: dob,
                            password: sellerRegister.password,
                            confirm_password: sellerRegister.confirm_password,
                        );
                        postSellerRegister(sellerRegister, context).then((value) => Scaffold.of(context).showSnackBar(SnackBar(content:Text(value))));
                      }
                    },
                    color: Color(0xFF003585),
                  ),
                  SizedBox(height: 20),
                  Text('OR', style: TextStyle(fontWeight: FontWeight.bold,color: Colors.grey[600])),
                  SizedBox(height: 20),
                  Row(
                    mainAxisAlignment: MainAxisAlignment.center,
                    children: [
                      RawMaterialButton(
                          child: FaIcon(FontAwesomeIcons.facebookF,
                              size: 24, color: Colors.white),
                          fillColor: Colors.blue[900],
                          shape: CircleBorder(),
                          onPressed: () {
                            initiateFacebookLoginSeller(context);
                          }),
                      SizedBox(width: 20),
                      RawMaterialButton(
                          child: FaIcon(FontAwesomeIcons.googlePlusG,
                              size: 24, color: Colors.white),
                          fillColor: Colors.red,
                          shape: CircleBorder(),
                          onPressed: () {
                            signInSellerWithGoogle();
                          }),
                    ],
                  ),
                  SizedBox(height: 20),
                  Row(
                    children: [
                      Expanded(
                        child: Divider(
                          color: Color(0xFF71C7E3),
                          height: 5.0,
                          thickness: 5,
                        ),
                      ),
                    ],
                  ),
                  SizedBox(height: 10),
                  Align(
                    alignment: Alignment.bottomCenter,
                    child: Row(
                      mainAxisAlignment: MainAxisAlignment.center,
                      children: [
                        Padding(
                          padding: const EdgeInsets.only(bottom: 20.0),
                          child: RichText(
                            text: TextSpan(
                              children: <TextSpan>[
                                TextSpan(
                                    text: 'Already Have an Account? ',
                                    style: TextStyle(
                                        fontFamily: 'Dosis',
                                        fontSize: 18,
                                        color: Colors.grey,
                                        fontWeight: FontWeight.bold)),
                                TextSpan(
                                    text: 'Login Here',
                                    style: TextStyle(
                                        fontFamily: 'Dosis',
                                        color: Color(0xFF71C7E3),
                                        fontSize: 18,
                                        fontWeight: FontWeight.bold),
                                    recognizer: TapGestureRecognizer()
                                      ..onTap = () {
                                        Navigator.pushNamed(
                                            context, '/seller_login');
                                      }),
                              ],
                            ),
                          ),
                        ),
                      ],
                    ),
                  ),
                ],
              ),
            ),
          ),
        ),
      ),
    );
  }

  getLocation() async {
    LocationPermission permission = await Geolocator.requestPermission();
    Position position = await Geolocator.getCurrentPosition(
        desiredAccuracy: LocationAccuracy.high);
    lat = position.latitude;
    long = position.longitude;
    print(lat);
    print(long);
  }
}
