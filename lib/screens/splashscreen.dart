import 'dart:async';
import 'package:firebase_messaging/firebase_messaging.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter/scheduler.dart';
import 'package:rent2buy/di/service_locator.dart';
import 'package:rent2buy/models/userinfo.dart';
import 'package:rent2buy/screens/buyer/home/home.dart';
import 'package:rent2buy/utils/constants.dart' as Constants;
import 'package:rent2buy/utils/data_manager.dart';

class SplashScreen extends StatefulWidget {
  static final String routeName = "/splash_screen";
  final DataManager dataManager = serviceLocator<DataManager>();

  @override
  State<StatefulWidget> createState() {
    // TODO: implement createState
    return _SplashState();
  }
}

class _SplashState extends State<SplashScreen> {
  FirebaseMessaging _firebaseMessaging;

  Future<dynamic> getData() async {
    return widget.dataManager.getUser();
  }

  @override
  void initState() {
    _firebaseMessaging = FirebaseMessaging();
    _firebaseMessaging.getToken().then((token) {
      widget.dataManager.saveFCMToken(token);
      debugPrint(token);
    }).catchError((e) {
      debugPrint(e);
    });
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return FutureBuilder(
      future: getData(),
      builder: (context, snapshot) {
        if (snapshot.connectionState == ConnectionState.waiting) {
          return Scaffold(
            body: Container(
              child: Column(
                children: [
                  Spacer(),
                  Center(
                    child: Padding(
                      padding: const EdgeInsets.all(30.0),
                      child: Image.asset('assets/Rent2S.png'),
                    ),
                  ),
                  Spacer(),
                  Padding(
                      padding: EdgeInsets.only(bottom: 30.0),
                      child: CupertinoActivityIndicator(
                          radius: 16, animating: true)),
                ],
              ),
            ),
          );
        }

        if (!snapshot.hasData) {
          SchedulerBinding.instance.addPostFrameCallback((_) {
            Navigator.of(context).pushNamedAndRemoveUntil(
                '/login', (Route<dynamic> route) => false);
          });
        } else {
          SchedulerBinding.instance.addPostFrameCallback((_) async {
            if (snapshot.data['is_seller'] == 0 ||
                snapshot.data['is_seller'] == null) {
              Navigator.of(context).pushNamedAndRemoveUntil(
                  '/home', (Route<dynamic> route) => false);
            } else {
              int role = await widget.dataManager.getUserRole();
              if (role == Constants.BUYER) {
                Navigator.of(context).pushNamedAndRemoveUntil(
                    Home.routeName, (Route<dynamic> route) => false);
              } else {
                Navigator.of(context).pushNamedAndRemoveUntil(
                    '/seller_home', (Route<dynamic> route) => false);
              }
            }
          });
        }
        return Container();
      },
    );
  }
}
