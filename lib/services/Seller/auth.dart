import 'package:dio/dio.dart';
import 'package:flutter/material.dart';
import 'package:flutter_facebook_login/flutter_facebook_login.dart';
import 'package:google_sign_in/google_sign_in.dart';
import 'package:firebase_auth/firebase_auth.dart';
import 'package:rent2buy/models/Seller/sellerRegister.dart';
import 'package:rent2buy/models/User/userLogin.dart';
import 'package:rent2buy/screens/seller/otp_verify.dart';
import 'package:rent2buy/services/apiProvider.dart';
import 'dart:convert';
import 'package:rent2buy/shared_prefs/shared_preferences.dart';
import 'package:http/http.dart' as http;

Future<String> postSellerRegister(
    SellerRegister sellerRegister, BuildContext context) async {
  Dio dio = Dio();
  try {
    var response = await dio.post('${ApiProvider.signUpSeller}',
        options:
            Options(contentType: "application/json", followRedirects: false),
        data: json.encode(sellerRegister.toJson()));
    if (response.statusCode == 200) {
      Navigator.push(
          context,
          MaterialPageRoute(
              builder: (context) =>
                  SellerOTPVerify(email: sellerRegister.email)));
    }
  } on DioError catch (e) {
    print(e.response.data);
    return e.response.data['errors']['email'][0].toString();
  }
  return null;
}

postSellerLogin(UserLogin userLogin, BuildContext context) async {
  Dio dio = Dio();
  try {
    var response = await dio.post('${ApiProvider.loginSeller}',
        options: Options(
          contentType: "application/json",
          followRedirects: false,
        ),
        data: json.encode(userLogin.toJson()));
    if (response.statusCode == 200) {
      localStorageSeller(response);
      Navigator.of(context).pushNamedAndRemoveUntil(
          '/seller_home', (Route<dynamic> route) => false);
    }
  } on DioError catch (e) {
    print(e.response.data);
  }
}

void initiateFacebookLoginSeller(context) async {
  var facebookLogin = FacebookLogin();
  var facebookLoginResult = await facebookLogin.logIn(['email']);
  switch (facebookLoginResult.status) {
    case FacebookLoginStatus.error:
      break;
    case FacebookLoginStatus.cancelledByUser:
      break;
    case FacebookLoginStatus.loggedIn:
      var graphResponse = await http.get(
          'https://graph.facebook.com/v2.12/me?fields=name,first_name,last_name,email,picture.height(200)&access_token=${facebookLoginResult.accessToken.token}');
      var profile = json.decode(graphResponse.body);
      print(profile.toString());
      Navigator.push(
          context,
          MaterialPageRoute(
              builder: (context) => SellerOTPVerify(email: profile['email'])));
      break;
  }
}

void signInSellerWithGoogle() async {
  SellerRegister sellerRegister = SellerRegister();
  final GoogleSignIn googleSignIn = new GoogleSignIn();
  GoogleSignInAccount googleUser = await googleSignIn.signIn();
  GoogleSignInAuthentication googleSignInAuthentication =
      await googleUser.authentication;
  final AuthCredential credential = GoogleAuthProvider.credential(
      accessToken: googleSignInAuthentication.accessToken,
      idToken: googleSignInAuthentication.idToken);
  final UserCredential authResult =
      await FirebaseAuth.instance.signInWithCredential(credential);
  final User user = authResult.user;
  assert(user.email != null);
  assert(user.displayName != null);
  assert(user.photoURL != null);
  sellerRegister.email = user.email;
  sellerRegister.first_name = user.displayName;
  sellerRegister.phone = user.phoneNumber;
  sellerRegister.password = user.email;
  sellerRegister.confirm_password = sellerRegister.password;
  sellerRegister.address.latitude = null;
  sellerRegister.address.longitude = null;
  sellerRegister.address.location = null;

  BuildContext context;
  postSellerRegister(sellerRegister, context);
  //googleUserSeller(user,googleSignInAuthentication);
//
//  documentReference = FirebaseFirestore.instance.collection(usertype).doc(uid);
//  Map<String, dynamic> person = {
//    'email':email,
//    'name':name,
//    'photoURL':imageUrl,
//    'uid':uid,
//  };
//  documentReference.set(person);
//  return 'signInWithGoogle succeed: $user';
}
