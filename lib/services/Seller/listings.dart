import 'dart:io';
import 'package:http/http.dart' as http;
import 'package:flutter/material.dart';
import 'package:rent2buy/models/Seller/addListings.dart';
import 'package:rent2buy/screens/seller/item_add.dart';
import 'package:shared_preferences/shared_preferences.dart';
import 'package:rent2buy/services/apiProvider.dart';
import 'dart:convert';

void postAddListing(AddListings addListings,BuildContext context) async {
  SharedPreferences prefs = await SharedPreferences.getInstance();
  String token = prefs.getString('token');
  http.Response response = await http.post('${ApiProvider.addListing}',
      headers: {HttpHeaders.authorizationHeader: 'Bearer $token',"Content-Type":'application/json'},
    body: json.encode(addListings.toJson())
  );
  if(response.statusCode == 200){
    print(response.body);
    //Navigator.push(context, MaterialPageRoute(builder: (context) => ItemAdded()));
  }else{
    print(response.statusCode);
    print(response.body);
  }
}
