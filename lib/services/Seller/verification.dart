import 'dart:convert';
import 'package:dio/dio.dart';
import 'package:flutter/material.dart';
import 'package:rent2buy/models/Verification/verifyOtp.dart';
import 'package:rent2buy/services/apiProvider.dart';

void postVerifyOtp(VerifyOtp verifyOtp,BuildContext context) async{
  Dio dio = Dio();
  var response = await dio.post('${ApiProvider.verifyOtp}',
      options: Options(contentType: "application/json"),
      data: json.encode(verifyOtp.toJson())
  );
  if(response.statusCode == 200){
    Navigator.pushNamedAndRemoveUntil(context, '/seller_login', (route) => false);
  }else{
    print(response.statusCode);
  }
}