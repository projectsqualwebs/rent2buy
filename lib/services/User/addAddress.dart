import 'dart:convert';
import 'package:dio/dio.dart';
import 'package:flutter/cupertino.dart';
import 'package:rent2buy/models/User/addAddress.dart';
import 'package:rent2buy/services/apiProvider.dart';
import 'package:shared_preferences/shared_preferences.dart';

void postAddAddress(AddAddress addAddress,BuildContext context) async {
  SharedPreferences prefs = await SharedPreferences.getInstance();
  String token = prefs.getString('token');
  Dio dio = Dio();
  dio.options.headers['Authorization'] = 'Bearer $token';
  var response = await dio.post('${ApiProvider.addAddress}',
      options: Options(
        contentType: "application/json",
        followRedirects: false,
      ),
      data: json.encode(addAddress.toJson()));
  if (response.statusCode == 200) {
    Navigator.pop(context,addAddress.address);
  } else {
    print(response.statusCode);
  }

}
