import 'dart:math';

import 'package:dio/dio.dart';
import 'package:firebase_auth/firebase_auth.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:fluttertoast/fluttertoast.dart';
import 'package:google_sign_in/google_sign_in.dart';
import 'dart:convert';
import 'package:rent2buy/models/User/userLogin.dart';
import 'package:rent2buy/models/User/userRegister.dart';
import 'package:rent2buy/networking/api_result.dart';
import 'package:rent2buy/networking/dioclient.dart';
import 'package:rent2buy/networking/network_exceptions.dart';
import 'package:rent2buy/networking/standard_response.dart';
import 'package:rent2buy/screens/common/verification/otp_verify.dart';
import 'package:rent2buy/screens/buyer/signup/signup.dart';
import 'package:rent2buy/services/apiProvider.dart';
import 'package:rent2buy/shared_prefs/shared_preferences.dart';

Future<String> postUserLogin(UserLogin userLogin, BuildContext context) async {
  Dio dio = Dio();
  try {
    var response =
        await dio.post('${ApiProvider.baseUrl}' + '${ApiProvider.loginUser}',
            options: Options(
              contentType: "application/json",
              followRedirects: false,
            ),
            data: json.encode(userLogin.toJson()));
    if (response.statusCode == 200) {
      localStorage(response);
      print(response.data);
      Navigator.of(context)
          .pushNamedAndRemoveUntil('/home', (Route<dynamic> route) => false);
    }
  } on DioError catch (e) {
    Map<String, dynamic> error = jsonDecode(e.response.data)['message'];
    print(jsonDecode(e.response.data['message']));
    return e.response.data['message'].toString();
  }
  return null;
}

// Future<String> postUserRegister(
//     UserRegister userRegister, BuildContext context) async {
//   var dio = DioClient().http;
//   try {
//     var response = await dio.post('${ApiProvider.signUpUser}',
//         options: Options(
//             contentType: "application/json", responseType: ResponseType.json),
//         data: json.encode(userRegister.toJson()));
//     if (response.statusCode == 200) {
//       print(response.statusCode);
//       Navigator.push(
//           context,
//           MaterialPageRoute(
//               builder: (context) => OtpVerify(email: userRegister.email)));
//     }
//   } on DioError catch (e) {
//
//   //  print(e.response.data);
//     //return e.response.data['errors']['email'][0];
//   }
//   return null;
// }

Future<ApiResult<String>> postUserRegister(
    UserRegister userRegister, BuildContext context) async {
  try {
    var dioClient = DioClient();
    final response = await dioClient.post(ApiProvider.signUpUser,
        data: json.encode(userRegister.toJson()));

    return ApiResult.success(data: StandardResponse.fromJson(response).message);
  } catch (e) {
    return ApiResult.failure(error: NetworkExceptions.getDioException(e));
  }
}

void signInUserWithGoogle(context) async {
  UserRegister userRegister = UserRegister();
  final GoogleSignIn googleSignIn = new GoogleSignIn();
  GoogleSignInAccount googleUser = await googleSignIn.signIn();
  GoogleSignInAuthentication googleSignInAuthentication =
      await googleUser.authentication;
  final AuthCredential credential = GoogleAuthProvider.credential(
      accessToken: googleSignInAuthentication.accessToken,
      idToken: googleSignInAuthentication.idToken);
  final UserCredential authResult =
      await FirebaseAuth.instance.signInWithCredential(credential);
  final User user = authResult.user;
  assert(user.email != null);
  assert(user.displayName != null);
  assert(user.photoURL != null);
  Navigator.push(
      context,
      MaterialPageRoute(
          builder: (context) => SignUp(
                name: user.displayName,
                email: user.email,
              )));
//  userRegister.email = user.email;
//  userRegister.first_name = user.displayName;
//  userRegister.phone = user.phoneNumber;
//  userRegister.password = user.email;
//  userRegister.confirm_password = user.email;
//  postUserRegister(userRegister, context);
  //googleUserSeller(user,googleSignInAuthentication);
//
//  documentReference = FirebaseFirestore.instance.collection(usertype).doc(uid);
//  Map<String, dynamic> person = {
//    'email':email,
//    'name':name,
//    'photoURL':imageUrl,
//    'uid':uid,
//  };
//  documentReference.set(person);
//  return 'signInWithGoogle succeed: $user';
}
