import 'package:flutter/material.dart';
import 'package:flutter_facebook_login/flutter_facebook_login.dart';
import 'package:http/http.dart' as http;
import 'package:rent2buy/models/User/userRegister.dart';
import 'dart:convert';
import 'package:rent2buy/screens/common/verification/otp_verify.dart';
import 'package:rent2buy/screens/buyer/signup/signup.dart';
import 'package:rent2buy/services/User/auth.dart';

void initiateFacebookLogin(context) async {
  UserRegister userRegister = UserRegister();
  var facebookLogin = FacebookLogin();
  var facebookLoginResult =
  await facebookLogin.logIn(['email']);
  switch (facebookLoginResult.status) {
    case FacebookLoginStatus.error:
      break;
    case FacebookLoginStatus.cancelledByUser:
      break;
    case FacebookLoginStatus.loggedIn:
      var graphResponse = await http.get(
          'https://graph.facebook.com/v2.12/me?fields=name,first_name,last_name,email,picture.height(200)&access_token=${facebookLoginResult
              .accessToken.token}');
      var profile = json.decode(graphResponse.body);
      print(profile.toString());
      Navigator.push(context, MaterialPageRoute(builder:(context) => SignUp(name: profile['name'],email: profile['email'],)));
      break;
  }
}