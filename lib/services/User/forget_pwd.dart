import 'dart:convert';
import 'dart:io';
import 'package:flutter/material.dart';
import 'package:fluttertoast/fluttertoast.dart';
import 'package:http/http.dart' as http;
import 'package:http/http.dart';
import 'package:rent2buy/services/apiProvider.dart';
import 'package:shared_preferences/shared_preferences.dart';

Future<Response> resetPwdMail(String email) async {
  http.Response response =
      await http.post('${ApiProvider.resendOtp}', body: {"email": email});
  print(response.body);
  return response;
}

forgetPwd(String current, String new_pwd, String confirm,
    BuildContext context) async {
  SharedPreferences prefs = await SharedPreferences.getInstance();
  String token = prefs.getString('token');
  http.Response response = await http.post('${ApiProvider.forgotPassword}',
      body: {
        "email": current,
        "new_password": new_pwd,
        "confirm_password": confirm
      });
  if (response.statusCode == 200) {
    print(response.body);
    Fluttertoast.showToast(msg: 'Password Reset Successfully');
    Navigator.pop(context);
  }
}
