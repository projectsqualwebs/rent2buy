class ApiProvider {
  static const String baseUrl = "http://54.176.179.158/renttobuy/public/api/";
  static const String user = "user/";

// static const String loginUser = 'login/5fa391b0e94eb';
  static const String loginUser =
      '${ApiProvider.baseUrl}' + 'login/null'; //Add cart ID later
  static const String signUpUser =
      '${ApiProvider.baseUrl}' + 'register/null'; //Add Cart Id Later
  static const String getProfile = '${ApiProvider.baseUrl}' + 'user/profile';
  static const String sellerProfile =
      '${ApiProvider.baseUrl}' + 'seller/profile';
  static const String addAddress =
      '${ApiProvider.baseUrl}' + '${ApiProvider.user}' + 'locations';
  static const String verifyOtp = '${ApiProvider.baseUrl}' + 'verify-otp';
  static const String resendOtp = '${ApiProvider.baseUrl}' + 'resend-otp';
  static const String signUpSeller =
      '${ApiProvider.baseUrl}' + 'seller/register';
  static const String loginSeller = '${ApiProvider.baseUrl}' + 'seller/login';
  static const String addListing =
      '${ApiProvider.baseUrl}' + 'seller/add-listings';
  static const String uploadImage = '${ApiProvider.baseUrl}' + 'upload';
  static const String getCategories = '${ApiProvider.baseUrl}' + 'categories';
  static const String getSubCategories =
      '${ApiProvider.baseUrl}' + 'sub-categories/';
  static const String productsByCategory =
      '${ApiProvider.baseUrl}' + 'listings';
  static const String searchSellListings =
      '${ApiProvider.baseUrl}' + 'sell-radius/listings';
  static const String addToCart = '${ApiProvider.baseUrl}' + 'user/cart/';
  static const String addToCartLoggedIn =
      '${ApiProvider.baseUrl}' + 'user/cart/null';

  static const String clearCart =
      ApiProvider.baseUrl + 'user/cart-clear/'; // Pass Cart Id;
  static const String getCartItem =
      '${ApiProvider.baseUrl}' + 'user/cart-items/';
  static const String getLoggedInCartItem =
      '${ApiProvider.baseUrl}' + 'user/cart-items/null';
  static const String searchForRent =
      '${ApiProvider.baseUrl}' + 'rent-radius/listings';
  static const String getSellersWithRating =
      '${ApiProvider.baseUrl}' + 'sellers';
  static const String addCard = '${ApiProvider.baseUrl}' + 'user/cards';
  static const String defaultCard = '${ApiProvider.baseUrl}' + 'user/cards/';
  static const String getCartCount =
      '${ApiProvider.baseUrl}' + 'user/cart-items-count/';
  static const String getSingleItem = '${ApiProvider.baseUrl}' + 'listing/';
  static const String order = '${ApiProvider.baseUrl}' + 'user/orders';
  static const String singleOrder = '${ApiProvider.baseUrl}' + 'orders/';
  static const String sellerBankAcc =
      '${ApiProvider.baseUrl}' + 'seller/accounts';
  static const String removeBankAccount =
      '${ApiProvider.baseUrl}' + 'seller/accounts/';
  static const String thread = '${ApiProvider.baseUrl}' + 'chat/threads';
  static const String chat = "${ApiProvider.baseUrl}" + 'chat';
  static const String getMessages = '${ApiProvider.baseUrl}' + 'chat-messages/';
  static const String sellerListing =
      '${ApiProvider.baseUrl}' + 'seller/listings';
  static const String editListing =
      '${ApiProvider.baseUrl}' + 'seller/listings/';
  static const String updateListingStatus = baseUrl + 'seller/listings/status/';
  static const String listings = '${ApiProvider.baseUrl}' + 'listings';
  static const String sellerOrders = '${ApiProvider.baseUrl}' + 'seller/orders';
  static const String updateOrderStatus =
      '${ApiProvider.baseUrl}' + 'seller/orders-status/';
  static const String forgotPassword =
      '${ApiProvider.baseUrl}' + 'forgot-password';
  static const String resetPassword =
      '${ApiProvider.baseUrl}' + 'profile/reset-password';
  static const String listingFeedback =
      '${ApiProvider.baseUrl}' + '${ApiProvider.user}' + 'feedback';
  static const String getFeedback =
      '${ApiProvider.baseUrl}' + 'listing/feedback/';

  static const String getActions = '${ApiProvider.baseUrl}' + 'actions';
  static const String getDeliveryTypes =
      '${ApiProvider.baseUrl}' + 'delivery-types';
  static const String notifications =
      '${ApiProvider.baseUrl}' + 'auth/notifications';

  static const String userDisputes =
      '${ApiProvider.baseUrl}' + 'disputed-user-listings';

  static const String sellerDisputes =
      '${ApiProvider.baseUrl}' + 'disputed-seller-listings';

  static const String dispute = '${ApiProvider.baseUrl}' + 'dispute/';

  static String getDeliveryMethods =
      baseUrl + "seller/delivery-types/"; //{card_id}

  static const String userToSeller = baseUrl + "user/to-seller";

  static String address = baseUrl + "address";

  static String deactivateAccount = baseUrl + "seller/disable";

  static String cartItemCount = baseUrl + "user/cart-items-count/";

  static String appFeedBack = baseUrl + "app/feedback";
}
