import 'package:dio/dio.dart';
import 'package:firebase_auth/firebase_auth.dart';
import 'package:google_sign_in/google_sign_in.dart';
import 'package:shared_preferences/shared_preferences.dart';

void localStorage(Response response) async{
  SharedPreferences prefs = await SharedPreferences.getInstance();
  prefs.clear();
  prefs.setString('email',response.data['response']['email'] ?? '') ;
  prefs.setString('name',response.data['response']['first_name'] ?? '') ;
  prefs.setString('last_name',response.data['response']['last_name'] ?? '') ;
  prefs.setString('_id',response.data['response']['_id'] ?? '') ;
  prefs.setString('phone',response.data['response']['phone'] ?? '') ;
  prefs.setString('token',response.data['response']['token'] ?? '') ;
  prefs.setInt('is_seller',response.data['response']['is_seller'] ?? '') ;
  print(prefs.getString('first_name'));
}

void googleUserSeller(User user,GoogleSignInAuthentication googleSignInAuthentication) async{
  SharedPreferences prefs = await SharedPreferences.getInstance();
  prefs.clear();
  prefs.setString('name', user.displayName);
  prefs.setString('email', user.email);
  prefs.setString('imageUrl', user.photoURL);
  prefs.setString('token', googleSignInAuthentication.idToken);
  prefs.setString('access_token', googleSignInAuthentication.accessToken);
  print(prefs.getString('name'));
  print(prefs.getString('email'));
  print(prefs.getString('imageUrl'));
  print(prefs.getString('token'));
  print(prefs.getString('access_token'));
}
void localStorageSeller(Response response) async{
  SharedPreferences prefs = await SharedPreferences.getInstance();
  prefs.clear();
  prefs.setString('email',response.data['response']['seller']['email'] ?? '') ;
  prefs.setString('name',response.data['response']['seller']['full_name'] ?? '') ;
  prefs.setString('_id',response.data['response']['seller']['_id'] ?? '') ;
  prefs.setString('phone',response.data['response']['seller']['phone'] ?? '') ;
  prefs.setString('token',response.data['response']['token'] ?? '') ;
  prefs.setInt('is_seller',response.data['response']['seller']['is_seller'] ?? '') ;
}