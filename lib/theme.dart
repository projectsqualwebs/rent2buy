import 'package:flutter/material.dart';

ThemeData sellerTheme = ThemeData(
    fontFamily: 'Dosis',
).copyWith(
    scaffoldBackgroundColor: Color(0xFFE8F5FA),
    primaryColor: Color(0xFF71C7E3),
    appBarTheme: AppBarTheme(
      color: Color(0xFF71C7E3),
        iconTheme:IconThemeData(color: Color(0xFF003585))
    ),
    buttonTheme: ButtonThemeData(buttonColor: Color(0xFF71C7E3))
);