import 'package:flutter/material.dart';
import 'package:rent2buy/utils/colors.dart';

class CustomTheme {
  static ThemeData get buyerTheme {
    //1
    return ThemeData(
        //2
        primaryColor: colorPrimary,
        accentColor: colorAccent,
        inputDecorationTheme: InputDecorationTheme(
          focusedBorder: getBorder(colorAccent),
          border: getBorder(colorAccent),
          enabledBorder: getBorder(colorAccent),
          focusedErrorBorder: getBorder(colorAccent),
          errorBorder: getBorder(colorAccent),
        ),
        scaffoldBackgroundColor: colorBackgroundBuyer,
        fontFamily: 'Dosis', //3
        buttonTheme: ButtonThemeData(
          buttonColor: colorPrimary,
        ));
  }

  static ThemeData get sellerTheme {
    //1
    return ThemeData(
        //2
        primaryColor: colorSeller,
        accentColor: colorSellerDark,
        inputDecorationTheme: InputDecorationTheme(
          focusedBorder: getBorder(colorSellerDark),
          border: getBorder(colorSellerDark),
          enabledBorder: getBorder(colorSellerDark),
          focusedErrorBorder: getBorder(colorSellerDark),
          errorBorder: getBorder(colorSellerDark),
        ),
        scaffoldBackgroundColor: colorBackgroundSeller,
        fontFamily: 'Dosis', //3
        buttonTheme: ButtonThemeData(
          buttonColor: colorSeller,
        ));
  }

  static getBorder(Color color) {
    return OutlineInputBorder(
        borderSide: BorderSide(color: color, width: 4),
        borderRadius: BorderRadius.all(Radius.circular(12)));
  }
}
