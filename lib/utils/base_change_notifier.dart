import 'package:dio/dio.dart';
import 'package:flutter/cupertino.dart';
import 'package:rent2buy/networking/network_exceptions.dart';
import 'package:rent2buy/utils/strings.dart';

abstract class BaseChangeNotifier extends ChangeNotifier {
  void logPrint(String s) {
    debugPrint(s);
  }

  String parseError(e) {
    try {
      if (e is DioError) {
        if (e.type == DioErrorType.RESPONSE) {
          if (e.response.statusCode == 500) {
            return Strings.somethingWentWrong;
          }else if(e.response.statusCode == 302){
            return e.response.data['message'];
          } else {
            return e.response.data['message'];
          }
        } else {
          return NetworkExceptions.getErrorMessage(
              NetworkExceptions.getDioException(e));
        }
      } else {
        return Strings.somethingWentWrong;
      }
    } catch (exception) {
      logPrint(exception);
    }
    return Strings.somethingWentWrong;
  }
}
