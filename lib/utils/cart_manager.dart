import 'package:flutter_session/flutter_session.dart';
import 'package:rent2buy/di/service_locator.dart';

class CartManager {
  String cartDetails = "cart_details"; //For Adding To Cart and Updating to cart
  String cartIdDetails = "cart_id";//For Clearing Cart Data
  FlutterSession _flutterSession = serviceLocator<FlutterSession>();

  void saveCartId(String cartId) async {
    await _flutterSession.set(cartDetails, cartId);
  }

  Future<String> getCartId() async {
    dynamic cartId = await _flutterSession.get(cartDetails);
    return cartId==null?"":cartId;
  }

    void saveCartListingId(String actualCartId) async {
    await _flutterSession.set(cartIdDetails, actualCartId);
  }

  Future<String> getCartListingId() async {
    dynamic cartId = await _flutterSession.get(cartIdDetails);
    return cartId == null ? null : cartId.toString();
  }
}
