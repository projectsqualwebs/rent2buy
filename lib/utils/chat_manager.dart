import 'package:flutter_session/flutter_session.dart';
import 'package:rent2buy/di/service_locator.dart';

class ChatManager {
  FlutterSession _flutterSession = serviceLocator<FlutterSession>();

  void saveChatId(String key, String value) async {
    await _flutterSession.set(key, value);
  }

  Future<String> getChatId(String key) async {
    return await _flutterSession.get(key);
  }
}
