import 'dart:ui';

import 'package:pdf/pdf.dart';

const colorPrimary = Color.fromARGB(255, 247, 167, 77);
const colorBackgroundBuyer = Color.fromARGB(255, 255, 252, 222);
const colorAccent = Color(0xFF97080E);
const colorAccentLight = Color.fromARGB(200, 151, 8, 14);
const colorBackgroundSeller = Color.fromARGB(255, 232, 245, 250);

const colorSeller = Color.fromARGB(255, 113, 199, 227);
const colorSellerDark = Color.fromARGB(255, 0, 54, 133);
final colorPrimaryPdf = PdfColor.fromHex('#f7a84d');
const colorBorder = Color.fromARGB(255, 196, 196, 196);
