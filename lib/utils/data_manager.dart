import 'package:flutter_session/flutter_session.dart';
import 'package:rent2buy/di/service_locator.dart';
import 'package:rent2buy/models/userinfo.dart';

class DataManager {
  String userDetails = "user_info";
  String userType = "loggedInUser";
  String deliveryId = "deliveryId";
  String selfPickUpId = "selfpickup";
  String sellId = "sellId";
  String rentId = "rentId";
  String currentrole = "currentRole";
  var fcmToken = "fcm_token";

  FlutterSession _flutterSession = serviceLocator<FlutterSession>();
  String token;

  String getToken() {
    return token;
  }

  Future<String> loadToken() async {
    dynamic info = await _flutterSession.get(userDetails);
    if (info == null) return "";
    return info['token'];
  }

  Future<String> loadDeliveryId() async {
    dynamic id = await _flutterSession.get(deliveryId);
    return id;
  }

  Future<String> loadSelfPickupId() async {
    dynamic id = await _flutterSession.get(selfPickUpId);
    return id;
  }

  Future<String> loadRentId() async {
    dynamic id = await _flutterSession.get(rentId);
    return id;
  }

  Future<String> loadSellId() async {
    dynamic id = await _flutterSession.get(sellId);
    return id;
  }

  Future<int> isSeller() async {
    dynamic info = await _flutterSession.get(userDetails);
    return info['is_seller'] == null ? 0 : info['is_seller'];
  }

  Future<bool> isGuestUser() async {
    dynamic info = await _flutterSession.get(userDetails);
    return info == null;
  }

  void saveUserRole(int type) async {
    await _flutterSession.set(userType, type);
  }

  void saveFCMToken(String token) async {
    await _flutterSession.set(fcmToken, token);
  }

  void saveUser(Userinfo userinfo) async {
    token = userinfo.token;
    await _flutterSession.set(userDetails, userinfo);
  }

  void saveDeliveryId(String id) async {
    await _flutterSession.set(deliveryId, id);
  }

  void saveSelfPickUpId(String id) async {
    await _flutterSession.set(selfPickUpId, id);
  }

  void saveSellId(String id) async {
    await _flutterSession.set(sellId, id);
  }

  void saveRentId(String id) async {
    await _flutterSession.set(rentId, id);
  }

  Future<String> getFCMToken() async {
    return await _flutterSession.get(fcmToken);
  }

  Future<dynamic> getUser() async {
    return _flutterSession.get(userDetails);
  }

  Future<int> getUserRole() async {
    dynamic role = await _flutterSession.get(userType);
    return role;
  }

  void removeUser() async {
    bool remove = await _flutterSession.prefs.remove(userDetails);
  }
}
