import 'package:flutter/material.dart';
import 'package:rent2buy/utils/colors.dart';
import 'package:flutter/cupertino.dart';
import 'package:rent2buy/utils/constants.dart';
import 'package:rent2buy/utils/dimens.dart';

class DesignUtils {
  static OutlineInputBorder getDefaultBorder() {
    return new OutlineInputBorder(
        borderSide: BorderSide(width: defaultBorderWidth, color: colorAccent),
        borderRadius: BorderRadius.circular(default_radius));
  }

  static InputDecoration textInputLayoutOutLined(String hintText,
      {Widget suffixIcon, Widget prefixIcon}) {
    return InputDecoration(
      contentPadding: EdgeInsets.all(margin_large),
      prefixIcon: prefixIcon,
      suffixIcon: suffixIcon,
      enabledBorder: getBorder(),
      focusedBorder: getBorder(),
      errorBorder: getBorder(),
      focusedErrorBorder: getBorder(),
      hintText: hintText,
    );
  }

  static getBorder() {
    return OutlineInputBorder(
        borderSide: BorderSide(color: colorAccent, width: 4),
        borderRadius: BorderRadius.all(Radius.circular(12)));
  }

  static getTextStyle() {
    return TextStyle(
        fontWeight: FontWeight.w600, fontSize: 16, color: Color(0xff717171));
  }

  static textApperanceRegular({Color color, double fontSize}) {
    return TextStyle(
        fontWeight: FontWeight.w400,
        fontSize: fontSize == null ? 14 : fontSize,
        color: color == null ? colorAccent : color);
  }

  static textApperanceMedium() {
    return TextStyle(
        fontWeight: FontWeight.w600, fontSize: 16, color: colorAccent);
  }

  static textApperanceLarge() {
    return TextStyle(
        fontWeight: FontWeight.w600, fontSize: 24, color: colorAccent);
  }

  static textApperanceButton() {
    return TextStyle(
        fontWeight: FontWeight.w700,
        fontSize: textSizeLarge,
        color: colorAccent);
  }

  static roundedButtonStyle({Color color, Color textColor, double width}) {
    return ElevatedButton.styleFrom(
        minimumSize:
            Size(width == null ? double.infinity : width, minButtonHeight),
        textStyle:
            TextStyle(color: textColor == null ? Colors.white : textColor),
        shape: RoundedRectangleBorder(
            borderRadius: BorderRadius.circular(defaultBtnCorner)),
        primary: color == null ? colorPrimary : color);
  }

  static textApperanceToolbar() {
    return TextStyle(
        fontWeight: FontWeight.w600, fontSize: 16, color: colorAccent);
  }

  static textApperanceHeader({int role}) {
    return TextStyle(
        fontWeight: FontWeight.w700,
        fontSize: 24,
        color: role == null
            ? colorAccent
            : role == SELLER
                ? colorSellerDark
                : colorAccent);
  }

  static textApperanceTitle() {
    return TextStyle(
        fontWeight: FontWeight.w700, fontSize: 24, color: Colors.black);
  }

  static getDefaultRoundedBorder() {
    return RoundedRectangleBorder(
        side: BorderSide(color: Colors.black, width: 4.0),
        borderRadius: BorderRadius.circular(6));
  }

  static getProgressButton({Color color}) {
    return Center(
      child: Container(
        width: 48.0,
        height: 48.0,
        padding: EdgeInsets.all(padding_medium),
        decoration: BoxDecoration(
          shape: BoxShape.circle,
          color: color == null ? colorPrimary : color,
        ),
        child: CircularProgressIndicator(),
      ),
    );
  }
}
