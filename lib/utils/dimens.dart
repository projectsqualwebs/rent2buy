const default_radius = 8.0;
const defaultStrokeWidth = 1.0;
const defaultBorderWidth = 4.0;
const defaultBtnCorner = 8.0;
const defaultCircularRadius = 16.0;
const margin_large = 16.0;
const padding_large = margin_large;
const margin_small = 4.0;
const padding_small = margin_small;
const margin_medium = 8.0;
const padding_medium = margin_medium;
const margin_extra_large = 32.0;
const avatarRadius = 32.0;
const minButtonHeight = 48.0;

const minButtonWidth = 64.0;
const textSizeLarge = 16.0;
const defaultSize = 48.0;
