import 'package:flutter/material.dart';
import 'package:rent2buy/models/menu_item.dart';

class Helper {
  List<MenuItem> getSellerMenuItems() {
    List<MenuItem> list = List<MenuItem>();
    list.add(MenuItem(1, "Home", Icons.home, ""));
    list.add(MenuItem(2, "Package", null, 'assets/tag.png'));
    list.add(MenuItem(3, "Login", null, 'assets/tag.png'));
    list.add(MenuItem(4, "Register", null, 'assets/tag.png'));
    list.add(MenuItem(5, "Chat", null, 'assets/tag.png'));
    list.add(MenuItem(6, "All Listing", null, 'assets/tag.png'));
    list.add(MenuItem(7, "Listing Status", null, 'assets/tag.png'));
    return list;
  }
}
