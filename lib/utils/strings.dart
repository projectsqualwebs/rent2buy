import 'package:flutter/src/widgets/framework.dart';

class Strings {
  static const textMessage = "test_message";

  static var enterAddress = "Enter Address";

  static var msgEnterFullName = "Please enter Full Name";

  static var msgEnterAddress = "Please enter address";

  static String msgSelectOption = "Select a purchase method";

  static String somethingWentWrong = "Something went wrong.Try again.";

  static var verify = "verify";

  static var enterOTP = "Enter OTP";

  static String txtOTPNotReceived = "OTP not received?";

  static String txtOTPVerified = "OTP verified successfully.";

  static String perDay = " per day";

  static String txtAgreeWithTerms = "Agree with our Terms and Conditions.";

  static var msgAcceptTerms = "Please accept terms and conditions.";

  static String addToCart = "Add To Cart";

  static var goToCart = "Go To Cart";

  static var msgRemoveAccount =
      "Warning: This will deactivate your account from Rent2Buy. You can login again to enable your account";

  static String cartIsEmpty = "Cart is empty";

  static String buyNow = "Buy Now!";

  static String msgClearCart = "Clear Cart!";

  static String msgAreYouSureClear =
      "Are your sure? you want to empty your cart?";

  static String msgDeleteChats =
      "Are your sure? you want to delete all your chats?";

  static String select = "Select";

  static String msgDelete = "Delete";

  static String txtAlreadyItems =
      "You already have items from other sellers, clear cart to continue adding this item.";

  static String txtRemoveCartItem = "Remove Item";
  static String msgDeleteCartItem =
      "Are you sure? you want to remove this item?";
  static String txtDisputeItem = "Dispute Product";
  static String msgDisputeItem =
      "Are you sure? you want to mark this product for dispute.";
  static String cancel = "Cancel";
  static String dispute = "Dispute";
  static String remove = "Remove";
  static String companyDetails = "Company Details";
  static String msgDisableAccount = "Disabling Account..";
  static String otpNotMatched = "OTP not matched";
  static String vat = "VAT";
  static String taxable = "Taxable?";
  static String becomeSeller = "Become Seller";
  static String txtEnterCompanyDetails = "Please enter company details";
  static String fieldRequired = "This field is required.";
  static String msgSelectAddress = "Select address";
  static String msgSelectDeliveryMethod = "Select delivery method";
  static String msgSelectPaymentMethod = "Select payment method";
  static String msgSelectPickupDate = "Select Pick-Up Date";
  static String msgSelectDropOffDate = "Select DropOff Date";
  static String msgSelectPickupTime = "Select Pick-Up Time";
  static String msgSelectDropOffTime = "Select DropOff Time";

  static String proceed = "Proceed";

  static var txtAddingCard = "Adding Card...";

  static String fieldReq = "field is required.";

  static String numberIsInvalid = "Invalid";

  static String exp = "EXP.";

  static String msgConfirmingOTP = "Confirming OTP...";
  static String msgNoListingFound = "No listing found.";
  static String msgNoDisputesFound = "No disputes found.";
  static String addingProduct = "Adding...";

  static var titleShippingAddress = "Delivery Address";

  static String selfPickup = "Self PickUp";

  static String delivery = "Delivery";

  static String msgOrderPlaced = "Order Placed";

  static String txtPlacingOrder = "Placing Order...";
  static String loading = "Loading...";

  static String addNewAddress = "+Add New Delivery Address";

  static String txtSavingLocation = "Saving Location...";

  static String txtGettingLocation = "Getting Location...";

  static var msgSelectPurchaseAction = "Select Purchase Action";

  static String msgSelectCategory = "Select Category";

  static String msgSelectSubCategory = "Select Sub Category";

  static var location = "Location";

  static var please_login_to_your_account = "Please login to your account";

  static var dateOfBirth = "Date Of Birth";

  static var txtPickUp = "Pick Up";

  static var shipping = "Shipping";

  static String messageTo = "Message to";

  static String startDate = "Start Date";

  static String endDate = "End Date";

  static String orderAccepted = "Order Accepted";
  static String orderDeclined = "Order Declined";
  static String txtMessageIsDeleted = "This Message was deleted.";

  static String msgSubmittingReview = "Submitting Review...";

  static String sellAndRent = "Sell and Rent";

  static String sellOnly = "Sell Only";

  static String rentOnly = "Rent Only";

  static var txtUpdate = "Update";

  static var txtSubmit = "Submit";
  static var txtEditListing = "Edit Listing";
  static var txtAddListing = "Submit Listing";

  static String msgNoNotifications = "No notificatoins found.";

  static String guest = "Guest";

  static String msgNoOrdersFound = "No orders found.";
  static String msgNoCardsFound = "No cards found.";
  static String deleteChats = "Delete Chat";

  static String chatWithDealer = "Chat with dealer";

  static String noSellersFound = "No sellers found.";
  static String buy = "Buy";
  static String rent = "Rent";

  static String txtPickUpTime = "Pick-Up Time";
  static String txtPickUpDate = "Pick-Up Date";
  static String txtDropOffTime = "DropOff Time";
  static String txtDropOffDate = "DropOff Date";
  static String noChats = "No chats found.";

  static var appName = "R2B";
  static var sendAgain = "click here";

  static var currentPassword = "Current Password";
  static var newPassword = "New Password";
  static var confirmPassword = "Confirm Password";
  static String email = "Email Address";

  static var txtDeleteAccount = "Remove Account";
  static String active = "Active";
  static String inactive = "Inactive";
  static String txtPaymentMethods = "Payment Methods";
  static String markAsDefault = "Mark As Default";
  static String removeCard = "Remove";
  static String msgNoCategoriesFound = "No categories found.";
  static String feedBack = "Feedback";
  static String enterMessage = "Enter Feedback Message";
  static String setAvailability = "Set Availablity";
  static String camera = "Camera";
  static String file = "Image";
}
