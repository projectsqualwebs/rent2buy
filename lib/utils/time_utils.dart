import 'package:flutter/material.dart';
import 'package:intl/intl.dart';

class TimeUtils {
  static getHourMinuteFromEpoch(int timeStamp) {
    int hour = DateTime.fromMillisecondsSinceEpoch(timeStamp).hour;
    int minute = DateTime.fromMillisecondsSinceEpoch(timeStamp).minute;
    return (hour.toString() + ":" + minute.toString());
    ;
  }

  static String getTimeInPeriod(TimeOfDay time) {
    final now = new DateTime.now();
    final dt = DateTime(now.year, now.month, now.day, time.hour, time.minute);
    final format = DateFormat.jm(); //"6:00 AM"
    return format.format(dt);
  }

  static String getMMDDYYY(DateTime createdAt) {
    int month = createdAt.month;
    int day = createdAt.day;
    int year = createdAt.year;
    return month.toString() + "/" + day.toString() + "/" + year.toString();
  }
}
