import 'package:flutter/material.dart';
import 'package:rent2buy/models/purchase_actions.dart';
import 'package:rent2buy/utils/constants.dart';
import 'package:rent2buy/utils/strings.dart';

class Utils {
  static SnackBar showMessage(String msg) {
    return SnackBar(
      content: Text(msg),
      duration: Duration(milliseconds: 800),
    );
  }

  static String getFullName(String first, String last) {
    if (first == null) return Strings.guest;
    return first + " " + last;
  }

  static String getPriceInDollar(dynamic price) {
    return '\u0024' + '$price';
  }

  static String getCartNoOfItem(int count) {
    return "Price(${count})";
  }

  static String getQty(String x) {
    return "Qty: " + x;
  }

  static String getDate(String createdAt) {
    var indexOfT = createdAt.indexOf("T");
    return createdAt.substring(0, indexOfT);
  }

  static Widget getEmptyView(String msg) {
    return Center(
      child: Text(msg),
    );
  }

  static String getPurchaseActions(List<PurchaseActions> purchaseActions) {
    if (purchaseActions.length == 2) return ACTION_BOTH;
    if (purchaseActions.first.name == ACTION_SELL) {
      return ACTION_SELL;
    } else {
      return ACTION_RENT;
    }
  }

  
}
