import 'package:flutter/material.dart';

class SendedMsgWidget extends StatelessWidget {
  final String content;

  const SendedMsgWidget({this.content});

  @override
  Widget build(BuildContext context) {
    return Container(
      child: Padding(
        padding: EdgeInsets.only(right: 8.0,left: 50.0,top: 4.0,bottom: 4.0),
        child: ClipRRect(
          borderRadius: BorderRadius.only(
            bottomLeft: Radius.circular(15),
            bottomRight: Radius.circular(0),
            topLeft: Radius.circular(15),
            topRight: Radius.circular(15)
          ),
          child: Container(
            color: Color(0xFFF7A74D),
            child: Stack(
              children: [
                Padding(
                    padding:EdgeInsets.only(right: 12,left: 23,top: 8,bottom: 15),
                  child: Text(content,style: TextStyle(color: Color(0xFF97080E))),
                ),
              ],
            ),
          ),
        ),
      ),
    );
  }
}
