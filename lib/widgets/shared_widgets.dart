import 'package:firebase_auth/firebase_auth.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter/scheduler.dart';
import 'package:flutter/services.dart';
import 'package:flutter/widgets.dart';
import 'package:flutter_switch/flutter_switch.dart';
import 'package:rent2buy/di/service_locator.dart';
import 'package:rent2buy/models/userinfo.dart';
import 'package:rent2buy/screens/buyer/cart/my_cart.dart';
import 'package:rent2buy/screens/buyer/chats.dart';
import 'package:rent2buy/screens/buyer/home/home.dart';
import 'package:rent2buy/screens/custom/custom_switch.dart';
import 'package:rent2buy/screens/dialogs/become_seller_dialog.dart';
import 'package:rent2buy/screens/seller/admin_approval_listing.dart';
import 'package:rent2buy/screens/seller/home/home.dart';
import 'package:rent2buy/screens/seller/inbox/seller_inbox_screen.dart';
import 'package:rent2buy/screens/seller/listing/add_listing.dart';
import 'package:rent2buy/screens/splashscreen.dart';
import 'package:rent2buy/utils/colors.dart';
import 'package:rent2buy/utils/data_manager.dart';
import 'package:rent2buy/utils/constants.dart' as Constants;
import 'package:rent2buy/utils/strings.dart';
import 'package:rent2buy/utils/utils.dart';
import 'package:shared_preferences/shared_preferences.dart';

class MyButton extends StatelessWidget {
  String text;
  Function onPressed;
  Color color;
  double width;

  MyButton({Key key, this.width, this.text, this.onPressed, this.color})
      : super(key: key);

  @override
  Widget build(BuildContext context) {
    return SizedBox(
      width: width == null ? 131 : width,
      height: 50,
      child: RaisedButton(
        onPressed: onPressed,
        child: Text(
          text,
          style: TextStyle(
              color: color, fontSize: 16, fontWeight: FontWeight.w800),
        ),
      ),
    );
  }
}

class CustomTextField extends StatelessWidget {
  String hinttext;
  bool isPassword;
  TextEditingController textEditingController;
  bool isNumber;
  Color borderColor;
  Function validator;
  Function onTap;

  CustomTextField(
      {this.hinttext,
      this.validator,
      this.isPassword,
      this.textEditingController,
      this.isNumber,
      this.borderColor,
      this.onTap});

  @override
  Widget build(BuildContext context) {
    return TextFormField(
      validator: validator,
      onTap: onTap,
      decoration: InputDecoration(
        enabled: true,
        enabledBorder: getBorder(),
        focusedBorder: getBorder(),
        errorBorder: getBorder(),
        focusedErrorBorder: getBorder(),
        contentPadding: EdgeInsets.fromLTRB(36, 24, 24, 24),
        hintText: hinttext,
        hintStyle: TextStyle(color: Colors.grey[700]),
      ),
      obscureText: isPassword ? true : false,
      controller: textEditingController,
      keyboardType: isNumber ? TextInputType.number : TextInputType.text,
    );
  }

  getBorder() {
    return OutlineInputBorder(
        borderSide: BorderSide(color: borderColor, width: 4),
        borderRadius: BorderRadius.all(Radius.circular(12)));
  }
}

class AuthTextField extends StatelessWidget {
  String hinttext;
  bool isNumber;
  Widget prefixIcon;
  bool isPassword;
  TextEditingController textEditingController;
  Widget suffixIcon;
  Color color;
  Function validator;
  Function onSaved;
  Function onTap;
  List<TextInputFormatter> inputFormatters;
  AuthTextField(
      {this.hinttext,
      this.inputFormatters,
      this.isNumber,
      this.prefixIcon,
      this.isPassword,
      this.textEditingController,
      this.suffixIcon,
      this.color,
      this.validator,
      this.onSaved,
      this.onTap});

  @override
  Widget build(BuildContext context) {
    return TextFormField(
      inputFormatters: inputFormatters,
      decoration: InputDecoration(
          suffixIcon: suffixIcon,
          enabled: true,
          enabledBorder: UnderlineInputBorder(
              borderSide: BorderSide(width: 3, color: color)),
          prefixIcon: prefixIcon,
          hintText: hinttext),
      obscureText: isPassword ? true : false,
      controller: textEditingController,
      onSaved: onSaved,
      validator: validator,
      onTap: onTap,
      keyboardType: isNumber ? TextInputType.number : TextInputType.text,
    );
  }
}

class CustomDrawer extends StatefulWidget {
  final DataManager dataManager = serviceLocator<DataManager>();

  @override
  _CustomDrawerState createState() => _CustomDrawerState();
}

class _CustomDrawerState extends State<CustomDrawer> {
  bool loggedIn = false;
  bool status = false;
  bool checkedRole = false;
  Userinfo currentUserInfo;
  final _bigFont = const TextStyle(
      fontSize: 18, fontWeight: FontWeight.bold, color: Color(0xFF616161));
  @override
  void initState() {
    // TODO: implement initState
    super.initState();
    getUser();
    getUserRole();
  }

  void getUser() async {
    currentUserInfo = Userinfo.fromJson(await widget.dataManager.getUser());
  }

  @override
  Widget build(BuildContext context) {
    return Theme(
        data: Theme.of(context).copyWith(canvasColor: Colors.yellow[100]),
        child: FutureBuilder(
            future: widget.dataManager.loadToken(),
            builder: (context, snapshot) {
              loggedIn = snapshot.hasData;
              return Drawer(
                child: ListView(
                  children: [
                    DrawerHeader(
                      margin: EdgeInsets.all(0.0),
                      decoration: BoxDecoration(color: Color(0xFFF7A74D)),
                      child: Align(
                        alignment: Alignment.center,
                        child: Column(
                          mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                          children: [
                            GestureDetector(
                              onTap: () {
                                Navigator.pushNamed(context, '/user');
                              },
                              child: CircleAvatar(
                                //backgroundImage: NetworkImage(snapshot.data['photoURL'] ?? 'null'),
                                radius: 36,
                              ),
                            ),
                            Text(
                              Utils.getFullName(currentUserInfo?.firstName,
                                  currentUserInfo?.lastName),
                              style: TextStyle(
                                  fontWeight: FontWeight.w800,
                                  fontSize: 24,
                                  color: Color(0xFF97080E)),
                            ),
                            Row(
                              mainAxisAlignment: MainAxisAlignment.spaceAround,
                              children: [
                                Text(
                                  'Buyer',
                                  style: TextStyle(
                                      color: Color(0xFF97080E),
                                      fontSize: 20,
                                      fontWeight: FontWeight.bold),
                                ),
                                FlutterSwitch(
                                    activeColor: colorAccent,
                                    inactiveColor: colorAccent,
                                    toggleColor: colorPrimary,
                                    width: 130.0,
                                    height: 25,
                                    value: checkedRole,
                                    onToggle: (val) {
                                      checkedRole = val;
                                      WidgetsBinding.instance
                                          .addPostFrameCallback((_) async {
                                        int isSeller =
                                            await widget.dataManager.isSeller();
                                        if (checkedRole) {
                                          //Switch To Seller
                                          if (isSeller == 0) {
                                            //Only Buyer then update Profile
                                            showUpdateDialog(context);
                                          } else {
                                            //Seller
                                            widget.dataManager
                                                .saveUserRole(Constants.SELLER);
                                            Navigator.of(context)
                                                .pushNamedAndRemoveUntil(
                                                    SellerHome.routeName,
                                                    (Route<dynamic> route) =>
                                                        false);
                                          }
                                        } else {
                                          //Switch To Buyer

                                        }
                                      });
                                    }),
                                Text(
                                  'Seller',
                                  style: TextStyle(
                                      color: Color(0xFF97080E),
                                      fontSize: 20,
                                      fontWeight: FontWeight.bold),
                                )
                              ],
                            )
                          ],
                        ),
                      ),
                    ),
                    ListTile(
                      title: Text(
                        'Home',
                        style: _bigFont,
                      ),
                      leading: Icon(Icons.home, size: 30),
                      onTap: () {
                        Navigator.pop(context);
                        Navigator.pushNamed(context, '/home');
                      },
                    ),
                    ListTile(
                      title: Text(
                        'Advance Search',
                        style: _bigFont,
                      ),
                      leading: Icon(Icons.search, size: 30),
                      onTap: () {
                        Navigator.pop(context);
                        Navigator.pushNamed(context, '/search');
                      },
                    ),
                    Visibility(
                      visible: !loggedIn,
                      child: Column(
                        children: [
                          ListTile(
                            title: Text(
                              'Login',
                              style: _bigFont,
                            ),
                            leading: Icon(Icons.exit_to_app, size: 30),
                            onTap: () {
                              Navigator.pop(context);
                              Navigator.pushNamed(context, '/login');
                            },
                          ),
                          ListTile(
                            title: Text(
                              'Register',
                              style: _bigFont,
                            ),
                            leading: Icon(Icons.person_outline, size: 30),
                            onTap: () {
                              Navigator.pop(context);
                              Navigator.pushNamed(context, '/signup');
                            },
                          ),
                        ],
                      ),
                    ),
                    ListTile(
                      title: Text(
                        'Sellers',
                        style: _bigFont,
                      ),
                      leading: Icon(Icons.account_circle_outlined, size: 30),
                      onTap: () {
                        Navigator.pop(context);
                        Navigator.pushNamed(context, '/sellers');
                      },
                    ),
                    ListTile(
                      title: Text(
                        'Chats',
                        style: _bigFont,
                      ),
                      leading: Icon(Icons.chat, size: 30),
                      onTap: () {
                        Navigator.pop(context);
                        Navigator.pushNamed(context, Chats.routeName);
                      },
                    ),
                    ListTile(
                      title: Text(
                        'My Cart',
                        style: _bigFont,
                      ),
                      leading: Icon(Icons.shopping_cart, size: 30),
                      onTap: () {
                        Navigator.of(context).popAndPushNamed(MyCart.routeName);
                      },
                    ),
                    ListTile(
                      title: Text(
                        'My Orders',
                        style: _bigFont,
                      ),
                      leading: Icon(Icons.list, size: 30),
                      onTap: () {
                        Navigator.pop(context);
                        Navigator.pushNamed(context, '/my_orders');
                      },
                    ),
                    ListTile(
                      title: Text(
                        'Dispute',
                        style: _bigFont,
                      ),
                      leading: Image.asset('assets/dispute.png', height: 32),
                      onTap: () {
                        Navigator.pop(context);
                        Navigator.pushNamed(context, '/dispute');
                      },
                    ),
                    Divider(color: Color(0xFFF7A74D), thickness: 3),
                    Padding(
                      padding: const EdgeInsets.only(left: 16.0),
                      child: Text('Others',
                          style: TextStyle(
                              fontWeight: FontWeight.bold,
                              color: Color(0xFFF7A74D))),
                    ),
                    ListTile(
                      title: Text(
                        'Settings',
                        style: _bigFont,
                      ),
                      leading: Icon(Icons.settings_outlined, size: 30),
                      onTap: () {
                        Navigator.pop(context);
                        Navigator.pushNamed(context, '/app_settings');
                      },
                    ),
                    ListTile(
                      title: Text(
                        'Notifications',
                        style: _bigFont,
                      ),
                      leading:
                          Icon(Icons.notifications_active_outlined, size: 30),
                      onTap: () {
                        Navigator.pop(context);
                        Navigator.pushNamed(context, '/notifications');
                      },
                    ),
                    ListTile(
                      title: Text(
                        'Logout',
                        style: _bigFont,
                      ),
                      leading: Icon(Icons.logout, size: 30),
                      onTap: () {
                        SchedulerBinding.instance.addPostFrameCallback((_) {
                          widget.dataManager.removeUser();
                          Navigator.of(context).pushNamedAndRemoveUntil(
                              SplashScreen.routeName,
                              (Route<dynamic> route) => false);
                        });
                      },
                    ),
                  ],
                ),
              );
            }));
  }

  _getPrefs() async {
    SharedPreferences prefs = await SharedPreferences.getInstance();
    return prefs.getString('name');
  }

  void getUserRole() async {
    checkedRole = await widget.dataManager.getUserRole() == Constants.SELLER;
  }

  void showUpdateDialog(BuildContext context) {
    showDialog(
        context: context,
        builder: (BuildContext context) {
          return BecomeSellerDialog();
        });
  }
}

class CustomIconButton extends StatelessWidget {
  String hinttext;
  Function onPressed;
  Widget icon;
  Color textColor;

  CustomIconButton({this.hinttext, this.onPressed, this.icon, this.textColor});

  @override
  Widget build(BuildContext context) {
    return SizedBox(
      width: 340,
      height: 50,
      child: RaisedButton.icon(
          onPressed: onPressed,
          icon: icon,
          label: Text(
            hinttext,
            style: TextStyle(
                color: textColor, fontSize: 24, fontWeight: FontWeight.w700),
          )),
    );
  }
}

class SuffixedIconTextField extends StatelessWidget {
  String hinttext;
  TextEditingController textEditingController;
  bool isNumber;
  Widget suffixIcon;
  Color fillColor;
  bool filled;
  TextAlign textAlign;
  Color borderColor;
  Function onChanged;

  SuffixedIconTextField(
      {this.isNumber,
      this.onChanged,
      this.suffixIcon,
      this.textEditingController,
      this.hinttext,
      this.fillColor,
      this.filled,
      this.textAlign,
      this.borderColor});

  @override
  Widget build(BuildContext context) {
    return TextField(
      onChanged: onChanged,
      style: TextStyle(
          fontSize: 20, fontWeight: FontWeight.w600, color: Colors.grey[700]),
      textAlign: textAlign,
      decoration: InputDecoration(
          filled: filled,
          fillColor: fillColor,
          contentPadding:
              EdgeInsets.only(top: 4, bottom: 4, right: 8, left: 18),
          suffixIcon: suffixIcon,
          enabled: true,
          enabledBorder: OutlineInputBorder(
              borderSide: BorderSide(color: borderColor, width: 4),
              borderRadius: BorderRadius.all(Radius.circular(12))),
          focusedBorder: OutlineInputBorder(
              borderSide: BorderSide(color: borderColor, width: 4),
              borderRadius: BorderRadius.all(Radius.circular(12))),
          hintText: hinttext,
          hintStyle: TextStyle(
              fontWeight: FontWeight.w600,
              fontSize: 18,
              color: Colors.grey[700])),
      controller: textEditingController,
      keyboardType: isNumber ? TextInputType.number : TextInputType.text,
    );
  }
}

Widget appBar(Color color) {
  return AppBar(
    title: Text('R2S',
        style:
            TextStyle(fontFamily: 'Rammetto One', color: color, fontSize: 24)),
    centerTitle: true,
    automaticallyImplyLeading: false,
  );
}

Widget CustomDropDown(dynamic value, Function onChanged,
    List<DropdownMenuItem<dynamic>> items, String hinttext) {
  return DropdownButtonFormField(
      decoration: InputDecoration(
        enabled: true,
        enabledBorder: OutlineInputBorder(
            borderSide: BorderSide(color: Color(0xFF97080E), width: 4),
            borderRadius: BorderRadius.all(Radius.circular(12))),
        focusedBorder: OutlineInputBorder(
            borderSide: BorderSide(color: Color(0xFF97080E), width: 4),
            borderRadius: BorderRadius.all(Radius.circular(12))),
        contentPadding: EdgeInsets.fromLTRB(36, 24, 24, 24),
      ),
      icon: Icon(
        Icons.keyboard_arrow_down,
        color: Color(0xFF97080E),
      ),
      value: value,
      items: items,
      hint: Text(
        hinttext,
        style:
            TextStyle(fontWeight: FontWeight.w600, fontSize: 20, height: 1.1),
      ),
      onChanged: onChanged);
}

class BaseAppBar extends StatelessWidget implements PreferredSizeWidget {
  final title;
  final AppBar appBar;
  final List<Widget> widgets;
  final bool automaticallyImplyLeading;
  final bool isCentered;
  final Widget leading;
  final Color iconTheme;
  final Color titleColor;

  const BaseAppBar(
      {Key key,
      this.title,
      this.appBar,
      this.widgets,
      this.automaticallyImplyLeading,
      this.isCentered,
      this.leading,
      this.iconTheme,
      this.titleColor})
      : super(key: key);

  @override
  Widget build(BuildContext context) {
    return AppBar(
      leading: leading,
      centerTitle: isCentered,
      automaticallyImplyLeading: automaticallyImplyLeading,
      iconTheme: IconThemeData(color: iconTheme),
      textTheme: TextTheme(),
      title: Text(
        title,
        style: TextStyle(
            fontSize: 24, fontWeight: FontWeight.bold, color: titleColor),
      ),
      actions: widgets,
    );
  }

  @override
  Size get preferredSize => Size.fromHeight(appBar.preferredSize.height);
}

class SellerDrawer extends StatefulWidget {
  final DataManager dataManager = serviceLocator<DataManager>();

  @override
  _SellerDrawerState createState() => _SellerDrawerState();
}

class _SellerDrawerState extends State<SellerDrawer> {
//  String userId = FirebaseAuth.instance.currentUser.uid.toString();
  bool status = true;
  final _bigFont = const TextStyle(
      fontSize: 18, fontWeight: FontWeight.bold, color: Color(0xFF616161));
  Userinfo currentUserInfo;

  @override
  void initState() {
    getUser();
    super.initState();
  }

  void getUser() async {
    currentUserInfo = Userinfo.fromJson(await widget.dataManager.getUser());
  }

  @override
  Widget build(BuildContext context) {
    bool loggedIn = false;
    return FutureBuilder(
        future: widget.dataManager.loadToken(),
        builder: (context, snapshot) {
          loggedIn = snapshot.hasData;
          return Drawer(
            child: ListView(
              children: [
                DrawerHeader(
                  margin: EdgeInsets.all(0.0),
                  decoration: BoxDecoration(color: Color(0xFF71C7E3)),
                  child: Align(
                    alignment: Alignment.center,
                    child: Column(
                      mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                      children: [
                        GestureDetector(
                          onTap: () {
                            Navigator.pushNamed(context, '/seller_profile');
                          },
                          child: CircleAvatar(
//                            backgroundImage: NetworkImage(
//                                snapshot.data['photoURL'] ?? 'null'),
                            radius: 36,
                          ),
                        ),
                        Text(
                          Utils.getFullName(currentUserInfo?.firstName,
                              currentUserInfo?.lastName),
                          style: TextStyle(
                              fontWeight: FontWeight.w800,
                              fontSize: 24,
                              color: Color(0xFF003585)),
                        ),
                        Row(
                          mainAxisAlignment: MainAxisAlignment.spaceAround,
                          children: [
                            Text(
                              'Buyer',
                              style: TextStyle(
                                  color: Color(0xFF003585),
                                  fontSize: 20,
                                  fontWeight: FontWeight.bold),
                            ),
                            FlutterSwitch(
                                activeColor: Color(0xFF003585),
                                inactiveColor: Color(0xFF003585),
                                toggleColor: Colors.white,
                                width: 130.0,
                                height: 25,
                                value: status,
                                onToggle: (val) {
                                  setState(() {
                                    status = val;
                                    WidgetsBinding.instance
                                        .addPostFrameCallback((_) async {
                                      int isSeller =
                                          await widget.dataManager.isSeller();
                                      widget.dataManager
                                          .saveUserRole(Constants.BUYER);
                                      Navigator.pushNamedAndRemoveUntil(context,
                                          Home.routeName, (r) => false);
                                    });
                                  });
                                }),
                            Text(
                              'Seller',
                              style: TextStyle(
                                  color: Color(0xFF003585),
                                  fontSize: 20,
                                  fontWeight: FontWeight.bold),
                            )
                          ],
                        )
                      ],
                    ),
                  ),
                ),
                ListTile(
                  title: Text(
                    'Home',
                    style: _bigFont,
                  ),
                  leading: Icon(Icons.home, size: 30),
                  onTap: () {
                    Navigator.pop(context);
                    Navigator.pushNamed(context, '/seller_home');
                  },
                ),
                ListTile(
                  title: Text(
                    'Add Listing',
                    style: _bigFont,
                  ),
                  leading: Icon(Icons.home, size: 30),
                  onTap: () {
                    Navigator.pop(context);
                    Navigator.pushNamed(context, AddListing.routeName);
                  },
                ),
                // ListTile(
                //   title: Text(
                //     'Package',
                //     style: _bigFont,
                //   ),
                //   leading: Image.asset('assets/tag.png', height: 30),
                //   onTap: () {},
                // ),
                Visibility(
                  visible: !loggedIn,
                  child: ListTile(
                    title: Text(
                      'Login',
                      style: _bigFont,
                    ),
                    leading: Icon(Icons.exit_to_app, size: 30),
                    onTap: () {
                      Navigator.pop(context);
                      Navigator.pushNamed(context, '/seller_login');
                    },
                  ),
                ),
                Visibility(
                  visible: !loggedIn,
                  child: ListTile(
                    title: Text(
                      'Register',
                      style: _bigFont,
                    ),
                    leading: Icon(Icons.person_outline, size: 30),
                    onTap: () {
                      Navigator.pop(context);
                      Navigator.pushNamed(context, '/seller_signup');
                    },
                  ),
                ),
                ListTile(
                  title: Text(
                    'Chat',
                    style: _bigFont,
                  ),
                  leading: Icon(Icons.message_outlined, size: 30),
                  onTap: () {
                    Navigator.pop(context);
                    Navigator.pushNamed(context, SellerInBoxScreen.routeName);
                  },
                ),
                ListTile(
                  title: Text(
                    'All Listing',
                    style: _bigFont,
                  ),
                  leading: Icon(Icons.description_outlined, size: 30),
                  onTap: () {
                    Navigator.pop(context);
                    Navigator.pushNamed(context, '/all_listing');
                  },
                ),
                ListTile(
                  title: Text(
                    'Listing Status',
                    style: _bigFont,
                  ),
                  leading: Icon(Icons.description_outlined, size: 30),
                  onTap: () {
                    Navigator.pop(context);
                    Navigator.push(
                        context,
                        MaterialPageRoute(
                            builder: (context) => AdminApprovalListing()));
                  },
                ),
                ListTile(
                  title: Text(
                    'Order',
                    style: _bigFont,
                  ),
                  leading: Image.asset('assets/list.png', height: 32),
                  onTap: () {
                    Navigator.pop(context);
                    Navigator.pushNamed(context, '/seller_order');
                  },
                ),
                ListTile(
                  title: Text(
                    'Dispute',
                    style: _bigFont,
                  ),
                  leading: Image.asset('assets/dispute.png', height: 32),
                  onTap: () {
                    Navigator.pop(context);
                    Navigator.pushNamed(context, '/seller_dispute');
                  },
                ),
                Divider(color: Color(0xFF71C7E3), thickness: 3),
                Padding(
                  padding: const EdgeInsets.only(left: 16.0),
                  child: Text('Others',
                      style: TextStyle(
                          fontWeight: FontWeight.bold,
                          color: Color(0xFF71C7E3))),
                ),
                ListTile(
                  title: Text(
                    'Settings',
                    style: _bigFont,
                  ),
                  leading: Icon(Icons.settings_outlined, size: 30),
                  onTap: () {
                    Navigator.pop(context);
                    Navigator.pushNamed(context, '/seller_app_settings');
                  },
                ),
                ListTile(
                  title: Text(
                    'Notifications',
                    style: _bigFont,
                  ),
                  leading: Icon(Icons.notifications_active_outlined, size: 30),
                  onTap: () {
                    Navigator.pop(context);
                    Navigator.pushNamed(context, '/seller_notifications');
                  },
                ),
                ListTile(
                  title: Text(
                    'Logout',
                    style: _bigFont,
                  ),
                  leading: Icon(Icons.logout, size: 30),
                  onTap: () {
                    SchedulerBinding.instance.addPostFrameCallback((_) {
                      widget.dataManager.removeUser();
                      Navigator.of(context).pushNamedAndRemoveUntil(
                          SplashScreen.routeName,
                          (Route<dynamic> route) => false);
                    });
                  },
                ),
              ],
            ),
          );
        });
  }

  _getPrefs() async {
    SharedPreferences prefs = await SharedPreferences.getInstance();
    return prefs.getString('name');
  }
}

class CustomDivider extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Divider(
      color: Color(0xFF71C7E3),
      height: 5.0,
      thickness: 3,
    );
  }
}

class RoundedIconButton extends StatelessWidget {
  RoundedIconButton(
      {@required this.icon, @required this.onPress, @required this.iconSize});

  final IconData icon;
  final Function onPress;
  final double iconSize;

  @override
  Widget build(BuildContext context) {
    return RawMaterialButton(
      constraints: BoxConstraints.tightFor(width: iconSize, height: iconSize),
      elevation: 6.0,
      onPressed: onPress,
      shape: RoundedRectangleBorder(
          borderRadius: BorderRadius.circular(iconSize * 0.2)),
      fillColor: Color(0xFFF7A74D),
      child: Icon(
        icon,
        color: Colors.white,
        size: iconSize * 0.8,
      ),
    );
  }
}

class CustomStepper extends StatefulWidget {
  CustomStepper(
      {@required this.lowerLimit,
      @required this.upperLimit,
      @required this.stepValue,
      @required this.iconSize,
      @required this.value,
      @required this.onChanged});

  final int lowerLimit;
  final int upperLimit;
  final int stepValue;
  final double iconSize;
  final ValueChanged<int> onChanged;
  int value = 1;

  @override
  _CustomStepperState createState() => _CustomStepperState();
}

class _CustomStepperState extends State<CustomStepper> {
  @override
  Widget build(BuildContext context) {
    return Row(
      mainAxisAlignment: MainAxisAlignment.center,
      children: [
        RoundedIconButton(
          icon: Icons.remove,
          iconSize: widget.iconSize,
          onPress: () {
            setState(() {
              widget.value = widget.value == widget.lowerLimit
                  ? widget.lowerLimit
                  : widget.value -= widget.stepValue;
            });
            widget.onChanged(widget.value);
          },
        ),
        Container(
          width: widget.iconSize,
          child: Text(
            '${widget.value}',
            style: TextStyle(
              fontSize: widget.iconSize * 0.8,
            ),
            textAlign: TextAlign.center,
          ),
        ),
        RoundedIconButton(
          icon: Icons.add,
          iconSize: widget.iconSize,
          onPress: () {
            setState(() {
              widget.value = widget.value == widget.upperLimit
                  ? widget.upperLimit
                  : widget.value += widget.stepValue;
            });
            widget.onChanged(widget.value);
          },
        ),
      ],
    );
  }

}
